# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


--------------------------------

## v1.1.13, 2025.03.04

### Dependencies

- chore(composer.lock): update symfony (v6.4.18 => v6.4.19), doctrine/*, twig/* #129

--------------------------------

## v1.1.12, 2025.01.29

### Dependencies

- chore(composer.lock): update symfony (v6.4.16 => v6.4.18), doctrine/*, twig/* #128


--------------------------------

## v1.1.11, 2025.01.02

### Dependencies

- chore(composer.lock): update symfony (6.4.16 => 6.4.17), doctrine, twig, monolog #127


--------------------------------

## v1.1.10, 2024.11.27

### Dependencies

- chore(composer.lock): upgrade symfony (6.4.15 => 6.4.16) #125


--------------------------------

## v1.1.9, 2024.11.22

### Security

- chore(composer.lock): upgrade symfony (6.4.14 => 6.4.15), twig, monolog, ... #124


--------------------------------

## v1.1.8, 2024.11.07

### Security

- chore(composer.lock): upgrade twig, symfony (6.4.13 => 6.4.14), doctrine/* #123


--------------------------------

## v1.1.7, 2024.10.28

### Dependencies

- chore(composer.lock): upgrade symfony (6.4.12 => 6.4.13), doctrine/* ... #122


--------------------------------

## v1.1.6, 2024.09.23

### Added

feat(devops): allow to generate Trivy HTML report (sbom + security) #121

### Fixed

- fix(cert-remind): add forgot password tip (cert manager) #120
- fix: PHP ext-intl is mandatory to i18n #119

### Dependencies

- chore(composer.lock): upgrade symfony (6.4.11 => 6.4.12), composer/semver, nikic/php-parser, ... #118


--------------------------------

## v1.1.5, 2024.09.11

### Added

- feat(make): allow to generate SBOM files #113
- doc: add auto generated SBOM files (Software Bill of Materials) #114

### Changed

- ci: twig linter is not allowed to fail #115

### Fixed

- chore(nelmio): xss_protection is deprecated since nelmio/security-bundle 3.4.0 #116

### Dependencies

- chore(composer.lock): upgrade symfony/polyfill*, phpstan/phpdoc-parser, nelmio/security-bundle, twig/* #117
- chore(composer.lock): upgrading doctrine/annotations, nelmio/security-bundle #112


--------------------------------

## v1.1.4, 2024.09.03

### Added

- ci: add Twig linter #111

### Dependencies

- chore(composer.lock): upgrade symfony (6.4.10 => 6.4.11), twig, doctrine #110


--------------------------------

## v1.1.3, 2024.07.26

### Dependencies

- chore(composer.lock): upgrade symfony (6.4.9 => 6.4.10), composer/*, nelmio/security-bundle  #109


--------------------------------

## v1.1.2, 2024.06.28

### Dependencies

- chore(composer.lock): upgrading doctrine/dbal, ...
- chore(composer.lock): upgrade symfony (6.4.8 => 6.4.9), doctrine/*, monolog #107


--------------------------------

## v1.1.1, 2024.06.04

### Changed

- Certificate reminder: set `false` as default value for
  `WEBAPP_CERTIFICAT_COPY_REMINDER_TO_EMAIL_ALERTING` environment variable #105

```bash
# Webapp - Send copy of certificate end-of-life notifications to WEBAPP_EMAIL_ALERTING_TO
WEBAPP_CERTIFICAT_COPY_REMINDER_TO_EMAIL_ALERTING=false # (default: false)
```

### Fixed

- ADMIN > configuration display : add missing environment variables #102

### Dependencies

- chore(composer.lock): upgrading doctrine/event-manager, doctrine-fixtures #101
- chore(composer.lock): upgrading symfony (6.4.7 => 6.4.8) #106


--------------------------------

## v1.1.0, 2024.05.22

### Added

- ci: add PHP 8.2 and PHP 8.3 (linter, composer validate and tests)
- ci: update Dockerfile (php 8.1, 8.2 and 8.3)
- ci: activate mutation tests
- feat(admin > certificate): display email reminder notices
- feat(cert reminder): Send end of life certificate reminder email to certifcate end user #93
- feat(cert reminder): Send end of life certificate reminder email to certifcate creator  #93
- feat(cert reminder): allow to load reminders by HTTP resquest #93
- feat(cert reminder): allow to send copy (BCC) to `WEBAPP_EMAIL_ALERTING_TO` (see new environment variable: `WEBAPP_CERTIFICAT_COPY_REMINDER_TO_EMAIL_ALERTING`)

new environment variable in .env file:

```bash
# Webapp - Send copy of certificate end-of-life notifications to WEBAPP_EMAIL_ALERTING_TO
WEBAPP_CERTIFICAT_COPY_REMINDER_TO_EMAIL_ALERTING=true # (default: true)
```

### Changed

- chore(composer.json): allow PHP 8.1, 8.2 and 8.3
- remove `DEBUG_CERT_BY_ADMIN` in .env file

### Fixed

- test: improve infection configuration
- ci(composer.lock): fix plugin-api-version
- chore: add some missing return type
- refactor: rename some directories and class
- refactor: add PublicKeyRawConvertor, PublicKeyRawDto and FailedDecodeRawPublicKeyException class

### Dependencies

- chore(dev): remove deprecated tool (dePHPend)
- chore(dev): improve dev tool configuration (PhpMD, Pslam)
- chore(dev): udpate dev tool (Infection, PhpMD, PHP-CS-Fixer, PHP_CodeSniffer, Psalm)
- chore(composer.lock): upgrade `symfony/*` (`v6.4.6` => `v6.4.7`), `doctrine`, `monolog`, `twig` #96
- chore(composer.lock): upgrade `symfony/*` (`v6.4.5` => `v6.4.6`), ... #94
- chore(composer.lock): upgrade `symfony/*` (`v6.4.2` => `v6.4.5`), ... #92
- chore(composer.json): upgrade `symfony/*` (`v6.4.1` => `v6.4.2`), `doctrine/orm` #104
- chore(composer.lock): upgrade symfony/flex (`v2.4.2` => `v2.4.3`)

--------------------------------

## v1.0.2, 2023.12.18

### Fixed

- test: allow to run Infection (mutation testing)
- test(pkcs12 convertor): allow mutation testing
- fix(convertHexadecimalToDecimal): remove hexadecimal prefix [ `0x` ]
- fix(NormalizesDataForCertificate): remove special characters used by JSON format
- fix(composer.json): unblock doctrine/orm version #48
- test: add unit tests for new Symfony services and exceptions
- refactor: add specific Symfony services (Certificate, CertificateAuthority, CfsslService, ...)
- refactor: add dedicated exceptions
  - KeySizeRangeException
  - InvalidAlgorithmException
  - Pkcs12ConvertorFailed
  - InvalidPrivateKey
  - InvalidPublicKey
  - NotMatchingPrivateAndPublicKeys

### Dependencies

- chore(composer.lock): upgrade `symfony` (`v6.4.0` => `v6.4.1`)
- chore(composer.json): upgrade `symfony` (`v6.3.9` => `v6.4.0`)
- chore(composer.lock): upgrade `symfony` (`v6.3.8` => `v6.3.9`)
- chore(composer.lock): upgrade `phpunit`, `doctrine`, `nelmio/security-bundle`, `composer/ca-bundle`


--------------------------------

## v1.0.1, 2023.11.13

### Security

- fix: escape special characters of password sent to openssl command #81
- chore(composer.lock): doctrine, symfony/* (`v6.3.7` => `v6.3.8`)


--------------------------------

## v1.0.0, 2023.11.09

### Fixed

- fix(ux): remove misspelling in tab name. (see: #80)
- chore(composer.lock): upgrade `symfony/monolog-bundle` (`v3.8.0` => `v3.10.0`)


--------------------------------

## v1.0.0-rc.2.0, 2023.10.31

### Fixed

- fix(ux): use browser native "minlength" attribute for password fields
- fix(a11y): add a missing "nav" HTML tag

### Security

- chore(composer.lock): update `monolog`, `symfony/*` (`v6.3.6` => `v6.3.7`)


--------------------------------

## v1.0.0-rc.1.0, 2023.10.25

### Added

- feat(i18n): as an anonymous user, the interface is in French
- feat(webperf): add HTTP "Cache-Control: immutable" headers for CSS and JS files


--------------------------------

## v0.17.0, 2023.10.25

### Added

- feat(i18n): as a manager, the interface is in French
- feat(css): use AssetMapper to bust browser cache when css, js or images change
- ci(release-build): add checksum of webapp files

### Changed

#### Breaking change

- ci(release): use lower file and directory permissions
- feat(security)!: HTTPS is mandatory for PROD environment

### Fixed

- fix: specify application language in HTML code

### Security

- chore(composer.lock): update `symfony/*` (v6.3.5 => v6.3.6)


--------------------------------

## v0.16.0, 2023.10.19

### Added

- feat: allow anonymous user to use "forgot password" form

### Changed

- chore(composer.lock): update `doctrine/dbal`


--------------------------------

## v0.15.0, 2023.10.06

### Added

- feat: allow connected user to change password

### Changed

- chore(composer.lock): update `doctrine/collections`, `egulias/email-validator` and `dbrekelmans/bdi`


--------------------------------

## v0.14.0, 2023.10.02

### Added

- feat(admin): display missing configuration
- feat(form): display min password length
- feat(user): use minimum password length defined by env variable
- feat(certificate): use minimum password length defined by env variable
- feat(env): allow to configure minimum password length (user and certificate)
- feat(env): allow to configure i18n default locale
- feat(env): allow to configure lifetime of reset password token (default: 20 minutes)

see `.env` file:

```bash
WEBAPP_I18N_DEFAULT_LOCALE                        # User interface language (default: 'en')
WEBAPP_USER_CONFIG_PASSWORD_RESET_TOKEN_LIFETIME  # Lifetime of reset password token in seconds (default: 1200 = 20 minutes).
WEBAPP_USER_CONFIG_MIN_PASSWORD_LENGTH            # Minimum user password length (default: 12)
WEBAPP_CERTIFICAT_CONFIG_MIN_PASSWORD_LENGTH      # Minimum certificate password length (default: 12)
```

### Fixed

- test(manager): add tests for create certificate form

### Security

- chore(composer.lock): update `symfony/*` (v6.3.4 => v6.3.5), `doctrine/*`, `phpdoc-parser`
- chore(composer.lock): update `phpunit/*`, `symfony/maker-bundle`


--------------------------------

## v0.13.0, 2023.09.06

### Added

- feat: replace simple lists with paginated lists
- feat(env): configure the number of items in paginated lists
  - see `WEBAPP_ADMIN_CONFIG_DEFAULT_MAX_USER_PER_PAGE` in .env file.
  - see `WEBAPP_ADMIN_CONFIG_DEFAULT_MAX_CERT_PER_PAGE` in .env file.
  - see `WEBAPP_MANAGER_CONFIG_DEFAULT_MAX_CERT_PER_PAGE` in .env file.

### Fixed

- fix(template): remove an excess closing HTML tag

### Security

- feat(security): strengthen the session fixation strategy
- feat(security): send HTTP header to clear browsing data on logout
- fix(security): enable by default CSRF protection


--------------------------------

## v0.12.1, 2023.09.01

### Security

- chore(composer.lock): update `symfony/*` (v6.3.2 => v6.3.3), `doctrine/*`, `twig/*`, ...


--------------------------------

## v0.12.0, 2023.08.11

### Added

- feat: manager can see his created certficates and download PEM files
- test: manager can see his created certficates and download PEM files
- feat(env): allow to configure lifetime of the session (default: 3600 = 1 hour),
  see `WEBAPP_SESSION_LIFETIME` in .env file.

### Changed

- ci(php_package-outdated): job is not allowed to fail

### Fixed

- fix(ui): improve nav items (manager + admin)
- fix: allow repeat phpunit tests
- fix(deprecation): add some return type

### Security

- fix: enable max lifetime of the session,  see `WEBAPP_SESSION_LIFETIME` in .env file.
- chore(composer.lock): update `symfony/*` (v6.3.1 => v6.3.2), `doctrine/*`, ...


--------------------------------

## v0.11.0, 2023.07.28

### Added

- test: add "manager can create new certificate"
- test: add "manager can only download certificate public key he created"
- feat(dev): add fake CFSSL server
- chore(make): add cfssl_run_fake_server cmd
- ci(php8.1_tests): run fake CFSSL server

### Changed

#### Breaking change

- fix(UI): remove software version from footer (except for administrator)

### Fixed

- fix(composer): use doctrine/orm 2.15.3, do not allow to use 2.15.4

### Security

- feat(security): add HTTP "Cross-Origin Opener|Resource|Embedder Policy" header
- chore(composer): update `symfony/*` (v6.2 => v6.3), `doctrine/*`, `twig/*`, ...


--------------------------------

## v0.10.0, 2023.06.20

> You may need to manually clear cache with following command line,
> before/after deploying Tajine `v0.10.0`:

```bash
rm -rvf var/cache/prod/
```

### Added

- feat(user story): allow admin to display creator (manager) of each certificate (#38)
- feat(user story): allow admin to display end-user of each certificate (#35)
- feat(user story): allow admin to download PEM file (public key) of each certificate (#34)

### Fixed

- fix(admin): display more certificates (last 150 instead of 12)
- fix(UX): visually differentiate breadcrumb for admins
- test: add unit tests for legacy certificate convertor (Pem to Pkcs12) via shell cmd (#39)
  - try to convert pem certificate to pkcs 12 with not valid public key
  - try to convert pem certificate to pkcs 12 with not valid private key
  - try to convert pem certificate to pkcs 12 with private key not corresponded to public key
  - convert pem certificate to pkcs 12 without password
  - convert pem certificate to pkcs 12 with empty password
  - convert pem certificate to pkcs 12 with password

### Security

- feat(security): add HTTP `Permissions-Policy` header, via `.htaccess` rules (#33)


--------------------------------

## v0.9.3, 2023.06.14

### Fixed

- Revert "chore(security): add HTTP strict-transport-security header (prod)"
- ci: php_package-outdated is allowed to fail  (temporary)
- ci: php_package-outdated is no longer needed by php8.1_release-build (temporary)
- chore(composer): update `symfony/*`, `doctrine/*`, `twig/*`, ...


--------------------------------

## v0.9.2, 2023.05.12

### Added

- feat: add robots.txt files (by default: block all web crawlers from all content)
- feat: add public/contribute.json file

### Fixed

- chore(security): add HTTP strict-transport-security header (prod)
- chore(security/csp): add frame-ancestors and form-action rules
- chore: add empty index.html files into public/(...)/ directories
- chore: update some outdated PHP packages


--------------------------------

## v0.9.1, 2023.05.09

### Fixed

- chore: update some outdated PHP packages


--------------------------------

## v0.9.0, 2023.04.20

### Breaking change

> You must manually clear the cache with the following command line,
> before deploy Tajine v0.9.0:

```bash
rm -rvf var/cache/prod/
```

### Added

- feat(env): allow to configure time zone of webapp (default is "Europe/Paris"), see `WEBAPP_TIMEZONE` in .env file
- feat(env): allow administrator to DEBUG certificates (default is disabled), see `DEBUG_CERT_BY_ADMIN` in .env file
- feat(log): enable log rotating

### Fixed

- pkcs12 convertor: use Pkcs12ConvertorViaShellCmd() with legacy option


--------------------------------

## v0.8.3, 2023.04.19

### Fixed

- rename mail template
- rename tmp/ directory
- chore: update some outdated PHP packages


--------------------------------

## v0.8.2, 2023.04.14

### Added

- debug: hardcoded password for CLI version  (OpenSSL command line)


--------------------------------

## v0.8.1, 2023.04.11

### Added

- feat(admin): display user status (enabled or disabled)
- debug: convert the certificate with a 2nd method (OpenSSL command line)


--------------------------------

## v0.8.0, 2023.04.07

### Added

- feat(certificate): display CRL link
- feat(certificate authority): display CRL link of root CA
- feat(certificate authority): display CRL link of intermediate CA
- feat(certificate authority): allow to download public key of Intermediate CA
- feat: store email subject in a template

### Changed

- remove all generated certificates
- remove all users
- add a  1st user with `ROLE_ADMIN` permissions

> A 1st user is added automatically with `ROLE_ADMIN` permissions,
> when the software is installed or updated.
> - login: `admin-tajine@example.org`
> - password: `admin-tajine_PaSsWord_IsNotSoSecretChangeIt`

#### Breaking change

> `bcmath` (PHP extension) is mandatory
>
> Allows to convert certificate serial number
> in Hexadecimal format to decimal format (used as ID by CFSSL).



--------------------------------

## v1.0.0-rc.1, 2023.03.30

identical to `v0.7.0`


--------------------------------

## v0.7.0, 2023.03.29

### Added

- feat(admin): allow to display PHP configuration

### Fixed

- fix: allow to debug p12 file
- fix(admin): allow to see all user (isActive is not implemented)


--------------------------------

## v0.6.0,  2023.03.29

### Added

- feat: allow manager to create X509 certificate
- feat: allow to display Certification Authority (CA) data
- feat(certificat): display decimal serial number
- feat(db): add some fields on certificate table (created_by_id, used_by_id and public_key)
- docs: add CFSSL API and database examples

> note:
> - display certificat decimal serial number need to enable PHP extension `bcmath`
> - The PHP extension `bcmath`
>   - is not required for this version.
>   - will certainly be needed in a future version.

### Fixed

- fix(admin): list only active users
- fix: clean up template


--------------------------------

## v0.5.0,  2023.03.27

### Added

- feat: add user management
  - a 1st user is added automatically with `ROLE_ADMIN` permissions
  - an account with `ROLE_ADMIN` permissions can:
    - add a user with `ROLE_CERTIFICATE_MANAGER` permissions
    - edit users (only mail and password)
    - remove users

> A 1st user is added automatically with `ROLE_ADMIN` permissions,
> when the software is installed or updated.
> - login: `admin-tajine@example.org`
> - password: `admin-tajine_PaSsWord_IsNotSoSecretChangeIt`

### Changed

- feat: customize error pages
- refactor(design): use Bootstrap 5.3 + Twig template

#### Breaking change

- fix!: remove a URL and unnecessary files
  > Check deployment :
  > an `HTTP 404 Not found` error is expected
  > when following URL is used:  `/poc/cfssl_display_certification-authority`

--------------------------------

## v0.4.0,  2023.03.13

### Added

- #24 feat: add /health-check route
- docs(prod): improve configuration
- docs(dev): add xdebug (mandatory to use Infection)
- docs(dev): create auto generated directories for PHPUnit and Infection
- feat(dev): add Pkcs12Convertor class
- ci: improve Dockerfile (allow to use xdebug)
- ci: update docker image (allow to use xdebug)
- ci: add PHPUnit job (testing framework for PHP)
- ci: add a job that will run Infection (PHP Mutation Testing Framework)
- test(certificate): add example  files (.pem, .p12, ...)
- test(cfssl): add Json payloads and CFSSL API responses
- chore(make): add phpunit and infection

### Changed

- ci: php_package-outdated and  php8.1_tests jobs needs composer_validate job
- chore: use PHP strict_types

#### Breaking change

- feat(trusted_hosts)!: allow regular expression  (`WEBAPP_TRUSTED_HOST` is replaced by `WEBAPP_TRUSTED_HOSTS`)
- fix(env)!: replace `WEBAPP_EMAIL_ADMIN` by `WEBAPP_EMAIL_ALERTING_TO`

### Fixed

- ci(phpunit): fix WEBAPP_TRUSTED_HOST
- ci: update docker image (add php8.1-zip)
- chore: update some outdated PHP packages


--------------------------------

## v0.3.0,  2023.03.02

### Added

> **TODO**: to be completed

- ci: add PHP linter and PHPCS (Coding Standard) jobs
- ci: a CI job that will check for outdated packages
- ci(release-build): create the .tar.gz file with a root directory
- feat(dev): add some "make" sub-commands (phpcs, php_linter, ...)

### Changed

> **TODO**: to be completed

- chore(webapp): enable session
- chore(composer): add required PHP extensions
- chore(phive): use copy instead of symbolic link
- ci: use new docker image (php-8.1_composer_phive:v1.3.0)
- ci: improve Dockerfile (prerequisites for PHP 8.1)

#### Breaking change

- refactor(database)!: use PostgreSQL instead of SQLlite
- doc!: replace PHP 7.4 by PHP 8.1
- ci!: use PHP 8.1 instead of PHP 7.4
- chore: add .htaccess

### Fixed

> **TODO**: to be completed

### Security

> **TODO**: to be completed

- feat(security): enable CSP rules
- feat: use nelmio/security-bundle (HTTP security headers: CSP, ...)
- chore: update some outdated PHP packages
- ci: add php-security-checker job

### TODO: must be checked and stored in right section

> **TODO**: must be checked and stored in right section

- docs: add Symfony upgrade command lines
- chore: add Makefile
- docs: add auto-generated documentation of webapp routes
- chore(webapp): add the HTTP methods to route annotations
- refactor(symfony 6.2): use PHP annotations
- chore(symfony 6.2): regenerate first database migration
- chore(symfony 6.2): update .env file
- chore(symfony 6.2): update config/routes.yaml
- chore(symfony 6.2): update config/packages/framework.yaml
- chore(symfony 6.2): remove config/routes/annotations.yaml
- chore(symfony 6.2): update config/parckages/doctrine.yaml
- chore(symfony 6.2): update config/parckages/security.yaml
- chore(symfony 6.2): update config/parckages/web_profiler.yaml
- chore(symfony 6.2): update composer.json|lock and symfony.lock files
- ci: add php8.2_composer_validate job
- ci: add Dockerfile (prerequisites for PHP 8.2)
- ci: improve Dockerfile (prerequisites for PHP 8.1)
- chore: add .php-version file (used by Symfony CLI)


--------------------------------

## v0.2.0,  2023.02.09

### Added

- #9 - chore: add src/webapp
- #9 - chore: configure webapp parameters
- #9 - feat(webapp): add / route
- #9 - feat(webapp): add "/poc/send_mail" route
- #9 - feat(webapp): add "/poc/config_display" route
- #9 - feat(webapp): add "/poc/create_certificate" route
- #9 - fix(SendEmailMessage): use sync instead of async to send email


--------------------------------

## v0.1.0,  2023.02.08

### Added

- #1 - Documentation: add LICENSE, CODE OF CONDUCT, CHANGELOG, CONTRIBUTING and README files
- #2 - Chore: add Gitlab templates (issue and MR)
- #3 - CI: add Gitlab CI (linter, documentation)
- #4 - Chore: add .editorconfig, .gitignore and .gitattributes files
- #5 - Chore: add QA tools installed by PHIVE
- #6 - Chore: add vagrant prototype
- #7 - Chore: add src/symfony_skeleton/
- #8 - ci: add php stage (build + release)


--------------------------------

## Template

```markdown

## <major>.<minor>.patch_DEV     (unreleased)

### Added

### Changed

#### Breaking change

### Fixed

### Dependencies

### Security


<details>
<summary>

Update `symfony/*` (v6.x.x => v6.x.x) : 0 installs, xx updates, 0 removals

see: https://symfony.com/blog/

</summary>

Lock file operations: 0 installs, xx updates, 0 removals
- Upgrading ...

</details>


--------------------------------

```
