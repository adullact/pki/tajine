# Fake CFSSL server

## Start HTTP server

Start PHP internal HTTP server to return fake CFSSL responses

```bash
# Start PHP internal HTTP server to return fake CFSSL responses
php -S 127.0.0.1:9999
```

## Fake CFSSL responses implemented

| CFSSL enpoint           | URL                                          |
|-------------------------|----------------------------------------------|
| `/api/v1/cfssl/newcert` | `http://127.0.0.1:9999/api/v1/cfssl/newcert` |
| ...                     | ...                                          |


### Get a new certificate (private key + public key)

#### CFSSL request example

```bash
# /api/v1/cfssl/newcert
# ---> Get a new certificate (private key + public key)
CFSSL_SERVER_IP='127.0.0.1'
CFSSL_SERVER_PORT='9999'
REQUEST='{
    "request": {
        "hosts": [
            ""
        ],
        "key": {
            "algo": "rsa",
            "size": 2048
        },
        "names": [
            {
                "C": "FR",
                "ST": "Herault",
                "L": "Montpellier",
                "O": "Exemple.org",
                "OU": "IT department"
            }
        ],
        "CN": "john.doe@exemple.org"
    }
}'
CFSSL_SERVER="${CFSSL_SERVER_IP}:${CFSSL_SERVER_PORT}"
curl -s                \
     -d  "${REQUEST}"  \
     -H "Content-Type: application/json"  \
     -X POST ${CFSSL_SERVER}/api/v1/cfssl/newcert | jq
```

#### Fake CFSSL response

For the time being, CFSSL false response is always the same,
no matter which CFSSL request you send.

- [CFSSL_API_newcert.response.json](../cfssl/CFSSL_API_newcert.response.json)
- Hash (`sha256`) of this fake CFSSL response: `021f390afcc8c2180d5d6dc4d25cb5a29079550789d0a19ef7ae2b85158f3b2c`

```json
{
  "success": true,
  "result": {
    "certificate": "-----BEGIN CERTIFICATE-----\nMIIEJzCCAw+gAwIBAgIUfj/Xy8tDuiX/o7TOY+15QPdsWW4wDQYJKoZIhvcNAQEL\nBQAwZjELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMREwDwYDVQQK\nEwhBRFVMTEFDVDEuMCwGA1UEAxMlVkFHUkFOVCBBRFVMTEFDVCBJTlRFUk1FRElB\nVEUgQ0EgR0VOMTAeFw0yMzAzMTMwMzQ1MDBaFw0yNDAzMTIwMzQ1MDBaMIGNMQsw\nCQYDVQQGEwJGUjEQMA4GA1UECBMHSGVyYXVsdDEUMBIGA1UEBxMLTW9udHBlbGxp\nZXIxHjAcBgNVBAoTFU1haXJpZSBkZSBNb250cGVsbGllcjEbMBkGA1UECxMSNDY1\nMTQ2MTU0MzY1MTQzNjUxMRkwFwYDVQQDDBB1c2VyQGV4YW1wbGUub3JnMIIBIjAN\nBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyWue+Ip48WO5jroWn5ZXrjFM6JQ0\nWiftVx1KtlRWs9HlRQ6NnCwNd/DHinaahvlpj8CP36UCaYw4tdPcIrIBxtAWFYV8\nS2T0IJ0CGpJNB5PAzh3AlMzTkSpX2nhZ6TYrH2JV8qG/9OGQKQy6Z42bzIib95Ci\noEKngWromi5jAEn2L1zmCcLYO+3mkTH1Ct4r96CI/VIx2npIZbcesOoaveVaIQmp\nisuAYvXywpAeQ3EuSnvjeZP8TNlpTMH0FWSpuoi0Q/BsdW4hB520zhzss5EoYHzC\nZ9v3ViPMDq8BLhLrYGGkGwMTGEmvA0UWXkqmRqDbMqb07fKjedXdAzl/4QIDAQAB\no4GkMIGhMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0O\nBBYEFPquORoMf9Qg9aErq75mrjO7p62xMB8GA1UdIwQYMBaAFL+UFyOYqKqxUJdG\nylKkHb1w3ppFMAsGA1UdEQQEMAKCADAvBgNVHR8EKDAmMCSgIqAghh5odHRwOi8v\ncGtpLmV4YW1wbGUub3JnL2NybC5wZW0wDQYJKoZIhvcNAQELBQADggEBAII1vYnU\nGQnbBQzHh9R2uHYI833df/zHVfPgS+AC86a81WR/3YiZkpewV6sZS7RGvpZ5HQgG\no4eUriJEm0yAmqhZpn5akplVgz0LdT5h4Hr1JFj0VPhdlPKRS7IncOXDIxU4xlCk\nYv3YTvzdAECzjgX98mOjtbE5/TyEYl02HmNYMrQvZM4fBlTS7h93urwGcTp19rVz\n571LQZWC3FLVoQU4mwtVcMRxVcNUHDC8df1Na0AZzQGEI3uzti7d0ruW4pSnvpBZ\nbf+L+OBzQ3Y4zXkR8J1Q4nXXZ4D6DBqYXNnZw9jnTLGsH6YhOtOB020Vp6pyMuN5\nN3lor8nW4jt/z24=\n-----END CERTIFICATE-----\n",
    "certificate_request": "-----BEGIN CERTIFICATE REQUEST-----\nMIIC8TCCAdkCAQAwgY0xCzAJBgNVBAYTAkZSMRAwDgYDVQQIEwdIZXJhdWx0MRQw\nEgYDVQQHEwtNb250cGVsbGllcjEeMBwGA1UEChMVTWFpcmllIGRlIE1vbnRwZWxs\naWVyMRswGQYDVQQLExI0NjUxNDYxNTQzNjUxNDM2NTExGTAXBgNVBAMMEHVzZXJA\nZXhhbXBsZS5vcmcwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDJa574\ninjxY7mOuhaflleuMUzolDRaJ+1XHUq2VFaz0eVFDo2cLA138MeKdpqG+WmPwI/f\npQJpjDi109wisgHG0BYVhXxLZPQgnQIakk0Hk8DOHcCUzNORKlfaeFnpNisfYlXy\nob/04ZApDLpnjZvMiJv3kKKgQqeBauiaLmMASfYvXOYJwtg77eaRMfUK3iv3oIj9\nUjHaekhltx6w6hq95VohCamKy4Bi9fLCkB5DcS5Ke+N5k/xM2WlMwfQVZKm6iLRD\n8Gx1biEHnbTOHOyzkShgfMJn2/dWI8wOrwEuEutgYaQbAxMYSa8DRRZeSqZGoNsy\npvTt8qN51d0DOX/hAgMBAAGgHjAcBgkqhkiG9w0BCQ4xDzANMAsGA1UdEQQEMAKC\nADANBgkqhkiG9w0BAQsFAAOCAQEAqQHAklH0E5GfJsDheoqJpwkMj85DtbC8hgXH\nH/vBZlVKRJK1/wTVd592kSuCHNUzqRSsdLyS7Hr3bFUlfCGX0jCgQgB6nRfyaWEi\nsZ5bnkrFbcOl9+sKPGP2oCIFh0eAQhLWYoLlclseGkLX2bddeTMhOQZvyNG1EUvA\nT42S0rJm7MQd49vk/MLehhrPRW6bkr9jOGzjMzwWYgiDQQlUWEH/1w3TQmfk6xbW\nH9XRkXGd5EmISkVYBQJhqXsCMnNtkIwHxacbR9louZMeP8WDlSCK+aBnHlR7DI8M\nfW2XkOn2McD3/qYXA3vG2pkT4t8NpsCUggnv7vSM4jKjTLT3zg==\n-----END CERTIFICATE REQUEST-----\n",
    "private_key": "-----BEGIN RSA PRIVATE KEY-----\nMIIEpQIBAAKCAQEAyWue+Ip48WO5jroWn5ZXrjFM6JQ0WiftVx1KtlRWs9HlRQ6N\nnCwNd/DHinaahvlpj8CP36UCaYw4tdPcIrIBxtAWFYV8S2T0IJ0CGpJNB5PAzh3A\nlMzTkSpX2nhZ6TYrH2JV8qG/9OGQKQy6Z42bzIib95CioEKngWromi5jAEn2L1zm\nCcLYO+3mkTH1Ct4r96CI/VIx2npIZbcesOoaveVaIQmpisuAYvXywpAeQ3EuSnvj\neZP8TNlpTMH0FWSpuoi0Q/BsdW4hB520zhzss5EoYHzCZ9v3ViPMDq8BLhLrYGGk\nGwMTGEmvA0UWXkqmRqDbMqb07fKjedXdAzl/4QIDAQABAoIBAH99eVAEBe10m9z1\neKBmvvTpC68jL8fpAddkGQgzdfW345DEV2oEkDnc09CWpdTNRb3Rur9qaI56TOL6\nSJpFZc57o57XsQbfzY5fExfShqNHKIDOYDlPNJLr5FPKXc65lK32xkQfkarkoyT4\ng3xMK9NYnLAkgmtCBVhrXPNKbSuehZhy+0M+/Q4XYi1rsrrHq27k3CtJUmBUbi6t\nRowNU0JaTcUaS66l/qD39eF080m+v4IaBM5Kw+UyTmx41IXDiHSlPZorMvOTYvyt\n3/0pF+LmY0U47mMuVjePpos8Z0os720hWHsgc4aFUemvQtfaqmFOa/VtWujT6MCv\n+ltwV+ECgYEA4pVU09q6A5JROUJCyYs+HyZayBwyI+bz0gC9pOevx9YEM8UHqPOP\nnmKkDer8WcmD87LNarmwN8LIbv32LISXkTJCzPckhgf/TusEPiRb+E3DgROpTOr+\nIA4FCvmFovm8DS/If7TZLg5EYYAudaotUAw24y7hFx/mlugbhOxoWPUCgYEA45H7\nA6SCwy25wTZPs4ZGg8kJv74y0UbVYOlKqJGsknkB9+SOA1mFu9G/S107MRAmROS3\nGyDU+uJtLhr2FY41duD7YHJ/nYOsH5xzw8lIXCmuKo4IARVB6C30VOsHtMjxr1Ym\n/KcUnKF/W2qE6BMVFyg6arqPa7IJUu5qxs6Ip70CgYEAzhd58sCuFPWPP3+Pc/Zc\n6MRgKkfLK3C0MT1PQaEi96coZ1Y9Y2U2KqAaDDbClYB/rrfhs8N2DyHyYVJKCbAI\nlJ6cuAXLiMXZaRTG5Sb5KF7DBrK1eSr9PnbCC3tuHBD/BReDTPVX3VZIyE5o88l4\nMakKe5V2UyxG9tfjwAGv38kCgYEArWFMGENfaOrOilkhsgpMcR1a8x4qw20vJs23\nFuUd0N6m1EHqvLyY+jbcgsO+VCwUrAqBQwbpnVejjSv3bwbQzwisHJg/iDeIjPD6\n9iDFo4oR6tQZXxHeC9HvRD9ouKEcGdUOkRGOz6LvUCvApLvdJq58i1Il8QxMtEV3\nTo6OaRUCgYEA35zn5p6ZHEuhku5u0HoAUEIa5M+qzeYh74WrSqXaj4ZqXh9h/L+7\najzBt3VEgN9Cgtf9u/HIC84EFdju368SHKTemYdf+AJFmjDI9im/U+J1lw6U84Kl\nIwlo6okPPlJcn41PhbFzD8WlPpBvvuCWqpYEoq64CxeMeeln2mr+nsY=\n-----END RSA PRIVATE KEY-----\n",
    "sums": {
      "certificate": {
        "md5": "5B4E52037490A03C14F446E80AC2EF84",
        "sha-1": "5433C401817660C52FF7E9EB8648F9D66ED2E5B8"
      },
      "certificate_request": {
        "md5": "CD6702F6C8D6F0BE13B4B2CAB13A00EF",
        "sha-1": "F206505BBDC84E0AC0A9B19AE57694D44F106E72"
      }
    }
  },
  "errors": [],
  "messages": []
}
```

This fake CFSLL response corresponds to the following payload:
[CFSSL_API_newcert.payload.json](../cfssl/CFSSL_API_newcert.payload.json)

```json
{
  "request": {
    "hosts": [
      ""
    ],
    "key": {
        "algo":  "rsa",
        "size": 2048
    },
    "names": [
      {
        "C": "FR",
        "ST": "Herault",
        "L": "Montpellier",
        "O": "Mairie de Montpellier",
        "OU": "465146154365143651"
      }
    ],
    "CN": "user@example.org"
  }
}
```
