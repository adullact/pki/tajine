<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

trait TestHelperFormTrait
{
    public function checkFormField(
        Crawler $crawler,
        string $htmlFormName,
        string $fieldIdsuffix,
        array $fieldAttributes = [ 'type' => 'text'],
        string $labelText = '',
        string $helpText = '',
        string $fieldHtmlTag = 'input',
    ) {
        $fieldId = $htmlFormName . "_$fieldIdsuffix";
        if ($labelText !== '') {
            $this->checkAttribute($crawler, "label[for=$fieldId]", ['_text' => $labelText]);
        }
        if ($helpText !== '') {
            $this->checkAttribute($crawler, "#$fieldId" . '_help', ['_text' => $helpText]);
            $fieldAttributes['aria-describedby'] = $fieldId . '_help';
        }
        $this->checkAttribute($crawler, "$fieldHtmlTag#$fieldId", $fieldAttributes);
    }


    public function commonCheckerIfFormFieldIsInvalid(
        Crawler $crawler,
        string $cssFilterOfInvadFormField,
        string $cssFilterOfInvalidFeedback,
        string $invalidFeedback,
        array $expectedFieldAttributes = ['class' => 'form-control is-invalid'],
    ): void {
        $this->checkAttribute(
            crawler: $crawler,
            cssFilter: "$cssFilterOfInvadFormField",
            attributesExpected: $expectedFieldAttributes
        );
        $this->assertSelectorTextContains("$cssFilterOfInvalidFeedback", "$invalidFeedback");
    }

    public function commonCheckerWhenFormIsSentWithMissingOrEmptyField(
        KernelBrowser $kernelBrowser,
        array $validFormData,
        string $formName,
        string $fieldName,
        string $fieldRepeatName = null,
        string $invalidFeedback = 'This field is mandatory.',
        array $expectedFieldAttributes = ['class' => 'form-control is-invalid'],
        bool $checkEmptyField = true,
        bool $checkMissingField = true,
        string $methodNameToSendFormWithWrongData = 'methodNameToSendFormWithWrongData'
    ): void {

        $fielFullName = "${formName}[${fieldName}]";
        $fielId = "#${formName}_${fieldName}";
        if (!\is_null($fieldRepeatName)) {
            $fielFullName = "${formName}[${fieldName}][${fieldRepeatName}]";
            $fielId = "#${formName}_${fieldName}_${fieldRepeatName}";
            if ($fieldRepeatName === 'second') {
                $fielId = "#${formName}_${fieldName}_first";
            }
        }

        // Field is missing
        if ($checkMissingField === true) {
            $formDataWithMissingField = $validFormData;
            unset($formDataWithMissingField["$fielFullName"]);
            $crawler = $this->$methodNameToSendFormWithWrongData(
                $formDataWithMissingField,
                $kernelBrowser
            );
            $this->commonCheckerIfFormFieldIsInvalid(
                crawler: $crawler,
                cssFilterOfInvadFormField: "$fielId",
                cssFilterOfInvalidFeedback: '.invalid-feedback',
                invalidFeedback: $invalidFeedback,
                expectedFieldAttributes: $expectedFieldAttributes
            );
        }

        // Field is empty
        if ($checkEmptyField === true) {
            $formDataWithEmptyField = $validFormData;
            $formDataWithEmptyField["$fielFullName"] = '';
            $crawler = $this->$methodNameToSendFormWithWrongData(
                $formDataWithEmptyField,
                $kernelBrowser
            );
            $this->commonCheckerIfFormFieldIsInvalid(
                crawler: $crawler,
                cssFilterOfInvadFormField: "$fielId",
                cssFilterOfInvalidFeedback: '.invalid-feedback',
                invalidFeedback: $invalidFeedback,
                expectedFieldAttributes: $expectedFieldAttributes
            );
        }
    }



    private function getCurrentHashOfUserPassword(string $userEmail): string
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail($userEmail);
//      $this->assertInstanceOf('App\Entity\User', $user);
//      $this->assertSame($this->emailManager1, $user->getEmail());
        return $user->getPassword();
    }

    private function resetUserPasswordInDatabaseWithProvidedPasswordHash(
        string $userEmail,
        string $newUserPasswordHash
    ): void {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail($userEmail);
        $user->setPassword($newUserPasswordHash);
        $userRepository->save(entity: $user, flush: true);
        $this->assertSame(
            $newUserPasswordHash,
            $this->getCurrentHashOfUserPassword($userEmail)
        );
    }

    private function checkPasswordHasBeenChangedInDatabase(
        string $userEmail,
        string $initialUserPasswordHash
    ): void {
        $currentUserPasswordHash = $this->getCurrentHashOfUserPassword($userEmail);
        $this->assertNotSame("$initialUserPasswordHash", "$currentUserPasswordHash");
        $this->assertSame(\strlen("$initialUserPasswordHash"), \strlen("$currentUserPasswordHash"));
    }


    public function commonCheckWhenFormIsSentWithoutSimilarNewPasswordFields(
        KernelBrowser $kernelBrowser,
        array $validFormData,
        string $formName,
        string $fieldName,
        int $minPasswordLength,
        string $methodNameToSendFormWithWrongData = 'methodNameToSendFormWithWrongData'
    ): void {
        $NewPassword_first = $this->generateRandomString($minPasswordLength + 2);
        $NewPassword_second = "bad_repeat_NewPassword_" . $NewPassword_first;

        $formData = $validFormData;
        $formData["${formName}[${fieldName}][first]"] = $NewPassword_first;
        $formData["${formName}[${fieldName}][second]"] = $NewPassword_second;
        $crawler = $this->$methodNameToSendFormWithWrongData(
            $formData,
            $kernelBrowser
        );
        $this->commonCheckerIfFormFieldIsInvalid(
            crawler: $crawler,
            cssFilterOfInvadFormField: "#${formName}_${fieldName}_first",
            cssFilterOfInvalidFeedback: '.invalid-feedback',
            invalidFeedback: 'The new password fields must match.',
        );
    }

    public function commonCheckWhenFormIsSentWithTooSmallRepeatPassword(
        KernelBrowser $kernelBrowser,
        array $validFormData,
        string $formName,
        string $fieldName,
        int $minPasswordLength,
        string $methodNameToSendFormWithWrongData = 'methodNameToSendFormWithWrongData'
    ): void {
        $tooSmallPasswords = [
            '1', // very small password size (1 characters)
            '123', // very small password size (3 characters)
            $this->generateRandomString($minPasswordLength - 2), // password is 2 character short of required size
            $this->generateRandomString($minPasswordLength - 1), // password is 1 character short of required size
        ];

        foreach ($tooSmallPasswords as $tooSmallPassword) {
            $formData = $validFormData;
            $formData["${formName}[${fieldName}][first]"] = $tooSmallPassword;
            $formData["${formName}[${fieldName}][second]"] = $tooSmallPassword;
            $crawler = $this->$methodNameToSendFormWithWrongData(
                $formData,
                $kernelBrowser
            );
            $this->commonCheckerIfFormFieldIsInvalid(
                crawler: $crawler,
                cssFilterOfInvadFormField: "#${formName}_${fieldName}_first",
                cssFilterOfInvalidFeedback: '.invalid-feedback',
                invalidFeedback: 'Password size is too small.',
            );
        }
    }



    /**
     * Check that the user can only login with his new password
     * - Logout current user
     * - User can not login with his old password
     * - User can login wtih his new password
     *
     * @param KernelBrowser $kernelBrowser
     * @param string $userEmail
     * @param string $newPlainPassword  used to test that user can login wtih his new password
     * @param string $oldPlainPassword  used to test that user can not login with his old password
     * @return void
     */
    private function checkUserCanLoginOnlyWithNewPassword(
        KernelBrowser $kernelBrowser,
        string $userEmail,
        string $newPlainPassword,
        string $oldPlainPassword
    ): void {
        // Logout current user
        $crawler = $kernelBrowser->request('GET', '/account/logout');
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND); // HTTP status code = 302
        $this->assertResponseHeaderSame("Location", 'http://localhost/');

        // User can not login with his old password
        $this->commonCheckerUserCanNotLogin(
            mail: "$userEmail",
            password:  "$oldPlainPassword",
            kernelBrowser: $kernelBrowser,
        );

        // User can login wtih his new password
        $this->commonCheckerUserCanLoginV2(
            kernelBrowser: $kernelBrowser,
            mail: "$userEmail",
            password: "$newPlainPassword"
        );
    }
}
