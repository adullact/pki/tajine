<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Administrator;

use App\Repository\UserRepository;
use App\Tests\Functional\TestHelperTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class FunctionalTestAdministratorManageUsersTest extends WebTestCase
{
    use TestHelperTrait;

    private string $emailAdmin1 = "test_admin_1_tajine@example.org";
    private string $passwordAdmin1 = "admin-tajine_PaSsWord_IsNotSoSecretChangeIt";


    public function testAdminCanNotLoginWithEmptyPassword(): void
    {
        $emptyPassword = "";
        $this->commonCheckerUserCanNotLogin(
            mail:  $this->emailAdmin1,
            password: "$emptyPassword"
        );
//      $this->commonCheckkerUserExistInDatabaseWithExpectedRoles(
//          userEmail: $this->emailAdmin1,
//          expectedPrincipalUserRole: "ROLE_ADMIN"
//      );
    }

    public function testAdminCanNotLoginWithBadPassword(): void
    {
        $this->commonCheckerUserCanNotLogin(
            mail:  $this->emailAdmin1,
            password: "this is a wrong password"
        );
//      $this->commonCheckkerUserExistInDatabaseWithExpectedRoles(
//          userEmail: $this->emailAdmin1,
//          expectedPrincipalUserRole: "ROLE_ADMIN"
//      );
    }

    public function testAdminCanNotLoginWithBadEmail(): void
    {
        $this->commonCheckerUserCanNotLogin(
            mail:  "bad.email." . $this->emailAdmin1,
            password: $this->passwordAdmin1
        );
//      $this->commonCheckkerUserExistInDatabaseWithExpectedRoles(
//          userEmail: $this->emailAdmin1,
//          expectedPrincipalUserRole: "ROLE_ADMIN"
//      );
    }

    /**
     * @todo add chekker "connected admin can see admin links"
     */
    public function testAdminCanLogin(): void
    {
        $this->commonCheckerUserCanLogin(
            mail: $this->emailAdmin1,
            password: $this->passwordAdmin1,
            role:'ROLE_ADMIN'
        );
    }

    public function testAdminCanLogout(): void
    {
        $this->commonCheckerUserCanLogout(mail: $this->emailAdmin1);
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////
    public function commonAdminCanAddUserAccount(
        string $userAddedEmail,
        string $userAddedPassword,
        string $formUrl,
        string $expectedPrincipalUserRole,
    ): void {
        $expectedRoute = '';
        if ($expectedPrincipalUserRole === 'ROLE_CERTIFICAT_MANAGER') {
            $expectedRoute = 'app_admin_users_add_manager';
        } elseif ($expectedPrincipalUserRole === 'ROLE_ADMIN') {
            $expectedRoute = 'app_admin_users_add_admin';
        }
        $emailAdmin1 =  $this->emailAdmin1;
        $kernelBrowserConnectedAdmin1 =  $this->getKernelBrowserWithConnectedUser($emailAdmin1);
        $crawler = $kernelBrowserConnectedAdmin1->request('GET', "$formUrl");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertRouteSame("$expectedRoute");
        $crawler = $kernelBrowserConnectedAdmin1->submitForm('admin-userForm-edit-or-add', [
            'user[email]' => "$userAddedEmail",
            'user[plainPassword]' => "$userAddedPassword",
        ]);
        $this->assertResponseStatusCodeSame(Response::HTTP_SEE_OTHER); // HTTP status code = 303
        $this->assertResponseHeaderSame("Location", '/admin/users');

        // The added user is present in database with expected principal ROLE
        $this->commonCheckkerUserExistInDatabaseWithExpectedRoles(
            "$userAddedEmail",
            "$expectedPrincipalUserRole"
        );

        // Logout Admin1
        $crawler = $kernelBrowserConnectedAdmin1->request('GET', '/account/logout');
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND); // HTTP status code = 302
        $this->assertResponseHeaderSame("Location", 'http://localhost/');

        // New user can connect with is credentials
        $this->commonCheckerUserCanLoginV2(
            kernelBrowser: $kernelBrowserConnectedAdmin1,
            mail: "$userAddedEmail",
            password: "$userAddedPassword",
            role: "$expectedPrincipalUserRole",
        );
    }


    ///////////////////////////////////////////////////////////////////////////////////////////////

    public function testAdminCanAddManagerAccount(): void
    {
        $alea = mt_rand();
        $this-> commonAdminCanAddUserAccount(
            userAddedEmail: "manager_added_by_test_$alea@example.org",
            userAddedPassword: "PasswordManager@$alea",
            formUrl: "/admin/users/add-manager",
            expectedPrincipalUserRole: "ROLE_CERTIFICAT_MANAGER"
        );
    }


    public function testAdminCanAddAdminstrotorAccount(): void
    {
        $alea = mt_rand();
        $this-> commonAdminCanAddUserAccount(
            userAddedEmail: "administrator_added_by_test_$alea@example.org",
            userAddedPassword: "PasswordAdmin@$alea",
            formUrl: "/admin/users/add-administrator",
            expectedPrincipalUserRole: "ROLE_ADMIN"
        );
    }

    public function testAdminCanDeleteUserAccount(): void
    {
        $emailAdmin1 = "test_admin_1_tajine@example.org";
//      $kernelBrowserConnectedManager1 =  $this->getKernelBrowserWithConnectedUser($emailAdmin1);
    }

    public function testAdminCanUpdateUserPassword(): void
    {
        $emailAdmin1 = "test_admin_1_tajine@example.org";
//      $kernelBrowserConnectedManager1 =  $this->getKernelBrowserWithConnectedUser($emailAdmin1);
    }

    public function testAdminCanUpdateUserEmail(): void
    {
        $emailAdmin1 = "test_admin_1_tajine@example.org";
//      $kernelBrowserConnectedManager1 =  $this->getKernelBrowserWithConnectedUser($emailAdmin1);
    }
}
