<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace Functional\Administrator;

use App\Repository\UserRepository;
use App\Tests\Functional\TestHelperDownloadCertificatePublicKeyTrait;
use App\Tests\Functional\TestHelperTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @group admin_manage_certificates
 */
class FunctionalTestAdministratorManageCertificatesTest extends WebTestCase
{
    use TestHelperTrait;
    use TestHelperDownloadCertificatePublicKeyTrait;

    private string $emailAdmin1 = "test_admin_1_tajine@example.org";

    /**
     * @todo refactor
     * @group refactor
     */
    public function testAdminCanSeeLastCertificates(): void
    {
        $certSerialNumber1 = '0x468B91C7D3F967FBD62F969A251CF740E1F066DA';
        $certSerialNumber2 = '0x58F3C5938D6B1863BC56525264FBDD2209C60BCF';
        $certSerialNumber3 = '0x33BDE2E6F0138FA1E24B629B0E3C5D44F4E4386C';
        $certUsedBy1 = 'test_user_1_tajine@example.org';
        $certUsedBy2 = 'test_user_2_tajine@example.org';
        $certUsedBy3 = 'test_user_3_tajine@example.org';
        $certCreator1 = 'test_manager_2_tajine@example.org';
        $certCreator2 = $certCreator1;
        $certCreator3 = 'test_manager_1_tajine@example.org';
        ;
        $url =  "/admin/certificates";
        $expectedRoute = 'app_admin_certificates';
        $kernelBrowserConnectedAdmin1 =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
        $crawler = $kernelBrowserConnectedAdmin1->request('GET', "$url");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertRouteSame("$expectedRoute");
        $this->assertSelectorTextSame('h1', 'All generated certificates');
//        $this->assertSelectorTextSame("#cert_${certSerialNumber1} a.cert_serialNumber", "$certSerialNumber1");
//        $this->assertSelectorTextSame("#cert_${certSerialNumber2} a.cert_serialNumber", "$certSerialNumber2");
//        $this->assertSelectorTextSame("#cert_${certSerialNumber3} a.cert_serialNumber", "$certSerialNumber3");
//        $this->assertSelectorTextSame("#cert_${certSerialNumber1} a.cert_usedBy", "$certUsedBy1");
//        $this->assertSelectorTextSame("#cert_${certSerialNumber2} a.cert_usedBy", "$certUsedBy2");
//        $this->assertSelectorTextSame("#cert_${certSerialNumber3} a.cert_usedBy", "$certUsedBy3");
//        $this->assertSelectorTextSame("#cert_${certSerialNumber1} a.cert_creator", "$certCreator1");
//        $this->assertSelectorTextSame("#cert_${certSerialNumber2} a.cert_creator", "$certCreator2");
//        $this->assertSelectorTextSame("#cert_${certSerialNumber3} a.cert_creator", "$certCreator3");
        //////////////////////////////////////////////////
        $attributesExpected = [ '_text' => "$certSerialNumber1", 'href' => "/admin/certificates/$certSerialNumber1"];
        $this->checkAttribute($crawler, "#cert_${certSerialNumber1} a.cert_serialNumber", $attributesExpected);

        $attributesExpected = [ '_text' => "$certSerialNumber2", 'href' => "/admin/certificates/$certSerialNumber2"];
        $this->checkAttribute($crawler, "#cert_${certSerialNumber2} a.cert_serialNumber", $attributesExpected);

        $attributesExpected = [ '_text' => "$certSerialNumber3", 'href' => "/admin/certificates/$certSerialNumber3"];
        $this->checkAttribute($crawler, "#cert_${certSerialNumber3} a.cert_serialNumber", $attributesExpected);
        //////////////////////////////////////////////////
        $user1 =  $this->getDatabaseUserByEmail("$certUsedBy1");
        $attributesExpected = [ '_text' => $user1->getEmail(), 'href' => '/admin/users/' .  $user1->getId()];
        $this->checkAttribute($crawler, "#cert_${certSerialNumber1} a.cert_usedBy", $attributesExpected);

        $user2 =  $this->getDatabaseUserByEmail("$certUsedBy2");
        $attributesExpected = [ '_text' => $user2->getEmail(), 'href' => '/admin/users/' .  $user2->getId()];
        $this->checkAttribute($crawler, "#cert_${certSerialNumber2} a.cert_usedBy", $attributesExpected);

        $user3   =  $this->getDatabaseUserByEmail("$certUsedBy3");
        $attributesExpected = [ '_text' => $user3->getEmail(), 'href' => '/admin/users/' .  $user3->getId()];
        $this->checkAttribute($crawler, "#cert_${certSerialNumber3} a.cert_usedBy", $attributesExpected);
        //////////////////////////////////////////////////
        $creator1 =  $this->getDatabaseUserByEmail("$certCreator1");
        $attributesExpected = [ '_text' => $creator1->getEmail(), 'href' => '/admin/users/' .  $creator1->getId()];
        $this->checkAttribute($crawler, "#cert_${certSerialNumber1} a.cert_creator", $attributesExpected);

        $creator2 =  $this->getDatabaseUserByEmail("$certCreator2");
        $attributesExpected = [ '_text' => $creator2->getEmail(), 'href' => '/admin/users/' .  $creator2->getId()];
        $this->checkAttribute($crawler, "#cert_${certSerialNumber2} a.cert_creator", $attributesExpected);

        $creator3 =  $this->getDatabaseUserByEmail("$certCreator3");
        $attributesExpected = [ '_text' => $creator3->getEmail(), 'href' => '/admin/users/' .  $creator3->getId()];
        $this->checkAttribute($crawler, "#cert_${certSerialNumber3} a.cert_creator", $attributesExpected);
        //////////////////////////////////////////////////
    }



    public function testAdminCanDisplayCertificate(): void
    {
        $certSerialNumber = '0x468B91C7D3F967FBD62F969A251CF740E1F066DA';
        $url =  "/admin/certificates/${certSerialNumber}";
        $expectedRoute = 'app_admin_certificates_display';
        $kernelBrowserConnectedAdmin1 =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
        $crawler = $kernelBrowserConnectedAdmin1->request('GET', "$url");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertRouteSame("$expectedRoute");

        // HTML content checks
        $this->assertPageTitleSame("Admin - Certificates: $certSerialNumber");
        $this->assertSelectorTextSame('h1', 'Certificate');
        $this->assertSelectorTextSame('#cert_serial-number', "$certSerialNumber");
    }
    public function testAdminCanDisplayCertificateAndSeeCertSubject(): void
    {
        $certSerialNumber = '0x468B91C7D3F967FBD62F969A251CF740E1F066DA';
        $url =  "/admin/certificates/${certSerialNumber}";
        $expectedRoute = 'app_admin_certificates_display';
        $kernelBrowserConnectedAdmin1 =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
        $crawler = $kernelBrowserConnectedAdmin1->request('GET', "$url");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertRouteSame("$expectedRoute");

        // HTML content checks
        $this->assertSelectorTextSame('#cert_serial-number', "$certSerialNumber");
        $this->assertSelectorTextContains('#cert_subject', "ST=PYRENEES-ATLANTIQUES");
        $this->assertSelectorTextContains('#cert_subject', "L=CAUDIES-DE-CONFLENT");
        $this->assertSelectorTextContains('#cert_subject', "O=MAIRIE-DE-CAUDIES-DE-CONFLENT");
        $this->assertSelectorTextContains('#cert_subject', "OU=SERVICE-DES-SPORS-DE-LA-VILLE-DE-CAUDIES-DE-CONFLENT");
        $this->assertSelectorTextContains('#cert_subject', "CN=TEST_USER_1_TAJINE@EXAMPLE.ORG");
    }
    public function testAdminCanDisplayCertificateAndSeeCreatorEmail(): void
    {
        $certSerialNumber = '0x468B91C7D3F967FBD62F969A251CF740E1F066DA';
        $certCreator = 'test_manager_2_tajine@example.org';

        $url =  "/admin/certificates/${certSerialNumber}";
        $expectedRoute = 'app_admin_certificates_display';
        $kernelBrowserConnectedAdmin1 =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
        $crawler = $kernelBrowserConnectedAdmin1->request('GET', "$url");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200

        // HTML content checks
        $this->assertSelectorTextSame('#cert_serial-number', "$certSerialNumber");
        $this->assertSelectorTextSame('#cert_creator', "$certCreator");
    }
    public function testAdminCanDisplayCertificateAndSeeEndUserEmail(): void
    {
        $certSerialNumber = '0x468B91C7D3F967FBD62F969A251CF740E1F066DA';
        $certUsedBy = 'test_user_1_tajine@example.org';

        $url =  "/admin/certificates/${certSerialNumber}";
        $expectedRoute = 'app_admin_certificates_display';
        $kernelBrowserConnectedAdmin1 =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
        $crawler = $kernelBrowserConnectedAdmin1->request('GET', "$url");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200

        // HTML content checks
        $this->assertSelectorTextSame('#cert_serial-number', "$certSerialNumber");
        $this->assertSelectorTextSame('#cert_used-by', "$certUsedBy");
    }
    public function testAdminCanDisplayCertificateAndSeeCertDates(): void
    {
        $certSerialNumber = '0x468B91C7D3F967FBD62F969A251CF740E1F066DA';
//        $url =  "/admin/certificates/${certSerialNumber}";
//        $expectedRoute = 'app_admin_certificates_display';
//        $kernelBrowserConnectedAdmin1 =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
//        $crawler = $kernelBrowserConnectedAdmin1->request('GET', "$url");
//        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
//
//        // HTML content checks
//        $this->assertSelectorTextSame('#cert_serial-number', "$certSerialNumber");
    }

    public function testAdminCanDisplayCertificateAndSeePublicKey(): void
    {
        $certSerialNumber = '0x468B91C7D3F967FBD62F969A251CF740E1F066DA';
        $certPublicKey = '-----BEGIN CERTIFICATE-----
MIIEqDCCA5CgAwIBAgIURouRx9P5Z/vWL5aaJRz3QOHwZtowDQYJKoZIhvcNAQEL
BQAweTELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMSUwIwYDVQQK
ExxWQUdSQU5UIEVYQU1QTEUgT1JHQU5JWkFUSU9OMS0wKwYDVQQDEyRWQUdSQU5U
IEVYQU1QTEUgSU5URVJNRURJQVRFIENBIEdFTjEwHhcNMjMwNjE5MTYxMTAwWhcN
MjQwNjE4MTYxMTAwWjCB2jELMAkGA1UEBhMCRlIxHTAbBgNVBAgTFFBZUkVORUVT
LUFUTEFOVElRVUVTMRwwGgYDVQQHExNDQVVESUVTLURFLUNPTkZMRU5UMSYwJAYD
VQQKEx1NQUlSSUUtREUtQ0FVRElFUy1ERS1DT05GTEVOVDE9MDsGA1UECxM0U0VS
VklDRS1ERVMtU1BPUlMtREUtTEEtVklMTEUtREUtQ0FVRElFUy1ERS1DT05GTEVO
VDEnMCUGA1UEAwweVEVTVF9VU0VSXzFfVEFKSU5FQEVYQU1QTEUuT1JHMIIBIjAN
BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqNp6FK6KZbSPNjhvis9+bA3z73aH
MiISjB+aC3m3DEQo4C429Tg8s5YdKbdmY2TYYhS6S/H8tlrzQN5Tnwr98FhG+ZFh
wLkhYu9fBH668NhQGJCxucwDDCr5O84lNVonHAO6tG2govS7seBoCqU/F+T8yggN
Bk0QmGthMwabDYLUHBEflBq6hbLFZF2z4Yr3EWa/GEriZPKfH5PDlOOGJN6/a/UQ
gKNyl0ZUPyAxDDdsw0XobZNrHSjhSu53ItDrlbHwAAz4ORs8kkJ13Ya1X0ukDzcw
n+YVTkzEITQMunOtoQTvY/fnSky+WAosuri+/2AcaVNc2NejFF+P+bZLaQIDAQAB
o4HFMIHCMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0O
BBYEFN5bWACRzSNTtWtXLgq4jWNUrkyUMB8GA1UdIwQYMBaAFHErrb3INhdd387l
t+oHFBVkUjv4MAsGA1UdEQQEMAKCADBQBgNVHR8ESTBHMEWgQ6BBhj9odHRwOi8v
Y3JsLmV4YW1wbGUub3JnL2NybC1WQUdSQU5URVhBTVBMRUlOVEVSTUVESUFURUNB
R0VOMS5jcmwwDQYJKoZIhvcNAQELBQADggEBAJurjjB99x81yVq1jMhLuK1KV9qH
8KslQFBSGSYo6ACeiSqeNhK3hgQKt5jD4GHVHgctZ6TlShVkvQvnLdL6msFHqgb2
MDmJOh0LKTb9Sv4Cyvuibpey0EIVqA/LNl4PR7/dwlrpnEawuG4ywbZV4TCIKcuD
6WlVdPfDSr4+j9+4Zk99UtaIFfhCEnFCZyv8cbQ4dKeL4wbzWE/kNV9QRm8o1Z2D
udHDhXunee/i3hxzrnvCzsTe+J8xAHgKNCwMnwDNcl0HE+KWmg522tsGBxI+OYSc
F4JfLMqxlpcp7p/8AZNHZeevRAO23l8p/N7QMj1bTp+AOBFsSaKLoR24Q1s=
-----END CERTIFICATE-----';

        $url =  "/admin/certificates/${certSerialNumber}";
        $expectedRoute = 'app_admin_certificates_display';
        $kernelBrowserConnectedAdmin1 =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
        $crawler = $kernelBrowserConnectedAdmin1->request('GET', "$url");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200

        // HTML content checks
        $this->assertSelectorTextSame('#cert_serial-number', "$certSerialNumber");
        $attributes = $crawler
            ->filter('#cert_public-key')
            ->extract(['_name', '_text']);
        $this->assertEquals(1, count($attributes));
        $htmlTag = $attributes[0][0];
        $htmlContent = trim($attributes[0][1]);
        $this->assertEquals("pre", "$htmlTag");
        $this->assertEquals("$certPublicKey", "$htmlContent");
    }

    public function testAdminCanDisplayCertificateAndSeeLinkToDownloadPublicKey(): void
    {
        $certSerialNumber = '0x468B91C7D3F967FBD62F969A251CF740E1F066DA';

        $url =  "/admin/certificates/${certSerialNumber}";
        $expectedRoute = 'app_admin_certificates_display';
        $kernelBrowserConnectedAdmin1 =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
        $crawler = $kernelBrowserConnectedAdmin1->request('GET', "$url");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200

        // HTML content checks
        $this->assertSelectorTextSame('#cert_serial-number', "$certSerialNumber");
        $this->assertSelectorTextSame(
            'a#cert_download-public-key',
            "Download public key (PEM file) of certificate"
        );
        $attributes = $crawler
            ->filter('a#cert_download-public-key')
            ->extract(['title', 'href', '_text']);
        $this->assertEquals(1, count($attributes));
        $linkTitle = $attributes[0][0];
        $linkHref = $attributes[0][1];
        $linkText = trim($attributes[0][2]);
        $expectedLinkTitle = "Download public key (PEM file) of certificate $certSerialNumber";
        $expectedLinkHref = "/admin/certificates/$certSerialNumber/download";
        $expectedLinkText = "Download public key (PEM file) of certificate";
        $this->assertEquals("$expectedLinkTitle", "$linkTitle");
        $this->assertEquals("$expectedLinkHref", "$linkHref");
        $this->assertEquals("$expectedLinkText", "$linkText");
    }


    public function testAdminCanDownloadCertificatePublicKey(): void
    {
        $certSerialNumber = '0x468B91C7D3F967FBD62F969A251CF740E1F066DA';
        $expectedCertPublicKey = '-----BEGIN CERTIFICATE-----
MIIEqDCCA5CgAwIBAgIURouRx9P5Z/vWL5aaJRz3QOHwZtowDQYJKoZIhvcNAQEL
BQAweTELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMSUwIwYDVQQK
ExxWQUdSQU5UIEVYQU1QTEUgT1JHQU5JWkFUSU9OMS0wKwYDVQQDEyRWQUdSQU5U
IEVYQU1QTEUgSU5URVJNRURJQVRFIENBIEdFTjEwHhcNMjMwNjE5MTYxMTAwWhcN
MjQwNjE4MTYxMTAwWjCB2jELMAkGA1UEBhMCRlIxHTAbBgNVBAgTFFBZUkVORUVT
LUFUTEFOVElRVUVTMRwwGgYDVQQHExNDQVVESUVTLURFLUNPTkZMRU5UMSYwJAYD
VQQKEx1NQUlSSUUtREUtQ0FVRElFUy1ERS1DT05GTEVOVDE9MDsGA1UECxM0U0VS
VklDRS1ERVMtU1BPUlMtREUtTEEtVklMTEUtREUtQ0FVRElFUy1ERS1DT05GTEVO
VDEnMCUGA1UEAwweVEVTVF9VU0VSXzFfVEFKSU5FQEVYQU1QTEUuT1JHMIIBIjAN
BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqNp6FK6KZbSPNjhvis9+bA3z73aH
MiISjB+aC3m3DEQo4C429Tg8s5YdKbdmY2TYYhS6S/H8tlrzQN5Tnwr98FhG+ZFh
wLkhYu9fBH668NhQGJCxucwDDCr5O84lNVonHAO6tG2govS7seBoCqU/F+T8yggN
Bk0QmGthMwabDYLUHBEflBq6hbLFZF2z4Yr3EWa/GEriZPKfH5PDlOOGJN6/a/UQ
gKNyl0ZUPyAxDDdsw0XobZNrHSjhSu53ItDrlbHwAAz4ORs8kkJ13Ya1X0ukDzcw
n+YVTkzEITQMunOtoQTvY/fnSky+WAosuri+/2AcaVNc2NejFF+P+bZLaQIDAQAB
o4HFMIHCMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0O
BBYEFN5bWACRzSNTtWtXLgq4jWNUrkyUMB8GA1UdIwQYMBaAFHErrb3INhdd387l
t+oHFBVkUjv4MAsGA1UdEQQEMAKCADBQBgNVHR8ESTBHMEWgQ6BBhj9odHRwOi8v
Y3JsLmV4YW1wbGUub3JnL2NybC1WQUdSQU5URVhBTVBMRUlOVEVSTUVESUFURUNB
R0VOMS5jcmwwDQYJKoZIhvcNAQELBQADggEBAJurjjB99x81yVq1jMhLuK1KV9qH
8KslQFBSGSYo6ACeiSqeNhK3hgQKt5jD4GHVHgctZ6TlShVkvQvnLdL6msFHqgb2
MDmJOh0LKTb9Sv4Cyvuibpey0EIVqA/LNl4PR7/dwlrpnEawuG4ywbZV4TCIKcuD
6WlVdPfDSr4+j9+4Zk99UtaIFfhCEnFCZyv8cbQ4dKeL4wbzWE/kNV9QRm8o1Z2D
udHDhXunee/i3hxzrnvCzsTe+J8xAHgKNCwMnwDNcl0HE+KWmg522tsGBxI+OYSc
F4JfLMqxlpcp7p/8AZNHZeevRAO23l8p/N7QMj1bTp+AOBFsSaKLoR24Q1s=
-----END CERTIFICATE-----';

        $kernelBrowserConnectedAdmin1 =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
        $this->commonAllowedUserCanDownloadCertificatePublicKey(
            kernelBrowserForConnnectedUser: $kernelBrowserConnectedAdmin1,
            uri:  "/admin/certificates/${certSerialNumber}/download",
            expectedRoute: 'app_admin_certificates_download',
            certSerialNumber: $certSerialNumber,
            certPublicKey: $expectedCertPublicKey
        );
    }

    public function testAdminSeeClassicNotFoundPageWhenDisplayNotFoundCertificate(): void
    {
        $certSerialNumber = '0x4999999999999999999999999999999999999999';
        $url =  "/admin/certificates/${certSerialNumber}";
        $expectedRoute = 'app_admin_certificates_display';
        $kernelBrowserConnectedAdmin1 =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
        $kernelBrowserConnectedAdmin1->catchExceptions(false);
        $this->expectException(NotFoundHttpException::class);
        $crawler = $kernelBrowserConnectedAdmin1->request('GET', "$url");
    }

    public function testAdminSeeBasicNotFoundPageWhenDownloadNotFoundCertificate(): void
    {
        $certSerialNumber = '0x4999999999999999999999999999999999999999'; // Fake certificate
        $kernelBrowserConnectedAdmin1 =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
        $this->commonAllowedUserSeeBasicNotFoundPageWhenDownloadNotFoundCertificate(
            kernelBrowserForConnnectedUser: $kernelBrowserConnectedAdmin1,
            downloadUri: "/admin/certificates/${certSerialNumber}/download",
            expectedRoute: 'app_admin_certificates_download'
        );
    }
}
