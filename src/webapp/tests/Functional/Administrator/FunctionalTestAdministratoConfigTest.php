<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace Functional\Administrator;

use App\Tests\CommonHelpers\TestHelperEmailTrait;
use App\Tests\Functional\TestHelperTrait;
use Symfony\Bundle\FrameworkBundle\Test\MailerAssertionsTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class FunctionalTestAdministratoConfigTest extends WebTestCase
{
    use TestHelperTrait;
    use TestHelperEmailTrait;
    use MailerAssertionsTrait;

    private string $emailAdmin1 = "test_admin_1_tajine@example.org";

    /**
     * @group config
     * @group debug
     */
    public function testAdminCanNotSendTestingEmailWhenMailerDsnEnvIsNotValid(): void
    {
        $expectedEmailTo = "sysadmin@test.example.org";

        // Check current value of MAILER_DSN environment variable
        $initialMailerDsnEnv = array_key_exists('MAILER_DSN', $_ENV) ? $_ENV['MAILER_DSN'] : false;
        $this->assertSame("null://null", "$initialMailerDsnEnv");

        // Set MAILER_DSN environment variable to a value that will cause mail sending to fail
        $expectedFakeMailerDsn = "smtp://smtp.example.org:465";
        $_ENV['MAILER_DSN'] = "$expectedFakeMailerDsn";
        $fakeMailerDsnEnv = array_key_exists('MAILER_DSN', $_ENV) ? $_ENV['MAILER_DSN'] : false;
        $this->assertSame("$expectedFakeMailerDsn", "$fakeMailerDsnEnv");

        // Load "Tested sending emails" page
        $kernelBrowserForConnnectedAdmin =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
        $crawler = $kernelBrowserForConnnectedAdmin->request('GET', '/admin/configuration/test-mail');

        // Reset MAILER_DSN environment variable to intial value. Mandatory for other tests.
        $_ENV['MAILER_DSN'] = "$initialMailerDsnEnv";

        // Checks HTML on "Tested sending emails" page
        $this->assertRouteSame("app_admin_config_send_testing_email");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextContains('h1', "Tested sending emails");
        $this->assertSelectorExists('.alert-danger');
        $this->assertSelectorNotExists('.alert-success');
        $this->assertSelectorTextContains('.alert[role=alert]', "FAIL");
        $this->assertSelectorTextNotContains('.alert[role=alert]', "SUCCESFUL");
        $this->assertSelectorTextContains(
            '.alert[role=alert]',
            "Sending email to $expectedEmailTo was FAIL"
        );
        $this->assertSelectorTextContains(
            '.alert[role=alert]',
            "Connection could not be established with host"
        );

        // No EMAIL message sent  ---> TODO can't figure out how to test this point
//      $this->assertEmailCount(0);
//      $this->assertNull($this->getMailerMessage());
//      $this->assertNull($this->getMailerEvent());

        // Error message 1        MAILER_DSN=smtp://smtp.example.org:465
        //                       Symfony\Component\Mailer\Exception\TransportException
        //          Connection could not be established with host "ssl://smtp.example.org:465":
        //          stream_socket_client(): php_network_getaddresses: getaddrinfo for smtp.example.org failed:
        //          Name or service not known
        //
        // Error message 2       MAILER_DSN=smtp://smtp.example.org:25
        //                       Symfony\Component\Mailer\Exception\TransportException
        //          Connection could not be established with host "smtp.example.org:25":
        //          stream_socket_client(): php_network_getaddresses: getaddrinfo for smtp.example.org failed:
        //          Name or service not known
        //
        // Error message 3       MAILER_DSN=smtp://127.0.0.1:235  (not used port)
        //                       Symfony\Component\Mailer\Exception\TransportException
        //          Connection could not be established with host "127.0.0.1:235":
        //          stream_socket_client(): Unable to connect to 127.0.0.1:235 (Connection refused)
        //
        // Error message 4       MAILER_DSN=smtp://127.0.0.1:9999  (invalid port number)
        //                       Symfony\Component\Mailer\Exception\TransportException
        //          Connection to "127.0.0.1:9999" timed out.
        //
        // Error message 5       MAILER_DSN=not_valid_mailer_DSN
        //                       Symfony\Component\Mailer\Exception\InvalidArgumentException
        //          The mailer DSN must contain a scheme.
        //
        // Error message 6       MAILER_DSN=notvalid://smtp.example.org:465
        //                       Symfony\Component\Mailer\Exception\UnsupportedSchemeException
        //          The "notvalid" scheme is not supported.
    }

    /**
     * @group config
     * @group debug
     */
    public function testAdminCanSendTestingEmailWhenMailerDsnEnvIsValid(): void
    {
        $expectedEmailHeaderTo = "sysadmin@test.example.org";
        $expectedEmailHeaderFrom = "no-reply@test.example.org";
        $expectedAppShortName = "TajineTest (RA)";

        // Check .env configuration
        $appShortName = array_key_exists('WEBAPP_SHORTNAME', $_ENV) ? $_ENV['WEBAPP_SHORTNAME'] : false;
        $appEmailFrom = array_key_exists('WEBAPP_EMAIL_FROM', $_ENV) ? $_ENV['WEBAPP_EMAIL_FROM'] : false;
        $appEmailAlertingTo = false;
        if (array_key_exists('WEBAPP_EMAIL_ALERTING_TO', $_ENV)) {
            $appEmailAlertingTo = $_ENV['WEBAPP_EMAIL_ALERTING_TO'] ;
        }
        $this->assertSame("$expectedEmailHeaderTo", "$appEmailAlertingTo");
        $this->assertSame("$expectedEmailHeaderFrom", "$appEmailFrom");
        $this->assertSame("$expectedAppShortName", "$appShortName");

        // Load "Tested sending emails" page
        $kernelBrowserForConnnectedAdmin =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
        $crawler = $kernelBrowserForConnnectedAdmin->request('GET', '/admin/configuration/test-mail');
        $this->assertRouteSame("app_admin_config_send_testing_email");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->checkFooter($crawler, true);
        $this->assertSelectorTextContains('h1', "Tested sending emails");
        $this->assertSelectorNotExists('.alert-danger');
        $this->assertSelectorTextNotContains('.alert[role=alert]', "FAIL");
        $this->assertSelectorExists('.alert-success[role=alert]');
        $this->assertSelectorTextContains(
            '.alert-success[role=alert]',
            "Sending email to $expectedEmailHeaderTo was SUCCESFUL"
        );

        # EMAIL sent to WEBAPP_EMAIL_ALERTING_TO
        $this->assertEmailIsQueued($this->getMailerEvent(0));
        $this->assertEmailCount(1); // use assertQueuedEmailCount() when using Messenger
        $email = $this->getMailerMessage();

        # EMAIL sent to end-user ---> check email content (HTML and TXT versions)
        $this->assertEmailHtmlBodyContains($email, 'Adullact PKI');
        $this->assertEmailTextBodyContains($email, 'Adullact PKI');
        $this->assertEmailHtmlBodyContains($email, "Email de test envoyé à $expectedEmailHeaderTo");
        $this->assertEmailTextBodyContains($email, "Email de test envoyé à $expectedEmailHeaderTo");

        # EMAIL sent to WEBAPP_EMAIL_ALERTING_TO ---> check email HEADERS (technical, correspondent)
        $expectedMailSubject = "Adullact PKI: email de test envoyé à sysadmin@test.example.org"; // TODO refactpr
        $this->assertEmailHeaderSame($email, "Subject", "$expectedMailSubject");
        $this->checkCommonTechnicalEmailHeaders(email: $email);
        $this->checkCommonCorrespondentsEmailHeaders(
            email: $email,
            expectedEmailTo: "$expectedEmailHeaderTo",
            expectedEmailFrom: "$expectedEmailHeaderFrom",
        );
    }

    /**
     * @todo to be implemented
     * @group config
     * @group debug
     */
    public function testAdminCanDisplayConfiguration(): void
    {
        // $kernelBrowserForConnnectedAdmin =  $this->getKernelBrowserWithConnectedUser($this->emailAdmin1);
        // $crawler = $kernelBrowserForConnnectedAdmin->request('GET', '/poc/display_config');
        // $this->assertResponseIsSuccessful();
    }
}
