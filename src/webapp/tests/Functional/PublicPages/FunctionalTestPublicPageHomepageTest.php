<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\tests\Functional\PublicPages;

use App\Tests\Functional\TestHelperTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group allow_mutation_testing_by_infection
 */
class FunctionalTestPublicPageHomepageTest extends WebTestCase
{
    use TestHelperTrait;

    // Accessing Internal Objects
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // $crawler = $client->getCrawler();     //  Crawler instance
    // $history = $client->getHistory();     // client history
    // $cookieJar = $client->getCookieJar(); // client cookie jar
    // $requestHttpKernel = $client->getRequest();           // HttpKernel request instance
    // $requestBrowserKit = $client->getInternalRequest();   // BrowserKit request instance
    // $responseHttpKernel = $client->getResponse();         // HttpKernel response instance
    // $responseBrowserKit = $client->getInternalResponse(); // BrowserKit response instance
    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // dump($crawler);
    // dump($history);
    // dump($cookieJar);
    // dump($requestHttpKernel);
    // dump($requestBrowserKit);
    // dump($responseHttpKernel);
    // dump($responseBrowserKit);
    // dump($requestBrowserKit->getCookies());
    //////////////////////////////////////////////////////////////////////////////////////////////////////

    public function testHomePageWorks(): void
    {
        $client = static::createClient();
        $session = $this->createSession($client);
        $crawler = $client->request('GET', '/');

        // Basic checks
        $this->assertRouteSame('app_home');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
//      $this->assertResponseIsSuccessful(); // HTTP status code >= 200 and HTTP status code < 300;

        // HTML content checks
        $this->assertSelectorTextSame('h1', 'TajineTest (RA)');
        $this->assertPageTitleSame('TajineTest - Registration authority (RA)');
        $this->checkFooter($crawler, false);

        // Cookies checks
        //////////////////////////////////////////////////////////////////////////////////////////////////////
//      $this->assertBrowserHasCookie('MOCKSESSID');
//      $this->assertResponseHasCookie('MOCKSESSID'); // ---> FAIL ---> @@@TODO fixme

        // HTTP Header checks
        // ---> already tested in [ testAnonymousCanBrowsePublicUrls() ] method
        //      using [ commonSecurityHttpHeadersChecker() ]  method
        //////////////////////////////////////////////////////////////////////////////////////////////////////
//      $this->assertResponseHeaderSame("Cache-Control", 'max-age=0, must-revalidate, private');
//      $this->assertResponseHeaderSame(
//          "Content-Security-Policy",
//          'report-uri /csp/report'
//      );  // see:  NelmioSecurityBundle
//      $this->assertResponseHeaderSame("Content-Type", 'text/html; charset=UTF-8');
//      $this->assertResponseHeaderSame(
//          "Referrer-Policy",
//          'no-referrer, strict-origin-when-cross-origin'
//      );
//      $this->assertResponseHeaderSame("X-Content-Type-Options", 'nosniff');
//      $this->assertResponseHeaderSame("X-Frame-Options", 'DENY');
//      $this->assertResponseHeaderSame("X-Robots-Tag", 'noindex');
//      $this->assertResponseHeaderSame("X-Xss-Protection", '1; mode=block');
    }
}
