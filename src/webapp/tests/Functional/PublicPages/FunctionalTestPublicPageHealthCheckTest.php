<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\tests\Functional\PublicPages;

use App\Tests\Functional\TestHelperTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group allow_mutation_testing_by_infection
 */
class FunctionalTestPublicPageHealthCheckTest extends WebTestCase
{
    use TestHelperTrait;

    public function testTryHttpPostMethodToHealthCheckRoute(): void
    {
        $client = static::createClient();
        $crawler = $client->request('POST', '/health-check');
        $this->assertResponseStatusCodeSame(Response::HTTP_METHOD_NOT_ALLOWED); // 405 Method Not Allowed
        $this->assertResponseNotHasHeader("X-Tajine-Database-Status");
        $this->assertResponseNotHasHeader("X-Tajine-Cfssl-Status");
    }

    public function testHttpHeadMethodToHealthCheckRoute(): void
    {
        $client = static::createClient();
        $crawler = $client->request('HEAD', '/health-check');
        $this->assertResponseHasHeader("X-Tajine-Database-Status");
        $this->assertResponseHasHeader("X-Tajine-Cfssl-Status");
        $statusCode = $client->getResponse()->getStatusCode();
        if ($statusCode !== 200 && $statusCode !== 503) {
            $this->assertTrue(false, "Bad HTTP status code [ $statusCode ] (Allowed is 503 or 200");
        }
    }

    public function testHttpGetMethodToHealthCheckRoute(): void
    {
        /*
        dump($_SERVER['CFSSL_API_SERVER']); // http://127.0.0.1:8888
        $_SERVER['CFSSL_API_SERVER'] = 'http://127.0.0.1:7777';
        dump($_SERVER['CFSSL_API_SERVER']);
         */

        $client = static::createClient();
        $crawler = $client->request('GET', '/health-check');

        $this->assertResponseHasHeader("X-Tajine-Database-Status");
        $this->assertResponseHasHeader("X-Tajine-Cfssl-Status");
        $this->assertSelectorTextContains('#webapp-database-status', 'DB_CONNECTION_');
        $this->assertSelectorTextContains('#webapp-cfssl-status', 'CFSSL_CONNECTION_');
        $this->checkFooter($crawler, false);

        $statusCode = $client->getResponse()->getStatusCode();
        $cfsslStatus = $crawler->filter('#webapp-cfssl-status')->innerText();
        $dbStatus = $crawler->filter('#webapp-database-status')->innerText();
        if ($statusCode !== 200 && $statusCode !== 503) {
            $this->assertTrue(false, "Bad HTTP status code [ $statusCode ] (Allowed is 503 or 200");
        } elseif ($statusCode === 200) {
            $this->assertSelectorTextSame('#webapp-database-status', 'DB_CONNECTION_SUCCESSFUL');
            $this->assertSelectorTextSame('#webapp-cfssl-status', 'CFSSL_CONNECTION_SUCCESSFUL');
            $this->assertResponseHeaderSame("X-Tajine-Database-Status", 'DB_CONNECTION_SUCCESSFUL');
            $this->assertResponseHeaderSame("X-Tajine-Cfssl-Status", 'CFSSL_CONNECTION_SUCCESSFUL');
        } elseif (
            $statusCode === 503 &&
            ($cfsslStatus === 'CFSSL_CONNECTION_FAILED' | $dbStatus === 'DB_CONNECTION_FAILED')
        ) {
            // Check CFSSL status
            if ($cfsslStatus === 'CFSSL_CONNECTION_FAILED') {
                $this->assertResponseHeaderSame("X-Tajine-Cfssl-Status", 'CFSSL_CONNECTION_FAILED');
            } elseif ($cfsslStatus === 'CFSSL_CONNECTION_SUCCESSFUL') {
                $this->assertResponseHeaderSame("X-Tajine-Cfssl-Status", 'CFSSL_CONNECTION_SUCCESSFUL');
            } else { // Bad CFSSL status code
                $this->assertTrue(false, "Bad CFSSL status code [ $cfsslStatus ]");
            }

            // Check database status
            if ($dbStatus === 'DB_CONNECTION_FAILED') {
                $this->assertResponseHeaderSame("X-Tajine-Database-Status", 'DB_CONNECTION_FAILED');
            } elseif ($dbStatus === 'DB_CONNECTION_SUCCESSFUL') {
                $this->assertResponseHeaderSame("X-Tajine-Database-Status", 'DB_CONNECTION_SUCCESSFUL');
            } else {// Bad database status code
                $this->assertTrue(false, "Bad database status code [ $dbStatus ]");
            }
        } else { // HTTP status code 503, but not FAILED database status or not FAILED Cfssl status
            $msg = "Bad HTTP status code [ $statusCode ] with ";
            $msg .= "database status code [ $dbStatus ]  and CFSSL status code  [ $cfsslStatus ] ";
            $this->assertTrue(false, "$msg");
        }
    }
}
