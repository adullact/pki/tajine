<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockFileSessionStorage;
use Symfony\Component\DomCrawler\Crawler;

use function PHPUnit\Framework\isNull;

trait TestHelperTrait
{
    /**
     * Get a list of emails in invalid format
     * @return string[]
     */
    public function getListOfEmailsInInvalidFormat(): array
    {
        return [
            'this_is_not_a_valid_email',
            'this_is_not_a_valid_email@example',
            'this_is_not_a_valid_email@example.',
            '@example.org',
        ];
    }

    public function generateRandomString(int $length = 10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-!(!.;:/,)]@_(';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[\random_int(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function getKernelBrowserWithConnectedUser(
        string $userEmail,
        KernelBrowser|null $kernelBrowser = null
    ): KernelBrowser {
        if (is_null($kernelBrowser)) {
            $kernelBrowser = static::createClient();
        }
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail("$userEmail");

        $kernelBrowser->loginUser($testUser);
        $this->commonCheckerUserIsConnected($kernelBrowser, "$userEmail");
        return $kernelBrowser;
    }

    public function checkFooter(Crawler $crawler, bool $displayVersion = false): void
    {

        $version = '';
        if ($displayVersion === true) {
            $version = ' major.minor.patch';
        }
        $footerExpectedLink1 = [
            '_name' => 'a',
            '_text' => "Tajine$version",
            'href'  => 'https://gitlab.adullact.net/adullact/pki/tajine'
        ];
        $footerExpectedLink2 = [
            '_name' => 'a',
            '_text' => 'Adullact',
            'href'  => 'https://adullact.org'
        ];
        $footerExpectedLink3 = [
            '_name' => 'a',
            '_text' => 'GNU AGPL v3',
            'href'  => 'https://www.gnu.org/licenses/agpl-3.0.html'
        ];
        $this->checkAttribute($crawler, '#footer_link-to-repository', $footerExpectedLink1);
        $this->checkAttribute($crawler, '#footer_link-to-creator-website', $footerExpectedLink2);
        $this->checkAttribute($crawler, '#footer_link-to-license', $footerExpectedLink3);
    }

    /**
     * @todo refactor
     * @group refactor
     */
    public function checkAttribute(
        Crawler $crawler,
        string $cssFilter,
        array $attributesExpected = ['_text' => '', '_name' => 'a', 'href' => '#']
    ): void {
        $attributeKeys = array_keys($attributesExpected);
        $attributesCount = count($attributeKeys);
        $attributes = $crawler
            ->filter("$cssFilter")
            ->extract($attributeKeys);
        $errorMsg = "No match for the following CSS filter : [ $cssFilter ]";
        $this->assertEquals(1, count($attributes), "$errorMsg");
        foreach ($attributeKeys as $keyIndex => $keyName) {
            $expected = $attributesExpected["$keyName"];
            if ($keyName === '_text') {
                $this->assertSelectorTextSame("$cssFilter", "$expected");
            } else {
                if ($attributesCount === 1) {
                    $value = $attributes[$keyIndex];
                } else {
                    $value = $attributes[0][$keyIndex];
                }
                $this->assertEquals("$expected", "$value");
            }
        }
    }

    public function createSession(KernelBrowser $client): Session
    {
        $container = $client->getContainer();
        $sessionSavePath = $container->getParameter('session.save_path');
        $sessionStorage = new MockFileSessionStorage($sessionSavePath);

        $session = new Session($sessionStorage);
        $session->start();
        $session->save();

        $sessionCookie = new Cookie(
            $session->getName(),
            $session->getId(),
            null,
            null,
            'localhost',
        );
        $client->getCookieJar()->set($sessionCookie);

        return $session;
    }


    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    public function commonCheckerUserIsConnected(
        KernelBrowser $kernelBrowser,
        string $userEmail,
        string $checkedRoute = 'app_home'
    ): Crawler {

        $crawler = $kernelBrowser->request('GET', '/');
//      $this->assertRouteSame('app_home');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('.connected_user_email', "$userEmail");
        return $crawler;
    }
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    private function commonCheckkerUserExistInDatabaseWithExpectedRoles(
        string $userEmail,
        string $expectedPrincipalUserRole
    ): void {
        // New user is add into database with expected principal ROLE
        $userRepository = static::getContainer()->get(UserRepository::class);
        $testUser = $userRepository->findOneByEmail("$userEmail");
        $this->assertInstanceOf('App\Entity\User', $testUser);
        $expectedRoles = ["$expectedPrincipalUserRole", 'ROLE_USER'];
        $this->assertSame($expectedRoles, $testUser->getRoles());
        $this->assertSame("$userEmail", $testUser->getEmail());
    }
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    public function getDatabaseUserByEmail(
        string $userEmail
    ): User {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $user = $userRepository->findOneByEmail("$userEmail");
        $this->assertInstanceOf('App\Entity\User', $user);
        return $user;
    }
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    public function commonCheckerUserCanNotLogin(
        string $mail,
        string $password = '',
        KernelBrowser $kernelBrowser = null
    ): void {
        if (\is_null($kernelBrowser)) {
            $kernelBrowser = static::createClient();
        }
        $crawler = $kernelBrowser->request('GET', '/account/login');
        $this->assertRouteSame('app_account_login');

        // https://symfony.com/doc/current/testing.html#submitting-forms
        $crawler = $kernelBrowser->submitForm('login-form-button-submit', [
            '_username' => "$mail",
            '_password' => "$password",
        ]);
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND); // HTTP status code = 302
        $this->assertResponseHasHeader("Location");
        $this->assertResponseHeaderSame("Location", 'http://localhost/account/login');

        $crawler = $kernelBrowser->request('GET', '/account/login');
        $this->assertSelectorTextSame('div.alert-warning', "Invalid credentials.");
    }


    public function commonCheckerUserCanLogin(
        string $mail,
        string $password = 'admin-tajine_PaSsWord_IsNotSoSecretChangeIt',
        string $role = 'ROLE_USER'
    ): void {
        $kernelBrowser = static::createClient();
        $crawler = $kernelBrowser->request('GET', '/account/login');
        $this->assertRouteSame('app_account_login');

        // https://symfony.com/doc/current/testing.html#submitting-forms
        $crawler = $kernelBrowser->submitForm('login-form-button-submit', [
            '_username' => "$mail",
            '_password' => "$password",
        ]);
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND); // HTTP status code = 302
        $this->assertResponseHasHeader("Location");
        $this->assertResponseHeaderSame("Location", 'http://localhost/');

        $crawler = $kernelBrowser->request('GET', 'http://localhost/');
        $this->assertRouteSame('app_home');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('.connected_user_email', "$mail");
    }


    public function commonCheckerUserCanLoginV2(
        KernelBrowser $kernelBrowser,
        string $mail,
        string $password = 'admin-tajine_PaSsWord_IsNotSoSecretChangeIt',
        string $role = 'ROLE_USER'
    ): void {
        $crawler = $kernelBrowser->request('GET', '/account/login');
        $this->assertRouteSame('app_account_login');

        // https://symfony.com/doc/current/testing.html#submitting-forms
        $crawler = $kernelBrowser->submitForm('login-form-button-submit', [
            '_username' => "$mail",
            '_password' => "$password",
        ]);
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND); // HTTP status code = 302
        $this->assertResponseHasHeader("Location");
        $this->assertResponseHeaderSame("Location", 'http://localhost/');

        $crawler = $kernelBrowser->request('GET', 'http://localhost/');
        $this->assertRouteSame('app_home');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('.connected_user_email', "$mail");
    }

    public function commonCheckerUserCanLogout(string $mail): void
    {
        $kernelBrowser =  $this->getKernelBrowserWithConnectedUser($mail);
        $crawler = $kernelBrowser->request('GET', '/');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('.connected_user_email', "$mail");
        $this->assertSelectorTextContains('body', "$mail");

        $crawler = $kernelBrowser->request('GET', '/account/logout');
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND); // HTTP status code = 302
        $this->assertResponseHasHeader("Location");
        $this->assertResponseHeaderSame("Location", 'http://localhost/');

        $crawler = $kernelBrowser->request('GET', '/');
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorNotExists('.connected_user_email');
        $this->assertSelectorTextNotContains('body', "$mail");
        $this->checkFooter($crawler, false);
    }

    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    public function commonCheckerHasExpectedSecurityHttpHeaders(): void
    {
        // Basic HTTP Header checks
//      $this->assertResponseHasHeader("Cache-Control");
//      $this->assertResponseHasHeader("Content-Security-Policy");
//      $this->assertResponseHasHeader("Content-Type");
//      $this->assertResponseHasHeader("Referrer-Policy");
//      $this->assertResponseHasHeader("X-Content-Type-Options");
//      $this->assertResponseHasHeader("X-Frame-Options");
//      $this->assertResponseHasHeader("X-Robots-Tag");
//      $this->assertResponseHasHeader("X-Xss-Protection");

        // HTTP Header checks
        $this->assertResponseHeaderSame("Cache-Control", 'max-age=0, must-revalidate, private');
        $this->assertResponseHeaderSame(
            "Content-Security-Policy",
            'report-uri /csp/report'
        );  // see:  NelmioSecurityBundle
        $this->assertResponseHeaderSame("Content-Type", 'text/html; charset=UTF-8');
        $this->assertResponseHeaderSame(
            "Referrer-Policy",
            'no-referrer, strict-origin-when-cross-origin'
        );
        $this->assertResponseHeaderSame("X-Content-Type-Options", 'nosniff');
        $this->assertResponseHeaderSame("X-Frame-Options", 'DENY');
        $this->assertResponseHeaderSame("X-Robots-Tag", 'noindex');
//      $this->assertResponseHeaderSame("X-Xss-Protection", '1; mode=block');
    }
}
