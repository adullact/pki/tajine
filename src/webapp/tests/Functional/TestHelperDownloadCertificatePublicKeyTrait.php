<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional
;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

trait TestHelperDownloadCertificatePublicKeyTrait
{
    protected function commonUserNotAllowedUserToDownloadCertificatePublicKey(
        KernelBrowser $kernelBrowserForConnnectedUser,
        string $downloadUri,
        string $expectedRoute,
    ): void {
        $crawler = $kernelBrowserForConnnectedUser->request('GET', "$downloadUri");
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN); // HTTP status code = 403
        $this->assertRouteSame("$expectedRoute");
        $this->assertResponseHeaderSame("Content-Type", "text/html; charset=UTF-8");
        $fileContent = trim($kernelBrowserForConnnectedUser->getResponse()->getContent());
        $this->assertEquals("Error 403 - Not allowed to download this certificate", "$fileContent");
    }

    protected function commonAllowedUserSeeBasicNotFoundPageWhenDownloadNotFoundCertificate(
        KernelBrowser $kernelBrowserForConnnectedUser,
        string $downloadUri,
        string $expectedRoute,
    ): void {
//        $kernelBrowserConnectedAdmin1->catchExceptions(false);
//        $this->expectException(NotFoundHttpException::class);
        $crawler = $kernelBrowserForConnnectedUser->request('GET', "$downloadUri");
        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND); // HTTP status code = 404
        $this->assertRouteSame("$expectedRoute");
        $this->assertResponseHeaderSame("Content-Type", "text/html; charset=UTF-8");
        $fileContent = trim($kernelBrowserForConnnectedUser->getResponse()->getContent());
        $this->assertEquals("Error 404 - Not Found Certificate", "$fileContent");
    }

    protected function commonAllowedUserCanDownloadCertificatePublicKey(
        KernelBrowser $kernelBrowserForConnnectedUser,
        string $uri,
        string $expectedRoute,
        string $certSerialNumber,
        string $certPublicKey,
    ): void {
        $crawler = $kernelBrowserForConnnectedUser->request('GET', "$uri");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertRouteSame("$expectedRoute");
        $this->assertResponseHeaderSame("Content-Type", "mime/type");
        $this->assertResponseHeaderSame(
            "Content-Disposition",
            "attachment;filename=\"cert_${certSerialNumber}.pem"
        );
        $fileContent = trim($kernelBrowserForConnnectedUser->getResponse()->getContent());
        $this->assertEquals("$certPublicKey", "$fileContent");
    }


    protected function commonManagerCanSeeHisGeneratedCertificat(
        Crawler $crawler,
        string $expectedCertSerialNumber,
        string $expectedCertUsedBy,
        string $expectedCertValidToDate = '',
        string $expectedCertValidFromDate = '',
        string $expectedCertValidFromTime = '',
    ): void {

        $this->checkAttribute(
            $crawler,
            "#cert_$expectedCertSerialNumber a.cert_download-public-key",
            [
                '_name' => 'a',
                '_text' => 'Download public key (PEM file) of certificate',
                'title' => "Download public key (PEM file) of certificate $expectedCertSerialNumber",
                'href'  => "/cert-manager/certificate/$expectedCertSerialNumber/download"
            ]
        );

        $cssFilter = "#cert_$expectedCertSerialNumber .cert_serialNumber";
        $this->checkAttribute($crawler, "$cssFilter", ['_text' => "$expectedCertSerialNumber"]);

        $cssFilter = "#cert_$expectedCertSerialNumber td.cert_usedBy";
        $this->checkAttribute($crawler, "$cssFilter", ['_text' => "$expectedCertUsedBy"]);

        if (!empty($expectedCertValidToDate)) {
            $cssFilter = "#cert_$expectedCertSerialNumber .cert_validToDate";
            $this->checkAttribute($crawler, "$cssFilter", ['_text' => "$expectedCertValidToDate"]);
        }
        if (!empty($expectedCertValidFromDate)) {
            $cssFilter = "#cert_$expectedCertSerialNumber .cert_validFromDate";
            $this->checkAttribute($crawler, "$cssFilter", ['_text' => "$expectedCertValidFromDate"]);
        }
        if (!empty($expectedCertValidFromTime)) {
            $cssFilter = "#cert_$expectedCertSerialNumber .cert_validFromTime";
            $this->checkAttribute($crawler, "$cssFilter", ['_text' => "$expectedCertValidFromTime"]);
        }
    }
}
