<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional\Manager;

use App\Tests\CommonHelpers\TestHelperEmailTrait;
use App\Tests\Functional\TestHelperTrait;
use App\Tests\Functional\TestHelperDownloadCertificatePublicKeyTrait;
use App\Tests\Functional\TestHelperFormTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

class FunctionalTestManagerTest extends WebTestCase
{
    use TestHelperTrait;
    use TestHelperFormTrait;
    use TestHelperDownloadCertificatePublicKeyTrait;
    use TestHelperEmailTrait;

    private string $emailManager1 = "test_manager_1_tajine@example.org";
    private string $passwordManager1 = "admin-tajine_PaSsWord_IsNotSoSecretChangeIt";
    private string $emailManager2 = "test_manager_2_tajine@example.org";

    private int $minCertPasswordLength;

    protected function setUp(): void
    {
        $this->minCertPasswordLength = (int) $_ENV['WEBAPP_CERTIFICAT_CONFIG_MIN_PASSWORD_LENGTH'];
    }


    public function testManagerCanNotLoginWithEmptyPassword(): void
    {
        $emptyPassword = "";
        $this->commonCheckerUserCanNotLogin(
            mail: $this->emailManager1,
            password:  "$emptyPassword"
        );
        $this->commonCheckkerUserExistInDatabaseWithExpectedRoles(
            userEmail: $this->emailManager1,
            expectedPrincipalUserRole: "ROLE_CERTIFICAT_MANAGER"
        );
    }

    public function testManagerCanNotLoginWithBadPassword(): void
    {
        $this->commonCheckerUserCanNotLogin(
            mail: $this->emailManager1,
            password:  "this is a wrong password"
        );
        $this->commonCheckkerUserExistInDatabaseWithExpectedRoles(
            userEmail: $this->emailManager1,
            expectedPrincipalUserRole: "ROLE_CERTIFICAT_MANAGER"
        );
    }

    public function testManagerCanNotLoginWithBadEmail(): void
    {
        $this->commonCheckerUserCanNotLogin(
            mail: "bad.email." . $this->emailManager1,
            password:  $this->passwordManager1
        );
    }

    /**
     * @todo add chekker "connected admin can see admin links"
     */
    public function testManagerCanLogin(): void
    {
        $this->commonCheckerUserCanLogin(
            mail: $this->emailManager1,
            password: $this->passwordManager1,
            role:'ROLE_CERTIFICAT_MANAGER'
        );
    }

    public function testManagerCanLogout(): void
    {
        $emailManager1 = $this->emailManager1;
        $this->commonCheckerUserCanLogout(mail: $emailManager1);
    }


    /**
     * @group Certificate
     * @group Certificate_create
     * @group Certificate_send-email
     */
    public function testManagerCanCreateNewCertificateAndAEmailAreSendToEndUserWithPkcs12File(): void
    {
        ////////////////////////////////////////////////////////////////////////////

        // Form values
        $certEmail = 'user@example.org';
        $formEmail = "$certEmail";
        $formPassword = $certEmail . "_Password";
        $formCountryName = 'FR';
        $formStateOrProvinceName = 'Herault';
        $formLocalityName = 'Montpellier';
        $formOrganizationName = 'Mairie de Montpellier';
        $formOrganizationalUnitName = '465146154365143651';
        $formCommonName = "$certEmail";

        ////////////////////////////////////////////////////////////////////////////

        # Expected certificate data
        $expectedCertSerialNumber = '0x7E3FD7CBCB43BA25FFA3B4CE63ED7940F76C596E';
        $expectedCertCrl = 'http://pki.example.org/crl.pem';
        $expectedCertValidFrom = '2023-03-13 04:45:00';
        $expectedCertValidTo = '2024-03-12 04:45:00';
        $expectedCertCountryName = "$formCountryName";
        $expectedCertStateOrProvinceName = "$formStateOrProvinceName";
        $expectedCertLocalityName = "$formLocalityName";
        $expectedCertOrgName = "$formOrganizationName";
        $expectedCertOrgUnitName = "$formOrganizationalUnitName";
        $expectedCertCommonName = "$formCommonName";
        $expectedCertSubject = '';
        $expectedCertSubject .= "/C=$expectedCertCountryName";
        $expectedCertSubject .= "/ST=$expectedCertStateOrProvinceName";
        $expectedCertSubject .= "/L=$expectedCertLocalityName";
        $expectedCertSubject .= "/O=$expectedCertOrgName";
        $expectedCertSubject .= "/OU=$expectedCertOrgUnitName";
        $expectedCertSubject .= "/CN=$expectedCertCommonName";
        $expectedCertPublicKey = '-----BEGIN CERTIFICATE-----
MIIEJzCCAw+gAwIBAgIUfj/Xy8tDuiX/o7TOY+15QPdsWW4wDQYJKoZIhvcNAQEL
BQAwZjELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMREwDwYDVQQK
EwhBRFVMTEFDVDEuMCwGA1UEAxMlVkFHUkFOVCBBRFVMTEFDVCBJTlRFUk1FRElB
VEUgQ0EgR0VOMTAeFw0yMzAzMTMwMzQ1MDBaFw0yNDAzMTIwMzQ1MDBaMIGNMQsw
CQYDVQQGEwJGUjEQMA4GA1UECBMHSGVyYXVsdDEUMBIGA1UEBxMLTW9udHBlbGxp
ZXIxHjAcBgNVBAoTFU1haXJpZSBkZSBNb250cGVsbGllcjEbMBkGA1UECxMSNDY1
MTQ2MTU0MzY1MTQzNjUxMRkwFwYDVQQDDBB1c2VyQGV4YW1wbGUub3JnMIIBIjAN
BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyWue+Ip48WO5jroWn5ZXrjFM6JQ0
WiftVx1KtlRWs9HlRQ6NnCwNd/DHinaahvlpj8CP36UCaYw4tdPcIrIBxtAWFYV8
S2T0IJ0CGpJNB5PAzh3AlMzTkSpX2nhZ6TYrH2JV8qG/9OGQKQy6Z42bzIib95Ci
oEKngWromi5jAEn2L1zmCcLYO+3mkTH1Ct4r96CI/VIx2npIZbcesOoaveVaIQmp
isuAYvXywpAeQ3EuSnvjeZP8TNlpTMH0FWSpuoi0Q/BsdW4hB520zhzss5EoYHzC
Z9v3ViPMDq8BLhLrYGGkGwMTGEmvA0UWXkqmRqDbMqb07fKjedXdAzl/4QIDAQAB
o4GkMIGhMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0O
BBYEFPquORoMf9Qg9aErq75mrjO7p62xMB8GA1UdIwQYMBaAFL+UFyOYqKqxUJdG
ylKkHb1w3ppFMAsGA1UdEQQEMAKCADAvBgNVHR8EKDAmMCSgIqAghh5odHRwOi8v
cGtpLmV4YW1wbGUub3JnL2NybC5wZW0wDQYJKoZIhvcNAQELBQADggEBAII1vYnU
GQnbBQzHh9R2uHYI833df/zHVfPgS+AC86a81WR/3YiZkpewV6sZS7RGvpZ5HQgG
o4eUriJEm0yAmqhZpn5akplVgz0LdT5h4Hr1JFj0VPhdlPKRS7IncOXDIxU4xlCk
Yv3YTvzdAECzjgX98mOjtbE5/TyEYl02HmNYMrQvZM4fBlTS7h93urwGcTp19rVz
571LQZWC3FLVoQU4mwtVcMRxVcNUHDC8df1Na0AZzQGEI3uzti7d0ruW4pSnvpBZ
bf+L+OBzQ3Y4zXkR8J1Q4nXXZ4D6DBqYXNnZw9jnTLGsH6YhOtOB020Vp6pyMuN5
N3lor8nW4jt/z24=
-----END CERTIFICATE-----';

        # Expected email data
        $appShortName = array_key_exists('WEBAPP_SHORTNAME', $_ENV) ? $_ENV['WEBAPP_SHORTNAME'] : false;
        $appEmailFrom = array_key_exists('WEBAPP_EMAIL_FROM', $_ENV) ? $_ENV['WEBAPP_EMAIL_FROM'] : false;
        $expectedMailSubject = "$appShortName - Your new X509 certificate in PKCS12 format ($expectedCertSerialNumber)";
        $expectedEndUserEmail = "$certEmail";
        $expectedEmailFrom = "$appEmailFrom";
        $expectedEmailTo = "$expectedEndUserEmail";

        ////////////////////////////////////////////////////////////////////////////

        // Display FORM
        $kernelBrowserForConnnectedManager = $this->loadEmptyCertificateCreateForm(enableFormAssertions: true);

        // Send FORM data
        // - documentation :  https://symfony.com/doc/current/testing.html#submitting-forms
        $crawler = $kernelBrowserForConnnectedManager->submitForm('addCertificate-form-button-submit', [
            'add_certificate[email]' => "$formEmail",
            'add_certificate[certificatePassword]' => "$formPassword",
            'add_certificate[countryName]' => "$formCountryName",
            'add_certificate[stateOrProvinceName]' => "$formStateOrProvinceName",
            'add_certificate[localityName]' => "$formLocalityName",
            'add_certificate[organizationName]' => "$formOrganizationName",
            'add_certificate[organizationalUnitName]' => "$formOrganizationalUnitName",
            'add_certificate[commonName]' => "$formCommonName",
        ]);
        $this->assertRouteSame("app_cert-manager_certificate_add");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200

        ////////////////////////////////////////////////////////////////////////////

        // HTML page content after FORM submission ---> Alert message
        $this->assertSelectorTextSame('.alert-success h2', 'SUCCESS');
        $this->assertSelectorTextContains('.alert-success', 'PKCS12 certificate has been generated');
        $this->assertSelectorTextContains('.alert-success', "sent to $expectedEndUserEmail with PKCS12 file");

        // HTML page content after FORM submission  ---> Technical informations
        $this->assertSelectorTextSame('#cert_serial-number', (string)$expectedCertSerialNumber);
        $this->assertSelectorTextSame('#cert_validFrom', (string)$expectedCertValidFrom);
        $this->assertSelectorTextSame('#cert_validTo', (string)$expectedCertValidTo);
        $this->assertSelectorTextContains('#cert_Crl', "$expectedCertCrl");

        // HTML page content after FORM submission  ---> Subject
        $this->assertSelectorTextSame('#cert_subject', (string)$expectedCertSubject);
        $this->assertSelectorTextSame('#cert_subject_countryName', (string)$expectedCertCountryName);
        $this->assertSelectorTextSame('#cert_subject_stateOrProvinceName', (string)$expectedCertStateOrProvinceName);
        $this->assertSelectorTextSame('#cert_subject_localityName', (string)$expectedCertLocalityName);
        $this->assertSelectorTextSame('#cert_subject_organizationName', (string)$expectedCertOrgName);
        $this->assertSelectorTextSame('#cert_subject_organizationalUnitName', (string)$expectedCertOrgUnitName);
        $this->assertSelectorTextSame('#cert_subject_commonName', (string)$expectedCertCommonName);

        // HTML page content after FORM submission  ---> Download PEM file link
        $downloadUrl = "/cert-manager/certificate/$expectedCertSerialNumber/download";
        $dowloadLinkAttributesExpected = [
            '_text' => "download public key (PEM file)",
            'title' => "Download public key (PEM file) of certificate $expectedCertSerialNumber",
            'href' => "/cert-manager/certificate/$expectedCertSerialNumber/download"
        ];
        $this->checkAttribute($crawler, "#cert_download-public-key", $dowloadLinkAttributesExpected);

        ////////////////////////////////////////////////////////////////////

        # EMAIL sent to end-user
        $this->assertEmailIsQueued($this->getMailerEvent(0));
        $this->assertEmailCount(1); // use assertQueuedEmailCount() when using Messenger
        $email = $this->getMailerMessage();

        # EMAIL sent to end-user ---> check email HEADERS
        $this->assertEmailHeaderSame($email, "Subject", "$expectedMailSubject");
        $this->checkCommonTechnicalEmailHeaders(email: $email);
        $this->checkCommonCorrespondentsEmailHeaders(
            email: $email,
            expectedEmailTo: "$expectedEmailTo",
            expectedEmailFrom: "$expectedEmailFrom",
        );

        # EMAIL sent to end-user ---> check email Attachment
        $this->commonCheckPkcs12CertificatesIsInEmailAsAttachment(
            email: $email,
            certificateSerialNumber: $expectedCertSerialNumber,
            checkPkcs12FileContent: false,
        );

        # EMAIL sent to end-user ---> check email content (HTML version)
        $this->assertEmailHtmlBodyContains($email, 'Please find your new X509 certificate in PKCS12 format.');
        $this->assertEmailHtmlBodyContains($email, 'It is valid until');
        $this->assertEmailHtmlBodyContains($email, '12/03/2024');
        $this->assertEmailHtmlBodyContains($email, 'It is valid until <strong>12/03/2024</strong>');
        $this->assertEmailHtmlBodyContains($email, "Certificate serial number: $expectedCertSerialNumber");

        # EMAIL sent to end-user ---> check email content (TEXT version)
        $this->assertEmailTextBodyContains($email, 'Please find your new X509 certificate in PKCS12 format.');
        $this->assertEmailTextBodyContains($email, 'It is valid until');
        $this->assertEmailTextBodyContains($email, '12/03/2024');
        $this->assertEmailTextBodyContains($email, 'It is valid until 12/03/2024');
        $this->assertEmailTextBodyContains($email, "Certificate serial number: $expectedCertSerialNumber");

        // Manager can download certificate public key (PEM file)
        // ---> the test is valid when run alone, but fails in the test suite
        $this->commonAllowedUserCanDownloadCertificatePublicKey(
            kernelBrowserForConnnectedUser: $kernelBrowserForConnnectedManager,
            uri: $downloadUrl,
            expectedRoute: 'app_cert-manager_certificate_download',
            certSerialNumber: $expectedCertSerialNumber,
            certPublicKey: $expectedCertPublicKey
        );

        // The manager can see this certificate in the list of certificates he has created.
        // ----> the test is valid when run alone, but fails in the test suite
//        $crawler = $kernelBrowserForConnnectedManager->request('GET', '/cert-manager/certificates');
//        $this->assertRouteSame("app_cert-manager_certificates");
//        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
//        $this->commonManagerCanSeeHisGeneratedCertificat(
//            crawler: $crawler,
//            expectedCertSerialNumber: "$expectedCertSerialNumber",
//            expectedCertUsedBy: "$certEmail"
//        );
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * @group Form
     * @group Role_Manager
     * @group Create_Certificate_Form
     * @group Create_Certificate_Form_With_Wrong_Data
     */
    public function testManagerCanNotCreateCertificateWithTooSmallPassword(): void
    {
        $kernelBrowserForConnnectedManager = $this->loadEmptyCertificateCreateForm(enableFormAssertions: false);
        $validFormData = $this->getValidDataForCertificateCreateForm();

        $formName = 'add_certificate';
        $fieldName = 'certificatePassword';
        $minPasswordLength = (int) $_ENV['WEBAPP_CERTIFICAT_CONFIG_MIN_PASSWORD_LENGTH'];
        $tooSmallPasswords = [
            '1', // very small password size (1 characters)
            '123', // very small password size (3 characters)
            $this->generateRandomString($minPasswordLength - 2), // password is 2 character short of required size
            $this->generateRandomString($minPasswordLength - 1), // password is 1 character short of required size
        ];
        foreach ($tooSmallPasswords as $tooSmallPassword) {
            $formDataWithEmptyField = $validFormData;
            $formDataWithEmptyField["${formName}[${fieldName}]"] = $tooSmallPassword;
            $crawler = $this->sendCertificateCreationFormWithWrongData(
                $formDataWithEmptyField,
                $kernelBrowserForConnnectedManager
            );
            $this->commonCheckerIfFormFieldIsInvalid(
                crawler: $crawler,
                cssFilterOfInvadFormField: "#${formName}_${fieldName}",
                cssFilterOfInvalidFeedback: '.invalid-feedback',
                invalidFeedback: 'Password size is too small.',
            );
            $this->assertNull($this->getMailerEvent(0)); // check no email was send to end user
        }
    }

    /**
     * @group Form
     * @group Role_Manager
     * @group Create_Certificate_Form
     * @group Create_Certificate_Form_With_Wrong_Data
     */
    public function testManagerCanNotCreateCertificateWithInvalidEmailFormat(): void
    {
        $kernelBrowserForConnnectedManager = $this->loadEmptyCertificateCreateForm(enableFormAssertions: false);
        $validFormData = $this->getValidDataForCertificateCreateForm();

        $formName = 'add_certificate';
        $fieldName = 'email';
        foreach ($this->getListOfEmailsInInvalidFormat() as $notValidEmail) {
            $formDataWithEmptyField = $validFormData;
            $formDataWithEmptyField["${formName}[${fieldName}]"] = $notValidEmail;
            $crawler = $this->sendCertificateCreationFormWithWrongData(
                $formDataWithEmptyField,
                $kernelBrowserForConnnectedManager
            );
            $this->commonCheckerIfFormFieldIsInvalid(
                crawler: $crawler,
                cssFilterOfInvadFormField: "#${formName}_${fieldName}",
                cssFilterOfInvalidFeedback: '.invalid-feedback',
                invalidFeedback: 'Please enter a valid email address.',
            );
            $this->assertNull($this->getMailerEvent(0)); // check no email was send to end user
        }
    }



    /**
     * @group Form
     * @group Role_Manager
     * @group Create_Certificate_Form
     * @group Create_Certificate_Form_With_Wrong_Data
     */
    public function testManagerCanNotCreateCertificateWithoutEmailField(): void
    {
        $kernelBrowserForConnnectedManager = $this->loadEmptyCertificateCreateForm(enableFormAssertions: false);
        $this->commonCheckerWhenFormIsSentWithMissingOrEmptyField(
            kernelBrowser: $kernelBrowserForConnnectedManager,
            validFormData: $this->getValidDataForCertificateCreateForm(),
            formName: 'add_certificate',
            fieldName: 'email',
            methodNameToSendFormWithWrongData: 'sendCertificateCreationFormWithWrongData'
        );
        $this->assertNull($this->getMailerEvent(0)); // check no email was send to end user
    }



    /**
     * @group Form
     * @group Role_Manager
     * @group Create_Certificate_Form
     * @group Create_Certificate_Form_With_Wrong_Data
     */
    public function testManagerCanNotCreateCertificateWithoutPasswordField(): void
    {
        $kernelBrowserForConnnectedManager = $this->loadEmptyCertificateCreateForm(enableFormAssertions: false);
        $this->commonCheckerWhenFormIsSentWithMissingOrEmptyField(
            kernelBrowser: $kernelBrowserForConnnectedManager,
            validFormData: $this->getValidDataForCertificateCreateForm(),
            formName: 'add_certificate',
            fieldName: 'certificatePassword',
            methodNameToSendFormWithWrongData: 'sendCertificateCreationFormWithWrongData'
        );
        $this->assertNull($this->getMailerEvent(0)); // check no email was send to end user
    }

    /**
     * @group Form
     * @group Role_Manager
     * @group Create_Certificate_Form
     * @group Create_Certificate_Form_With_Wrong_Data
     */
    public function testManagerCanNotCreateCertificateWithoutCountryField(): void
    {
//        $kernelBrowserForConnnectedManager = $this->loadEmptyCertificateCreateForm(enableFormAssertions: false);
//
//        // Empty field
//        $this->checkWhenFormIsSentWithMissingOrEmptyField(
//            kernelBrowser: $kernelBrowserForConnnectedManager,
//            validFormData: $this->getValidDataForCertificateCreateForm(),
//            formName: 'add_certificate',
//            fieldName: 'countryName',
//            checkMissingField: false,
//            expectedFieldAttributes: ['class' => 'form-select is-invalid']
//        );
//        ////////////////////////////////////////////////////////////////////
//        // Real Browser = 'This field is mandatory.'
//        // vs   Unit test = FAIL
//        //                  InvalidArgumentException: Input "add_certificate[countryName]" cannot take ""
//        //                  as a value (possible values: "FR", "-------------------", "AF",
//        ////////////////////////////////////////////////////////////////////
//        $this->assertNull($this->getMailerEvent(0)); // check no email was send to end user
//
//        // Missing field
//        $this->checkWhenFormIsSentWithMissingOrEmptyField(
//            kernelBrowser: $kernelBrowserForConnnectedManager,
//            validFormData: $this->getValidDataForCertificateCreateForm(),
//            formName: 'add_certificate',
//            fieldName: 'countryName',
//            checkEmptyField: false,
//            expectedFieldAttributes: ['class' => 'form-select is-invalid']
//        );
//        ////////////////////////////////////////////////////////////////////
//        // Real Browser = 'This field is mandatory.'
//        // vs   Unit test  = FAIL
//        //                   Failed asserting that Symfony\Component\DomCrawler\Crawler Object
//        ////////////////////////////////////////////////////////////////////
//        $this->assertNull($this->getMailerEvent(0)); // check no email was send to end user
    }

    /**
     * @group Form
     * @group Role_Manager
     * @group Create_Certificate_Form
     * @group Create_Certificate_Form_With_Wrong_Data
     */
    public function testManagerCanNotCreateCertificateWithoutProvinceField(): void
    {
        $kernelBrowserForConnnectedManager = $this->loadEmptyCertificateCreateForm(enableFormAssertions: false);
        $this->commonCheckerWhenFormIsSentWithMissingOrEmptyField(
            kernelBrowser: $kernelBrowserForConnnectedManager,
            validFormData: $this->getValidDataForCertificateCreateForm(),
            formName: 'add_certificate',
            fieldName: 'stateOrProvinceName',
            methodNameToSendFormWithWrongData: 'sendCertificateCreationFormWithWrongData'
        );
        $this->assertNull($this->getMailerEvent(0)); // check no email was send to end user
    }

    /**
     * @group Form
     * @group Role_Manager
     * @group Create_Certificate_Form
     * @group Create_Certificate_Form_With_Wrong_Data
     */
    public function testManagerCanNotCreateCertificateWithoutLocalityField(): void
    {
        $kernelBrowserForConnnectedManager = $this->loadEmptyCertificateCreateForm(enableFormAssertions: false);
        $this->commonCheckerWhenFormIsSentWithMissingOrEmptyField(
            kernelBrowser: $kernelBrowserForConnnectedManager,
            validFormData: $this->getValidDataForCertificateCreateForm(),
            formName: 'add_certificate',
            fieldName: 'localityName',
            methodNameToSendFormWithWrongData: 'sendCertificateCreationFormWithWrongData'
        );
        $this->assertNull($this->getMailerEvent(0)); // check no email was send to end user
    }

    /**
     * @group Form
     * @group Role_Manager
     * @group Create_Certificate_Form
     * @group Create_Certificate_Form_With_Wrong_Data
     */
    public function testManagerCanNotCreateCertificateWithoutOrganizationField(): void
    {
        $kernelBrowserForConnnectedManager = $this->loadEmptyCertificateCreateForm(enableFormAssertions: false);
        $this->commonCheckerWhenFormIsSentWithMissingOrEmptyField(
            kernelBrowser: $kernelBrowserForConnnectedManager,
            validFormData: $this->getValidDataForCertificateCreateForm(),
            formName: 'add_certificate',
            fieldName: 'organizationName',
            methodNameToSendFormWithWrongData: 'sendCertificateCreationFormWithWrongData'
        );
        $this->assertNull($this->getMailerEvent(0)); // check no email was send to end user
    }

    /**
     * @group Form
     * @group Role_Manager
     * @group Create_Certificate_Form
     * @group Create_Certificate_Form_With_Wrong_Data
     */
    public function testManagerCanNotCreateCertificateWithoutUnitNameField(): void
    {
        $kernelBrowserForConnnectedManager = $this->loadEmptyCertificateCreateForm(enableFormAssertions: false);
        $this->commonCheckerWhenFormIsSentWithMissingOrEmptyField(
            kernelBrowser: $kernelBrowserForConnnectedManager,
            validFormData: $this->getValidDataForCertificateCreateForm(),
            formName: 'add_certificate',
            fieldName: 'organizationalUnitName',
            methodNameToSendFormWithWrongData: 'sendCertificateCreationFormWithWrongData'
        );
        $this->assertNull($this->getMailerEvent(0)); // check no email was send to end user
    }


    /**
     * @group Form
     * @group Role_Manager
     * @group Create_Certificate_Form
     * @group Create_Certificate_Form_With_Wrong_Data
     */
    public function testManagerCanNotCreateCertificateWithoutCommonNameField(): void
    {
        $kernelBrowserForConnnectedManager = $this->loadEmptyCertificateCreateForm(enableFormAssertions: false);
        $this->commonCheckerWhenFormIsSentWithMissingOrEmptyField(
            kernelBrowser: $kernelBrowserForConnnectedManager,
            validFormData: $this->getValidDataForCertificateCreateForm(),
            formName: 'add_certificate',
            fieldName: 'commonName',
            methodNameToSendFormWithWrongData: 'sendCertificateCreationFormWithWrongData'
        );
        $this->assertNull($this->getMailerEvent(0)); // check no email was send to end user
    }



    private function getValidDataForCertificateCreateForm(): array
    {
        $certEmail = 'user@example.org';
        $formEmail = "$certEmail";
        $formPassword = $certEmail . "_Password";
        $formCountryName = 'FR';
        $formStateOrProvinceName = 'Herault';
        $formLocalityName = 'Montpellier';
        $formOrganizationName = 'Mairie de Montpellier';
        $formOrganizationalUnitName = '465146154365143651';
        $formCommonName = "$certEmail";
        return [
            'add_certificate[email]' => "$formEmail",
            'add_certificate[certificatePassword]' => "$formPassword",
            'add_certificate[countryName]' => "$formCountryName",
            'add_certificate[stateOrProvinceName]' => "$formStateOrProvinceName",
            'add_certificate[localityName]' => "$formLocalityName",
            'add_certificate[organizationName]' => "$formOrganizationName",
            'add_certificate[organizationalUnitName]' => "$formOrganizationalUnitName",
            'add_certificate[commonName]' => "$formCommonName",
        ];
    }



    private function loadEmptyCertificateCreateForm(
        bool $enableFormAssertions = false,
    ): KernelBrowser {
        $emailManager1 = $this->emailManager1;
        $kernelBrowserForConnnectedManager =  $this->getKernelBrowserWithConnectedUser($emailManager1);
        $crawler = $kernelBrowserForConnnectedManager->request('GET', '/cert-manager/certificate/add');
        $this->assertRouteSame("app_cert-manager_certificate_add");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200

        if ($enableFormAssertions === true) {
            $minPasswordLength = $this->minCertPasswordLength;
            $htmlFormName = "add_certificate";
            $this->checkAttribute(crawler: $crawler, cssFilter: "form[name=$htmlFormName]", attributesExpected: []);
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'email',
                fieldAttributes: ['type' => 'email', 'required' => 'required',],
                labelText: 'Email',
                helpText: "User email who will receive PKCS12 file"
            );
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'certificatePassword',
                fieldAttributes: [
                    'type' => 'password',
                    'required' => 'required',
                    'autocomplete' => 'new-password',
                    'minlength' => "$minPasswordLength",
                    'maxlength' => "4096",
                ],
                labelText: 'Certificate password',
                helpText: "Enter a password to protect PKCS12 certificate. "
                . "It should be at least $minPasswordLength characters."
            );
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'countryName',
                //              fieldAttributes: ['required' => 'required',],
                fieldAttributes: [],
                labelText: 'Country name',
                fieldHtmlTag: 'select'
            );
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'stateOrProvinceName',
                fieldAttributes: ['type' => 'text', 'required' => 'required',],
                labelText: 'State or province name',
            );
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'localityName',
                fieldAttributes: ['type' => 'text', 'required' => 'required',],
                labelText: 'Locality name',
            );
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'organizationName',
                fieldAttributes: ['type' => 'text', 'required' => 'required',],
                labelText: 'Organization name',
            );
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'organizationalUnitName',
                fieldAttributes: ['type' => 'text', 'required' => 'required',],
                labelText: 'Organizational unit name',
            );
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'commonName',
                fieldAttributes: ['type' => 'text', 'required' => 'required',],
                labelText: 'Common name',
            );
            $this->checkAttribute(
                $crawler,
                "button#addCertificate-form-button-submit",
                ['_text' => 'Create Certificate']
            );
        }
        return $kernelBrowserForConnnectedManager;
    }

    private function sendCertificateCreationFormWithWrongData(
        array $formDataWithWrongData,
        KernelBrowser $kernelBrowserForConnnectedManager
    ): Crawler {
        $crawler = $kernelBrowserForConnnectedManager->submitForm(
            'addCertificate-form-button-submit',
            $formDataWithWrongData
        );
        $this->assertRouteSame("app_cert-manager_certificate_add");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200

        $this->assertSelectorTextSame('h1', 'Create a new certificate');
        return $crawler;
    }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public function testManagerCanDownloadCertificatePublicKeyHeCreated(): void
    {
        $emailManager2 = $this->emailManager2;
        $kernelBrowserForConnnectedManager =  $this->getKernelBrowserWithConnectedUser($emailManager2);

        # Expected certificate data
        $expectedCertSerialNumber = '0x468B91C7D3F967FBD62F969A251CF740E1F066DA';
        $expectedCertPublicKey = '-----BEGIN CERTIFICATE-----
MIIEqDCCA5CgAwIBAgIURouRx9P5Z/vWL5aaJRz3QOHwZtowDQYJKoZIhvcNAQEL
BQAweTELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMSUwIwYDVQQK
ExxWQUdSQU5UIEVYQU1QTEUgT1JHQU5JWkFUSU9OMS0wKwYDVQQDEyRWQUdSQU5U
IEVYQU1QTEUgSU5URVJNRURJQVRFIENBIEdFTjEwHhcNMjMwNjE5MTYxMTAwWhcN
MjQwNjE4MTYxMTAwWjCB2jELMAkGA1UEBhMCRlIxHTAbBgNVBAgTFFBZUkVORUVT
LUFUTEFOVElRVUVTMRwwGgYDVQQHExNDQVVESUVTLURFLUNPTkZMRU5UMSYwJAYD
VQQKEx1NQUlSSUUtREUtQ0FVRElFUy1ERS1DT05GTEVOVDE9MDsGA1UECxM0U0VS
VklDRS1ERVMtU1BPUlMtREUtTEEtVklMTEUtREUtQ0FVRElFUy1ERS1DT05GTEVO
VDEnMCUGA1UEAwweVEVTVF9VU0VSXzFfVEFKSU5FQEVYQU1QTEUuT1JHMIIBIjAN
BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqNp6FK6KZbSPNjhvis9+bA3z73aH
MiISjB+aC3m3DEQo4C429Tg8s5YdKbdmY2TYYhS6S/H8tlrzQN5Tnwr98FhG+ZFh
wLkhYu9fBH668NhQGJCxucwDDCr5O84lNVonHAO6tG2govS7seBoCqU/F+T8yggN
Bk0QmGthMwabDYLUHBEflBq6hbLFZF2z4Yr3EWa/GEriZPKfH5PDlOOGJN6/a/UQ
gKNyl0ZUPyAxDDdsw0XobZNrHSjhSu53ItDrlbHwAAz4ORs8kkJ13Ya1X0ukDzcw
n+YVTkzEITQMunOtoQTvY/fnSky+WAosuri+/2AcaVNc2NejFF+P+bZLaQIDAQAB
o4HFMIHCMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0O
BBYEFN5bWACRzSNTtWtXLgq4jWNUrkyUMB8GA1UdIwQYMBaAFHErrb3INhdd387l
t+oHFBVkUjv4MAsGA1UdEQQEMAKCADBQBgNVHR8ESTBHMEWgQ6BBhj9odHRwOi8v
Y3JsLmV4YW1wbGUub3JnL2NybC1WQUdSQU5URVhBTVBMRUlOVEVSTUVESUFURUNB
R0VOMS5jcmwwDQYJKoZIhvcNAQELBQADggEBAJurjjB99x81yVq1jMhLuK1KV9qH
8KslQFBSGSYo6ACeiSqeNhK3hgQKt5jD4GHVHgctZ6TlShVkvQvnLdL6msFHqgb2
MDmJOh0LKTb9Sv4Cyvuibpey0EIVqA/LNl4PR7/dwlrpnEawuG4ywbZV4TCIKcuD
6WlVdPfDSr4+j9+4Zk99UtaIFfhCEnFCZyv8cbQ4dKeL4wbzWE/kNV9QRm8o1Z2D
udHDhXunee/i3hxzrnvCzsTe+J8xAHgKNCwMnwDNcl0HE+KWmg522tsGBxI+OYSc
F4JfLMqxlpcp7p/8AZNHZeevRAO23l8p/N7QMj1bTp+AOBFsSaKLoR24Q1s=
-----END CERTIFICATE-----';

        $downloadUrl = "/cert-manager/certificate/$expectedCertSerialNumber/download";
        $this->commonAllowedUserCanDownloadCertificatePublicKey(
            kernelBrowserForConnnectedUser: $kernelBrowserForConnnectedManager,
            uri: $downloadUrl,
            expectedRoute: 'app_cert-manager_certificate_download',
            certSerialNumber: $expectedCertSerialNumber,
            certPublicKey: $expectedCertPublicKey
        );
    }


    public function testManagerCanNotDownloadCertificatePublicKeyCreatedByOtherManager(): void
    {
        $emailManager2 = $this->emailManager2;
        $kernelBrowserForConnnectedManager =  $this->getKernelBrowserWithConnectedUser($emailManager2);
        $certSerialNumber = '0x33BDE2E6F0138FA1E24B629B0E3C5D44F4E4386C'; // created by  $this->emailManager1
        $this->commonUserNotAllowedUserToDownloadCertificatePublicKey(
            kernelBrowserForConnnectedUser: $kernelBrowserForConnnectedManager,
            downloadUri: "/cert-manager/certificate/$certSerialNumber/download",
            expectedRoute: 'app_cert-manager_certificate_download'
        );
    }

    public function testManagerSeeBasicNotFoundPageWhenDownloadNotFoundCertificate(): void
    {
        $emailManager2 = $this->emailManager2;
        $kernelBrowserForConnnectedManager =  $this->getKernelBrowserWithConnectedUser($emailManager2);
        $certSerialNumber = '0x4999999999999999999999999999999999999999'; // Fake certificate
        $this->commonAllowedUserSeeBasicNotFoundPageWhenDownloadNotFoundCertificate(
            kernelBrowserForConnnectedUser: $kernelBrowserForConnnectedManager,
            downloadUri: "/cert-manager/certificate/$certSerialNumber/download",
            expectedRoute: 'app_cert-manager_certificate_download'
        );
    }


    public function testManagerCanSeeHisGeneratedCertificats(): void
    {
        $emailManager2 = $this->emailManager2;
        $kernelBrowserForConnnectedManager =  $this->getKernelBrowserWithConnectedUser($emailManager2);

        $crawler = $kernelBrowserForConnnectedManager->request('GET', '/cert-manager/certificates');
        $this->assertRouteSame("app_cert-manager_certificates");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200

        // "Manager 2" can see his generated certificats
        $this->commonManagerCanSeeHisGeneratedCertificat(
            crawler: $crawler,
            expectedCertSerialNumber: "0x468B91C7D3F967FBD62F969A251CF740E1F066DA",
            expectedCertUsedBy: "test_user_1_tajine@example.org",
            expectedCertValidToDate: '2024-06-18',
            expectedCertValidFromDate: '2023-06-19',
            expectedCertValidFromTime: '18:11'
        );
        $this->commonManagerCanSeeHisGeneratedCertificat(
            crawler: $crawler,
            expectedCertSerialNumber: "0x58F3C5938D6B1863BC56525264FBDD2209C60BCF",
            expectedCertUsedBy: "test_user_2_tajine@example.org",
            expectedCertValidToDate: '2024-06-18',
            expectedCertValidFromDate: '2023-06-19',
            expectedCertValidFromTime: '19:19'
        );

        // "Manager 2" can not certificates created by "Manager 1"
        $notExpectedCertSerialNumber = "0x33BDE2E6F0138FA1E24B629B0E3C5D44F4E4386C";
        $attributes = $crawler
            ->filter("#cert_$notExpectedCertSerialNumber")
            ->extract(['_name', '_text']);
        $this->assertEquals(0, count($attributes));
    }

    public function testManagerCanNotSeeCertificatsCreatedByOtherManager(): void
    {
        $kernelBrowserForConnnectedManager =  $this->getKernelBrowserWithConnectedUser($this->emailManager1);

        $crawler = $kernelBrowserForConnnectedManager->request('GET', '/cert-manager/certificates');
        $this->assertRouteSame("app_cert-manager_certificates");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200

        // "Manager 1" can see his generated certificats
        $this->commonManagerCanSeeHisGeneratedCertificat(
            crawler: $crawler,
            expectedCertSerialNumber: "0x33BDE2E6F0138FA1E24B629B0E3C5D44F4E4386C",
            expectedCertUsedBy: "test_user_3_tajine@example.org",
        );

        // "Manager 1" can not certificates created by "Manager 2"
        $notExpectedCertSerialNumber = "0x468B91C7D3F967FBD62F969A251CF740E1F066DA";
        $attributes = $crawler
            ->filter("#cert_$notExpectedCertSerialNumber")
            ->extract(['_name', '_text']);
        $this->assertEquals(0, count($attributes));

        $notExpectedCertSerialNumber = "0x58F3C5938D6B1863BC56525264FBDD2209C60BCF";
        $attributes = $crawler
            ->filter("#cert_$notExpectedCertSerialNumber")
            ->extract(['_name', '_text']);
        $this->assertEquals(0, count($attributes));
    }
}
