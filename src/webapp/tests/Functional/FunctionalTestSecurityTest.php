<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Repository\UserRepository;
use App\Tests\Functional\TestHelperTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class FunctionalTestSecurityTest extends WebTestCase
{
    use TestHelperTrait;

    /**
     * @todo replace string type by enum for $aclRole
     */
    private function getUrls(string $aclRole = 'PUBLIC'): array
    {
        $routes = [
            'PUBLIC' => [
                'app_home                                ' => ['GET|HEAD', '/                                       '],
                'app_account_login                       ' => ['GET|POST', '/account/login                          '],
                'app_account_forgot_password_request     ' => ['GET|POST', '/account/forgot-password                '],
//              'app_account_forgot_password_new-password' => ['GET|POST', '/account/forgot-password/t/{{tokenKey}} '],
//              'app_account_logout                      ' => ['GET     ', '/account/logout                         '],
//              'app_health_check                        ' => ['GET|HEAD', '/health-check           ', [200, 503]],
//              'app_public_certificat-authority         ' => ['GET|HEAD', '/certification-authority', [200, 503]],
            ],
            'ROLE_USER' => [
//              'app_account_logout              ' => ['GET     ', '/account/logout               '],
                'app_user_profile                 ' => ['GET     ', '/user/profile                  '],
                'app_user_profile_change_password ' => ['GET     ', '/user/profile/password         '],
            ],
            'ROLE_CERTIFICAT_MANAGER' => [
//              'app_cert-manager_certificate_download' => ['GET', '/cert-manager/certificate/{serialNumber}/download'],
                'app_cert-manager_certificate_add     ' => ['GET', '/cert-manager/certificate/add             '],
                'app_cert-manager_certificates        ' => ['GET', '/cert-manager/certificates                '],
//              'app_cert-manager                     ' => ['GET', '/cert-manager                             '],
            ],
            'ROLE_ADMIN' => [
//              'app_admin                          ' => ['GET    ', '/admin                                      '],
                'app_admin_config                   ' => ['GET     ', '/admin/configuration                       '],
                'app_admin_config_phpinfo           ' => ['GET     ', '/admin/configuration/phpinfo               '],
                'app_admin_config_send_testing_email' => ['GET     ', '/admin/configuration/test-mail             '],
                'app_admin_certificates             ' => ['GET     ', '/admin/certificates                        '],
//              'app_admin_certificates_display     ' => ['GET     ', '/admin/certificates/{serialNumber}         '],
//              'app_admin_certificates_download    ' => ['GET     ', '/admin/certificates/{serialNumber}/download'],
                'app_admin_users_list               ' => ['GET|POST', '/admin/users                               '],
                'app_admin_users_add_manager        ' => ['GET|POST', '/admin/users/add-manager                   '],
                'app_admin_users_add_admin          ' => ['GET|POST', '/admin/users/add-administrator             '],
//              'app_admin_users_edit               ' => ['GET|POST', '/admin/users/1/edit                        '],
//              'app_admin_users_delete             ' => ['POST    ', '/admin/users/{id}/delete                   '],
//              'app_admin_users_display            ' => ['GET     ', '/admin/users/1                             '],
            ],
        ];

        // @TODO check $aclRole allowed value
        return match ($aclRole) {
            'PUBLIC' => $routes['PUBLIC'],
            'ROLE_USER' => $routes['ROLE_USER'],
            'ROLE_CERTIFICAT_MANAGER' => $routes['ROLE_CERTIFICAT_MANAGER'],
            'ROLE_ADMIN' => $routes['ROLE_ADMIN'],
        };
    }

    /////////////// Anonymous /////////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /**
     * @group allow_mutation_testing_by_infection
     */
    public function testAnonymousCanBrowsePublicUrls(): void
    {
        $client = static::createClient();
        $urls = $this->getUrls('PUBLIC');
/*      Array ([app_account_login]  => Array  (  [0] => GET|POST
                                              [1] => /account/login
       Array ([app_health_check] => Array  (  [0] => GET|HEAD
                                              [1] => /health-check ),
                                              [2] => [200, 503]   ---> expected code HTTP   */
        foreach ($urls as $routeKey => $routeData) {
            $route = trim($routeKey);
            $url = trim($routeData[1]);
//          echo "---> $url\n";
            $crawler = $client->request('GET', $url);
            $this->assertRouteSame("$route");
            $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
            $this->assertSelectorNotExists('.connected_user_email');
            $this->commonCheckerHasExpectedSecurityHttpHeaders();
        }
    }
    /**
     * Anonymous are deny to browse ROLE_ADMIN | ROLE_CERTIFICAT_MANAGER | ROLE_USER urls
     * and being redirected to LOGIN page.
     */
    /**
     * @group allow_mutation_testing_by_infection
     */
    public function testAnonymousIsDenyToBrowseAdministratorUrlsAndBeingRedirectedToLoginPage(): void
    {
        $this->commonIsRedirectingToLoginPage('ROLE_ADMIN');
    }
    /**
     * @group allow_mutation_testing_by_infection
     */
    public function testAnonymousIsDenyToBrowseManagerUrlsAndBeingRedirectedToLoginPage(): void
    {
        $this->commonIsRedirectingToLoginPage('ROLE_CERTIFICAT_MANAGER');
    }
    /**
     * @group allow_mutation_testing_by_infection
     */
    public function testAnonymousIsDenyToBrowseConnectedUserUrlsAndBeingRedirectedToLoginPage(): void
    {
        $this->commonIsRedirectingToLoginPage('ROLE_USER');
    }
    public function commonIsRedirectingToLoginPage($aclRole = 'ROLE_ADMIN'): void
    {
        $client = static::createClient();
        $urls = $this->getUrls("$aclRole");
        foreach ($urls as $routeKey => $routeData) {
            $route = trim($routeKey);
            $url = trim($routeData[1]);
//          echo "---> $url\n";
            $crawler = $client->request('GET', $url);
            $this->assertRouteSame("$route");
            $this->assertResponseStatusCodeSame(Response::HTTP_FOUND); // HTTP status code = 302
            $this->assertResponseHasHeader("Location");
            $this->assertResponseHeaderSame("Location", 'http://localhost/account/login');
            /*
             ├ HTTP/1.1 302 Found
               ├ Cache-Control:   max-age=0, must-revalidate, private
               ├ Content-Type:    text/html; charset=UTF-8
               ├ Date:            Thu, 16 Mar 2023 11:13:22 GMT
               ├ Expires:         Thu, 16 Mar 2023 11:13:22 GMT
               ├ Location:        http://localhost/account/login
               ├ Referrer-Policy: no-referrer, strict-origin-when-cross-origin
               ├ Set-Cookie:      MOCKSESSID=1d1a161437; path=/; httponly; samesite=strict
               ├ X-Robots-Tag:    noindex
                         */
        }
    }


    /////////////// Connected USER ////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    public function testConnectedUserCanBrowseHisUrls(): void
    {
        $urls = $this->getUrls('ROLE_USER');
        $mail = "test_user_1_tajine@example.org";
        $this->commonCheckerConnectedUserCanBrowseMultipleUrls("$mail", $urls);
    }
    /**
     * Connected USER are deny to browse ROLE_ADMIN | ROLE_CERTIFICAT_MANAGER urls
     */
    public function testConnectedUserIsDenyToBrowseAdministratorUrls(): void
    {
        $urls = $this->getUrls('ROLE_ADMIN');
        $mail = "test_user_1_tajine@example.org";
        $this->commonCheckerConnnectedUserIsDenyToBrowseMultipleUrls("$mail", $urls);
    }
    public function testConnectedUserIsDenyToBrowseManagerUrls(): void
    {
        $urls = $this->getUrls('ROLE_CERTIFICAT_MANAGER');
        $mail = "test_user_1_tajine@example.org";
        $this->commonCheckerConnnectedUserIsDenyToBrowseMultipleUrls("$mail", $urls);
    }

    /////////////// Connected MANAGER /////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    public function testManagerCanBrowseHisUrls(): void
    {
        $urls = $this->getUrls('ROLE_CERTIFICAT_MANAGER');
        $mail = "test_manager_1_tajine@example.org";
        $this->commonCheckerConnectedUserCanBrowseMultipleUrls("$mail", $urls);
    }
    /**
     * MANAGER are deny to browse ROLE_ADMIN urls
     */
    public function testManagerIsDenyToBrowseAdministratorUrls(): void
    {
        $urls = $this->getUrls('ROLE_ADMIN');
        $mail = "test_manager_1_tajine@example.org";
        $this->commonCheckerConnnectedUserIsDenyToBrowseMultipleUrls("$mail", $urls);
    }

    /////////////// Connected ADMINISTRATOR ///////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    public function testAdministratorCanBrowseHisUrls(): void
    {
        $urls = $this->getUrls('ROLE_ADMIN');
        $mail = "test_admin_1_tajine@example.org";
        $this->commonCheckerConnectedUserCanBrowseMultipleUrls("$mail", $urls);
    }

    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    public function commonCheckerConnnectedUserIsDenyToBrowseMultipleUrls(string $mail, array $urls): void
    {
        $kernelBrowserForConnnectedUser =  $this->getKernelBrowserWithConnectedUser($mail);
        foreach ($urls as $routeKey => $routeData) {
            $route = trim($routeKey);
            $url = trim($routeData[1]);
            $crawler = $kernelBrowserForConnnectedUser->request('GET', $url);
            $this->assertRouteSame("$route");
            $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN); // HTTP status code = 403
        }
    }

    /// ///////////////////////////////////////////////////////////////////////////////////////////
    /// ///////////////////////////////////////////////////////////////////////////////////////////
    public function commonCheckerConnectedUserCanBrowseMultipleUrls(string $mail, array $urls): void
    {
        $kernelBrowserForConnnectedUser =  $this->getKernelBrowserWithConnectedUser($mail);
        foreach ($urls as $routeKey => $routeData) {
            $route = trim($routeKey);
            $url = trim($routeData[1]);
            $crawler = $kernelBrowserForConnnectedUser->request('GET', $url);
            $this->assertRouteSame("$route");
            $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
            $this->commonCheckerHasExpectedSecurityHttpHeaders();
        }
    }
}
