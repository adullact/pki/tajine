<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Functional\User;

use App\Tests\Functional\TestHelperTrait;
use App\Tests\Functional\TestHelperFormTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

class FunctionalTestConnectedUserCanChangePasswordTest extends WebTestCase
{
    use TestHelperTrait;
    use TestHelperFormTrait;

    private string $emailManager1 = "test_manager_1_tajine@example.org";
    private string $passwordManager1 = "admin-tajine_PaSsWord_IsNotSoSecretChangeIt";
    private string $emailManager2 = "test_manager_2_tajine@example.org";

    private function getValidDataForChangePasswordForm(
        string $currentPlainPassword
    ): array {
        $newPlainPassword = "new_password_value_$currentPlainPassword";
        $newPlainPasswordConfirm = $newPlainPassword;
        return [
            'change_password_form[currentPlainPassword]' => "$currentPlainPassword",
            'change_password_form[newPlainPassword][first]' => "$newPlainPassword",
            'change_password_form[newPlainPassword][second]' => "$newPlainPasswordConfirm",
        ];
    }


    /**
     * @param array $formDataWithWrongData
     * @param KernelBrowser $kernelBrowserForConnnectedManager
     * @return Crawler
     */
    private function sendChangePasswordFormWithWrongData(
        array $formDataWithWrongData,
        KernelBrowser $kernelBrowserForConnnectedManager
    ): Crawler {
        $crawler = $kernelBrowserForConnnectedManager->submitForm(
            'user-changePasswordForm_submit',
            $formDataWithWrongData
        );
        $this->assertRouteSame("app_user_profile_change_password");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('h1', 'Change your password');
        $this->assertSelectorTextSame('div.alert-danger', 'The form contains some errors, as described below.');
        return $crawler;
    }

    private function loadEmptyChangePasswordForm(
        string $connnectedUserEmail,
        bool $enableAssertions = false
    ): KernelBrowser {
        $kernelBrowserForConnnectedUser =  $this->getKernelBrowserWithConnectedUser($connnectedUserEmail);
        $crawler = $kernelBrowserForConnnectedUser->request('GET', '/user/profile/password');
        if ($enableAssertions === true) {
            $this->assertRouteSame("app_user_profile_change_password");
            $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
            $this->assertSelectorTextSame('h1', 'Change your password');
//            $this->checkAttribute(
//                $crawler,
//                "form",
//                ['_name' => 'form',  'name' => 'change_password_form']
//            );
            $minPasswordLength = (int) $_ENV['WEBAPP_USER_CONFIG_MIN_PASSWORD_LENGTH'];
            $htmlFormName = "change_password_form";
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'currentPlainPassword',
                fieldAttributes: [
                    'type' => 'password',
                    'required' => 'required',
                    'autocomplete' => 'new-password',
                ],
                labelText: 'Your password',
                helpText: 'You must provide your current password in order to change it.'
            );
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'newPlainPassword_first',
                fieldAttributes: [
                    'type' => 'password',
                    'required' => 'required',
                    'autocomplete' => 'new-password',
                    'minlength' => "$minPasswordLength",
                    'maxlength' => "4096",
                ],
                labelText: 'Your new password',
                helpText: "Please enter your new password. It should be at least $minPasswordLength characters."
            );
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'newPlainPassword_second',
                fieldAttributes: [
                    'type' => 'password',
                    'required' => 'required',
                    'autocomplete' => 'new-password',
                    'minlength' => "$minPasswordLength",
                    'maxlength' => "4096",
                ],
                labelText: 'Repeat password',
                helpText: 'Please repeat your new password'
            );
            $this->checkAttribute(
                $crawler,
                "button#user-changePasswordForm_submit",
                ['_text' => 'Save your password']
            );
        }
        return $kernelBrowserForConnnectedUser;
    }

    /**
     * @group ConnectedUser
     * @group ConnectedUser_Change_Password
     */
    public function testConnectedUserCanSeeChangePasswordLinkOnProfilePage(): void
    {
        $onnnectedUserEmail = $this->emailManager1;
        $kernelBrowserForConnnectedUser =  $this->getKernelBrowserWithConnectedUser($onnnectedUserEmail);
        $crawler = $kernelBrowserForConnnectedUser->request('GET', '/user/profile');
        $this->assertRouteSame("app_user_profile");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->checkAttribute(
            $crawler,
            "#user_change-password_link",
            ['_name' => 'a',  '_text' => 'Change your password',  'href'  => "/user/profile/password"]
        );
    }

    /**
     * @group Form
     * @group ConnectedUser
     * @group ConnectedUser_Change_Password
     * @group ConnectedUser_Change_Password_Form
     */
    public function testConnectedUserCanDisplayChangePasswordForm(): void
    {
        $userEmail = $this->emailManager1;
        $this->loadEmptyChangePasswordForm(connnectedUserEmail: $userEmail, enableAssertions: true);
    }



    /**
     * @group Form
     * @group ConnectedUser
     * @group ConnectedUser_Change_Password
     * @group ConnectedUser_Change_Password_Form
     */
    public function testConnectedUserCanChangePasswordAndCanLoginWithNewPassword(): void
    {
        $userEmail = $this->emailManager1;
        $initialUserPassword = $this->passwordManager1; // Used to reset
        $kernelBrowserForConnnectedUser = $this->loadEmptyChangePasswordForm($userEmail);
        $initialUserPasswordHash = $this->getCurrentHashOfUserPassword($userEmail); // Used to compare at the end

        $minPasswordLength = (int) $_ENV['WEBAPP_USER_CONFIG_MIN_PASSWORD_LENGTH'];
        $newPassword = $this->generateRandomString($minPasswordLength + 2);

        $formName = 'change_password_form';
        $fieldName = 'newPlainPassword';
        $formData = $this->getValidDataForChangePasswordForm(currentPlainPassword: $initialUserPassword);
        $formData["${formName}[${fieldName}][first]"] = $newPassword;
        $formData["${formName}[${fieldName}][second]"] = $newPassword;
        $crawler = $kernelBrowserForConnnectedUser->submitForm(
            'user-changePasswordForm_submit',
            $formData
        );
        $this->assertRouteSame("app_user_profile_change_password");
        $this->assertResponseStatusCodeSame(Response::HTTP_SEE_OTHER); // HTTP status code = 303
        $this->assertResponseHeaderSame("Location", '/user/profile');

        $crawler = $kernelBrowserForConnnectedUser->request('GET', '/user/profile');
        $this->assertRouteSame("app_user_profile");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('h1', 'Profile');
        $this->assertSelectorTextSame('div.alert-success', 'Your password has been successfully changed.');

        // Check that password has been changed in database
        $this->checkPasswordHasBeenChangedInDatabase("$userEmail", "$initialUserPasswordHash");

        // Logout current user and check that the user can only login with his new password
        $this->checkUserCanLoginOnlyWithNewPassword(
            kernelBrowser: $kernelBrowserForConnnectedUser,
            userEmail: $userEmail,
            newPlainPassword: $newPassword,
            oldPlainPassword: $initialUserPassword
        );

        // Reset user password with initial password
        $this->resetUserPasswordInDatabaseWithProvidedPasswordHash(
            userEmail: $userEmail,
            newUserPasswordHash: $initialUserPasswordHash
        );
    }


    /**
     * @group Form
     * @group ConnectedUser
     * @group ConnectedUser_Change_Password
     * @group ConnectedUser_Change_Password_Form
     * @group ConnectedUser_Change_Password_Form_With_Wrong_Data
     */
    public function testConnectedUserCanNotChangePasswordWithBadCurrentPasswordField(): void
    {
        $userEmail = $this->emailManager1;
        $userPassword = $this->passwordManager1;
        $kernelBrowserForConnnectedUser = $this->loadEmptyChangePasswordForm($userEmail);
        $currentUserPasswordHash = $this->getCurrentHashOfUserPassword($userEmail); // Used to compare at the end

        $formName = 'change_password_form';
        $fieldName = 'currentPlainPassword';
        $minPasswordLength = (int) $_ENV['WEBAPP_USER_CONFIG_MIN_PASSWORD_LENGTH'];
        $badCurentPassword = $this->generateRandomString($minPasswordLength + 2);
        $formData = $this->getValidDataForChangePasswordForm($userPassword);
        $formData["${formName}[${fieldName}]"] = $badCurentPassword;
        $crawler = $this->sendChangePasswordFormWithWrongData(
            $formData,
            $kernelBrowserForConnnectedUser
        );
        $this->commonCheckerIfFormFieldIsInvalid(
            crawler: $crawler,
            cssFilterOfInvadFormField: "#${formName}_${fieldName}",
            cssFilterOfInvalidFeedback: '.invalid-feedback',
            invalidFeedback: 'The password provided is invalid.',
            expectedFieldAttributes: ['class' => 'is-invalid form-control']
        );
        // Check that password has not been changed in database
        $this->assertSame("$currentUserPasswordHash", $this->getCurrentHashOfUserPassword($userEmail));
    }

    /**
     * @group Form
     * @group ConnectedUser
     * @group ConnectedUser_Change_Password
     * @group ConnectedUser_Change_Password_Form
     * @group ConnectedUser_Change_Password_Form_With_Wrong_Data
     */
    public function testConnectedUserCanNotChangePasswordWithTooSmallNewPassword(): void
    {
        $userEmail = $this->emailManager1;
        $userPassword = $this->passwordManager1;
        $kernelBrowserForConnnectedUser = $this->loadEmptyChangePasswordForm($userEmail);
        $currentUserPasswordHash = $this->getCurrentHashOfUserPassword($userEmail); // Used to compare at the end
        $this->commonCheckWhenFormIsSentWithTooSmallRepeatPassword(
            kernelBrowser: $kernelBrowserForConnnectedUser,
            validFormData: $this->getValidDataForChangePasswordForm($userPassword),
            formName: 'change_password_form',
            fieldName: 'newPlainPassword',
            minPasswordLength: (int) $_ENV['WEBAPP_USER_CONFIG_MIN_PASSWORD_LENGTH'],
            methodNameToSendFormWithWrongData: 'sendChangePasswordFormWithWrongData'
        );
        // Check that password has not been changed in database
        $this->assertSame("$currentUserPasswordHash", $this->getCurrentHashOfUserPassword($userEmail));
    }


    /**
     * @group Form
     * @group ConnectedUser
     * @group ConnectedUser_Change_Password
     * @group ConnectedUser_Change_Password_Form
     * @group ConnectedUser_Change_Password_Form_With_Wrong_Data
     */
    public function testConnectedUserCanNotChangePasswordWithoutSimilarNewPasswordFields(): void
    {
        $userEmail = $this->emailManager1;
        $userPassword = $this->passwordManager1;
        $kernelBrowserForConnnectedUser = $this->loadEmptyChangePasswordForm($userEmail);
        $currentUserPasswordHash = $this->getCurrentHashOfUserPassword($userEmail); // Used to compare at the end
        $this->commonCheckWhenFormIsSentWithoutSimilarNewPasswordFields(
            kernelBrowser: $kernelBrowserForConnnectedUser,
            validFormData: $this->getValidDataForChangePasswordForm($userPassword),
            formName: 'change_password_form',
            fieldName: 'newPlainPassword',
            minPasswordLength: (int) $_ENV['WEBAPP_USER_CONFIG_MIN_PASSWORD_LENGTH'],
            methodNameToSendFormWithWrongData: 'sendChangePasswordFormWithWrongData'
        );
        // Check that password has not been changed in database
        $this->assertSame("$currentUserPasswordHash", $this->getCurrentHashOfUserPassword($userEmail));
    }


    /**
     * @group Form
     * @group ConnectedUser
     * @group ConnectedUser_Change_Password
     * @group ConnectedUser_Change_Password_Form
     * @group ConnectedUser_Change_Password_Form_With_Wrong_Data
     */
    public function testConnectedUserCanNotChangePasswordWithoutNewPasswordFields(): void
    {
        $userEmail = $this->emailManager1;
        $userPassword = $this->passwordManager1;
        $kernelBrowserForConnnectedUser = $this->loadEmptyChangePasswordForm($userEmail);
        $currentUserPasswordHash = $this->getCurrentHashOfUserPassword($userEmail); // Used to compare at the end

        $formName = 'change_password_form';
        $fieldName = 'newPlainPassword';
        $invalidFeedback = 'The new password fields must match.';
        $this->commonCheckerWhenFormIsSentWithMissingOrEmptyField(
            kernelBrowser: $kernelBrowserForConnnectedUser,
            validFormData: $this->getValidDataForChangePasswordForm($userPassword),
            formName: $formName,
            fieldName: $fieldName,
            fieldRepeatName: 'first',
            invalidFeedback: $invalidFeedback,
            methodNameToSendFormWithWrongData: 'sendChangePasswordFormWithWrongData'
        );
        // Check that password has not been changed in database
        $this->assertSame("$currentUserPasswordHash", $this->getCurrentHashOfUserPassword($userEmail));

        $this->commonCheckerWhenFormIsSentWithMissingOrEmptyField(
            kernelBrowser: $kernelBrowserForConnnectedUser,
            validFormData: $this->getValidDataForChangePasswordForm($userPassword),
            formName: $formName,
            fieldName: $fieldName,
            fieldRepeatName: 'second',
            invalidFeedback: $invalidFeedback,
            methodNameToSendFormWithWrongData: 'sendChangePasswordFormWithWrongData'
        );
        // Check that password has not been changed in database
        $this->assertSame("$currentUserPasswordHash", $this->getCurrentHashOfUserPassword($userEmail));
    }



    /**
     * @group Form
     * @group ConnectedUser
     * @group ConnectedUser_Change_Password
     * @group ConnectedUser_Change_Password_Form
     * @group ConnectedUser_Change_Password_Form_With_Wrong_Data
     */
    public function testConnectedUserCanNotChangePasswordWithoutCurentPasswordField(): void
    {
//        $kernelBrowserForConnnectedUser = $this->loadEmptyChangePasswordForm($this->emailManager1);
//        $this->checkWhenFormIsSentWithMissingOrEmptyField(
//            kernelBrowser: $kernelBrowserForConnnectedUser,
//            validFormData: $this->getValidDataForChangePasswordForm($this->passwordManager1),
//            formName: 'change_password_form',
//            fieldName: 'currentPlainPassword',
//            checkMissingField: false,
//            checkEmptyField: false
//        );
    }
}
