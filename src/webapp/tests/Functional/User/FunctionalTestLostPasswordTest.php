<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\tests\Functional\User;

use App\DataFixtures\AppUserFixtures;
use App\Entity\UserToken;
use App\Repository\UserRepository;
use App\Repository\UserTokenRepository;
use App\tests\CommonHelpers\TestHelperEmailTrait;
use App\Tests\Functional\TestHelperTrait;
use App\Tests\Functional\TestHelperFormTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\MailerAssertionsTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator;

class FunctionalTestLostPasswordTest extends WebTestCase
{
    use TestHelperTrait;
    use TestHelperEmailTrait;
    use TestHelperFormTrait;
    use MailerAssertionsTrait;

    private string $userEmail1 ;
    private string $userEmail2 ;
    private string $userEmail3 ;
    private string $userEmail4 ;
    private string $userEmail5 ;
    private string $userEmail6 ;
    private string $userEmail7 ;
    private string $communFixtureUserPassword = "admin-tajine_PaSsWord_IsNotSoSecretChangeIt";
    private int $minPasswordLength;
    private int $tokenLifetime;

    protected function setUp(): void
    {
        $this->userEmail1 = AppUserFixtures::USER_REFERENCE_MANAGER_3;
        $this->userEmail2 = AppUserFixtures::USER_REFERENCE_MANAGER_4;
        $this->userEmail3 = AppUserFixtures::USER_REFERENCE_MANAGER_5;
        $this->userEmail4 = AppUserFixtures::USER_REFERENCE_MANAGER_6;
        $this->userEmail5 = AppUserFixtures::USER_REFERENCE_MANAGER_7;
        $this->userEmail6 = AppUserFixtures::USER_REFERENCE_MANAGER_8;
        $this->userEmail7 = AppUserFixtures::USER_REFERENCE_MANAGER_9;
        $this->minPasswordLength = (int) $_ENV['WEBAPP_USER_CONFIG_MIN_PASSWORD_LENGTH'];
        $this->tokenLifetime = (int) $_ENV['WEBAPP_USER_CONFIG_PASSWORD_RESET_TOKEN_LIFETIME'];
    }


    private function cleanUpToken(UserToken $userToken): void
    {
        $userTokenRepository = static::getContainer()->get(UserTokenRepository::class);
        $userTokenRepository->remove(entity: $userToken, flush: true);
    }

    private function getUserToken(string $userEmail): ?UserToken
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $userTokenRepository = static::getContainer()->get(UserTokenRepository::class);
        $userId = $userRepository->findOneByEmail($userEmail)->getId();
        $userToken = $userTokenRepository->findOneByUserId($userId);
        return $userToken;
    }



    private function setNewUserToken(string $userEmail): ?UserToken
    {
        $userRepository = static::getContainer()->get(UserRepository::class);
        $userTokenRepository = static::getContainer()->get(UserTokenRepository::class);
        $user = $userRepository->findOneByEmail($userEmail);

        // Clean up old previous user token
        $userToken = $userTokenRepository->findOneByUserId($user->getId());
        if (!\is_null($userToken)) {
            $this->cleanUpToken($userToken);
        }

        // Create new user token
        $tokenLifetime = $this->tokenLifetime;
        $tokenGenerator = new UriSafeTokenGenerator();
        $now = new \DateTimeImmutable();
        $validTo = $now->add(\DateInterval::createFromDateString("${tokenLifetime} seconds"));
        $userToken = new UserToken();
        $userToken->setToken($tokenGenerator->generateToken());
        $userToken->setCreatedAt($now);
        $userToken->setValidTo($validTo);
        $userToken->setUsedBy($user);
        $userTokenRepository->save($userToken, true);

        return $userToken;
    }

    private function getValidDataForRequestForgotPasswordForm(
        string $userEmail
    ): array {
        return ['reset_password_request_form[email]' => "$userEmail"];
    }

    private function getValidDataForForgotPasswordNewPasswordForm(
        string $userPassword
    ): array {
        return [
            'reset_password_form[newPlainPassword][first]' => "$userPassword",
            'reset_password_form[newPlainPassword][second]' => "$userPassword"
        ];
    }

    private function commonCheckCanNotSendNewPasswordFormWithInvalidOrExpiredResetLink(
        string $userEmail,
        string $currentUserPasswordHash
    ): void {
        $this->assertRouteSame("app_account_forgot_password_new-password");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextContains(
            '#error_forgot-password_not-voalid-token.alert-danger',
            "Your password reset link is invalid or has expired."
        );
        // Check that password has not been changed in database
        $this->assertSame("$currentUserPasswordHash", $this->getCurrentHashOfUserPassword($userEmail));
    }

    private function sendForgotPasswordNewPasswordFormWithWrongData(
        array $formDataWithWrongData,
        KernelBrowser $kernelBrowser
    ): Crawler {
        $crawler = $kernelBrowser->submitForm(
            button: 'public_forgotPassword_newPasswordForm_submit',
            fieldValues: $formDataWithWrongData
        );
        $this->assertRouteSame("app_account_forgot_password_new-password");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('h2', 'Reset your password');
        $this->assertSelectorExists("div.invalid-feedback");
//      $this->assertSelectorTextSame('div.alert-danger', 'The form contains some errors, as described below.');
        return $crawler;
    }

    private function sendRequestForgotPasswordFormWithWrongData(
        array $formDataWithWrongData,
        KernelBrowser $kernelBrowser
    ): Crawler {
        $crawler = $kernelBrowser->submitForm(
            button: 'public_forgotPassword_requestForm_submit',
            fieldValues: $formDataWithWrongData
        );
        $this->assertRouteSame("app_account_forgot_password_request");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->assertSelectorTextSame('h2', 'Forgot your password?');
//      $this->assertSelectorTextSame('div.alert-danger', 'The form contains some errors, as described below.');
        return $crawler;
    }

    private function loadEmptyForgotPasswordNewPasswordForm(
        KernelBrowser $kernelBrowser,
        string $resetLink,
        bool $enableBasicAssertions = false,
        bool $enableFormAssertions = false,
        string $expectedErrorMsg = null,
        string $expectedErrorCssSelector = null,
    ): KernelBrowser {
        $crawler = $kernelBrowser->request('GET', "$resetLink");
        if ($enableBasicAssertions === true) {
            $this->assertRouteSame("app_account_forgot_password_new-password");
            $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
            $this->assertSelectorTextContains('h2', "Reset your password");
        }

        if (\is_null($expectedErrorMsg)) {
            $this->assertSelectorNotExists('.alert-danger');
            $this->assertSelectorNotExists('#error_forgot-password_not-voalid-token');
            $this->assertSelectorTextNotContains('body', "Your password reset link is invalid or has expired.");
        } elseif (\is_null($expectedErrorCssSelector)) {
            $this->assertSelectorTextContains('body', "$expectedErrorMsg");
        } else {
            $this->assertSelectorTextContains("$expectedErrorCssSelector", "$expectedErrorMsg");
        }

        if ($enableFormAssertions === true) {
            $minPasswordLength = $this->minPasswordLength;
            $htmlFormName = "reset_password_form";
            $this->checkAttribute(crawler: $crawler, cssFilter: "form[name=$htmlFormName]", attributesExpected: []);
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'newPlainPassword_first',
                fieldAttributes: [
                    'type' => 'password',
                    'required' => 'required',
                    'autocomplete' => 'new-password',
                    'minlength' => "$minPasswordLength",
                    'maxlength' => "4096",
                ],
                labelText: 'Your new password',
                helpText: "Please enter your new password. It should be at least $minPasswordLength characters."
            );
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'newPlainPassword_second',
                fieldAttributes: [
                    'type' => 'password',
                    'required' => 'required',
                    'autocomplete' => 'new-password',
                    'minlength' => "$minPasswordLength",
                    'maxlength' => "4096",
                ],
                labelText: 'Repeat password',
                helpText: 'Please repeat your new password'
            );
            $this->checkAttribute(
                $crawler,
                "button#public_forgotPassword_newPasswordForm_submit",
                ['_text' => 'Save your password']
            );
        }
        return $kernelBrowser;
    }

    private function loadEmptyForgotPasswordRequestForm(
        bool $enableAssertions = false
    ): KernelBrowser {
        $kernelBrowser = static::createClient();
        $crawler = $kernelBrowser->request('GET', '/account/forgot-password');
        if ($enableAssertions === true) {
            $this->assertRouteSame("app_account_forgot_password_request");
            $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
            $this->assertSelectorTextSame('h2', 'Forgot your password?');
            $htmlFormName = "reset_password_request_form";
            $this->checkAttribute(crawler: $crawler, cssFilter: "form[name=$htmlFormName]", attributesExpected: []);
            $this->checkFormField(
                crawler: $crawler,
                htmlFormName: $htmlFormName,
                fieldIdsuffix: 'email',
                fieldAttributes: [
                    'type' => 'email',
                    'required' => 'required'
                ],
                labelText: 'Email address',
                helpText: 'Enter your email address, and we will send you a link to reset your password.'
            );
            $this->checkAttribute(
                $crawler,
                "button#public_forgotPassword_requestForm_submit",
                ['_text' => 'Send password reset email']
            );
        }
        return $kernelBrowser;
    }



    /**
     * @group NoConnectedUser
     * @group NoConnectedUser_Login_Page
     * @group NoConnectedUser_Forgot_Password
     */
    public function testAnonymousUserCanSeeRequestForgotPasswordLinkOnLoginPage(): void
    {
        $kernelBrowser = static::createClient();
        $crawler = $kernelBrowser->request('GET', '/account/login');
        $this->assertRouteSame("app_account_login");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
        $this->checkAttribute(
            $crawler,
            "#public_lost-password_link",
            ['_name' => 'a',  '_text' => 'Forgot your password?',  'href'  => "/account/forgot-password"]
        );
    }


    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_Request_Form
     */
    public function testAnonymousUserCanDisplayRequestForgotPasswordForm(): void
    {
        $this->loadEmptyForgotPasswordRequestForm(enableAssertions: true);
    }

    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_Request_Form
     * @group NoConnectedUser_Forgot_Password_Request_Form_With_Wrong_Data
     */
    public function testAnonymousUserCanNotSendRequestForgotPasswordFormWithoutEmailField(): void
    {
        $userEmail = $this->userEmail1;
        $kernelBrowser = $this->loadEmptyForgotPasswordRequestForm(enableAssertions: false);
        $validFormData = $this->getValidDataForRequestForgotPasswordForm($userEmail);
        $this->commonCheckerWhenFormIsSentWithMissingOrEmptyField(
            kernelBrowser: $kernelBrowser,
            validFormData: $validFormData,
            formName: 'reset_password_request_form',
            fieldName: 'email',
            invalidFeedback: 'This field is mandatory',
            methodNameToSendFormWithWrongData: 'sendRequestForgotPasswordFormWithWrongData'
        );
        $this->assertNull($this->getMailerEvent(0)); // check no email was send to end user
    }


    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_Request_Form
     * @group NoConnectedUser_Forgot_Password_Request_Form_With_Wrong_Data
     */
    public function testAnonymousUserCanNotSendRequestForgotPasswordFormWithInvalidFormatEmail(): void
    {
        $userEmail = $this->userEmail1;
        $kernelBrowser = $this->loadEmptyForgotPasswordRequestForm(enableAssertions: false);
        $validFormData = $this->getValidDataForRequestForgotPasswordForm($userEmail);
        $formName = 'reset_password_request_form';
        $fieldName = 'email';
        foreach ($this->getListOfEmailsInInvalidFormat() as $notValidEmail) {
            $formDataWithWrongData = $validFormData;
            $formDataWithWrongData["${formName}[${fieldName}]"] = $notValidEmail;
            $crawler = $this->sendRequestForgotPasswordFormWithWrongData(
                formDataWithWrongData: $formDataWithWrongData,
                kernelBrowser: $kernelBrowser
            );
            $this->commonCheckerIfFormFieldIsInvalid(
                crawler: $crawler,
                cssFilterOfInvadFormField: "#${formName}_${fieldName}",
                cssFilterOfInvalidFeedback: '.invalid-feedback',
                invalidFeedback: 'Please enter a valid email address.',
            );
            $this->assertNull($this->getMailerEvent(0)); // check no email was send to end user
        }
    }

    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_Request_Form
     * @group NoConnectedUser_Forgot_Password_Request_Form_With_Wrong_Data
     */
    public function testAnonymousUserCanSendRequestForgotPasswordFormForNotExistingAccountAndResetMailIsNotSent()
    {
//      $this->markTestIncomplete();
        $userEmail = "not-existing-account@example.org";
        $kernelBrowser = $this->loadEmptyForgotPasswordRequestForm(enableAssertions: false);
        $validFormData = $this->getValidDataForRequestForgotPasswordForm($userEmail);
        $crawler = $this->sendRequestForgotPasswordFormWithWrongData(
            formDataWithWrongData: $validFormData,
            kernelBrowser: $kernelBrowser
        );
        $this->assertRouteSame("app_account_forgot_password_request");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200

        // HTML page content after FORM submission ---> Alert message
        $this->assertSelectorTextContains(
            '.alert-success',
            "sent you a message with a link you can use to reset your password"
        );

        // No EMAIL message sent
        $this->assertEmailCount(0);
        $this->assertNull($this->getMailerMessage());
        $this->assertNull($this->getMailerEvent());
    }

    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_Request_Form
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form
     */
    public function testAnonymousUserCanSendRequestForgotPasswordFormForExistingAccountAndReceiveResetMailLink(): void
    {
        $userEmail = $this->userEmail2;
        $kernelBrowser = $this->loadEmptyForgotPasswordRequestForm(enableAssertions: false);
        $validFormData = $this->getValidDataForRequestForgotPasswordForm($userEmail);
        $crawler = $this->sendRequestForgotPasswordFormWithWrongData(
            formDataWithWrongData: $validFormData,
            kernelBrowser: $kernelBrowser
        );

        //////////////////////////////////////////////////////////////////////////////////////////////////////

        $this->assertRouteSame("app_account_forgot_password_request");
        $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200

        // HTML page content after FORM submission ---> Alert message
        $this->assertSelectorTextContains(
            '.alert-success',
            "sent you a message with a link you can use to reset your password"
        );

        //////////////////////////////////////////////////////////////////////////////////////////////////////

        // Get current reset token
        $userToken = $this->getUserToken($userEmail);
        $this->assertInstanceOf('App\Entity\UserToken', $userToken);
        $token = $userToken->getToken();
        $resetLink = "/account/forgot-password/t/$token";

        //////////////////////////////////////////////////////////////////////////////////////////////////////

        # EMAIL sent to end-user
        $this->assertEmailIsQueued($this->getMailerEvent(0));
        $this->assertEmailCount(1); // use assertQueuedEmailCount() when using Messenger
        $email = $this->getMailerMessage();

        # EMAIL sent to end-user ---> check email Attachment
        $this->assertEmailAttachmentCount($email, 0);

        # EMAIL sent to end-user ---> check email HEADERS (technical, correspondent)
        $appShortName = array_key_exists('WEBAPP_SHORTNAME', $_ENV) ? $_ENV['WEBAPP_SHORTNAME'] : false;
        $appEmailFrom = array_key_exists('WEBAPP_EMAIL_FROM', $_ENV) ? $_ENV['WEBAPP_EMAIL_FROM'] : false;
        $expectedMailSubject = "$appShortName - Reset password instructions";
        $this->assertEmailHeaderSame($email, "Subject", "$expectedMailSubject");
        $this->checkCommonTechnicalEmailHeaders(email: $email);
        $this->checkCommonCorrespondentsEmailHeaders(
            email: $email,
            expectedEmailTo: "$userEmail",
            expectedEmailFrom: "$appEmailFrom",
        );

        # EMAIL sent to end-user ---> check email content (HTML and TXT versions)
        $msg = 'You can click on the following link to change your password:';
        $this->assertEmailHtmlBodyContains($email, "$msg");
        $this->assertEmailHtmlBodyContains($email, "$resetLink");
        $this->assertEmailTextBodyContains($email, "$msg");
        $this->assertEmailTextBodyContains($email, "$resetLink");

        //////////////////////////////////////////////////////////////////////////////////////////////////////

        // Check reset link
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $resetLink,
            enableBasicAssertions: false,
            enableFormAssertions: false,
            expectedErrorMsg: null,
            expectedErrorCssSelector: null,
        );

        // Clean up : remove currrent token
        // $this->cleanUpToken($userToken);
    }

    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form
     */
    public function testAnonymousUserCanDisplayNewPasswordFormIfResetLinkIsValid(): void
    {
//      $this->markTestIncomplete();
        $userEmail = $this->userEmail3;
        $kernelBrowser = static::createClient();

        // Create new token
        $userToken = $this->setNewUserToken($userEmail);
        $this->assertInstanceOf('App\Entity\UserToken', $userToken);
        $resetLink = "/account/forgot-password/t/" . $userToken->getToken();
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $resetLink,
            enableBasicAssertions: true,
            enableFormAssertions: true,
            expectedErrorMsg: null,
            expectedErrorCssSelector: null,
        );
        // Clean up : remove currrent token
         $this->cleanUpToken($userToken);
    }


    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form_With_invalid_reset-link
     */
    public function testAnonymousUserCanNotDisplayNewPasswordFormIfResetLinkIsNotTheLastOne(): void
    {
//      $this->markTestIncomplete();
        $userEmail = $this->userEmail4;
        $kernelBrowser = static::createClient();

        // Create new token and check reset link works without error
        $userToken = $this->setNewUserToken($userEmail);
        $oldResetLink = "/account/forgot-password/t/" . $userToken->getToken();
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $oldResetLink,
            enableBasicAssertions: true,
            enableFormAssertions: false,
            expectedErrorMsg: null,
            expectedErrorCssSelector: null,
        );

        // Create a new token and check old reset link display error message
        $lastUserToken = $this->setNewUserToken($userEmail);
        $lastResetLink = "/account/forgot-password/t/" . $lastUserToken->getToken();
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $oldResetLink,
            enableBasicAssertions: true,
            enableFormAssertions: false,
            expectedErrorMsg: 'Your password reset link is invalid or has expired.',
            expectedErrorCssSelector: '#error_forgot-password_not-voalid-token.alert-danger',
        );
    }


    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form_With_invalid_reset-link
     */
    public function testAnonymousUserCanNotDisplayNewPasswordFormIfResetLinkIsNoLongerValid(): void
    {
//      $this->markTestIncomplete();
        $userEmail = $this->userEmail6;
        $kernelBrowser = static::createClient();

        // Create new token and check reset link works without error
        $userToken = $this->setNewUserToken($userEmail);
        $resetLink = "/account/forgot-password/t/" . $userToken->getToken();
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $resetLink,
            enableBasicAssertions: true,
            enableFormAssertions: false,
            expectedErrorMsg: null,
            expectedErrorCssSelector: null,
        );

        // Update the user token to an expired valid date
        $tokenLifetime = $this->tokenLifetime;
        $now = new \DateTimeImmutable();
        $createdAt = $now->setTimestamp(\time() - ($tokenLifetime + 1));
        $validTo = $createdAt->add(\DateInterval::createFromDateString("${tokenLifetime} seconds"));
        $userToken->setCreatedAt($createdAt);
        $userToken->setValidTo($validTo);
        $userTokenRepository = static::getContainer()->get(UserTokenRepository::class);
        $userTokenRepository->save($userToken, true);

        // Reload user token from database and try to display Forgot Password form
        $userToken = $this->getUserToken($userEmail);
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $resetLink,
            enableBasicAssertions: true,
            enableFormAssertions: false,
            expectedErrorMsg: 'Your password reset link is invalid or has expired.',
            expectedErrorCssSelector: '#error_forgot-password_not-voalid-token.alert-danger',
        );
        $this->cleanUpToken($userToken); // cleanup
    }


    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form_With_invalid_reset-link
     */
    public function testAnonymousUserCanNotDisplayNewPasswordFormIfResetLinkNotExistInDatabase(): void
    {
//      $this->markTestIncomplete();
        $userEmail = $this->userEmail5;
        $kernelBrowser = static::createClient();

        // Create new token and check reset link works without error
        $userToken = $this->setNewUserToken($userEmail);
        $resetLink = "/account/forgot-password/t/" . $userToken->getToken();
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $resetLink,
            enableBasicAssertions: true,
            enableFormAssertions: false,
            expectedErrorMsg: null,
            expectedErrorCssSelector: null,
        );

        // Remove current token and check reset link display error message
        $this->cleanUpToken($userToken);
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $resetLink,
            enableBasicAssertions: true,
            enableFormAssertions: false,
            expectedErrorMsg: 'Your password reset link is invalid or has expired.',
            expectedErrorCssSelector: '#error_forgot-password_not-voalid-token.alert-danger',
        );
    }


    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form_With_invalid_reset-link
     */
    public function testAnonymousUserCanNotSendNewPasswordFormIfResetLinkIsNotTheLastOne(): void
    {
//      $this->markTestIncomplete();
        $userEmail = $this->userEmail5;
        $kernelBrowser = static::createClient();
        $currentUserPasswordHash = $this->getCurrentHashOfUserPassword($userEmail); // Used to compare at the end

        // Create new token and check reset link works without error
        $oldUserToken = $this->setNewUserToken($userEmail);
        $oldResetLink = "/account/forgot-password/t/" . $oldUserToken->getToken();
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $oldResetLink,
            enableBasicAssertions: true,
            enableFormAssertions: false,
            expectedErrorMsg: null,
            expectedErrorCssSelector: null,
        );

        // Create a new token, send reset password form and see error "password reset link is invalid or has expired"
        $lastUserToken = $this->setNewUserToken($userEmail);
        $newPlainPassword = $this->generateRandomString($this->minPasswordLength + rand(0, 15));
        $crawler = $kernelBrowser->submitForm(
            button: 'public_forgotPassword_newPasswordForm_submit',
            fieldValues: $this->getValidDataForForgotPasswordNewPasswordForm($newPlainPassword)
        );
        $this->commonCheckCanNotSendNewPasswordFormWithInvalidOrExpiredResetLink($userEmail, $currentUserPasswordHash);
        $this->cleanUpToken($this->getUserToken($userEmail)); // cleanup
    }


    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form_With_invalid_reset-link
     */
    public function testAnonymousUserCanNotSendNewPasswordFormIfResetLinkIsNoLongerValid(): void
    {
//      $this->markTestIncomplete();
        $userEmail = $this->userEmail6;
        $kernelBrowser = static::createClient();
        $currentUserPasswordHash = $this->getCurrentHashOfUserPassword($userEmail); // Used to compare at the end

        // Create new token and check reset link works without error
        $userToken = $this->setNewUserToken($userEmail);
        $token = $userToken->getToken(); // used to compare at the end
        $resetLink = "/account/forgot-password/t/" . $userToken->getToken();
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $resetLink,
            enableBasicAssertions: true,
            enableFormAssertions: false,
            expectedErrorMsg: null,
            expectedErrorCssSelector: null,
        );

        // Update the user token to an expired valid date
        $tokenLifetime = $this->tokenLifetime;
        $now = new \DateTimeImmutable();
        $createdAt = $now->setTimestamp(\time() - ($tokenLifetime + 1));
        $validTo = $createdAt->add(\DateInterval::createFromDateString("${tokenLifetime} seconds"));
        $userToken->setCreatedAt($createdAt);
        $userToken->setValidTo($validTo);
        $userTokenRepository = static::getContainer()->get(UserTokenRepository::class);
        $userTokenRepository->save($userToken, true);

        // Reload user token from database and try to display Forgot Password form
        $userToken = $this->getUserToken($userEmail);
        $this->assertEquals($token, $userToken->getToken());
        $newPlainPassword = $this->generateRandomString($this->minPasswordLength + rand(0, 15));
        $crawler = $kernelBrowser->submitForm(
            button: 'public_forgotPassword_newPasswordForm_submit',
            fieldValues: $this->getValidDataForForgotPasswordNewPasswordForm($newPlainPassword)
        );
        $this->commonCheckCanNotSendNewPasswordFormWithInvalidOrExpiredResetLink($userEmail, $currentUserPasswordHash);
        $this->assertNull($this->getUserToken($userEmail));
    }

    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form_With_invalid_reset-link
     */
    public function testAnonymousUserCanNotSendNewPasswordFormIfResetLinkNotExistInDatabase(): void
    {
//      $this->markTestIncomplete();
        $userEmail = $this->userEmail5;
        $kernelBrowser = static::createClient();
        $currentUserPasswordHash = $this->getCurrentHashOfUserPassword($userEmail); // Used to compare at the end

        // Create new token and check reset link works without error
        $userToken = $this->setNewUserToken($userEmail);
        $resetLink = "/account/forgot-password/t/" . $userToken->getToken();
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $resetLink,
            enableBasicAssertions: true,
            enableFormAssertions: false,
            expectedErrorMsg: null,
            expectedErrorCssSelector: null,
        );

        // Remove current token, send reset password form and see error "password reset link is invalid or has expired"
        $this->cleanUpToken($userToken);
        $newPlainPassword = $this->generateRandomString($this->minPasswordLength + rand(0, 15));
        $crawler = $kernelBrowser->submitForm(
            button: 'public_forgotPassword_newPasswordForm_submit',
            fieldValues: $this->getValidDataForForgotPasswordNewPasswordForm($newPlainPassword)
        );
        $this->commonCheckCanNotSendNewPasswordFormWithInvalidOrExpiredResetLink($userEmail, $currentUserPasswordHash);
        $this->assertNull($this->getUserToken($userEmail));
    }


    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form_With_Wrong_Data
     */
    public function testAnonymousUserCanNotValidNewPasswordFormWithTooSmallNewPassword(): void
    {
//      $this->markTestIncomplete();
        $userEmail = $this->userEmail5;
        $kernelBrowser = static::createClient();

        // Create new token and check reset link works without error
        $userToken = $this->setNewUserToken($userEmail);
        $resetLink = "/account/forgot-password/t/" . $userToken->getToken();
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $resetLink,
            enableBasicAssertions: true,
            enableFormAssertions: false,
            expectedErrorMsg: null,
            expectedErrorCssSelector: null,
        );
        $currentUserPasswordHash = $this->getCurrentHashOfUserPassword($userEmail); // Used to compare at the end
        $this->commonCheckWhenFormIsSentWithTooSmallRepeatPassword(
            kernelBrowser: $kernelBrowser,
            validFormData: $this->getValidDataForForgotPasswordNewPasswordForm($this->communFixtureUserPassword),
            formName: 'reset_password_form',
            fieldName: 'newPlainPassword',
            minPasswordLength: $this->minPasswordLength,
            methodNameToSendFormWithWrongData: 'sendForgotPasswordNewPasswordFormWithWrongData'
        );
        // Check that password has not been changed in database
        $this->assertSame("$currentUserPasswordHash", $this->getCurrentHashOfUserPassword($userEmail));
        $this->cleanUpToken($this->getUserToken($userEmail));
    }

    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form_With_Wrong_Data
     *
     */
    public function testAnonymousUserCanNotValidNewPasswordFormWithoutSimilarNewPasswordFields(): void
    {
//      $this->markTestIncomplete();
        $userEmail = $this->userEmail5;
        $kernelBrowser = static::createClient();

        // Create new token and check reset link works without error
        $userToken = $this->setNewUserToken($userEmail);
        $resetLink = "/account/forgot-password/t/" . $userToken->getToken();
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $resetLink,
            enableBasicAssertions: true,
            enableFormAssertions: false,
            expectedErrorMsg: null,
            expectedErrorCssSelector: null,
        );
        $currentUserPasswordHash = $this->getCurrentHashOfUserPassword($userEmail); // Used to compare at the end
        $this->commonCheckWhenFormIsSentWithoutSimilarNewPasswordFields(
            kernelBrowser: $kernelBrowser,
            validFormData: $this->getValidDataForForgotPasswordNewPasswordForm($this->communFixtureUserPassword),
            formName: 'reset_password_form',
            fieldName: 'newPlainPassword',
            minPasswordLength: $this->minPasswordLength,
            methodNameToSendFormWithWrongData: 'sendForgotPasswordNewPasswordFormWithWrongData'
        );
        // Check that password has not been changed in database
        $this->assertSame("$currentUserPasswordHash", $this->getCurrentHashOfUserPassword($userEmail));
        $this->cleanUpToken($this->getUserToken($userEmail));
    }

    /**
     * @group Form
     * @group NoConnectedUser
     * @group NoConnectedUser_Forgot_Password
     * @group NoConnectedUser_Forgot_Password_NewPassword_Form
     */
    public function testAnonymousUserCanSendNewPasswordFormAndLoginWithNewPassword(): void
    {
//      $this->markTestIncomplete();
        $userEmail = $this->userEmail5;
        $kernelBrowser = static::createClient();

        // Create new token and check reset link works without error
        $userToken = $this->setNewUserToken($userEmail);
        $resetLink = "/account/forgot-password/t/" . $userToken->getToken();
        $this->loadEmptyForgotPasswordNewPasswordForm(
            kernelBrowser: $kernelBrowser,
            resetLink: $resetLink,
            enableBasicAssertions: true,
            enableFormAssertions: false,
            expectedErrorMsg: null,
            expectedErrorCssSelector: null,
        );
        $initialUserPasswordHash = $this->getCurrentHashOfUserPassword($userEmail); // Used to compare at the end
        $newPlainPassword = $this->generateRandomString($this->minPasswordLength + rand(0, 15));
        $validDataForm = $this->getValidDataForForgotPasswordNewPasswordForm($newPlainPassword);
        $crawler = $kernelBrowser->submitForm(
            button: 'public_forgotPassword_newPasswordForm_submit',
            fieldValues: $validDataForm
        );
        $this->assertRouteSame("app_account_forgot_password_new-password");
        $this->assertResponseStatusCodeSame(Response::HTTP_SEE_OTHER); // HTTP status code = 303
        $this->assertResponseHeaderSame("Location", '/account/login');
        $crawler = $kernelBrowser->request('GET', '/account/login');
        $this->assertSelectorTextSame('h2', 'Login');
        $this->assertSelectorTextContains('.alert-success', "Your password has been successfully changed.");

        // Check that password has been changed in database
        $this->checkPasswordHasBeenChangedInDatabase("$userEmail", "$initialUserPasswordHash");

        // Logout current user and check that the user can only login with his new password
        $this->checkUserCanLoginOnlyWithNewPassword(
            kernelBrowser: $kernelBrowser,
            userEmail: $userEmail,
            newPlainPassword: $newPlainPassword,
            oldPlainPassword: $this->communFixtureUserPassword
        );

        // Reset user password with initial password
        $this->resetUserPasswordInDatabaseWithProvidedPasswordHash(
            userEmail: $userEmail,
            newUserPasswordHash: $initialUserPasswordHash
        );
    }
}
