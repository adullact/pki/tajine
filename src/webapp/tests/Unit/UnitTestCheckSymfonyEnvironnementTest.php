<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Unit;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UnitTestCheckSymfonyEnvironnementTest extends KernelTestCase
{
    public function testSymfonyEnvironnementIsTest(): void
    {
        $kernel = self::bootKernel();
        $this->assertSame('test', $kernel->getEnvironment());
        // $routerService = static::getContainer()->get('router');
        // $myCustomService = static::getContainer()->get(CustomService::class);
    }
}
