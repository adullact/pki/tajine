<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\tests\Unit\Domain\Certificate\PublicKey;

use App\Domain\Certificate\PublicKey\HexadecimalConvertor;
use PHPUnit\Framework\TestCase;

/**
 * @group allow_mutation_testing_by_infection
 */
class UnitTestHexadecimalConvertorTest extends TestCase
{
    /**
     * Converts hexadecimal to decimal
     * @group Certificate
     * @group Data_Transformer
     * @group Symfony_Service
     */
    public function testHexadecimalConvertorCanConvertsHexadecimalToDecimal(): void
    {
        $testedData = [
            [
                'input' => '0x7E3FD7CBCB43BA25FFA3B4CE63ED7940F76C596E',
                'expected' => '720756582560291965287434620192461387823693191534',
            ],
            [
                'input' => '0X7E3FD7CBCB43BA25FFA3B4CE63ED7940F76C596E',
                'expected' => '720756582560291965287434620192461387823693191534',
            ],
            [
                'input' => '7E3FD7CBCB43BA25FFA3B4CE63ED7940F76C596E',
                'expected' => '720756582560291965287434620192461387823693191534',
            ],
            [
                'input' => '0x598BF4B35B4589F9A157B800095EED6016C7687C',
                'expected' => '511221298615549317189117552054386042164472146044',
            ],
            [
                'input' => '0x64446026DCCCE0EC11689CAB7A5D6A0CA9787087',
                'expected' => '572423903759557376371086385645126551571313225863',
            ],
        ];
        foreach ($testedData as $data) {
            $output = HexadecimalConvertor::convertHexadecimalToDecimal($data['input']);
            $this->assertSame($data['expected'], $output);
        }
    }
}
