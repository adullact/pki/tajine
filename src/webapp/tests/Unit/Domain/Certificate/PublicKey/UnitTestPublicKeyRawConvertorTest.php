<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\tests\Unit\Domain\Certificate\PublicKey;

use App\Domain\Certificate\PublicKey\Dto\PublicKeyRawDto as PublicKeyRawDto;
use App\Domain\Certificate\PublicKey\Exception\FailedDecodeRawPublicKeyException;
use App\Domain\Certificate\PublicKey\PublicKeyRawConvertor;
use PHPUnit\Framework\TestCase;

class UnitTestPublicKeyRawConvertorTest extends TestCase
{
    /**
     * @group allow_mutation_testing_by_infection
     */
    public function testTryToLoadNotValidPublicKeyThrowsFailedDecodeRawPublicKey(): void
    {
        $rawPemPublicKey = "-----BEGIN CERTIFICATE-----\n (not valid public key) \n-----END CERTIFICATE-----";
        $this->expectException(FailedDecodeRawPublicKeyException::class);
        $this->expectExceptionMessage("Public key convertor: public key can't loaded");
        PublicKeyRawConvertor::convertPemCertificateToPublicKeyRawDto($rawPemPublicKey);
    }

    /**
     * @group allow_mutation_testing_by_infection
     */
    public function testLoadValidPemPublicKeyAndConvertToPublicKeyRawDto(): void
    {
        $filePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00";
        $rawPemPublicKey = file_get_contents("$filePath" . "_public.pem");
        $publicKeyRawDto = PublicKeyRawConvertor::convertPemCertificateToPublicKeyRawDto($rawPemPublicKey);
        /////////////////////////////////////////////////////////////////////////
        $expectedCertSerialNumber = '0x388855D3979C790E11EDA8C42ACDF26E8D29990E';
        $expectedCertName = '';
        $expectedCertName .= '/C=FR';
        $expectedCertName .= '/ST=Herault';
        $expectedCertName .= '/L=Montpellier';
        $expectedCertName .= '/O=Mairie de Montpellier';
        $expectedCertName .= '/OU=465146154365143651';
        $expectedCertName .= '/CN=user@example.org';
        $expectedCertSubject =         [
            "countryName" => "FR",
            "stateOrProvinceName" => "Herault",
            "localityName" => "Montpellier",
            "organizationName" => "Mairie de Montpellier",
            "organizationalUnitName" => "465146154365143651",
            "commonName" => "user@example.org",
        ];
        $expectedValidFromTimestamp = 1678377240;
        $expectedValidToTimestamp = 1709913240;
        $expectedValidFrom = new \DateTimeImmutable(date('Y-m-d H:i:s', $expectedValidFromTimestamp));
        $expectedValidTo =   new \DateTimeImmutable(date('Y-m-d H:i:s', $expectedValidToTimestamp));
        $expectedCrlUri =   'http://pki.example.org/crl.pem';
        $expectedAuthorityKeyIdentifier = 'BF:94:17:23:98:A8:AA:B1:50:97:46:CA:52:A4:1D:BD:70:DE:9A:45';
        /////////////////////////////////////////////////////////////////////////
        $this->assertInstanceOf(PublicKeyRawDto::class, $publicKeyRawDto);
        $this->assertSame("$expectedCertSerialNumber", $publicKeyRawDto->serialNumber);
        $this->assertSame("$expectedCertName", $publicKeyRawDto->name);
        $this->assertSame($expectedCertSubject, $publicKeyRawDto->subject);
        $this->assertSame($expectedValidFromTimestamp, $publicKeyRawDto->validFromTimestamp);
        $this->assertSame($expectedValidToTimestamp, $publicKeyRawDto->validToTimestamp);
        $this->assertInstanceOf('DateTimeImmutable', $expectedValidTo);
        $this->assertInstanceOf('DateTimeImmutable', $expectedValidFrom);
        $this->assertEquals($expectedValidFrom, $publicKeyRawDto->validFrom);
        $this->assertEquals($expectedValidTo, $publicKeyRawDto->validTo);
        $this->assertEquals($expectedCrlUri, $publicKeyRawDto->crlUri);
        $this->assertEquals($expectedAuthorityKeyIdentifier, $publicKeyRawDto->authorityKeyIdentifier);
    }
}
