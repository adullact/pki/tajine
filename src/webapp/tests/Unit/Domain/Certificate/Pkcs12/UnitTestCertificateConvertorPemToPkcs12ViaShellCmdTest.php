<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace Unit\Domain\Certificate\Pkcs12;

use App\Domain\Certificate\Pkcs12\Exception\InvalidPrivateKeyException;
use App\Domain\Certificate\Pkcs12\Exception\InvalidPublicKeyException;
use App\Domain\Certificate\Pkcs12\Exception\NotMatchingPrivateAndPublicKeysException;
use App\Domain\Certificate\Pkcs12\Pkcs12ConvertorViaShellCmd;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class UnitTestLegacyCertificateConvertorPemToPkcs12certViaShellCmdTest extends TestCase
{
    /**
     * Try to convert PEM certificate to PKCS12 certificat
     * with not valid public key  ---> a exception is expected
     *
     * @group allow_mutation_testing_by_infection
     */
    public function testTryToConvertPemCertificateToPkcs12WithNotValidPublicKey(): void
    {
        $filePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00";
        $rawPemPublicKey = "-----BEGIN CERTIFICATE-----\n (not valid public key) \n-----END CERTIFICATE-----";
        $rawPemPrivateKey = file_get_contents("$filePath" . "_private.pem");
        $this->expectException(InvalidPublicKeyException::class);
        $this->expectExceptionMessage("Public key can't loaded");
        Pkcs12ConvertorViaShellCmd::convertPemCertificateToPkcs12(
            rawPemPublicKey: "$rawPemPublicKey",
            rawPemPrivateKey: "$rawPemPrivateKey"
        );
    }

    /**
     * Try to convert PEM certificate to PKCS12 certificat
     * with not valid private key  ---> a exception is expected
     *
     * @group allow_mutation_testing_by_infection
     */
    public function testTryToConvertPemCertificateToPkcs12WithNotValidPrivateKey(): void
    {
        $filePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00";
        $rawPemPublicKey = file_get_contents("$filePath" . "_public.pem");
        $rawPemPrivateKey = '----BEGIN RSA PRIVATE KEY-----\n (not valid private key) \n-----END RSA PRIVATE KEY-----';
        $this->expectException(InvalidPrivateKeyException::class);
        $this->expectExceptionMessage("Private key can't loaded");
        Pkcs12ConvertorViaShellCmd::convertPemCertificateToPkcs12(
            rawPemPublicKey: "$rawPemPublicKey",
            rawPemPrivateKey: "$rawPemPrivateKey"
        );
    }

    /**
     * Try to convert PEM certificate to PKCS12 certificat
     * with private key not corresponded to public key  ---> a exception is expected
     *
     * @group allow_mutation_testing_by_infection
     */
    public function testTryToConvertPemCertificateToPkcs12WithPrivateKeyNotCorrespondedToPublicKey(): void
    {
        $filePathPublicKey = "data/certificate/cert_user@example.org_2024.03.08_16h54.00_public.pem";
        $filePathPrivateKey = "data/certificate/cert_user2@example.org_2024.03.11_22h53.00_private.pem";
        $rawPemPublicKey = file_get_contents("$filePathPublicKey");
        $rawPemPrivateKey = file_get_contents("$filePathPrivateKey");
        $this->expectException(NotMatchingPrivateAndPublicKeysException::class);
        $this->expectExceptionMessage("Private key does not correspond to public key");
        Pkcs12ConvertorViaShellCmd::convertPemCertificateToPkcs12(
            rawPemPublicKey: "$rawPemPublicKey",
            rawPemPrivateKey: "$rawPemPrivateKey"
        );
    }

    /**
     * Convert PEM certificate to PKCS12 certificate protected by a password
     *
     * 1. Convert PEM certificate to PKCS12 certificate
     * 2. Load generated PKCS12 with correct password and compare with public key of PEM certificate
     *
     * It is not possible to try to load the PKCS12 generated with wrong password,
     * as the way of checking this does not allow the use of the Infection tool (mutation tests).
     *
     * @group allow_mutation_testing_by_infection
     */
    public function testConvertPemCertificateToPkcs12WithPasswordForInfectionTool(): void
    {
        $pkcs12Password = "is.ValidPassword";
        $filePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00";
        $rawPemPublicKey = file_get_contents("$filePath" . "_public.pem");
        $rawPemPrivateKey = file_get_contents("$filePath" . "_private.pem");
        $temporaryConverterDirectory = 'var/tmp/test/cert-convertor-test/2';
        $rawPkcs12Cert = Pkcs12ConvertorViaShellCmd::convertPemCertificateToPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPemPrivateKey: $rawPemPrivateKey,
            pkcs12Password: $pkcs12Password,
            temporaryConverterDirectory: $temporaryConverterDirectory,
        );
        $this->commonTestsToLoadGeneratedPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPkcs12Cert: $rawPkcs12Cert,
            pkcs12Password: $pkcs12Password,
            temporaryConverterDirectory: $temporaryConverterDirectory,
            enableBadPasswordCheckForExecCmd: false,
        );
    }


    /**
     * Convert PEM certificate to PKCS12 certificat protected by a password
     *
     * 1. Convert PEM certificate to PKCS12 certificate
     * 2. Try to load generated PKCS12 with bad password
     * 3. Load generated PKCS12 with correct password and compare with public key of PEM certificate
     */
    public function testConvertPemCertificateToPkcs12WithPassword(): void
    {
        $pkcs12Password = "is.ValidPassword";
        $filePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00";
        $rawPemPublicKey = file_get_contents("$filePath" . "_public.pem");
        $rawPemPrivateKey = file_get_contents("$filePath" . "_private.pem");
        $rawPkcs12Cert = Pkcs12ConvertorViaShellCmd::convertPemCertificateToPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPemPrivateKey: $rawPemPrivateKey,
            pkcs12Password: $pkcs12Password
        );
        $this->commonTestsToLoadGeneratedPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPkcs12Cert: $rawPkcs12Cert,
            pkcs12Password: $pkcs12Password
        );
    }

    /**
     * Convert PEM certificate to PKCS12 certificat protected by a password with special chars
     *
     * 1. Convert PEM certificate to PKCS12 certificate
     * 2. Try to load generated PKCS12 with bad password
     * 3. Load generated PKCS12 with correct password and compare with public key of PEM certificate
     */
    public function testConvertPemCertificateToPkcs12WithSpecialCharsPassword(): void
    {
        $pkcs12Password = "pAsS_{- -  -#-\"-`-\-/-(-)-&-;-|-><-<!-'_ç.>à@é€è!§~,?^][{:#µ£\$129";
        $filePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00";
        $rawPemPublicKey = file_get_contents("$filePath" . "_public.pem");
        $rawPemPrivateKey = file_get_contents("$filePath" . "_private.pem");
        $rawPkcs12Cert = Pkcs12ConvertorViaShellCmd::convertPemCertificateToPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPemPrivateKey: $rawPemPrivateKey,
            pkcs12Password: $pkcs12Password
        );
        $this->commonTestsToLoadGeneratedPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPkcs12Cert: $rawPkcs12Cert,
            pkcs12Password: $pkcs12Password,
            enabledNativePhpCheck: false,
        );
    }


    /**
     * Convert PEM certificate to PKCS12 certificat without password
     *
     * 1. Convert PEM certificate to PKCS12 certificate
     * 2. Try to load generated PKCS12 with bad password
     * 3. Load generated PKCS12 with correct password and compare with public key of PEM certificate
     */
    public function testConvertPemCertificateToPkcs12WithoutPassword(): void
    {
        $filePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00";
        $rawPemPublicKey = file_get_contents("$filePath" . "_public.pem");
        $rawPemPrivateKey = file_get_contents("$filePath" . "_private.pem");
        $rawPkcs12Cert = Pkcs12ConvertorViaShellCmd::convertPemCertificateToPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPemPrivateKey: $rawPemPrivateKey
        );
        $this->commonTestsToLoadGeneratedPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPkcs12Cert: $rawPkcs12Cert,
            pkcs12Password: ""
        );
    }


    /**
     * Convert PEM certificate to PKCS12 certificat with empty password
     *
     * 1. Convert PEM certificate to PKCS12 certificate
     * 2. Try to load generated PKCS12 with bad password
     * 3. Load generated PKCS12 with correct password and compare with public key of PEM certificate
     */
    public function testConvertPemCertificateToPkcs12WithEmptyPassword(): void
    {
        $filePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00";
        $rawPemPublicKey = file_get_contents("$filePath" . "_public.pem");
        $rawPemPrivateKey = file_get_contents("$filePath" . "_private.pem");
        $rawPkcs12Cert = Pkcs12ConvertorViaShellCmd::convertPemCertificateToPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPemPrivateKey: $rawPemPrivateKey,
            pkcs12Password: ""
        );
        $this->commonTestsToLoadGeneratedPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPkcs12Cert: $rawPkcs12Cert,
            pkcs12Password: ""
        );
    }


    /**
     * Common tests to load generated PKCS12
     *
     * 1. Load generated PKCS12 with correct password and compare with public key of PEM certificate
     * 2. Try to load generated PKCS12 with bad password
     */
    private function commonTestsToLoadGeneratedPkcs12(
        string $rawPemPublicKey,
        string $rawPkcs12Cert,
        string $pkcs12Password,
        string|null $temporaryConverterDirectory = null,
        bool $enableBadPasswordCheckForExecCmd = true,
        bool $enabledNativePhpCheck = true,
    ): void {

        // OPENSSL_VERSION_TEXT     OpenSSL 3.0.2 15 Mar 2022 ---> Ubuntu 22.04
        //                          OpenSSL 1.1.1 11 Sep 2018 ---> Ubuntu 18.04
        if ($enabledNativePhpCheck && str_starts_with(OPENSSL_VERSION_TEXT, 'OpenSSL 1.')) {  // @@@TODO refactor
            // Load PKCS12 with correct password and compare public key
            $pkc12load = openssl_pkcs12_read(
                "$rawPkcs12Cert",
                $pkcs12Data,
                "$pkcs12Password"
            );
            self::AssertSame(true, $pkc12load);
            self::AssertSame($rawPemPublicKey, $pkcs12Data['cert']);
//          self::AssertSame($rawPemPrivateKey, $pkcs12Data['pkey']);  // we can not compare private key

            // Try to load PKCS12 with bad password
            $pkc12loadWithBadPassword = openssl_pkcs12_read(
                "$rawPkcs12Cert",
                $pkcs12DataWithBadPassword,
                "isNot.ValidPassword"
            );
            self::AssertSame(false, $pkc12loadWithBadPassword);
            self::AssertSame(false, isset($pkcs12DataWithBadPassword));
        }

        ///////////////////////////////////////////////////////////////
        $legacyOption = "-legacy";
        if (str_starts_with(OPENSSL_VERSION_TEXT, 'OpenSSL 1.')) {             // @@@TODO refactor
            // OPENSSL_VERSION_TEXT     OpenSSL 3.0.2 15 Mar 2022 ---> Ubuntu 22.04
            //                          OpenSSL 1.1.1 11 Sep 2018 ---> Ubuntu 18.04
            $legacyOption = "";
        }
        ///////////////////////////////////////////////////////////////
        $filesystem = new Filesystem();
        $temporaryTestDirectory = "./var/tmp/test/";
        if (!is_dir($temporaryTestDirectory)) {
            $filesystem->mkdir($temporaryTestDirectory);
        } elseif (!is_writable($temporaryTestDirectory)) {
            throw new \Exception("Can not write to [ $temporaryTestDirectory ] dir");
        }
        $pkcs12TmpFile = $filesystem->tempnam("$temporaryTestDirectory", 'cert_pkcs12_tmp_', '.p12');
        $pemTmpFile = $filesystem->tempnam("$temporaryTestDirectory", 'cert_pem_tmp_', '.pem');
        $filesystem->dumpFile("$pkcs12TmpFile", "$rawPkcs12Cert");
        ///////////////////////////////////////////////////////////////

        // Load PKCS12 with correct password and compare public key
        $cmd = "";
        $cmd .= "openssl pkcs12 -nodes ";
        $cmd .= "                $legacyOption";
        $cmd .= "               -passin pass:" . escapeshellarg("$pkcs12Password");
        $cmd .= "               -in  " . escapeshellarg("$pkcs12TmpFile");
        $cmd .= "               -out " . escapeshellarg("$pemTmpFile");
        exec("$cmd", $cmdOutput, $cmdResultcode);
        if ($cmdResultcode !== 0) {
            $filesystem->remove(["$pemTmpFile", "$pkcs12TmpFile",]);
            throw new \Exception("Read PKCS12 certificate via OpenSSL CLI didn't work");
        }
        $outputPemGlobalRaw = file_get_contents($pemTmpFile);

        $searchPublicKey = strstr("$outputPemGlobalRaw", "$rawPemPublicKey", true);
        self::assertIsNotBool($searchPublicKey);
        self::assertNotSame(false, $searchPublicKey);
        self::assertIsString($searchPublicKey); // Public key was found
        $filesystem->remove(["$pemTmpFile"]);
        if (!is_null($temporaryConverterDirectory)) {
            $finder = new Finder();
            $finder->files()->in("$temporaryConverterDirectory");
            $this->assertSame(
                expected: false,
                actual: $finder->hasResults(),
                message: "Temporary converter directory is not empty ---> $temporaryConverterDirectory"
            );
        }

        // Try to load PKCS12 with bad password
        if ($enableBadPasswordCheckForExecCmd) {
            $pemTmpFile2 = $filesystem->tempnam("$temporaryTestDirectory", 'cert_pem_tmp2_', '.pem');
            $cmd = "";
            $cmd .= "openssl pkcs12 -nodes ";
            $cmd .= "                $legacyOption";
            $cmd .= "               -passin pass:isNot.ValidPassword ";
            $cmd .= "               -in  " . escapeshellarg("$pkcs12TmpFile");
            $cmd .= "               -out " . escapeshellarg("$pemTmpFile");
            exec("$cmd", $cmdOutput, $cmdResultcode);
            self::assertNotSame(0, $cmdResultcode);
            self::assertSame(1, $cmdResultcode);
            self::assertSame('', file_get_contents("$pemTmpFile2"));
            $filesystem->remove(["$pemTmpFile2", "$pemTmpFile"]);
        }
        $filesystem->remove(["$pkcs12TmpFile",]);
        $filesystem->remove(["$temporaryConverterDirectory",]);
    }
}
