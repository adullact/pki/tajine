<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\tests\Unit\Domain\Certificate\Pkcs12;

use App\Domain\Certificate\Pkcs12\Exception\InvalidPrivateKeyException;
use App\Domain\Certificate\Pkcs12\Exception\InvalidPublicKeyException;
use App\Domain\Certificate\Pkcs12\Exception\NotMatchingPrivateAndPublicKeysException;
use App\Domain\Certificate\Pkcs12\Pkcs12HelperTrait;
use PHPUnit\Framework\TestCase;

/**
 * @group allow_mutation_testing_by_infection
 */
class UnitTestCertificateConvertorHelperTest extends TestCase
{
    use Pkcs12HelperTrait;

    public function testTryToLoadNotValidPublicKey(): void
    {
        $rawPemPublicKey = "-----BEGIN CERTIFICATE-----\n (not valid public key) \n-----END CERTIFICATE-----";
        $this->expectException(InvalidPublicKeyException::class);
        $this->expectExceptionMessage("Public key can't loaded");
        self::loadPublicKeyObject($rawPemPublicKey);
    }

    public function testTryToLoadNotValidPrivateKey(): void
    {
        $rawPemPrivateKey = '----BEGIN RSA PRIVATE KEY-----\n (not valid private key) \n-----END RSA PRIVATE KEY-----';
        $this->expectException(InvalidPrivateKeyException::class);
        $this->expectExceptionMessage("Private key can't loaded");
        self::loadPrivateKeyObject($rawPemPrivateKey);
    }

    public function testLoadValidPublicKey(): void
    {
        $filePathPublicKey = "data/certificate/cert_user@example.org_2024.03.08_16h54.00_public.pem";
        $rawPemPublicKey = file_get_contents("$filePathPublicKey");
        $publicKey = self::loadPublicKeyObject($rawPemPublicKey);
        $this->assertIsObject($publicKey);
    }

    public function testLoadValidPrivateKey(): void
    {
        $filePathPrivateKey = "data/certificate/cert_user@example.org_2024.03.08_16h54.00_private.pem";
        $rawPemPrivateKey = file_get_contents("$filePathPrivateKey");
        $privateKey = self::loadPrivateKeyObject($rawPemPrivateKey);
        $this->assertIsObject($privateKey);
    }

    public function testCheckPrivateKeyNotCorrespondedToPublicKey(): void
    {
        $filePathPublicKey = "data/certificate/cert_user@example.org_2024.03.08_16h54.00_public.pem";
        $filePathPrivateKey = "data/certificate/cert_user2@example.org_2024.03.11_22h53.00_private.pem";
        $rawPemPublicKey = file_get_contents("$filePathPublicKey");
        $rawPemPrivateKey = file_get_contents("$filePathPrivateKey");
        $this->expectException(NotMatchingPrivateAndPublicKeysException::class);
        $this->expectExceptionMessage("Private key does not correspond to public key");
        self::checkThatPublicKeyMatchesPrivateKey("$rawPemPublicKey", "$rawPemPrivateKey");
    }

    public function testCheckPrivateKeyCorrespondedToPublicKey(): void
    {
        $filePathPublicKey = "data/certificate/cert_user@example.org_2024.03.08_16h54.00_public.pem";
        $filePathPrivateKey = "data/certificate/cert_user@example.org_2024.03.08_16h54.00_private.pem";
        $rawPemPublicKey = file_get_contents("$filePathPublicKey");
        $rawPemPrivateKey = file_get_contents("$filePathPrivateKey");
        $result = self::checkThatPublicKeyMatchesPrivateKey("$rawPemPublicKey", "$rawPemPrivateKey");
        $this->assertTrue($result);
    }
}
