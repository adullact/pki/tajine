<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\Unit\Domain\Certificate\Pkcs12;

use App\Domain\Certificate\Pkcs12\Exception\InvalidPrivateKeyException;
use App\Domain\Certificate\Pkcs12\Exception\InvalidPublicKeyException;
use App\Domain\Certificate\Pkcs12\Exception\NotMatchingPrivateAndPublicKeysException;
use App\Domain\Certificate\Pkcs12\Pkcs12Convertor;
use PHPUnit\Framework\TestCase;

/**
 * @group allow_mutation_testing_by_infection
 */
class UnitTestCertificateConvertorPemToPkcs12Test extends TestCase
{
    /**
     * Try to convert PEM certificate to PKCS12 certificat
     * with not valid public key  ---> a exception is expected
     */
    public function testTryToConvertPemCertificateToPkcs12WithNotValidPublicKey(): void
    {
        $filePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00";
        $rawPemPublicKey = "-----BEGIN CERTIFICATE-----\n (not valid public key) \n-----END CERTIFICATE-----";
        $rawPemPrivateKey = file_get_contents("$filePath" . "_private.pem");
        $this->expectException(InvalidPublicKeyException::class);
        $this->expectExceptionMessage("Public key can't loaded");
        Pkcs12Convertor::convertPemCertificateToPkcs12(
            rawPemPublicKey: "$rawPemPublicKey",
            rawPemPrivateKey: "$rawPemPrivateKey"
        );
    }

    /**
     * Try to convert PEM certificate to PKCS12 certificat
     * with not valid private key  ---> a exception is expected
     */
    public function testTryToConvertPemCertificateToPkcs12WithNotValidPrivateKey(): void
    {
        $filePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00";
        $rawPemPublicKey = file_get_contents("$filePath" . "_public.pem");
        $rawPemPrivateKey = '----BEGIN RSA PRIVATE KEY-----\n (not valid private key) \n-----END RSA PRIVATE KEY-----';
        $this->expectException(InvalidPrivateKeyException::class);
        $this->expectExceptionMessage("Private key can't loaded");
        Pkcs12Convertor::convertPemCertificateToPkcs12(
            rawPemPublicKey: "$rawPemPublicKey",
            rawPemPrivateKey: "$rawPemPrivateKey"
        );
    }

    /**
     * Try to convert PEM certificate to PKCS12 certificat
     * with private key not corresponded to public key  ---> a exception is expected
     */
    public function testTryToConvertPemCertificateToPkcs12WithPrivateKeyNotCorrespondedToPublicKey(): void
    {
        $filePathPublicKey = "data/certificate/cert_user@example.org_2024.03.08_16h54.00_public.pem";
        $filePathPrivateKey = "data/certificate/cert_user2@example.org_2024.03.11_22h53.00_private.pem";
        $rawPemPublicKey = file_get_contents("$filePathPublicKey");
        $rawPemPrivateKey = file_get_contents("$filePathPrivateKey");
        $this->expectException(NotMatchingPrivateAndPublicKeysException::class);
        $this->expectExceptionMessage("Private key does not correspond to public key");
        Pkcs12Convertor::convertPemCertificateToPkcs12(
            rawPemPublicKey: "$rawPemPublicKey",
            rawPemPrivateKey: "$rawPemPrivateKey"
        );
    }

    /**
     * Convert PEM certificate to PKCS12 certificat without password
     *
     * 1. Convert PEM certificate to PKCS12 certificate
     * 2. Try to load generated PKCS12 with bad password
     * 3. Load generated PKCS12 with correct password and compare with public key of PEM certificate
     */
    public function testConvertPemCertificateToPkcs12WithoutPassword(): void
    {
        $filePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00";
        $rawPemPublicKey = file_get_contents("$filePath" . "_public.pem");
        $rawPemPrivateKey = file_get_contents("$filePath" . "_private.pem");
        $rawPkcs12Cert = Pkcs12Convertor::convertPemCertificateToPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPemPrivateKey: $rawPemPrivateKey
        );
        $this->commonTestsToLoadGeneratedPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPkcs12Cert: $rawPkcs12Cert,
            pkcs12Password: ""
        );
    }


    /**
     * Convert PEM certificate to PKCS12 certificat with empty password
     *
     * 1. Convert PEM certificate to PKCS12 certificate
     * 2. Try to load generated PKCS12 with bad password
     * 3. Load generated PKCS12 with correct password and compare with public key of PEM certificate
     */
    public function testConvertPemCertificateToPkcs12WithEmptyPassword(): void
    {
        $filePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00";
        $rawPemPublicKey = file_get_contents("$filePath" . "_public.pem");
        $rawPemPrivateKey = file_get_contents("$filePath" . "_private.pem");
        $rawPkcs12Cert = Pkcs12Convertor::convertPemCertificateToPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPemPrivateKey: $rawPemPrivateKey,
            pkcs12Password: ""
        );
        $this->commonTestsToLoadGeneratedPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPkcs12Cert: $rawPkcs12Cert,
            pkcs12Password: ""
        );
    }


    /**
     * Convert PEM certificate to PKCS12 certificat protected by a password
     *
     * 1. Convert PEM certificate to PKCS12 certificate
     * 2. Try to load generated PKCS12 with bad password
     * 3. Load generated PKCS12 with correct password and compare with public key of PEM certificate
     */
    public function testConvertPemCertificateToPkcs12WithPassword(): void
    {
        $pkcs12Password = "is.ValidPassword";
        $filePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00";
        $rawPemPublicKey = file_get_contents("$filePath" . "_public.pem");
        $rawPemPrivateKey = file_get_contents("$filePath" . "_private.pem");
        $rawPkcs12Cert = Pkcs12Convertor::convertPemCertificateToPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPemPrivateKey: $rawPemPrivateKey,
            pkcs12Password: $pkcs12Password
        );
        $this->commonTestsToLoadGeneratedPkcs12(
            rawPemPublicKey: $rawPemPublicKey,
            rawPkcs12Cert: $rawPkcs12Cert,
            pkcs12Password: $pkcs12Password
        );
    }


    /**
     * Common tests to load generated PKCS12
     *
     * 1. Try to load generated PKCS12 with bad password
     * 2. Load generated PKCS12 with correct password and compare with public key of PEM certificate
     */
    private function commonTestsToLoadGeneratedPkcs12(
        string $rawPemPublicKey,
        string $rawPkcs12Cert,
        string $pkcs12Password
    ): void {

        // Try to load PKCS12 with bad password
        $pkc12loadWithBadPassword = openssl_pkcs12_read(
            "$rawPkcs12Cert",
            $pkcs12DataWithBadPassword,
            "isNot.ValidPassword"
        );
        self::AssertSame(false, $pkc12loadWithBadPassword);
        self::AssertSame(false, isset($pkcs12DataWithBadPassword));

        // Load PKCS12 with correct password and compare public key
        $pkc12load = openssl_pkcs12_read(
            "$rawPkcs12Cert",
            $pkcs12Data,
            "$pkcs12Password"
        );
        self::AssertSame(true, $pkc12load);
        self::AssertSame($rawPemPublicKey, $pkcs12Data['cert']);
//      self::AssertSame($rawPemPrivateKey, $pkcs12Data['pkey']);  // we can not compare private key
    }
}
