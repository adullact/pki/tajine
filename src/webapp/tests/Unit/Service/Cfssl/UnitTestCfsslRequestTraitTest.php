<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\tests\Unit\Service\Cfssl;

use App\Service\Cfssl\CfsslRequestTrait;
use App\Service\Cfssl\Exception\FailedHttpRequestToCfsslServerException;
use App\Service\Cfssl\Exception\InvalidCfsslEndpointException;
use App\Service\Cfssl\Exception\FailedCfsslRequestException;
use App\Service\Cfssl\Exception\InvalidHttpMethodException;
use PHPUnit\Framework\TestCase;

/**
 * @group allow_mutation_testing_by_infection
 */
class UnitTestCfsslRequestTraitTest extends TestCase
{
    use CfsslRequestTrait;

    public function invalidUrlProvider()
    {
        return [
            ['https://cfssl. example.org'],  // no valid host
            ['example.org'],                 // no scheme
            ['//example.org'],               // no scheme
            ['://example.org'],              // no scheme
            ['/directory/sub-directory/'],   // no scheme, no host
            ['ftp://example.org'],           // only HTTP and HTTPS is allowed
            ['http://example.org#anchor'],   // fragement is forbidden
            ['http://example.org?query'],    // query is forbidden
            ['http:///cfssl.example.org'],   // parse() return FALSE
        ];
    }

    public function invalidEndpointNameProvider()
    {
        return [
            ['not-valid-endpoint-name'],  // no valid CFSSL endpoint name
            ['certinfo'],                 // CFSSL endpoint not yet implemented, but planned
            ['health'],                   // CFSSL endpoint not yet implemented, but planned
            ['info'],                    // CFSSL endpoint not yet implemented, but planned
            ['revoke'],                  // CFSSL endpoint not yet implemented, but planned
            ['scan'],                    // CFSSL endpoint not implemented and not planned
        ];
    }

    public function testGetCfsslEndpointUrlWithValidEndpointName(): void
    {
        $cfsslSerer = "https://cfssl.example.org";
        $this->setCfsslApiServer("$cfsslSerer");
        self::assertSame(
            "$cfsslSerer/api/v1/cfssl/newcert",
            $this->getCfsslEndpointUrl('newcert')
        );
    }

    public function testGetCfsslEndpointHttpMethodWithValidEndpointName(): void
    {
        $cfsslSerer = "https://cfssl.example.org";
        $this->setCfsslApiServer("$cfsslSerer");
        self::assertSame("POST", $this->getCfsslEndpointHttpMethod('newcert'));
    }

    /**
     * @dataProvider invalidEndpointNameProvider
     */
    public function testGetCfsslEndpointUrlWithInvalidEndpointNameThrowsInvalidCfsslEndpointException(
        string $invalidEndpointName
    ): void {
        $cfsslSerer = "https://cfssl.example.org";
        $this->setCfsslApiServer("$cfsslSerer");
        $this->expectException(InvalidCfsslEndpointException::class);
        $this->expectExceptionMessage("CFSSL endpoint is not implemented: [ $invalidEndpointName ]");
        $this->getCfsslEndpointUrl("$invalidEndpointName");
    }

    /**
     * @dataProvider invalidEndpointNameProvider
     */
    public function testGetCfsslEndpointHttpMethodWithInvalidEndpointNameThrowsInvalidCfsslEndpointException(
        string $invalidEndpointName
    ): void {
        $cfsslSerer = "https://cfssl.example.org";
        $this->setCfsslApiServer("$cfsslSerer");
        $this->expectException(InvalidCfsslEndpointException::class);
        $this->expectExceptionMessage("CFSSL endpoint is not implemented: [ $invalidEndpointName ]");
        $this->getCfsslEndpointHttpMethod("$invalidEndpointName");
    }

    public function testAllCfsslUrlAreValid(): void
    {
        $cfsslServers = [];
        $cfsslServers[0]['input'] = 'https://cfssl-1.example.org:8081';
        $cfsslServers[0]['expected'] = 'https://cfssl-1.example.org:8081';
        $cfsslServers[1]['input'] = 'https://cfssl.example.org:8081/';
        $cfsslServers[1]['expected'] = 'https://cfssl.example.org:8081';
        $cfsslServers[2]['input'] = 'https://cfssl.example.org:8081/directory/sub-directory/';
        $cfsslServers[2]['expected'] = 'https://cfssl.example.org:8081/directory/sub-directory';
        $cfsslServers[3]['input'] = '   https://cfssl.example.org:8081/   ';
        $cfsslServers[3]['expected'] = 'https://cfssl.example.org:8081';
        foreach ($cfsslServers as $key => $data) {
            $serverImput = $data['input'];
            $serverExpected = $data['expected'];
            $this->setCfsslApiServer("$serverImput");
            self::assertSame("$serverExpected", $this->getCfsslApiServer());
            self::assertSame("POST", $this->getCfsslEndpointHttpMethod('newcert'));
            self::assertSame(
                "$serverExpected/api/v1/cfssl/newcert",
                $this->getCfsslEndpointUrl('newcert')
            );
        }
    }

    /**
     * @dataProvider invalidUrlProvider
     */
    public function testInvalidCfsslServerUrlThrowsInvalidCfsslEndpointException(string $url): void
    {
            $this->expectException(InvalidCfsslEndpointException::class);
            $this->expectExceptionMessage("Invalid CFSSL server URL");
            $this->setCfsslApiServer("$url");
    }

    public function testDecodeCfsslResponse(): void
    {
        $jsonRaw = '{   "success":true,
                        "result":{
                            "certificate":"-----BEGIN CERTIFICATE-----\n ... \n-----END CERTIFICATE-----",
                            "usages":["client auth"],
                            "expiry":"8760h"    },
                        "errors":[],
                        "messages":[]    }';
        $json = $this->decodeCfsslResponse(jsonRaw: $jsonRaw);
        $this->assertSame(true, $json['success']);
        $this->assertSame([], $json['errors']);
        $this->assertSame([], $json['messages']);
        $this->assertSame('8760h', $json['result']['expiry']);
        $this->assertSame(['client auth'], $json['result']['usages']);
        $this->assertSame(
            "-----BEGIN CERTIFICATE-----\n ... \n-----END CERTIFICATE-----",
            $json['result']['certificate']
        );
    }

    /**
     * @dataProvider invalidCfsslResponseProvider
     */
    public function testInvalidCfsslResponseThrowsInvalidCfsslResponseException(
        string $jsonRaw,
        int|null $cfsslErrorCode = null,
        string|null $cfsslError = null,
    ): void {
        $this->expectException(FailedCfsslRequestException::class);
        $expectedExceptionMessage = "Invalid CFSSL response: unknown error";
        if (!\is_null($cfsslErrorCode)) {
            $expectedExceptionMessage = "Invalid CFSSL response: $cfsslErrorCode - $cfsslError";
        } elseif (!\is_null($cfsslError)) {
            $expectedExceptionMessage = "Invalid CFSSL response: $cfsslError";
        }
        $this->expectExceptionMessage("$expectedExceptionMessage");
        $this->decodeCfsslResponse(jsonRaw: $jsonRaw);
    }

    public function invalidCfsslResponseProvider()
    {
        return [
            ['{{{', null, 'unable to decode JSON response [ Syntax error ]'],
            ['{}', null, 'missing [ sucess ] property'],
            ['{ "success":true }', null, 'missing [ result ] property'],
            ['{ "success":false }'],
            [
                '{  "success":false,
                    "result":null,
                    "errors":[{"code":405,"message":"Method is not allowed:\"GET\""}],
                    "messages":[]   }',
                405,
                'Method is not allowed:"GET"'
            ],
            [
                '{  "success":false,
                    "result":null,
                    "errors":[{"code":400,"message":"unexpected end of JSON input"}],
                    "messages":[]   }',
                400,
                'unexpected end of JSON input'
            ],
//            [
//                '{  "success":false,
//                    "result":null,
//                    "errors":[{"code":400,"message":"invalid character (...)"}],
//                    "messages":[]    }',
//                400,
//                'invalid character (...)'
//            ],
            [
                '{  "success":false,
                    "result":null,
                    "errors":[{"code":2400,"message":"invalid algorithm"}],
                    "messages":[]   }',
                2400,
                'invalid algorithm'
            ],
//            [
//                '{  "success":false,
//                    "result":null,
//                    "errors":[{"code":2400,"message":"RSA key size too large"}],
//                    "messages":[]   }',
//                2400,
//                'RSA key size too large'
//            ],
//            [
//                '{  "success":false,
//                    "result":null,
//                    "errors":[{"code":2400,"message":"RSA key is too weak"}],
//                    "messages":[]   }',
//                2400,
//                'RSA key is too weak'
//            ],
        ];
    }

    public function testCfsslRequestWithInvalidServerThrowsFailedHttpRequestToCfsslServerException(): void
    {
        $this->setCfsslApiServer("https://cfssl.example.org");
        $this->expectException(FailedHttpRequestToCfsslServerException::class);
        $this->expectExceptionMessage("CFSSL - HTTP request failed [ https://cfssl.example.org/api/v1/cfssl/newcert ]");
        $rawCfsslResponse = $this->loadCfsslRequest(
            endpointName: 'newcert',
            jsonPayload: '{}'
        );
    }

    public function testCfsslRequestWithInvalidHttpMethodThrowsInvalidHttpMethodException(): void
    {
        $this->setCfsslApiServer($_ENV['CFSSL_API_SERVER']);
        $endpointName = 'newcert';
        $this->cfsslEndpoints["$endpointName"]['method'] = 'PUT';
        $this->expectException(InvalidHttpMethodException::class);
        $this->expectExceptionMessage("HTTP method [ PUT ] is not allowed");
        $rawCfsslResponse = $this->loadCfsslRequest(
            endpointName: "$endpointName",
            jsonPayload: '{}'
        );
    }

//    public function testCfsslRequest(): void {
//        $this->setCfsslApiServer($_ENV['CFSSL_API_SERVER']);
//        $rawCfsslResponse = $this->loadCfsslRequest(
//            endpointName: 'newcert',
//            jsonPayload: '{}',
//        );
//    }
}
