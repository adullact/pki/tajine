<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace Unit\Service\Cfssl;

use App\Service\Certificate\CertificateDataFormatter;
use App\Service\Cfssl\CfsslJsonPayloadTrait;
use App\Service\Cfssl\Exception\InvalidAlgorithmException;
use App\Service\Cfssl\Exception\KeySizeRangeException;
use PHPUnit\Framework\TestCase;

/**
 * @group allow_mutation_testing_by_infection
 */
class UnitTestCfsslJsonPayloadTraitTest extends TestCase
{
    use CfsslJsonPayloadTrait;


    private function commonInputCfsslNewCertficate(): array
    {
        return [
            'countryName' => 'FR',
            'stateOrProvinceName' => 'Hérault',
            'localityName' => 'Montpellier',
            'organizationName' => 'Mairie de Montpellier',
            'organizationalUnitName' => 'Service Centraux de la ville de Montpellier',
            'commonName' => 'user@example.org',
        ];
    }

    private function commonExpectedCfsslNewCertficate(): array
    {
        return [
            'key_algo' => 'rsa',
            'key_size' => $this->getRsaKeySizeMin(),
            'hosts' => [0 => ''],
            'countryName' => 'FR',
            'stateOrProvinceName' => 'HERAULT',
            'localityName' => 'MONTPELLIER',
            'organizationName' => 'MAIRIE-DE-MONTPELLIER',
            'organizationalUnitName' => 'SERVICE-CENTRAUX-DE-LA-VILLE-DE-MONTPELLIER',
            'commonName' => 'USER@EXAMPLE.ORG',
            'jsonSha256' => '065864ace109a77b3746138b947eaa727287405bf695ae1b67876cabd8878ae6',
        ];
    }


    public function testIsAllowedAlgo(): void
    {
        // Currently, CFSSL supports only ECDSA, RSA and ed25519 algorithms.
        // Currently, Tajine supports only RSA algorithm.
        $this->assertSame(true, $this->isAllowedAlgo('rsa'));
        $this->assertSame(false, $this->isAllowedAlgo('ecdsa')); // not yet implemented
        $this->assertSame(false, $this->isAllowedAlgo('ed25519')); // not yet implemented
        $this->assertSame(false, $this->isAllowedAlgo('badAlgo'));
    }

    public function testGetAllowedAlgo(): void
    {
        $this->assertSame(['rsa'], $this->getAllowedAlgo());
    }

    public function testGetRsaKeySizeMin(): void
    {
        $this->assertSame(2048, $this->getRsaKeySizeMin());
    }

    public function testGetRsaKeySizeMax(): void
    {
        $this->assertSame(8192, $this->getRsaKeySizeMax());
    }


    public function testGetCfsslNewCertificateJsonPayload(): void
    {
        $this->certificateDataFormatter = new CertificateDataFormatter();
        $commonInput = $this->commonInputCfsslNewCertficate();
        $commonExpected = $this->commonExpectedCfsslNewCertficate();
        $inputWithSpaceAndSpecialChars = " àa  éêèe   cç  d'où   456 \n\t à  @  û~    € bG     ";
        $expectedForInputWithSpaceAndSpecialChars = "AA-EEEE-CC-D-OU-456-A-U-EUR-BG";
        $expectedNoSluggedForInputWithSpaceAndSpecialChars = "ÀA ÉÊÈE CÇ D OÙ 456 À @ Û~ € BG";
        $testedData = [
            [ 'inputData' =>  $commonInput, 'expected' => $commonExpected ],
            [
                'inputData' => array_merge($commonInput, [
                    'countryName' =>  $inputWithSpaceAndSpecialChars,
                    'stateOrProvinceName' =>   $inputWithSpaceAndSpecialChars,
                    'localityName' =>   $inputWithSpaceAndSpecialChars,
                    'organizationName' =>   $inputWithSpaceAndSpecialChars,
                    'organizationalUnitName' =>   $inputWithSpaceAndSpecialChars,
                    'commonName' =>   $inputWithSpaceAndSpecialChars,
                ]),
                'expected' => array_merge($commonExpected, [
                    'countryName' =>  $expectedForInputWithSpaceAndSpecialChars,
                    'stateOrProvinceName' =>   $expectedForInputWithSpaceAndSpecialChars,
                    'localityName' =>   $expectedForInputWithSpaceAndSpecialChars,
                    'organizationName' =>   $expectedForInputWithSpaceAndSpecialChars,
                    'organizationalUnitName' =>   $expectedForInputWithSpaceAndSpecialChars,
                    'commonName' =>   $expectedNoSluggedForInputWithSpaceAndSpecialChars,
                    'jsonSha256' => 'e9fc4756ee7c6abfe0c07dffb3bcae250ee86c024f8d260f56b7cd93cf0ee877',
                ]),
            ],
        ];
        $dataVariations = [
            [
                'key' => 'stateOrProvinceName',
                'imput' => "Côtes d'Armor",
                'expected' => 'COTES-D-ARMOR',
                'jsonSha256' => '94d168b25754d01397149e723dd34354aac7846709f55f47872506df2a679d6a',
            ],
            [
                'key' => 'commonName',
                'imput' => ' hélène@exemple.org ',
                'expected' => 'HÉLÈNE@EXEMPLE.ORG',
                'jsonSha256' => 'ccab3fecea999011cbf361a2181ae7f59aad0b3a19b8bd872871eb8cdc42151e',
            ],
            [
                'key' => 'key_size',
                'imput' =>  $this->getRsaKeySizeMin(),
                'expected' => $this->getRsaKeySizeMin(),
                'jsonSha256' =>  '065864ace109a77b3746138b947eaa727287405bf695ae1b67876cabd8878ae6',
            ],
            [
                'key' => 'key_size',
                'imput' =>  $this->getRsaKeySizeMin() * 2,
                'expected' => $this->getRsaKeySizeMin() * 2,
                'jsonSha256' =>  '1f25d74f028aff2f5f7602146983fdc6c0c95cf11200a48bff7d90a15a99ea2d',
            ],
            [
                'key' => 'key_size',
                'imput' =>  $this->getRsaKeySizeMax(),
                'expected' => $this->getRsaKeySizeMax(),
                'jsonSha256' =>  'f07ba4c662be18c70f3696e8f6bb39e58866a612e6bfde1a3bd30dc486a91859',
            ],
            [
                'key' => 'localityName',
                'imput' =>  '   ] } :     [ , {  "no valid\'characters" : ["  ',
                'expected' => 'NO-VALID-CHARACTERS',
                'jsonSha256' =>  'e6ee163001d01d891b065ca0ef41471bf6957e1945a8e3ed904bf07e033d65fb',
            ],
            [
                'key' => 'commonName',
                'imput' =>  '   ] } :     [ , {  "no valid\'characters" : ["  ',
                'expected' => 'NO VALID CHARACTERS',
                'jsonSha256' =>  '4372ceae52ee8ca6140612ea8fec5dd554f738748bbee052cc2dcf8bba4d16ed',
            ],
        ];
        foreach ($dataVariations as $variation) {
            $testedData[] = [
                'inputData' => array_merge($commonInput, [ $variation['key'] => $variation['imput']]),
                'expected' => array_merge($commonExpected, [
                    $variation['key'] => $variation['expected'],
                    'jsonSha256' => $variation['jsonSha256']
                ]),
            ];
        }

        foreach ($testedData as $key => $data) {
            $inputData = $data['inputData'];
            $expectedData = $data['expected'];
            if (isset($inputData ['key_size'])) {
                $jsonPayload = $this->getCfsslNewCertificateJsonPayload(
                    countryName: $inputData['countryName'],
                    stateOrProvinceName: $inputData['stateOrProvinceName'],
                    localityName: $inputData['localityName'],
                    organizationName: $inputData['organizationName'],
                    organizationalUnit: $inputData['organizationalUnitName'],
                    commonName: $inputData['commonName'],
                    keySize: $inputData ['key_size']
                );
            } else {
                $jsonPayload = $this->getCfsslNewCertificateJsonPayload(
                    countryName: $inputData['countryName'],
                    stateOrProvinceName: $inputData['stateOrProvinceName'],
                    localityName: $inputData['localityName'],
                    organizationName: $inputData['organizationName'],
                    organizationalUnit: $inputData['organizationalUnitName'],
                    commonName: $inputData['commonName'],
                );
            }
//          $json = \json_decode($jsonPayload, false, 512, JSON_THROW_ON_ERROR);
            $json = \json_decode($jsonPayload);
            $this->assertNotNull($json, 'json_decode() fail');
            $this->assertSame($expectedData['key_algo'], $json->request->key->algo);
            $this->assertSame($expectedData['key_size'], $json->request->key->size);
            $this->assertSame($expectedData['hosts'], $json->request->hosts);
            $this->assertCount(1, $json->request->names);
            $this->assertSame($expectedData['countryName'], $json->request->names[0]->C);
            $this->assertSame($expectedData['stateOrProvinceName'], $json->request->names[0]->ST);
            $this->assertSame($expectedData['localityName'], $json->request->names[0]->L);
            $this->assertSame($expectedData['organizationName'], $json->request->names[0]->O);
            $this->assertSame($expectedData['organizationalUnitName'], $json->request->names[0]->OU);
            $this->assertSame($expectedData['commonName'], $json->request->CN);
            $this->assertSame($expectedData['jsonSha256'], \hash(algo: 'sha256', data: $jsonPayload));
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Currently, CFSSL supports only ECDSA, RSA and ed25519 algorithms.
     * Currently, Tajine supports only RSA algorithm.
     */
    public function cfsslInputWithInvalidAlgorithmProvider(): array
    {
        return [
            array_merge($this->commonInputCfsslNewCertficate(), ['key_algo' =>  "ecdsa",]),   // not yet implemented
            array_merge($this->commonInputCfsslNewCertficate(), ['key_algo' =>  "ed25519",]), // not yet implemented
            array_merge($this->commonInputCfsslNewCertficate(), ['key_algo' =>  "badAlgo",]),
            array_merge($this->commonInputCfsslNewCertficate(), ['key_algo' =>  "BAD ALGO",]),
        ];
    }

    /**
     * @dataProvider cfsslInputWithInvalidAlgorithmProvider
     */
    public function testTryToGetCfsslNewCertificateJsonPayloadWithInvalidAlgorithm(
        $countryName,
        $stateOrProvinceName,
        $localityName,
        $organizationName,
        $organizationalUnitName,
        $commonName,
        $keyAlgo,
    ): void {
        $this->transformer = new CertificateDataFormatter();
        $this->expectException(InvalidAlgorithmException::class);
        $this->expectExceptionMessage("Algorithm [ " . \strtolower($keyAlgo) . " ] is not supported.");
//      $this->expectExceptionMessage("Algorithm [ $keyAlgo ] is not supported.");
        $this->getCfsslNewCertificateJsonPayload(
            countryName: $countryName,
            stateOrProvinceName: $stateOrProvinceName,
            localityName: $localityName,
            organizationName: $organizationName,
            organizationalUnit: $organizationalUnitName,
            commonName: $commonName,
            keyAlgo: $keyAlgo,
        );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function cfsslInputWithTooWeakRsaKeySizeProvider(): array
    {
        return [
            array_merge($this->commonInputCfsslNewCertficate(), ['key_size' =>  1024,]),
            array_merge($this->commonInputCfsslNewCertficate(), ['key_size' =>  $this->getRsaKeySizeMin() - 1,]),
        ];
    }

    /**
     * @dataProvider cfsslInputWithTooWeakRsaKeySizeProvider
     */
    public function testTryToGetCfsslNewCertificateJsonPayloadWithTooWeakRsaKeySize(
        $countryName,
        $stateOrProvinceName,
        $localityName,
        $organizationName,
        $organizationalUnitName,
        $commonName,
        $keySize,
    ): void {
        $this->transformer = new CertificateDataFormatter();
        $this->expectException(KeySizeRangeException::class);
        $this->expectExceptionMessage("RSA key size [ $keySize ] is too weak. < 2048");
        $this->getCfsslNewCertificateJsonPayload(
            countryName: $countryName,
            stateOrProvinceName: $stateOrProvinceName,
            localityName: $localityName,
            organizationName: $organizationName,
            organizationalUnit: $organizationalUnitName,
            commonName: $commonName,
            keySize: $keySize,
        );
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function cfsslInputWithTooLargeRsaKeySizeProvider(): array
    {
        return [
            array_merge($this->commonInputCfsslNewCertficate(), ['key_size' => ($this->getRsaKeySizeMax() + 1),]),
            array_merge($this->commonInputCfsslNewCertficate(), ['key_size' => ($this->getRsaKeySizeMax() * 2),]),
        ];
    }

    /**
     * @dataProvider cfsslInputWithTooLargeRsaKeySizeProvider
     */
    public function testTryToGetCfsslNewCertificateJsonPayloadWithTooLargeRsaKeySize(
        $countryName,
        $stateOrProvinceName,
        $localityName,
        $organizationName,
        $organizationalUnitName,
        $commonName,
        $keySize,
    ): void {
        $this->transformer = new CertificateDataFormatter();
        $this->expectException(KeySizeRangeException::class);
        $this->expectExceptionMessage("RSA key size [ $keySize ] is too large. > 8192");
        $this->getCfsslNewCertificateJsonPayload(
            countryName: $countryName,
            stateOrProvinceName: $stateOrProvinceName,
            localityName: $localityName,
            organizationName: $organizationName,
            organizationalUnit: $organizationalUnitName,
            commonName: $commonName,
            keySize: $keySize,
        );
    }
}
