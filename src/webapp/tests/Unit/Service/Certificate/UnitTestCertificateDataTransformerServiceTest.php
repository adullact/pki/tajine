<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace Unit\Service\Certificate;

use App\Service\Certificate\CertificateDataFormatter;
use PHPUnit\Framework\TestCase;

/**
 * @group allow_mutation_testing_by_infection
 */
class UnitTestCertificateDataTransformerServiceTest extends TestCase
{
    /**
     * @group Certificate
     * @group Data_Transformer
     * @group Symfony_Service
     */
    public function testTransformerCanNormalizesDataForCertificate(): void
    {
        $formatter = new CertificateDataFormatter();
        $inputWithSpaceAndSpecialChars = "   éèç    456 \n\t à  @  û~    € bG";
        $emailInput = " hélène@exemple.org ";
        $testedData = [
            [
                'input' => $inputWithSpaceAndSpecialChars,
                'expected' => 'EEC-456-A-U-EUR-BG',
            ],
            [
                'input' => $inputWithSpaceAndSpecialChars,
                'expected' => 'EEC-456-A-U-EUR-BG',
                'useSlugger' => true,
            ],
            [
                'input' => $inputWithSpaceAndSpecialChars,
                'expected' => 'ÉÈÇ 456 À @ Û~ € BG',
                'useSlugger' => false,
            ],
            [
                'input' => $emailInput,
                'expected' => 'HELENE-EXEMPLE-ORG',
            ],
            [
                'input' => $emailInput,
                'expected' => 'HELENE-EXEMPLE-ORG',
                'useSlugger' => true,
            ],
            [
                'input' => $emailInput,
                'expected' => 'HÉLÈNE@EXEMPLE.ORG',
                'useSlugger' => false,
            ],
            [
                'input' => '] } : [ , {  "no valid   characters" : [" ',
                'expected' => 'NO VALID CHARACTERS',
                'useSlugger' => false,
            ],
            [
                'input' => "] } : [ , {  'no valid   characters' : [' ",
                'expected' => 'NO VALID CHARACTERS',
                'useSlugger' => false,
            ],
        ];
        foreach ($testedData as $data) {
            if (isset($data['useSlugger'])) {
                $output =  $formatter->normalizesData(
                    data: $data['input'],
                    useSlugger: $data['useSlugger']
                );
            } else {
                $output =  $formatter->normalizesData(data: $data['input']);
            }
            $this->assertSame($data['expected'], $output);
        }
    }
}
