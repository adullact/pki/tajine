<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace Unit\Service\Mailer;

use App\Service\Mailer\CertificateMailer;
use App\Tests\CommonHelpers\TestHelperEmailTrait;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\MailerAssertionsTrait;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Twig\Environment;

/**
 * @group allow_mutation_testing_by_infection
 */
class UnitTestCertificateMailerTest extends TestCase
{
    use MailerAssertionsTrait;
    use TestHelperEmailTrait;

    /**
     * Send email with pkcs 12 file to end user
     *
     * This test check only that email contains correct headers (except subject header)
     * and that the .p12 file is present as an attachment with  correct options (type, .
     *
     * Subject and message body (HTML and text versions) cannot be tested,
     * as assertions are made on a [ TemplatedEmail ] object
     *
     * @group Certificate
     * @group Certificate_send-email
     * @group Emails
     */
    public function testSendEmailWithPkcs12FileToEndUser(): void
    {
        $appShortName = "Tajine (RA)";
        $fromEmail = "no-reply@webapp.example.org";
        $endUserEmail = "end-user-of-pkc12-file@example.org";
        $pkcs12FilePath = "data/certificate/cert_user@example.org_2024.03.08_16h54.00_PKCS12.p12";
        $pkcs12FileContent = file_get_contents("$pkcs12FilePath");
        $certificateSerialNumber = "0x7E3FD7CBCB43BA25FFA3B4CE63ED7940F76C596E";
        $certificateValidToTimestamp = 1700053010; // 2023-11-15T13:56:50+01:00
        $expectedDisplayOfCertificateValidTo = "15/11/2023";

        // Mock
        $containerBag = $this->createMock(ContainerBagInterface::class);  // TODO refactor
        $twig = $this->createMock(Environment::class);
        $symfonyMailer = $this->createMock(MailerInterface::class);
        $symfonyMailer->expects($this->once())->method('send');
        $certificateMailer = new CertificateMailer(
            mailer: $symfonyMailer,
            twig: $twig,
            containerBag: $containerBag,
        );

        // Send PKCS12 certificate by email
        $email = $certificateMailer->sendPkc12FileByMail(
            msgFrom: new Address("$fromEmail"),
            msgTo: new Address("$endUserEmail"),
            certificateSerialNumber: "$certificateSerialNumber",
            certificateValidToTimestamp: $certificateValidToTimestamp,
            pkcs12FileContent: "$pkcs12FileContent",
        );

        // Check context variables to be used in the email template
        $this->assertCount(3, $email->getContext());
        $this->assertSame("$appShortName", $email->getContext()['app_name']);
        $this->assertSame("$certificateSerialNumber", $email->getContext()['cert_serial_number']);
        $this->assertSame("$expectedDisplayOfCertificateValidTo", $email->getContext()['cert_valid_to']);

        // Check email headers (technical, correspondent) and that PKCS12 certificate is attached.
        $this->checkCommonTechnicalEmailHeaders(email: $email);
        $this->checkCommonCorrespondentsEmailHeaders(
            email: $email,
            expectedEmailTo: "$endUserEmail",
            expectedEmailFrom: "$fromEmail",
        );
        $this->commonCheckPkcs12CertificatesIsInEmailAsAttachment(
            email: $email,
            certificateSerialNumber: $certificateSerialNumber,
            checkPkcs12FileContent: true,
            expectedPkcs12FileContent: $pkcs12FileContent
        );
    }
}
