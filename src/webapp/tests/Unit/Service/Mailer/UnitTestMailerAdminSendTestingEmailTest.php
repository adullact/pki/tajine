<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\tests\Unit\Service\Mailer;

use App\Service\Mailer\MailerAdminSendTestingEmail;
use App\Tests\CommonHelpers\TestHelperEmailTrait;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\MailerAssertionsTrait;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;

class UnitTestMailerAdminSendTestingEmailTest extends TestCase
{
    use MailerAssertionsTrait;
    use TestHelperEmailTrait;

    /**
     * Send email with pkcs 12 file to end user
     *
     * This test check only that email contains correct headers (except subject header)
     * and that the .p12 file is present as an attachment with  correct options (type, .
     *
     * Subject and message body (HTML and text versions) cannot be tested,
     * as assertions are made on a [ TemplatedEmail ] object
     *
     * @group Certificate
     * @group Certificate_send-email
     * @group Emails
     * @group allow_mutation_testing_by_infection
     */
    public function testServiceAllowAdminToSendTestingEmail(): void
    {
        $fromEmail = "no-reply@webapp.example.org";
        $endUserEmail = "alerting-webapp@example.org";

        // Mock
        $symfonyMailer = $this->createMock(MailerInterface::class);
        $symfonyMailer->expects($this->once())->method('send');
        $mailer = new MailerAdminSendTestingEmail(mailer: $symfonyMailer);

        // Send testing email
        $email = $mailer->sendTestingEmail(
            msgFrom: new Address("$fromEmail"),
            msgTo: new Address("$endUserEmail"),
        );

        // Check email has no attachment
        $this->assertEmailAttachmentCount($email, 0);

        # EMAIL sent to WEBAPP_EMAIL_ALERTING_TO ---> check email HEADERS (technical, correspondent)
        $expectedMailSubject = "Adullact PKI: email de test envoyé à alerting-webapp@example.org"; // TODO refactpr
        $this->assertEmailHeaderSame($email, "Subject", "$expectedMailSubject");
        $this->checkCommonTechnicalEmailHeaders(email: $email);
        $this->checkCommonCorrespondentsEmailHeaders(
            email: $email,
            expectedEmailTo: "$endUserEmail",
            expectedEmailFrom: "$fromEmail",
        );
    }
}
