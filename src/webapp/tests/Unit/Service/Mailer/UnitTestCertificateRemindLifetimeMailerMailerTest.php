<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace Unit\Service\Mailer;

use App\Service\Mailer\CertificateMailer;
use App\Service\Mailer\CertificateRemindLifetimeMailer;
use App\Tests\CommonHelpers\TestHelperEmailTrait;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\MailerAssertionsTrait;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Twig\Environment;

/**
 * @group allow_mutation_testing_by_infection
 */
class UnitTestCertificateRemindLifetimeMailerMailerTest extends TestCase
{
    use MailerAssertionsTrait;
    use TestHelperEmailTrait;

    /**
     * Send end of life certificate reminder email to certifcate end user
     *
     * This test check only that email contains correct headers (except subject header)
     *
     * Subject and message body (HTML and text versions) cannot be tested,
     * as assertions are made on a [ TemplatedEmail ] object
     *
     * @group Certificate
     * @group Certificate_remind-end-of-life
     * @group Emails
     */
    public function testSendEndOfLifeCertificateReminderEmailToCertifcateEndUser(): void
    {
        $appShortName = "Tajine (RA)";
        $fromEmail = "no-reply@test.example.org";
        $endUserEmail = "end-user-of-pkc12-file@example.org";
        $certificateCreatorEmail = "cert-manager@example.org";
        $expectedBcc = "sysadmin_bcc_remind@example.org";
        $certificateSerialNumber = "0x7E3FD7CBCB43BA25FFA3B4CE63ED7940F76C596E";

        $expectedDisplayOfCertificateValidFrom = "15/11/2022";
        $certificateValidFromTimestamp = 1668517010; // 2022-11-15T13:56:50+01:00
        $dateTime = new \DateTimeImmutable();
        $certificateValidFrom = $dateTime->setTimestamp($certificateValidFromTimestamp);

        $expectedDisplayOfCertificateValidTo = "15/11/2023";
        $certificateValidToTimestamp = 1700053010; // 2023-11-15T13:56:50+01:00
        $dateTime = new \DateTimeImmutable();
        $certificateValidTo = $dateTime->setTimestamp($certificateValidToTimestamp);

        $certificateSubject  = '';
        $certificateSubject .= '/C=FR/ST=PYRENEES-ATLANTIQUES/L=CAUDIES-DE-CONFLENT/O=MAIRIE-DE-CAUDIES-DE-CONFLENT';
        $certificateSubject .= '/OU=SERVICE-RH/CN=TEST_USER_1_TAJINE@EXAMPLE.ORG';

        // Mock
        $containerBag = $this->createMock(ContainerBagInterface::class);  // TODO refactor
        $twig = $this->createMock(Environment::class);
        $symfonyMailer = $this->createMock(MailerInterface::class);
        $symfonyMailer->expects($this->once())->method('send');
        $certificateMailer = new CertificateRemindLifetimeMailer(
            mailer: $symfonyMailer,
            twig: $twig,
            containerBag: $containerBag,
        );

        // Send email
        $email = $certificateMailer->remindCertificateEndOfLifeToEndUser(
            msgTo: new Address("$endUserEmail"),
            certificateSerialNumber: "$certificateSerialNumber",
            certificateValidFrom: $certificateValidFrom,
            certificateValidTo: $certificateValidTo,
            certificateSubject: "$certificateSubject",
            certificateCreatorEmail: "$certificateCreatorEmail",
        );

        // Check context variables to be used in the email template
        $this->assertCount(6, $email->getContext());
        $this->assertSame("$appShortName", $email->getContext()['app_name']);
        $this->assertSame("$certificateSerialNumber", $email->getContext()['cert_serial_number']);
        $this->assertSame("$expectedDisplayOfCertificateValidTo", $email->getContext()['cert_valid_to']);
        $this->assertSame("$expectedDisplayOfCertificateValidFrom", $email->getContext()['cert_valid_from']);
        $this->assertSame("$certificateSubject", $email->getContext()['cert_subject']);
        $this->assertSame("$certificateCreatorEmail", $email->getContext()['cert_creator_email']);

        // Check email headers (technical, correspondent)
        $this->checkCommonTechnicalEmailHeaders(email: $email);
        $this->checkCommonCorrespondentsEmailHeaders(
            email: $email,
            expectedEmailTo: "$endUserEmail",
            expectedEmailFrom: "$fromEmail",
            bccIsExpected: true,
            expectedEmailBcc: "$expectedBcc",
        );

        // Check custom email headers
        $this->assertEmailHasHeader($email, "X-Tajine-Certificate");
        $this->assertEmailHasHeader($email, "X-Tajine-Email-Type");
        $this->assertEmailHeaderSame($email, "X-Tajine-Certificate", "$certificateSerialNumber");
        $this->assertEmailHeaderSame($email, "X-Tajine-Email-Type", "remindCertificateEndOfLifeToEndUser");
    }


    /**
     * Send end of life certificate reminder email to certifcate creator
     *
     * This test check only that email contains correct headers (except subject header)
     *
     * Subject and message body (HTML and text versions) cannot be tested,
     * as assertions are made on a [ TemplatedEmail ] object
     *
     * @group Certificate
     * @group Certificate_remind-end-of-life
     * @group Emails
     */
    public function testSendEndOfLifeCertificateReminderEmailToCertifcateCreator(): void
    {
        $appShortName = "Tajine (RA)";
        $fromEmail = "no-reply@test.example.org";
        $certificateCreatorEmail = "cert-manager@example.org";
        $endUserEmail = "end-user-of-pkc12-file@example.org";
        $expectedBcc = "sysadmin_bcc_remind@example.org";
        $certificateSerialNumber = "0x7E3FD7CBCB43BA25FFA3B4CE63ED7940F76C596E";

        $expectedDisplayOfCertificateValidFrom = "15/11/2022";
        $certificateValidFromTimestamp = 1668517010; // 2022-11-15T13:56:50+01:00
        $dateTime = new \DateTimeImmutable();
        $certificateValidFrom = $dateTime->setTimestamp($certificateValidFromTimestamp);

        $expectedDisplayOfCertificateValidTo = "15/11/2023";
        $certificateValidToTimestamp = 1700053010; // 2023-11-15T13:56:50+01:00
        $dateTime = new \DateTimeImmutable();
        $certificateValidTo = $dateTime->setTimestamp($certificateValidToTimestamp);

        $certificateSubject  = '';
        $certificateSubject .= '/C=FR/ST=PYRENEES-ATLANTIQUES/L=CAUDIES-DE-CONFLENT/O=MAIRIE-DE-CAUDIES-DE-CONFLENT';
        $certificateSubject .= '/OU=SERVICE-RH/CN=TEST_USER_1_TAJINE@EXAMPLE.ORG';

        // Mock
        $containerBag = $this->createMock(ContainerBagInterface::class);  // TODO refactor
        $twig = $this->createMock(Environment::class);
        $symfonyMailer = $this->createMock(MailerInterface::class);
        $symfonyMailer->expects($this->once())->method('send');
        $certificateMailer = new CertificateRemindLifetimeMailer(
            mailer: $symfonyMailer,
            twig: $twig,
            containerBag: $containerBag,
        );

        // Send email
        $email = $certificateMailer->remindCertificateEndOfLifeToManager(
            msgTo: new Address("$certificateCreatorEmail"),
            certificateEndUserEmail: new Address("$endUserEmail"),
            certificateSerialNumber: "$certificateSerialNumber",
            certificateValidFrom: $certificateValidFrom,
            certificateValidTo: $certificateValidTo,
            certificateSubject: "$certificateSubject",
        );

        // Check context variables to be used in the email template
        $this->assertCount(6, $email->getContext());
        $this->assertSame("$appShortName", $email->getContext()['app_name']);
        $this->assertSame("$certificateSerialNumber", $email->getContext()['cert_serial_number']);
        $this->assertSame("$expectedDisplayOfCertificateValidTo", $email->getContext()['cert_valid_to']);
        $this->assertSame("$expectedDisplayOfCertificateValidFrom", $email->getContext()['cert_valid_from']);
        $this->assertSame("$endUserEmail", $email->getContext()['cert_user_email']);
        $this->assertSame("$certificateSubject", $email->getContext()['cert_subject']);

        // Check email headers (technical, correspondent)
        $this->checkCommonTechnicalEmailHeaders(email: $email);
        $this->checkCommonCorrespondentsEmailHeaders(
            email: $email,
            expectedEmailTo: "$certificateCreatorEmail",
            expectedEmailFrom: "$fromEmail",
            bccIsExpected: true,
            expectedEmailBcc: "$expectedBcc",
        );

        // Check custom email headers
        $this->assertEmailHasHeader($email, "X-Tajine-Certificate");
        $this->assertEmailHasHeader($email, "X-Tajine-Email-Type");
        $this->assertEmailHeaderSame($email, "X-Tajine-Certificate", "$certificateSerialNumber");
        $this->assertEmailHeaderSame($email, "X-Tajine-Email-Type", "remindCertificateEndOfLifeToManager");
    }
}
