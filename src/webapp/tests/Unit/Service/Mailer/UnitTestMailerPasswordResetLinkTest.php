<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\tests\Unit\Service\Mailer;

use App\Service\Mailer\MailerPasswordResetLink;
use App\Tests\CommonHelpers\TestHelperEmailTrait;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\MailerAssertionsTrait;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Twig\Environment;

/**
 * @group allow_mutation_testing_by_infection
 */
class UnitTestMailerPasswordResetLinkTest extends TestCase
{
    use MailerAssertionsTrait;
    use TestHelperEmailTrait;

    /**
     * Send by email a reset password link to end user
     *
     * This test check only that email contains correct headers (except subject header).
     *
     * Subject and message body (HTML and text versions) cannot be tested,
     * as assertions are made on a [ TemplatedEmail ] object
     *
     * @group Certificate
     * @group Certificate_send-email
     * @group Emails
     * @group allow_mutation_testing_by_infection
     */
    public function testServiceAllowSendEmailPasswordResetLink(): void
    {
        $appShortName = "Tajine (RA)";
        $appName = "Tajine (RA) PKI";
        $fromEmail = "no-reply@webapp.example.org";
        $endUserEmail = "user-who-lost-passord@example.org";

        // Mock
        $containerBag = $this->createMock(ContainerBagInterface::class);  // TODO refactor
        $twig = $this->createMock(Environment::class);
        $symfonyMailer = $this->createMock(MailerInterface::class);
        $symfonyMailer->expects($this->once())->method('send');
        $mailer = new MailerPasswordResetLink(
            mailer: $symfonyMailer,
            twig: $twig,
            containerBag: $containerBag,
        );

        // Send reset link password
        $resetPasswordUrl = 'http://127.0.0.1/account/forgot-password/t/pQ5Rb_zA-cYEEswRAtUYqCmk3awDGvLy4DnkiXLdGLA';
        $email = $mailer->sendPasswordResetLink(
            msgFrom: new Address("$fromEmail"),
            msgTo: new Address("$endUserEmail"),
            resetPasswordUrl: "$resetPasswordUrl",
        );

        // Check context variables to be used in the email template
        $this->assertCount(2, $email->getContext());
        $this->assertSame("$appName", $email->getContext()['app_name']);
        $this->assertSame("$resetPasswordUrl", $email->getContext()['reset_passord_url']);

        // Check email has no attachment
        $this->assertEmailAttachmentCount($email, 0);

        // Check email headers (technical, correspondent)
        $this->checkCommonTechnicalEmailHeaders(email: $email);
        $this->checkCommonCorrespondentsEmailHeaders(
            email: $email,
            expectedEmailTo: "$endUserEmail",
            expectedEmailFrom: "$fromEmail",
        );
    }
}
