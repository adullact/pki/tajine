<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace Scheduled;

use App\Entity\Certificate;
use App\Entity\UserNotice;
use App\Repository\CertificateRepository;
use App\Repository\UserNoticeRepository;
use App\Repository\UserRepository;
use App\Tests\CommonHelpers\TestHelperEmailTrait;
use App\Tests\Functional\TestHelperTrait;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;

use function PHPUnit\Framework\assertCount;

/**
 * @group gogogo
 */
class ScheduledTestRemindCertificateEndOfLifeTest extends WebTestCase
{
    use TestHelperTrait;
    use TestHelperEmailTrait;

    public const HTTP_REMIND_TOKEN = 'sysadmin@test.example.org';
    public const WEBAPP_ADMIN_LOGIN = 'test_admin_1_tajine@example.org';

    private $registry;
    private array $certificateInitialDates ;

    /**
     * This method is called before each test.
     *
     * - remove all UserNotices in database
     * - backup certificate date (ValidTo) for tearDown() method
     */
    protected function setUp(): void
    {
        $registry = self::getContainer()->get('Doctrine\Persistence\ManagerRegistry');

        $certificateRepository = new CertificateRepository($registry);
        $certificates = $certificateRepository->findAll();
        $this->certificateInitialDates = [];
        foreach ($certificates as $cert) {
            $this->certificateInitialDates[$cert->getId()] = $cert->getValidTo();
        }
        $this->cleanUpUserNoticeTable($registry);
        self::ensureKernelShutdown();
    }

    /**
     * This method is called after each test.
     *
     *  - remove all UserNotices in database
     *  - restore all certificate date (ValidTo) in database
     */
    protected function tearDown(): void
    {
        $registry = self::getContainer()->get('Doctrine\Persistence\ManagerRegistry');
        $certificateRepository = new CertificateRepository($registry);
        $certificates = $certificateRepository->findAll();
        foreach ($certificates as $cert) {
            $cert->setValidTo($this->certificateInitialDates[$cert->getId()]);
            $certificateRepository->save($cert, true);
        }
        $this->cleanUpUserNoticeTable($registry);
        self::ensureKernelShutdown();
    }

    /**
     * Remove all UserNotices in database
     */
    private function cleanUpUserNoticeTable($registry): void
    {
        $noticeRepository = new UserNoticeRepository($registry);
        $notices = $noticeRepository->findAll();
        foreach ($notices as $notice) {
            $noticeRepository->remove($notice, true);
        }
    }

    /**
     * Send HTTP request to load reminder
     */
    private function httpRemindLoad(KernelBrowser $kernelBrowser, $httpToken = self::HTTP_REMIND_TOKEN): Crawler
    {
        $crawler = $kernelBrowser->request('GET', "/poc/ping-reminder/$httpToken");
        $this->assertRouteSame('app_legacy_cron_certificateRemindLifetime');
        return $crawler;
    }

    /**
     * Common check that no new remind notices is not expetected
     * - Load reminder process
     * - Check that no new reminder notice stored in db
     * - Check that no reminder EMAIL sent
     */
    private function commonCheckerNewRemindNoticesIsNotExpected(
        KernelBrowser $kernelBrowser,
        int $initialNumberOfNoticeInDb = 0
    ): void {
        $this->httpRemindLoad($kernelBrowser);
        $this->assertResponseStatusCodeSame(Response::HTTP_ACCEPTED); // HTTP status code = 202

        // No EMAIL sent
        $this->assertNull($this->getMailerEvent(0));

        // No new reminder notice stored in db
        $noticeRepository = new UserNoticeRepository($this->registry);
        $notices = $noticeRepository->findAll();
        $this->assertSame($initialNumberOfNoticeInDb, count($notices));
    }

    /**
     * Common check with ADMIN role that there are no e-mail notices on certificate pages.
     */
    private function commonCheckerNoEmailNoticeOnEachCertPageForAdminRole(
        array $certificates,
        KernelBrowser|null $kernelBrowser = null
    ): void {
        $kernelBrowser =  $this->getKernelBrowserWithConnectedUser(self::WEBAPP_ADMIN_LOGIN, $kernelBrowser);
        foreach ($certificates as $cert) {
            $kernelBrowser->request('GET', '/admin/certificates/' . $cert->getSerialNumber());
            $this->assertRouteSame("app_admin_certificates_display");
            $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
            $this->assertSelectorTextSame('#cert_serial-number', $cert->getSerialNumber());
            $this->assertSelectorNotExists('#cert_managerReminderNotices');
            $this->assertSelectorNotExists('#cert_endUserReminderNotices');
            $this->assertSelectorTextNotContains('body', "Email reminder");
        }
        $kernelBrowser->request('GET', '/account/logout');
        $this->assertResponseStatusCodeSame(Response::HTTP_FOUND); // HTTP status code = 302
    }

    /**
     * Check with  ADMIN role that e-mail notifications are displayed
     * for certificates due to expire soon.
     */
    private function commonCheckerEmailNoticeOnExpiringSoonCertPageForAdminRole(
        array $certificatesExpiringSoon,
        KernelBrowser|null $kernelBrowser = null
    ): void {
        $noticeRepository = new UserNoticeRepository($this->registry);
        $kernelBrowser =  $this->getKernelBrowserWithConnectedUser(self::WEBAPP_ADMIN_LOGIN, $kernelBrowser);

        // Check with  ADMIN role that e-mail notifications are displayed
        // for certificates due to expire soon.
        $kernelBrowser =  $this->getKernelBrowserWithConnectedUser(self::WEBAPP_ADMIN_LOGIN, $kernelBrowser);
        foreach ($certificatesExpiringSoon as $certSerialNumber => $cert) {
            $crawler = $kernelBrowser->request('GET', '/admin/certificates/' . $certSerialNumber);
            $this->assertRouteSame("app_admin_certificates_display");
            $this->assertResponseStatusCodeSame(Response::HTTP_OK); // HTTP status code = 200
            $this->assertSelectorTextSame('#cert_serial-number', $certSerialNumber);
            $this->assertSelectorTextContains('#cert_managerReminderNotices', "Email reminder");
            $this->assertSelectorTextContains('#cert_endUserReminderNotices', "Email reminder");
            $managerNotices = $noticeRepository->findBy(
                ['certificate'  => $cert, 'noticeType' => UserNotice::NOTICE_TYPE_CERT_REDMIND_TO_MANAGER]
            );
            $this->assertSame(1, count($managerNotices));
            $this->assertSame($cert->getCreatedBy()->getEmail(), $managerNotices[0]->getNotifiedUser()->getEmail());
            $managerNoticeDate = $managerNotices[0]->getSentAt()->format("Y-m-d H\:i\:s T");
            $this->assertSelectorTextContains('#cert_managerReminderNotices', "$managerNoticeDate");

            $endUserNotices = $noticeRepository->findBy(
                ['certificate'  => $cert, 'noticeType' => UserNotice::NOTICE_TYPE_CERT_REDMIND_TO_END_USER]
            );
            $this->assertSame(1, count($endUserNotices));
            $this->assertSame($cert->getUsedBy()->getEmail(), $endUserNotices[0]->getNotifiedUser()->getEmail());
            $endUserNoticeDate = $endUserNotices[0]->getSentAt()->format("Y-m-d H\:i\:s T");
            $this->assertSelectorTextContains('#cert_endUserReminderNotices', "$endUserNoticeDate");
        }
    }


    /**
     * Common checker for e-mail notification of certificates to be processed.
     * - Checks that all expected emails have been sent
     * - Check header and content of each email
     * - If $certificatesNotToBeProcessed is provided,
     *   then we also check that no email has been sent for these certificates.
     *
     * For each email:
     * - Check that the email concerns a certificate to be processed
     * - Custom email HEADERS checking : X-Tajine-Certificate, X-Tajine-Email-Type
     * - Basic email HEADERS checking
     * - Complete email content check (subject + body)
     */
    private function commonCheckerEmailNoticeSendOfCertificatesToBeProcessed(
        int $expectedEmailsSent,
        array $certificatesToBeProcessed,
        ?array $certificatesNotToBeProcessed = null,
    ): void {
        $appEmailFrom = array_key_exists('WEBAPP_EMAIL_FROM', $_ENV) ? $_ENV['WEBAPP_EMAIL_FROM'] : false;
        $appShortName = array_key_exists('WEBAPP_SHORTNAME', $_ENV) ? $_ENV['WEBAPP_SHORTNAME'] : false;
        $bccFlag = false;
        if (array_key_exists('WEBAPP_CERTIFICAT_COPY_REMINDER_TO_EMAIL_ALERTING', $_ENV)) {
            $bccFlag = (bool) $_ENV['WEBAPP_CERTIFICAT_COPY_REMINDER_TO_EMAIL_ALERTING'];
        }

        $this->assertNotNull($this->getMailerEvent(0));
        $this->assertEmailIsQueued($this->getMailerEvent(0));
        $this->assertEmailCount($expectedEmailsSent); // use assertQueuedEmailCount() when using Messenger

        $emailToEndUserToBeProcessed = array_flip(array_keys($certificatesToBeProcessed));
        $emailToManagerToBeProcessed = $emailToEndUserToBeProcessed;
        foreach ($this->getMailerMessages('null://') as $email) {
            // custom email HEADERS checking
            $this->assertEmailHasHeader($email, "X-Tajine-Certificate");
            $this->assertEmailHasHeader($email, "X-Tajine-Email-Type");
            $emailHeaderCertSerialNumber = $email->getHeaders()->get("X-Tajine-Certificate")->getBody();
            $emailHeaderMsgType = $email->getHeaders()->get("X-Tajine-Email-Type")->getBody();
            $this->assertContains($emailHeaderMsgType, array(
                'remindCertificateEndOfLifeToManager',
                'remindCertificateEndOfLifeToEndUser'
            ));
            $emailCert = $certificatesToBeProcessed[$emailHeaderCertSerialNumber];

            // Check that the email concerns a certificate to be processed
            $this->assertTrue(\key_exists($emailHeaderCertSerialNumber, $certificatesToBeProcessed));
            if (is_array($certificatesNotToBeProcessed)) {
                $this->assertFalse(\key_exists($emailHeaderCertSerialNumber, $certificatesNotToBeProcessed));
            }

            # Basic email HEADERS checking
            $this->checkCommonTechnicalEmailHeaders(email: $email);

            # Complete email content check (subject + body)
            $endUserEmail = $emailCert->getUsedBy()->getEmail();
            $managerEmail = $emailCert->getCreatedBy()->getEmail();
            $certValidFrom = $emailCert->getValidFrom()->format('d/m/Y');
            $certValidTo = $emailCert->getValidTo()->format('d/m/Y');
            if ($emailHeaderMsgType === 'remindCertificateEndOfLifeToManager') {
                $expectedEmailTo = $managerEmail;
                $subject = "An X509 certificate expires on $certValidTo ($emailHeaderCertSerialNumber)";
                $msgIntro = "A certificate you created expires soon.";
                $msgCertInfo = "Information about this certificate:";
                $msgEnd = "If necessary, you can renew it by creating a new certificate: ";
                $msgForgotPasswordTip = "Forgot your account password? You can set a new one:";
                $this->assertEmailTextBodyContains($email, "Associated user: $endUserEmail");
                $this->assertEmailHtmlBodyContains($email, "Associated user:");
                $this->assertEmailHtmlBodyContains($email, "$endUserEmail");
                $this->assertEmailHtmlBodyContains($email, "$msgForgotPasswordTip");
            } elseif ($emailHeaderMsgType === 'remindCertificateEndOfLifeToEndUser') {
                $expectedEmailTo = $endUserEmail;
                $subject = "Your X509 certificate expires on $certValidTo ($emailHeaderCertSerialNumber)";
                $msgIntro = "Your X509 certificate, created by your administrator, expires soon.";
                $msgCertInfo = "Information about your certificate:";
                $msgEnd = "If you need to renew your certificate, we invite you to contact him.";
                $msgContact = "The administrator of your certificate can be reached via the e-mail address";
                $this->assertEmailTextBodyContains($email, "$msgContact $managerEmail");
                $this->assertEmailHtmlBodyContains($email, "$msgContact");
                $this->assertEmailHtmlBodyContains($email, "$managerEmail");
            }
            $this->checkCommonCorrespondentsEmailHeaders(
                email: $email,
                expectedEmailTo: "$expectedEmailTo",
                expectedEmailFrom: "$appEmailFrom",
                bccIsExpected: $bccFlag
            );
            $expectedMailSubject = "$appShortName - $subject";
            $this->assertEmailHeaderSame($email, "Subject", "$expectedMailSubject");
            $this->assertEmailBodyContains($email, "This is an automatic message sent by the service");
            $this->assertEmailBodyContains($email, "$msgIntro");
            $this->assertEmailBodyContains($email, "$msgCertInfo");
            $this->assertEmailBodyContains($email, "Creation date: $certValidFrom");
            $this->assertEmailTextBodyContains($email, "Expiry date: $certValidTo");
            $this->assertEmailHtmlBodyContains($email, "Expiry date: <strong>$certValidTo</strong>");
            $this->assertEmailBodyContains($email, "Serial number: $emailHeaderCertSerialNumber");
            $this->assertEmailBodyContains($email, "$msgEnd");

            // Remove certificate from checklists according to email type
            if ($emailHeaderMsgType === 'remindCertificateEndOfLifeToManager') {
                unset($emailToManagerToBeProcessed["$emailHeaderCertSerialNumber"]);
            } elseif ($emailHeaderMsgType === 'remindCertificateEndOfLifeToEndUser') {
                unset($emailToEndUserToBeProcessed["$emailHeaderCertSerialNumber"]);
            }
        }
        assertCount(0, $emailToEndUserToBeProcessed);
        assertCount(0, $emailToManagerToBeProcessed);
    }

    /**
     * Update all certificates so they do not expire soon
     */
    private function commonUpdateAllCertificatesSoTheyDoNotExpireSoon(): array
    {
        $certificateRepository = new CertificateRepository($this->registry);
        $now = new \DateTimeImmutable();
        $certificates = [];
        $minDays = CertificateRepository::NUMBER_OF_DAYS_WHEN_EXPIRY_IS_EXPECTED_SOON + 1;
        foreach ($certificateRepository->findAll() as $cert) {
            $days = $minDays + rand(0, CertificateRepository::NUMBER_OF_DAYS_WHEN_EXPIRY_IS_EXPECTED_SOON);
            $noRemindDate = $now->add(\DateInterval::createFromDateString("$days day"));
            $certSerialNumber = $cert->getSerialNumber();
            $cert->setValidTo($noRemindDate);
            $certificateRepository->save($cert, true);
            $certificates["$certSerialNumber"] = $cert;
        }
        return $certificates;
    }

    ///////////////////////////////////////////////////////////////////////////////////////

    /**
     * Send HTTP request to load reminder with valid token
     */
    public function testHttpRequestToLoadRemindWithValidToken(): void
    {
        $kernelBrowser = static::createClient();
        $crawler = $this->httpRemindLoad($kernelBrowser);
        $this->assertResponseStatusCodeSame(Response::HTTP_ACCEPTED); // HTTP status code = 202
        $this->assertSame(0, count($crawler->getIterator())); // = no content
    }

    /**
     * Send HTTP request to load reminder with invalid token
     *
     * @group allow_mutation_testing_by_infection__FIXME
     */
    public function testTryHttpRequestToLoadRemindWithInvalidToken(): void
    {
        $httpToken = 'not-valid-token';
        $kernelBrowser = static::createClient();
        $crawler = $this->httpRemindLoad($kernelBrowser, 'not-valid-http-token');
        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND); // HTTP status code = 404
        $this->assertSame('Error 404 - Not Found', $crawler->text());
    }


    /**
     * Try to send remind on certificates that are expired
     *
     * - Update all certificate dates so that they have already expired
     * - Load reminder process
     * - Check that no reminder notice stored in db
     * - Check that no reminder EMAIL sent
     * - Check with ADMIN role that there are no e-mail notices on certificate pages.
     */
    public function testTryToSendRemindOnCertificatesThatAreExpired(): void
    {
        $kernelBrowser = static::createClient();
        $this->registry = self::getContainer()->get('Doctrine\Persistence\ManagerRegistry');
        $certificateRepository = new CertificateRepository($this->registry);

        // Update all certificate dates so that they have already expired
        $now = new \DateTimeImmutable();
        $noRemindDate = $now->add(\DateInterval::createFromDateString("-1 day"));
        $certificates = $certificateRepository->findAll();
        foreach ($certificates as $cert) {
            $cert->setValidTo($noRemindDate);
            $certificateRepository->save($cert, true);
        }
        // Load reminder + check no EMAIL sent and no new reminder notice stored in db
        $this->commonCheckerNewRemindNoticesIsNotExpected($kernelBrowser, 0);
        // Check with ADMIN role that there are no e-mail notices on certificate pages.
        $this->commonCheckerNoEmailNoticeOnEachCertPageForAdminRole($certificates, $kernelBrowser);
    }


    /**
     * Try to send remind on certificates that are not at end of life
     *
     * - Update all certificate dates so they don't expire in the next few months.
     * - Load reminder process
     * - Check that no reminder notice stored in db
     * - Check that no reminder EMAIL sent
     * - Check with ADMIN role that there are no e-mail notices on certificate pages.
     */
    public function testTryToSendRemindOnCertificatesThatAreNotAtEndOfLife(): void
    {
        $kernelBrowser = static::createClient();
        $this->registry = self::getContainer()->get('Doctrine\Persistence\ManagerRegistry');
        $certificateRepository = new CertificateRepository($this->registry);

        // Update all certificate dates so they don't expire in the next few months.
        $certificates = $this->commonUpdateAllCertificatesSoTheyDoNotExpireSoon();
        // Load reminder + check no EMAIL sent and no new reminder notice stored in db
        $this->commonCheckerNewRemindNoticesIsNotExpected($kernelBrowser, 0);
        // Check with ADMIN role that there are no e-mail notices on certificate pages.
        $this->commonCheckerNoEmailNoticeOnEachCertPageForAdminRole($certificates, $kernelBrowser);
    }


    /**
     * Send remind on certificates that are at end of life
     * - Update some certificates so they so that they expire soon.
     * - Update other certificates so they don't expire in the next few months.
     * - Check with ADMIN role that there are no e-mail notices on certificate pages.
     * - Load reminder process
     * - New reminder notices are stored in db and reminder EMAILS are send
     * - Checks that all expected emails have been sent
     * - Check with ADMIN role that e-mail notifications are displayed for certificates due to expire soon.
     * - Check with ADMIN role that there are no e-mail notifications for certificates that won't be expiring soon.
     * - Reload reminder + check no EMAIL sent and no new reminder notice stored in db
     */
    public function testSendRemindOnCertificatesThatAreAtEndOfLife(): void
    {
        $kernelBrowser = static::createClient();
        $this->registry = self::getContainer()->get('Doctrine\Persistence\ManagerRegistry');
        $certificateRepository = new CertificateRepository($this->registry);
        $noticeRepository = new UserNoticeRepository($this->registry);

        // Update some certificates so they so that they expire soon
        // and update other certificates so they don't expire in the next few months.
        $now = new \DateTimeImmutable();
        $maxDay = CertificateRepository::NUMBER_OF_DAYS_WHEN_EXPIRY_IS_EXPECTED_SOON;
        $certificates = $certificateRepository->findAll();
        $numberOfCertificates = count($certificates);
        $certificatesExpiringSoon = [];
        $certificatesExpiringInLongTime = [];
        $randomKeys = range(0, $numberOfCertificates - 1);
        shuffle($randomKeys);
        array_splice($randomKeys, (int) \ceil($numberOfCertificates / 2));
        for ($i = 0; $i < $numberOfCertificates; $i++) {
            $certSerialNumber = $certificates[$i]->getSerialNumber();
            if (\in_array($i, $randomKeys)) {
                $numberOfDays = $maxDay - rand(0, $maxDay);
                $certificatesExpiringSoon["$certSerialNumber"] = $certificates[$i];
            } else {
                $numberOfDays = $maxDay + rand(1, $maxDay);
                $certificatesExpiringInLongTime["$certSerialNumber"] = $certificates[$i];
            }
            $noRemindDate = $now->add(\DateInterval::createFromDateString("$numberOfDays day"));
            $certificates[$i]->setValidTo($noRemindDate);
            $certificateRepository->save($certificates[$i], true);
        }

        // Check with ADMIN role that there are no e-mail notices on certificate pages.
        $this->commonCheckerNoEmailNoticeOnEachCertPageForAdminRole($certificates, $kernelBrowser);

        //////////////////////////////////////////////////////////////////////////////////
// REFACTOR
        // Load reminder
        $this->httpRemindLoad($kernelBrowser);
        $this->assertResponseStatusCodeSame(Response::HTTP_ACCEPTED); // HTTP status code = 202

        // Reminder notices stored in db (certificates x 2 = 1 for manager + 1 for end-user)
        $expectedNotice = count($certificatesExpiringSoon) * 2;
        $notices = $noticeRepository->findAll();
        $this->assertSame($expectedNotice, count($notices));

        // Check EMAILS sent (certificates x 2 = 1 for manager + 1 for end-user)
        $expectedEmailsSent = $expectedNotice;
        $this->commonCheckerEmailNoticeSendOfCertificatesToBeProcessed(
            expectedEmailsSent: $expectedEmailsSent,
            certificatesToBeProcessed: $certificatesExpiringSoon,
            certificatesNotToBeProcessed: $certificatesExpiringInLongTime
        );

        // Check with ADMIN role that e-mail notifications are displayed for certificates due to expire soon.
        $this->commonCheckerEmailNoticeOnExpiringSoonCertPageForAdminRole($certificatesExpiringSoon, $kernelBrowser);

        // Check with ADMIN role that there are no e-mail notifications for certificates that won't be expiring soon.
        $this->commonCheckerNoEmailNoticeOnEachCertPageForAdminRole($certificatesExpiringInLongTime, $kernelBrowser);
// END REFACTOR
        //////////////////////////////////////////////////////////////////////////////////

        // Reload reminder + check no EMAIL sent and no new reminder notice stored in db
        $this->commonCheckerNewRemindNoticesIsNotExpected(
            kernelBrowser: $kernelBrowser,
            initialNumberOfNoticeInDb: $expectedNotice
        );
    }


    /**
     * Send remind on end of life certificates to users
     * with multiple certificates (expired, expiring soon, not at end of life)
     *
     * - Update all certificates of one user so they so that they expire soon.
     * - Update all certificates of one user : 1 expire soon, 1 not expire soon and other are expired
     * - Update all other certificate dates so they don't expire in the next few months.
     * - Load reminder process
     * - New reminder notices are stored in db and reminder EMAILS are send
     * - Checks that all expected emails have been sent
     * - Check with ADMIN role that e-mail notifications are displayed for certificates due to expire soon.
     * - Check with ADMIN role that there are no e-mail notifications for certificates that won't be expiring soon.
     */
    public function testSendRemindOnEndOfLifeCertificatesToUsersWithMultipleCertificates(): void
    {
        $kernelBrowser = static::createClient();
        $this->registry = self::getContainer()->get('Doctrine\Persistence\ManagerRegistry');
        $certificateRepository = new CertificateRepository($this->registry);
        $noticeRepository = new UserNoticeRepository($this->registry);

        // Update all certificate dates so they don't expire in the next few months.
        $allCertificates = $this->commonUpdateAllCertificatesSoTheyDoNotExpireSoon();

        // Update certificates to some users with multiple certificates : expired, expiring soon, not at end of life
        $userWithAtLeast2Certificates = 'test_user_7_tajine@example.org'; // USER_REFERENCE_7
        $userWithAtLeast3Certificates = 'test_user_8_tajine@example.org'; // USER_REFERENCE_8
        $certificatesForuserWith3Certificates = [];
        $certificatesExpiringSoon = [];
        $certificatesNotExpiringSoon = $allCertificates;
        $maxDay = CertificateRepository::NUMBER_OF_DAYS_WHEN_EXPIRY_IS_EXPECTED_SOON;
        $now = new \DateTimeImmutable();
        foreach ($allCertificates as $cert) {
            $certSerialNumber = $cert->getSerialNumber();
            // Update all certificates of one user so they so that they expire soon.
            if ($cert->getUsedBy()->getEmail() === $userWithAtLeast2Certificates) {
                $days = $maxDay - rand(0, CertificateRepository::NUMBER_OF_DAYS_WHEN_EXPIRY_IS_EXPECTED_SOON);
                $cert->setValidTo($now->add(\DateInterval::createFromDateString("$days day")));
                $certificateRepository->save($cert, true);
                $certificatesExpiringSoon["$certSerialNumber"] = $cert;
                unset($certificatesNotExpiringSoon["$certSerialNumber"]);
            } elseif ($cert->getUsedBy()->getEmail() === $userWithAtLeast3Certificates) {
                // Prepare : update certificates of one user : 1 expire soon, 1 not expire soon and other are expired
                $certificatesForuserWith3Certificates[] = $cert;
            }
        }

        // Update certificates of one user : 1 expire soon, 1 not expire soon and other are expired
        shuffle($certificatesForuserWith3Certificates);
        for ($i = 0; $i < count($certificatesForuserWith3Certificates); $i++) {
            $cert = $certificatesForuserWith3Certificates[$i];
            $certSerialNumber = $cert->getSerialNumber();
            if ($i === 0) { // Expiring soon
                $days = $maxDay - rand(0, CertificateRepository::NUMBER_OF_DAYS_WHEN_EXPIRY_IS_EXPECTED_SOON);
                $cert->setValidTo($now->add(\DateInterval::createFromDateString("$days day")));
                $certificateRepository->save($cert, true);
                $certificatesExpiringSoon["$certSerialNumber"] = $cert;
                unset($certificatesNotExpiringSoon["$certSerialNumber"]);
            } elseif ($i === 1) { // Expiring NOT soon
                $days = $maxDay + rand(1, CertificateRepository::NUMBER_OF_DAYS_WHEN_EXPIRY_IS_EXPECTED_SOON);
                $cert->setValidTo($now->add(\DateInterval::createFromDateString("$days day")));
                $certificateRepository->save($cert, true);
            } else {  // Expired
                $days = rand(1, CertificateRepository::NUMBER_OF_DAYS_WHEN_EXPIRY_IS_EXPECTED_SOON);
                $cert->setValidTo($now->add(\DateInterval::createFromDateString("-$days day")));
                $certificateRepository->save($cert, true);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////

        // Load reminder
        $this->httpRemindLoad($kernelBrowser);
        $this->assertResponseStatusCodeSame(Response::HTTP_ACCEPTED); // HTTP status code = 202

        // Reminder notices stored in db (certificates x 2 = 1 for manager + 1 for end-user)
        $expectedNotice = count($certificatesExpiringSoon) * 2;
        $notices = $noticeRepository->findAll();
        $this->assertSame($expectedNotice, count($notices));

        // Check EMAILS sent (certificates x 2 = 1 for manager + 1 for end-user)
        $expectedEmailsSent = $expectedNotice;
        $this->commonCheckerEmailNoticeSendOfCertificatesToBeProcessed(
            expectedEmailsSent: $expectedEmailsSent,
            certificatesToBeProcessed: $certificatesExpiringSoon,
            certificatesNotToBeProcessed: $certificatesNotExpiringSoon
        );

        // Check with ADMIN role that e-mail notifications are displayed for certificates due to expire soon.
        $this->commonCheckerEmailNoticeOnExpiringSoonCertPageForAdminRole($certificatesExpiringSoon, $kernelBrowser);

        // Check with ADMIN role that there are no e-mail notifications for certificates that won't be expiring soon.
        $this->commonCheckerNoEmailNoticeOnEachCertPageForAdminRole($certificatesNotExpiringSoon, $kernelBrowser);

        //////////////////////////////////////////////////////////////////////////////////
    }
}
