<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Tests\E2e;

use Symfony\Component\BrowserKit\HttpBrowser as HttpBrowserClient;
use Symfony\Component\Panther\Client as PantherClient;
use Symfony\Component\Panther\PantherTestCase;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertFalse;
use function PHPUnit\Framework\assertIsBool;
use function PHPUnit\Framework\assertTrue;

/* *****************************************************
Panther
https://github.com/symfony/panther#gitlab-ci-integration
https://github.com/symfony/panther#docker-integration
https://github.com/symfony/panther#using-an-external-web-server
https://github.com/symfony/panther#interactive-mode

BrowserKit
https://symfony.com/doc/current/components/browser_kit.html
https://symfony.com/doc/current/http_client.html

Testing
https://symfony.com/doc/current/testing.html
https://symfony.com/doc/current/testing/bootstrap.html
https://symfony.com/doc/current/http_client.html#http-client-and-responses

Testing database
https://symfony.com/doc/current/testing.html#configuring-a-database-for-tests
https://symfony.com/doc/current/testing.html#load-dummy-data-fixtures
https://symfony.com/doc/current/testing.html#resetting-the-database-automatically-before-each-test
https://symfony.com/doc/current/testing/database.html#functional-testing-of-a-doctrine-repository


https://phpinsights.com/
https://getcomposer.org/doc/03-cli.md#status
https://getcomposer.org/doc/03-cli.md#suggests
https://fr.wikipedia.org/wiki/R%C3%A9cit_utilisateur
https://symfony.com/doc/6.3/routing.html#localized-routes-i18n
https://github.com/MacPaw/symfony-health-check-bundle
https://codeception.com/12-15-2013/testing-emails-in-php.html

Vidéo - Tester avec Symfony
https://www.youtube.com/playlist?list=PLUiuGjup8Vg4hx6dZrPpRoSwmmO6wK6T2
https://gitlab.com/DeveloppeurMuscle/symblog
https://gitlab.com/DeveloppeurMuscle/symrecipe

Make
https://gitlab.com/DeveloppeurMuscle/symblog/-/blob/master/Makefile
https://gitlab.adullact.net/soluris/madis/-/blob/develop/Makefile

Infection
https://www.spiriit.com/blog/fonctionnement-infection-php-framework-de-test/
https://infection.github.io/guide/command-line-options.html
https://infection.github.io/guide/supported-test-frameworks.html

Codeception vs Behat
https://codeception.com/docs/BDD

Behat
https://symfonycasts.com/blog/behat-symfony
https://nahan.fr/tutoriel-test-behat-sur-un-projet-symfony
https://nahan.fr/behat-et-symfonypage
https://www.mon-code.net/article/165/utiliser-behat-pour-ecrire-les-tests-fonctionnels-d-une-commande-symfony
https://blog.eleven-labs.com/fr/behat-structurez-vos-tests-fonctionnels/
http://thomasrossier.fr/papers/Installation-configuration-et-fonctionnement-Behat.php
https://grafikart.fr/tutoriels/behaviour-driven-development-behat-552
https://swag.industries/behat-tutorial-part-2-testing-symfony-6-application/
https://swag.industries/using-behat-part-1-simple-testing/
https://github.com/Sylius/Sylius/blob/1.12/features/account/registering.feature

******************************************************* */
class EndToEndTest extends PantherTestCase
{
    public function testEndToEndHomePage(): void
    {
//        $symfonyClient = static::createClient(); // A cute kitty: Symfony's functional test tool
//        $httpBrowserClient = static::createHttpBrowserClient(); // An agile lynx: HttpBrowser
//        $pantherClient = static::createPantherClient(); // A majestic Panther
//        $firefoxClient = static::createPantherClient(['browser' => static::FIREFOX]); // A splendid Firefox
                 // Both HttpBrowser and Panther benefits from the built-in HTTP server
        //////////////////////////////////////////////////////////////////////////////////////////////////
        $client = static::createHttpBrowserClient();
//      $client = static::createPantherClient();
//                var_dump($client instanceof HttpBrowserClient);
//                var_dump($client instanceof PantherClient);
        //////////////////////////////////////////////////////////////////////////////////////////////////
        //        $client = self::createPantherClient(
        //            [],
        //            [],
        //            [
        //                'capabilities' => [
        //                    'goog:loggingPrefs' => [
        //                        'browser' => 'ALL', // calls to console.* methods
        //                        'performance' => 'ALL', // performance data
        //                    ],
        //                ],
        //            ]
        //        );
        //        $crawler = $client->request('GET', '/');
        //        $consoleLogs = $client->getWebDriver()->manage()->getLog('browser'); // console logs
        //        $performanceLogs = $client->getWebDriver()->manage()->getLog('performance'); // performance
        //            dump($consoleLogs);
        //            dump($performanceLogs);
        //////////////////////////////////////////////////////////////////////////////////////////////////
        $crawler = $client->request('GET', '/account/login');

        // Basic checks
        // $this->assertResponseHasHeader("Content-Security-Policy"); // FAIL with Panther  and HttpBrowser
            // FAIL Panther     LogicException: HttpFoundation Response object is not available when using WebDriver
            // FAIL HttpBrowser PantherTestCase::getResponse(): Return value
            //                  must be of type HttpFoundation\Response, BrowserKit\Response returned

        // HTML content checks
        $expectedTitle = 'Login - Tajine - Registration authority (RA)';
        $this->assertPageTitleSame("$expectedTitle");           // DOM-Crawler assertion
        $this->assertSelectorTextSame('h1', "Tajine (RA)"); // DOM-Crawler assertion
        if ($client instanceof PantherClient) {
            // works with Panther, but failed with  HttpBrowser
            $this->assertSelectorIsEnabled('h1'); // Panther assertion
            $this->assertSelectorIsVisible('h1'); // Panther assertion
        }
            // Some other examples:
            //        $this->assertSelectorIsEnabled('.search');
            //        $this->assertSelectorIsDisabled('[type="submit"]');
            //        $this->assertSelectorIsVisible('.errors');
            //        $this->assertSelectorIsNotVisible('.loading');
            //        $this->assertSelectorAttributeContains('.price', 'data-old-price', '42');
            //        $this->assertSelectorAttributeNotContains('.price', 'data-old-price', '36');

        // Session cookie checks
        $this->assertBrowserHasCookie('webapp'); // session cookie
        $session = $client->getCookieJar()->get('webapp'); // Panther or HttpBrowser
        assertTrue($session->isHttpOnly(), "Session cookie -> 'HTTP only' flag is not TRUE");
        assertFalse($session->isSecure(), "Session cookie -> 'Secure' flag is not FALSE");
        if ($client instanceof HttpBrowserClient) {
            assertEquals( //  works with HttpBrowser, but failed with Panther
                "strict",
                $session->getSameSite(),
                "Session cookie -> 'SameSite' attribute is not 'strict'"
            );
        }
        assertEquals("/", $session->getPath(), "Session cookie -> 'Path' is not '/'");
        assertEquals("127.0.0.1", $session->getDomain(), "Session cookie -> 'Domain' is not '127.0.0.1'");
    }
}
