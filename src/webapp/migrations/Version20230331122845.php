<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230331122845 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add [ revoked ] and [ expired ] fields on [ certificate ] table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE certificate ADD revoked BOOLEAN NOT NULL DEFAULT FALSE');
        $this->addSql('ALTER TABLE certificate ADD expired BOOLEAN NOT NULL DEFAULT FALSE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE certificate DROP revoked');
        $this->addSql('ALTER TABLE certificate DROP expired');
    }
}
