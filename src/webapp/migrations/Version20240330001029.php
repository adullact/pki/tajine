<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240330001029 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add [ user_notice ] table ';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE user_notice_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE user_notice (id INT NOT NULL, notified_user_id INT NOT NULL, certificate_id INT DEFAULT NULL, sent_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, notice_type INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_2D5F5EFFF5D8C9A7 ON user_notice (notified_user_id)');
        $this->addSql('CREATE INDEX IDX_2D5F5EFF99223FFD ON user_notice (certificate_id)');
        $this->addSql('COMMENT ON COLUMN user_notice.sent_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE user_notice ADD CONSTRAINT FK_2D5F5EFFF5D8C9A7 FOREIGN KEY (notified_user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_notice ADD CONSTRAINT FK_2D5F5EFF99223FFD FOREIGN KEY (certificate_id) REFERENCES certificate (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE user_notice_id_seq CASCADE');
        $this->addSql('ALTER TABLE user_notice DROP CONSTRAINT FK_2D5F5EFFF5D8C9A7');
        $this->addSql('ALTER TABLE user_notice DROP CONSTRAINT FK_2D5F5EFF99223FFD');
        $this->addSql('DROP TABLE user_notice');
    }
}
