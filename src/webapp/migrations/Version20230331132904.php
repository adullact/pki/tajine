<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230331132904 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add [ authority_id ] field on [ certificate ] table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE certificate ADD authority_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE certificate ADD CONSTRAINT FK_219CDA4AC2CD957E FOREIGN KEY (authority_id) REFERENCES certificate_authority (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_219CDA4AC2CD957E ON certificate (authority_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE certificate DROP CONSTRAINT FK_219CDA4AC2CD957E');
        $this->addSql('DROP INDEX IDX_219CDA4AC2CD957E');
        $this->addSql('ALTER TABLE certificate DROP authority_id');
    }
}
