<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230327223624 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add [ used_by_id ] field on [ certificate ] table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE certificate ADD used_by_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE certificate ADD CONSTRAINT FK_219CDA4A4C2B72A8 FOREIGN KEY (used_by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_219CDA4A4C2B72A8 ON certificate (used_by_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE certificate DROP CONSTRAINT FK_219CDA4A4C2B72A8');
        $this->addSql('DROP INDEX IDX_219CDA4A4C2B72A8');
        $this->addSql('ALTER TABLE certificate DROP used_by_id');
    }
}
