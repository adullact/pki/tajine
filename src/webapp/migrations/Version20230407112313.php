<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230407112313 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'clean up old certificate';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER INDEX idx_219cda4ac2cd957e RENAME TO IDX_219CDA4A81EC865B');


        $this->addSql('TRUNCATE certificate CASCADE');
        $this->addSql('TRUNCATE certificate_authority CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER INDEX idx_219cda4a81ec865b RENAME TO idx_219cda4ac2cd957e');
    }
}
