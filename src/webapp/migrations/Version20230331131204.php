<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230331131204 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'add [ certificate_authority ] table';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE certificate_authority_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE certificate_authority (id INT NOT NULL, serial_number VARCHAR(255) DEFAULT NULL, subject_key_identifier VARCHAR(255) DEFAULT NULL, revoked BOOLEAN NOT NULL, expired BOOLEAN NOT NULL, valid_from TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, valid_to TIMESTAMP(0) WITH TIME ZONE DEFAULT NULL, subject TEXT DEFAULT NULL, public_key TEXT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('COMMENT ON COLUMN certificate_authority.valid_from IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('COMMENT ON COLUMN certificate_authority.valid_to IS \'(DC2Type:datetimetz_immutable)\'');
        $this->addSql('ALTER TABLE certificate ALTER revoked DROP DEFAULT');
        $this->addSql('ALTER TABLE certificate ALTER expired DROP DEFAULT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE certificate_authority_id_seq CASCADE');
        $this->addSql('DROP TABLE certificate_authority');
        $this->addSql('ALTER TABLE certificate ALTER revoked SET DEFAULT false');
        $this->addSql('ALTER TABLE certificate ALTER expired SET DEFAULT false');
    }
}
