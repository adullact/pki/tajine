<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230407113817 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'clean up user';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('TRUNCATE "user" CASCADE');
        $this->addSql(
            'INSERT INTO "user" ( id,
                                      email,
                                      roles,
                                      password,
                                      is_active  )
                         VALUES    (  1,
                                      \'admin-tajine@example.org\',
                                      \'["ROLE_ADMIN"]\',
                                      \'$2y$13$AtLx12OyrkdkNz3vAh06deqVR8m/xI3PpzqPmo/e51DzckpM24nKK\',
                                      true   )'
        );
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
    }
}
