# Dev tool - PostgreSQL and Mailcatcher via Docker

## Prerequisites

- [Docker](https://docs.docker.com/engine/install/) `19.03.0` (at least) is required
- [Docker Compose](https://docs.docker.com/compose/install/) `1.27.0` (at least) is required

## Ports, URL and credentials (user/password)

| Service  | Port | URL                                     | User             | Password       |
|----------|------|-----------------------------------------|------------------|----------------|
| Database | 5432 | `jdbc:postgresql://localhost:5432/tajine_pg_database` | `tajine_pg_user` | `tajine_pg_password_ChangeIt` |
| SMTP     | 1025 | `smtp://localhost:1025`                 |                  |                |
| Webmail  | 1080 | `http://localhost:1080`                 |                  |                |

Tip:
if you copy [`.env.dist`](.env.dist) file to `.env` file,
you can change **port** numbers, **IP** adresses and **database** credentials.

### Option 1: uses same database, if it already exists

```shell
# build docker image + uses same database, if it already exists
docker-compose up --build
```

### Option 2: uses a new database

```shell
# build docker images + uses a new database
docker-compose rm -fsv
docker-compose up --build
```
