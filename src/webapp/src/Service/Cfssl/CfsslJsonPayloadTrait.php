<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service\Cfssl;

use App\Service\Certificate\CertificateDataFormatter;
use App\Service\Cfssl\Exception\InvalidAlgorithmException;
use App\Service\Cfssl\Exception\KeySizeRangeException;

use function Symfony\Component\String\u;

trait CfsslJsonPayloadTrait
{
//    Replace $rsaKeySizeMin and $rsaKeySizeMax, when PHP 8.2 is required
//    const RSA_KEY_SIZE_MIN = 2048;
//    const RSA_KEY_SIZE_MAX = 8192;

    /**
     * @var int minimal RSA key size allowed by CFSSL.
     */
    private int $rsaKeySizeMin = 2048; // see: https://github.com/cloudflare/cfssl/blob/master/csr/csr.go#L74

    /**
     * @var int maximal RSA key size allowed by CFSSL.
     */
    private int $rsaKeySizeMax = 8192; // see: https://github.com/cloudflare/cfssl/blob/master/csr/csr.go#L74

    private array $allowedAlgo = ['rsa']; // see: https://github.com/cloudflare/cfssl/blob/master/csr/csr.go#L70C4
//  private array $allowedAlgo = ['rsa','ecdsa','ed25519']; // CFSSL supports only ECDSA, RSA and ed25519 algorithms.

    private CertificateDataFormatter $certificateDataFormatter;

    public function getRsaKeySizeMin(): int
    {
        return $this->rsaKeySizeMin;
    }

    public function getRsaKeySizeMax(): int
    {
        return $this->rsaKeySizeMax;
    }

    public function getAllowedAlgo(): array
    {
        return $this->allowedAlgo;
    }

    public function isAllowedAlgo(string $algo): bool
    {
        return in_array(needle: "$algo", haystack: $this->getAllowedAlgo(), strict: true);
    }

    public function getCfsslNewCertificateJsonPayload(
        string $countryName,
        string $stateOrProvinceName,
        string $localityName,
        string $organizationName,
        string $organizationalUnit,
        string $commonName,
        string $keyAlgo = 'rsa',
        int $keySize = 2048
    ): string {
        $algo = u($keyAlgo)->trim()->lower()->toString();
        if (false === $this->isAllowedAlgo("$algo")) {
            throw new InvalidAlgorithmException("Algorithm [ $algo ] is not supported.");
        }
        if ($algo === 'rsa') {
            if ($keySize < $this->rsaKeySizeMin) {
                throw new KeySizeRangeException("RSA key size [ $keySize ] is too weak. < " . $this->rsaKeySizeMin);
            }
            if ($keySize > $this->rsaKeySizeMax) {
                throw new KeySizeRangeException("RSA key size [ $keySize ] is too large. > " . $this->rsaKeySizeMax);
            }
        }
        $jsonPayload = '{
  "request": {
    "hosts": [""],
    "key": {
        "algo":  "' . $algo . '",
        "size": ' . $keySize . '
    },
    "names": [
      {
        "C": "' . $this->certificateDataFormatter->normalizesData(data: $countryName, useSlugger: true) . '",
        "ST": "' . $this->certificateDataFormatter->normalizesData(data: $stateOrProvinceName, useSlugger: true) . '",
        "L": "' . $this->certificateDataFormatter->normalizesData(data: $localityName, useSlugger: true) . '",
        "O": "' . $this->certificateDataFormatter->normalizesData(data: $organizationName, useSlugger: true) . '",
        "OU": "' . $this->certificateDataFormatter->normalizesData(data: $organizationalUnit, useSlugger: true) . '"
      }
    ],
    "CN": "' . $this->certificateDataFormatter->normalizesData(data: $commonName, useSlugger: false) . '"
  }
}';
        return $jsonPayload;
    }
}
