<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service\Cfssl;

use App\Service\Cfssl\Exception\FailedHttpRequestToCfsslServerException;
use App\Service\Cfssl\Exception\FailedCfsslRequestException;
use App\Service\Cfssl\Exception\InvalidCfsslEndpointException;
use App\Service\Cfssl\Exception\InvalidHttpMethodException;

////////////////////////////////////////////////////////////////////////////////////////////////////
// CFSSL Error code
// https://github.com/cloudflare/cfssl/blob/master/doc/errorcode.txt
//                      1XXX: CertificateError
//                      2XXX: PrivateKeyError
//                      ...
//                      11XXX: CertStoreError
//                           11100: InsertFailed
//                           11200: RecordNotFound
////////////////////////////////////////////////////////////////////////////////////////////////////
// CFSSL API endpoints
// https://github.com/cloudflare/cfssl/blob/master/doc/api/intro.txt
////////////////////////////////////////////////////////////////////////////////////////////////////
//        health        Return health status
//                      https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_health.txt
//                      Endpoint: /api/v1/cfssl/health
//                      Method:   GET
////////////////////////////////////////////////////////////////////////////////////////////////////
//        certinfo      Lookup a certificate's info
//                      https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_certinfo.txt
//                      Endpoint: /api/v1/cfssl/certinfo
//                      Method:   POST
////////////////////////////////////////////////////////////////////////////////////////////////////
//        info          Information about the CA, including the CA certificate
//                      https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_info.txt
//                      Endpoint: /api/v1/cfssl/info
//                      Method:   POST
////////////////////////////////////////////////////////////////////////////////////////////////////
//        newcert       Generate a new private key and certificate
//                      https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_newcert.txt
//                      Endpoint: /api/v1/cfssl/newcert
//                      Method:   POST
////////////////////////////////////////////////////////////////////////////////////////////////////
//        revoke        Revoke a certificate
//                      https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_revoke.txt
//                      Endpoint: /api/v1/cfssl/revoke
//                      Method:   POST
////////////////////////////////////////////////////////////////////////////////////////////////////



trait CfsslEndpointTrait
{
    // Replace $newCertificatePath, $revokeCertificatePath and $infoPath properties
    // by class constant, when PHP 8.2 is required
    //          const NEW_CERTIFICATE_PATH = '/api/v1/cfssl/newcert';
    //          const REVOKE_CERTIFICATE_PATH = '/api/v1/cfssl/revoke';
    //          const INFO_PATH = '/api/v1/cfssl/certinfo';

    private array $cfsslEndpoints = [
        'certinfo' => [
            'path' => '/api/v1/cfssl/certinfo',
            'method' => 'POST',
            'implemented' => false,
        ],
        'health' => [
            'path' => '/api/v1/cfssl/health',
            'method' => 'GET',
            'implemented' => false,
        ],
        'info' => [
            'path' => '/api/v1/cfssl/info',
            'method' => 'POST',
            'implemented' => false,
        ],
        'newcert' => [
            'path' => '/api/v1/cfssl/newcert',
            'method' => 'POST',
            'implemented' => true,
        ],
        'revoke' => [
            'path' => '/api/v1/cfssl/revoke',
            'method' => 'POST',
            'implemented' => false,
        ],
    ];

    private string $newCertificatePath = '/api/v1/cfssl/newcert';
    private string $revokeCertificatePath = '/api/v1/cfssl/revoke';
    private string $infoPath = '/api/v1/cfssl/certinfo';

    private string $cfsslApiServer;


    public function getCfsslEndpoinds(): array
    {
        return $this->cfsslEndpoints;
    }

    public function getCfsslNewCertificateUrl(): string
    {
        return $this->cfsslApiServer . $this->cfsslEndpoints['newcert']['path'];
    }

    public function getCfsslApiServer(): string
    {
        return $this->cfsslApiServer;
    }
    public function setCfsslApiServer(string $cfsslApiServer): void
    {
        $url = \trim($cfsslApiServer);
        if (false === $this->isValidUrl($url)) {
            throw new InvalidCfsslEndpointException("Invalid CFSSL server URL");
        }
        if ('/' === \substr($url, -1)) { // remove final [ / ]
            $url = \substr($url, 0, \strlen($url) - 1);
        }
        $this->cfsslApiServer = $url;
    }

    private function isValidUrl(string $url): bool
    {
        if (
            false === \filter_var($url, FILTER_VALIDATE_URL)
            | \is_null(\parse_url($url, PHP_URL_HOST))
            | \is_null(\parse_url($url, PHP_URL_SCHEME))
            | (\parse_url($url, PHP_URL_SCHEME) !== 'http' &&  \parse_url($url, PHP_URL_SCHEME) !== 'https')
            | !\is_null(\parse_url($url, PHP_URL_FRAGMENT))
            | !\is_null(\parse_url($url, PHP_URL_QUERY))
        ) {
            return false;
        }
        return true;
    }
}
