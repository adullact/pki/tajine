<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service\Cfssl;

use App\Service\Cfssl\Exception\FailedHttpRequestToCfsslServerException;
use App\Service\Cfssl\Exception\FailedCfsslRequestException;
use App\Service\Cfssl\Exception\InvalidCfsslEndpointException;
use App\Service\Cfssl\Exception\InvalidHttpMethodException;

////////////////////////////////////////////////////////////////////////////////////////////////////
// CFSSL Error code
// https://github.com/cloudflare/cfssl/blob/master/doc/errorcode.txt
//                      1XXX: CertificateError
//                      2XXX: PrivateKeyError
//                      ...
//                      11XXX: CertStoreError
//                           11100: InsertFailed
//                           11200: RecordNotFound
////////////////////////////////////////////////////////////////////////////////////////////////////
// CFSSL API endpoints
// https://github.com/cloudflare/cfssl/blob/master/doc/api/intro.txt
////////////////////////////////////////////////////////////////////////////////////////////////////
//        health        Return health status
//                      https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_health.txt
//                      Endpoint: /api/v1/cfssl/health
//                      Method:   GET
////////////////////////////////////////////////////////////////////////////////////////////////////
//        certinfo      Lookup a certificate's info
//                      https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_certinfo.txt
//                      Endpoint: /api/v1/cfssl/certinfo
//                      Method:   POST
////////////////////////////////////////////////////////////////////////////////////////////////////
//        info          Information about the CA, including the CA certificate
//                      https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_info.txt
//                      Endpoint: /api/v1/cfssl/info
//                      Method:   POST
////////////////////////////////////////////////////////////////////////////////////////////////////
//        newcert       Generate a new private key and certificate
//                      https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_newcert.txt
//                      Endpoint: /api/v1/cfssl/newcert
//                      Method:   POST
////////////////////////////////////////////////////////////////////////////////////////////////////
//        revoke        Revoke a certificate
//                      https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_revoke.txt
//                      Endpoint: /api/v1/cfssl/revoke
//                      Method:   POST
////////////////////////////////////////////////////////////////////////////////////////////////////



trait CfsslRequestTrait
{
// Replace $newCertificatePath, $revokeCertificatePath and $infoPath properties
// by class constant, when PHP 8.2 is required
//          const NEW_CERTIFICATE_PATH = '/api/v1/cfssl/newcert';
//          const REVOKE_CERTIFICATE_PATH = '/api/v1/cfssl/revoke';
//          const INFO_PATH = '/api/v1/cfssl/certinfo';

    private array $cfsslEndpoints = [
        'certinfo' => [
            'path' => '/api/v1/cfssl/certinfo',
            'method' => 'POST',
            'implemented' => false,
        ],
        'health' => [
            'path' => '/api/v1/cfssl/health',
            'method' => 'GET',
            'implemented' => false,
        ],
        'info' => [
            'path' => '/api/v1/cfssl/info',
            'method' => 'POST',
            'implemented' => false,
        ],
        'newcert' => [
            'path' => '/api/v1/cfssl/newcert',
            'method' => 'POST',
            'implemented' => true,
        ],
        'revoke' => [
            'path' => '/api/v1/cfssl/revoke',
            'method' => 'POST',
            'implemented' => false,
        ],
    ];

    private string $cfsslApiServer;


    private function isValidUrl(string $url): bool
    {
        if (
            false === \filter_var($url, FILTER_VALIDATE_URL)
            | \is_null(\parse_url($url, PHP_URL_HOST))
            | \is_null(\parse_url($url, PHP_URL_SCHEME))
            | (\parse_url($url, PHP_URL_SCHEME) !== 'http' &&  \parse_url($url, PHP_URL_SCHEME) !== 'https')
            | !\is_null(\parse_url($url, PHP_URL_FRAGMENT))
            | !\is_null(\parse_url($url, PHP_URL_QUERY))
        ) {
            return false;
        }
        return true;
    }

    public function setCfsslApiServer(string $cfsslApiServer): void
    {
        $url = \trim($cfsslApiServer);
        if (false === $this->isValidUrl($url)) {
            throw new InvalidCfsslEndpointException("Invalid CFSSL server URL");
        }
        if ('/' === \substr($url, -1)) { // remove final [ / ]
            $url = \substr($url, 0, \strlen($url) - 1);
        }
        $this->cfsslApiServer = $url;
    }

    public function getCfsslApiServer(): string
    {
        return $this->cfsslApiServer;
    }

    public function isValidCfsslEndpoint(string $endpointName): bool
    {
        if (
            isset($this->cfsslEndpoints["$endpointName"])
            && true === $this->cfsslEndpoints["$endpointName"]['implemented']
        ) {
            return true;
        }
        return false;
    }

    public function getCfsslEndpointUrl(string $endpointName): string
    {
        if (false === $this->isValidCfsslEndpoint($endpointName)) {
            throw new InvalidCfsslEndpointException("CFSSL endpoint is not implemented: [ $endpointName ]");
        }
        return  $this->cfsslApiServer . $this->cfsslEndpoints["$endpointName"]['path'];
    }

    public function getCfsslEndpointHttpMethod(string $endpointName): string
    {
        if (false === $this->isValidCfsslEndpoint($endpointName)) {
            throw new InvalidCfsslEndpointException("CFSSL endpoint is not implemented: [ $endpointName ]");
        }
        return  $this->cfsslEndpoints["$endpointName"]['method'];
    }


    /**
     * @throws InvalidHttpMethodException
     * @throws InvalidCfsslEndpointException
     * @throws FailedHttpRequestToCfsslServerException
     */
    public function loadCfsslRequest(
        string $endpointName,
        string $jsonPayload
    ): mixed {
        $cfsslUrl = $this->getCfsslEndpointUrl(endpointName: $endpointName);
        $httpMethod =  $this->getCfsslEndpointHttpMethod(endpointName: $endpointName);
        $allowedHttpMethod = ['POST', 'GET'];
        if (false === \in_array("$httpMethod", $allowedHttpMethod, true)) {
            throw new InvalidHttpMethodException("HTTP method [ $httpMethod ] is not allowed");
        }

        $ch = curl_init($cfsslUrl);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "$httpMethod");
        curl_setopt($ch, CURLOPT_POSTFIELDS, "$jsonPayload");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); # Return response instead of printing.
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type:application/json',
                'Content-Length: ' . strlen("$jsonPayload")
            )
        );
        $rawCfsslResponse = curl_exec($ch);
        curl_close($ch);
        if ($rawCfsslResponse === false) {
            throw new FailedHttpRequestToCfsslServerException("CFSSL - HTTP request failed [ $cfsslUrl ]");
        }
        return $rawCfsslResponse;
    }


    /**
     * @throws FailedCfsslRequestException
     */
    public function decodeCfsslResponse(
        string $jsonRaw,
    ): mixed {
        try {
            $json = json_decode(json: $jsonRaw, flags: JSON_THROW_ON_ERROR);
        } catch (\JsonException $e) {
            $msg = "Invalid CFSSL response: unable to decode JSON response [ " . $e->getMessage() . " ] ";
            throw new FailedCfsslRequestException("$msg");
        }

        if (!isset($json->success)) {
            throw new FailedCfsslRequestException("Invalid CFSSL response: missing [ sucess ] property");
        } elseif ($json->success === false && isset($json->errors)) {
            $cfsslErrorCode = $json->errors[0]->code;
            $cfsslErrorMsg = $json->errors[0]->message;
            $error = "Invalid CFSSL response: $cfsslErrorCode - $cfsslErrorMsg";
            throw new FailedCfsslRequestException("$error");
        } elseif ($json->success !== true) {
            throw new FailedCfsslRequestException("Invalid CFSSL response: unknown error");
        } elseif (!isset($json->result)) {
            throw new FailedCfsslRequestException("Invalid CFSSL response: missing [ result ] property");
        }
        return json_decode($jsonRaw, true);
        /* {   "success":false,
                "result":null,
                "errors":[{"code":405,"message":"Method is not allowed:\"GET\""}],
                "messages":[]    }       */
        /* {  "success":true,
                "result":{"certificate":"-----BEGIN CERTIFICATE-----\n ... \n-----END CERTIFICATE-----",
                "usages":["client auth"],
                "expiry":"8760h"},
                "errors":[],
                "messages":[]    }       */
    }
}
