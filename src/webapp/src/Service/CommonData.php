<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Bundle\SecurityBundle\Security;

class CommonData
{
    public function __construct(
        private readonly ContainerBagInterface $params,
        private Security $security,
    ) {
    }

    /**
     * Load common parameter used into templates
     *
     * @return array
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getCommonDataTemplate(): array
    {
        $data = [
            'appName' => $this->params->get('app.name'),
            'appShortName' => $this->params->get('app.shortname'),
            'softwareName' => $this->params->get('app.software.name'),
            'softwareRelease' => $this->params->get('app.software.release'),
            'app_timezone' => $this->params->get('app.timezone'),
            'app_i18n_default_locale' => $this->params->get('app.i18n.default_locale'),
            'app_session_lifetime' => $this->params->get('app.session_lifetime'),
            'app_admin_config_default_max_user_per_page' => $this->params->get(
                'app.admin_config.default_max_user_per_page'
            ),
            'app_admin_config_default_max_cert_per_page' => $this->params->get(
                'app.admin_config.default_max_cert_per_page'
            ),
            'app_manager_config_default_max_cert_per_page' => $this->params->get(
                'app.manager_config.default_max_cert_per_page'
            ),
            'app_user_config_reset_password_token_lifetime' => $this->params->get(
                'app.user_config.reset_password_token_lifetime'
            ),
            'app_user_config_min_password_length' => $this->params->get(
                'app.user_config.min_password_length'
            ),
            'app_cert_config_min_password_length' => $this->params->get(
                'app.cert_config.min_password_length'
            ),
        ];
        return $data;
    }
}
