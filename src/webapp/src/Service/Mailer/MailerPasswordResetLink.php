<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service\Mailer;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Twig\Environment;

use function Symfony\Component\String\u;

class MailerPasswordResetLink
{
    use MailerTrait;

    private string $appShortName;
    private string $appName;

    /**
     * @throws NotFoundExceptionInterface
     * @throws ContainerExceptionInterface
     */
    public function __construct(
        private readonly MailerInterface $mailer,
        private readonly Environment $twig,
        private readonly ContainerBagInterface $containerBag,
    ) {
        if (!is_null($this->containerBag->get('app.shortname'))) {
            $this->appShortName = $this->containerBag->get('app.shortname');
            $this->appName = $this->containerBag->get('app.name');
        } else {
            $this->appShortName = 'Tajine (RA)'; // mandatory by xxxx()
            $this->appName = 'Tajine (RA) PKI'; // mandatory by xxxx()
        }
    }


    /**
     * @param Address $msgFrom
     * @param Address $msgTo
     * @return TemplatedEmail
     * @throws TransportExceptionInterface
     */
    public function sendPasswordResetLink(
        Address $msgFrom,
        Address $msgTo,
        string $resetPasswordUrl
    ): TemplatedEmail {
        $mailSubject = $this->twig->render('app_emails/forgot-password.subject.txt.twig', [
            'app_shortname' => $this->appShortName,
        ]);
        $email = (new TemplatedEmail()) ;
        $email->from($msgFrom);
        $email->to($msgTo);
        $this->addCommonTechnicalEmailHeaders($email);
        $mailSubject = u(\strip_tags(\nl2br($mailSubject)))->trim()->toString();
        $email->subject("$mailSubject");
        $email->htmlTemplate('app_emails/forgot-password.html.twig');
        $email->context([
            'app_name' => $this->appName,
            'reset_passord_url' => $resetPasswordUrl,
        ]);
        $this->mailer->send($email);
        return $email;
    }
}
