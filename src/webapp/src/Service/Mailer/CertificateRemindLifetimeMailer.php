<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service\Mailer;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Twig\Environment;

use function Symfony\Component\String\u;

class CertificateRemindLifetimeMailer
{
    use MailerTrait;

    private string $appShortName;
    private Address $emailFrom;
    private Address $remindCopyEmail;
    private bool $sendCopyOfRemind;


    /**
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(
        private MailerInterface $mailer,
        private Environment $twig,
        private readonly ContainerBagInterface $containerBag,
    ) {
        if (!is_null($this->containerBag->get('app.shortname'))) {
            $this->appShortName = $this->containerBag->get('app.shortname');
            $this->emailFrom = new Address($this->containerBag->get('app.email.from'));
            $this->sendCopyOfRemind = (bool) $this->containerBag->get('app.cert_remind.copy_to_alerting');
            $this->remindCopyEmail = new Address($this->containerBag->get('app.email.alerting_to'));
        } else {
            // mandatory for testSendEndOfLifeCertificateReminderEmailTo*()
            $this->appShortName = 'Tajine (RA)';
            $this->emailFrom = new Address('no-reply@test.example.org');
            $this->sendCopyOfRemind = true;
            $this->remindCopyEmail = new Address('sysadmin_bcc_remind@example.org');
        }
    }

    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function remindCertificateEndOfLifeToManager(
        Address $msgTo,
        Address $certificateEndUserEmail,
        string $certificateSerialNumber,
        ?\DateTimeImmutable $certificateValidFrom,
        ?\DateTimeImmutable $certificateValidTo,
        string $certificateSubject,
    ): TemplatedEmail|false {
        $validTo = $certificateValidTo->format('d/m/Y');
        $validFrom = $certificateValidFrom->format('d/m/Y');
        $mailSubject = $this->twig->render(
            'app_emails/certificate-lifetime-coming-soon.to-cert-manager.subject.txt.twig',
            [
                'app_shortname' => $this->appShortName,
                'cert_serial_number' => $certificateSerialNumber,
                'cert_valid_to' => $validTo,
            ]
        );
        $email =  (new TemplatedEmail()) ;
        $email->from($this->emailFrom);
        $email->to($msgTo);
        if ($this->sendCopyOfRemind === true) {
            $email->bcc($this->remindCopyEmail);
        }
        $this->addCommonTechnicalEmailHeaders($email);
        $email->getHeaders()->addTextHeader('X-Tajine-Certificate', "$certificateSerialNumber");
        $email->getHeaders()->addTextHeader('X-Tajine-Email-Type', "remindCertificateEndOfLifeToManager");

        $email->subject(subject: u(\strip_tags(\nl2br($mailSubject)))->trim()->toString());
        $email->htmlTemplate('app_emails/certificate-lifetime-coming-soon.to-cert-manager.html.twig');
        $email->context([
            'app_name' => $this->appShortName,
            'cert_user_email' => $certificateEndUserEmail->getAddress(),
            'cert_serial_number' => $certificateSerialNumber,
            'cert_valid_from' => $validFrom,
            'cert_valid_to' => $validTo,
            'cert_subject' => $certificateSubject,
        ]);
        try {
            $this->mailer->send($email);
            return $email;
        } catch (\Throwable $e) {
            return false;
        }
    }


    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function remindCertificateEndOfLifeToEndUser(
        Address $msgTo,
        string $certificateSerialNumber,
        ?\DateTimeImmutable $certificateValidFrom,
        ?\DateTimeImmutable $certificateValidTo,
        string $certificateSubject,
        string $certificateCreatorEmail,
    ): TemplatedEmail|false {
        $validTo = $certificateValidTo->format('d/m/Y');
        $validFrom = $certificateValidFrom->format('d/m/Y');
        $mailSubject = $this->twig->render(
            'app_emails/certificate-lifetime-coming-soon.to-cert-user.subject.txt.twig',
            [
                'app_shortname' => $this->appShortName,
                'cert_serial_number' => $certificateSerialNumber,
                'cert_valid_to' => $validTo,
            ]
        );
        $email =  (new TemplatedEmail()) ;
        $email->from($this->emailFrom);
        $email->to($msgTo);
        if ($this->sendCopyOfRemind === true) {
            $email->bcc($this->remindCopyEmail);
        }
        $this->addCommonTechnicalEmailHeaders($email);
        $email->getHeaders()->addTextHeader('X-Tajine-Certificate', "$certificateSerialNumber");
        $email->getHeaders()->addTextHeader('X-Tajine-Email-Type', "remindCertificateEndOfLifeToEndUser");
        $email->subject(subject: u(\strip_tags(\nl2br($mailSubject)))->trim()->toString());
        $email->htmlTemplate('app_emails/certificate-lifetime-coming-soon.to-cert-user.html.twig');
        $email->context([
            'app_name' => $this->appShortName,
            'cert_serial_number' => $certificateSerialNumber,
            'cert_valid_from' => $validFrom,
            'cert_valid_to' => $validTo,
            'cert_subject' => $certificateSubject,
            'cert_creator_email' => $certificateCreatorEmail,
        ]);
        try {
            $this->mailer->send($email);
            return $email;
        } catch (\Throwable $e) {
            return false;
        }
    }
}
