<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service\Mailer;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Part\DataPart;
use Twig\Environment;

use function Symfony\Component\String\u;

class CertificateMailer
{
    use MailerTrait;

    private string $appShortName;

    public function __construct(
        private MailerInterface $mailer,
        private Environment $twig,
        private readonly ContainerBagInterface $containerBag,
    ) {
        if (!is_null($this->containerBag->get('app.shortname'))) {
            $this->appShortName = $this->containerBag->get('app.shortname');
        } else {
            $this->appShortName = 'Tajine (RA)'; // mandatory by testSendEmailWithPkcs12FileToEndUser()
        }
    }

    /**
     * @param Address $msgFrom
     * @param Address $msgTo
     * @param string $certificateSerialNumber
     * @param int $certificateValidToTimestamp
     * @param string $pkcs12FileContent
     * @return TemplatedEmail
     * @throws TransportExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function sendPkc12FileByMail(
        Address $msgFrom,
        Address $msgTo,
        string $certificateSerialNumber,
        int $certificateValidToTimestamp,
        string $pkcs12FileContent,
    ): TemplatedEmail {
        $mailSubject = $this->twig->render('app_emails/send-certificate.subject.txt.twig', [
            'app_shortname' => $this->appShortName,
            'cert_serial_number' => $certificateSerialNumber,
        ]);
        $email =  (new TemplatedEmail()) ;
        $email->from($msgFrom);
        $email->to($msgTo);
        $this->addCommonTechnicalEmailHeaders($email);
        $email->subject(subject: u(\strip_tags(\nl2br($mailSubject)))->trim()->toString());
        $email->htmlTemplate('app_emails/send-certificate.html.twig');
        $email->context([
            'app_name' => $this->appShortName,
            'cert_serial_number' => $certificateSerialNumber,
            'cert_valid_to' => date('d/m/Y', $certificateValidToTimestamp),
        ]);
        $email->addPart(new DataPart(
            body: "$pkcs12FileContent",
            filename: "cert_$certificateSerialNumber.p12",
            contentType: "application/pkcs12"
        ));
        $this->mailer->send($email);
        return $email;
    }
}
