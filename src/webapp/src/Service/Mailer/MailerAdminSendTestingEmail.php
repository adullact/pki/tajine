<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service\Mailer;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class MailerAdminSendTestingEmail
{
    use MailerTrait;

    public function __construct(private MailerInterface $mailer)
    {
    }

    /**
     * @param Address $msgFrom
     * @param Address $msgTo
     * @return TemplatedEmail
     * @throws TransportExceptionInterface
     */
    public function sendTestingEmail(
        Address $msgFrom,
        Address $msgTo
    ): Email {
        $email = (new Email()) ;
        $email->from($msgFrom);
        $email->to($msgTo);
        $this->addCommonTechnicalEmailHeaders($email);
        $email->subject("Adullact PKI: email de test envoyé à " . $msgTo->getAddress());
        $email->text("Adullact PKI\nEmail de test envoyé à " .  $msgTo->getAddress());
        $email->html(
            "<h1>Adullact PKI</h1>
                   <p>Email de test envoyé à " . $msgTo->getAddress() . " !</p>"
        );
        $this->mailer->send($email);
        return $email;
    }
}
