<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class UserService
{
    public function __construct(
        private UserRepository $userRepository,
        private UserPasswordHasherInterface $userPasswordHasher,
        private readonly TokenGeneratorInterface $tokenGenerator,
    ) {
    }

    public function getUserByEmailAndCreateItIfNotExist(
        string $userMail,
        bool $isActive = false
    ): ?User {
        $user = $this->userRepository->findOneByEmail(email: $userMail);
        if (is_null($user)) {
            $plainPassword = $this->tokenGenerator->generateToken();
            $user = new User();
            $user->setEmail(email: $userMail);
            $user->setIsActive(isActive: $isActive);
            $user->setPassword($this->userPasswordHasher->hashPassword(user: $user, plainPassword: $plainPassword));
            $this->userRepository->save(entity: $user, flush: true);
        }
        return $user;
    }
}
