<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service\Certificate;

use App\Entity\Certificate;
use App\Entity\UserNotice;
use App\Repository\CertificateRepository;
use App\Repository\UserNoticeRepository;
use App\Service\Mailer\CertificateRemindLifetimeMailer;
use Symfony\Component\Mime\Address;

class CertificateRemindLifetime
{
    public function __construct(
        private readonly CertificateRepository $certificateRepository,
        private readonly CertificateRemindLifetimeMailer $remindLifetimeMailer,
        private readonly UserNoticeRepository $noticeRepository,
    ) {
    }


    /**
     * @return int|false
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function remindCertificateEndOfLifeToManager(): int|false
    {
        $certificatesExpiringSoon = $this->certificateRepository->findCertificatesExpiringSoon(30);
        $numberOfCertificates = count($certificatesExpiringSoon);
        $numberOfCertificatesProcessed = 0;
        foreach ($certificatesExpiringSoon as $certificate) {
            if (count($certificate->getUserNotices()) === 0) { // TODO refactor
                $resultEmailToManager = $this->remindToManager($certificate);
                if ($resultEmailToManager === true) {
                    $resultEmailToEndUser = $this->remindToEndUser($certificate);
                }
                if (isset($resultEmailToEndUser)) {
                    $numberOfCertificatesProcessed += 1;
                }
            }
        }

        if ($numberOfCertificates === $numberOfCertificatesProcessed) {
            // @TODO add log
        }
        return $numberOfCertificatesProcessed;
    }


    /**
     * @param Certificate $certificate
     * @return bool
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function remindToManager(Certificate $certificate): bool
    {
        $result = $this->remindLifetimeMailer->remindCertificateEndOfLifeToManager(
            msgTo: new Address($certificate->getCreatedBy()->getEmail()),
            certificateEndUserEmail: new Address($certificate->getUsedBy()->getEmail()),
            certificateSerialNumber: $certificate->getSerialNumber(),
            certificateValidFrom: $certificate->getValidFrom(),
            certificateValidTo: $certificate->getValidTo(),
            certificateSubject: $certificate->getSubject(),
        );
        if ($result === false) {
            return false;
        }
        $notice = new UserNotice();
        $notice->setSentAt(new \DateTimeImmutable())
            ->setNotifiedUser($certificate->getCreatedBy())
            ->setNoticeType(UserNotice::NOTICE_TYPE_CERT_REDMIND_TO_MANAGER)
            ->setCertificate($certificate);
        $this->noticeRepository->save($notice, true);
        return true;
    }

    /**
     * @param Certificate $certificate
     * @return bool
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    private function remindToEndUser(Certificate $certificate): bool
    {
        $result = $this->remindLifetimeMailer->remindCertificateEndOfLifeToEndUser(
            msgTo: new Address($certificate->getUsedBy()->getEmail()),
            certificateSerialNumber: $certificate->getSerialNumber(),
            certificateValidFrom: $certificate->getValidFrom(),
            certificateValidTo: $certificate->getValidTo(),
            certificateSubject: $certificate->getSubject(),
            certificateCreatorEmail: $certificate->getCreatedBy()->getEmail(),
        );
        if ($result === false) {
            return false;
        }
        $notice = new UserNotice();
        $notice->setSentAt(new \DateTimeImmutable())
            ->setNotifiedUser($certificate->getUsedBy())
            ->setNoticeType(UserNotice::NOTICE_TYPE_CERT_REDMIND_TO_END_USER)
            ->setCertificate($certificate);
        $this->noticeRepository->save($notice, true);
        return true;
    }
}
