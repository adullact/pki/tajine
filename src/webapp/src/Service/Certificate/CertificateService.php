<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service\Certificate;

use App\Entity\Certificate;
use App\Entity\CertificateAuthority;
use App\Entity\User;
use App\Repository\CertificateRepository;

class CertificateService
{
    public function __construct(private CertificateRepository $certificateRepository)
    {
    }

    public function registerNewCertificate(
        string $serialNumber,
        string $subject,
        string $rawPublicKey,
        User $usedBy,
        User $createdBy,
        \DateTimeImmutable $validFrom,
        \DateTimeImmutable $validTo,
        CertificateAuthority $certificateAuthority,
    ): Certificate {
        $certificate = new Certificate();
        $certificate->setSerialNumber(serialNumber: "$serialNumber");
        $certificate->setSubject(subject: "$subject");
        $certificate->setValidFrom(validFrom: $validFrom);
        $certificate->setValidTo(validTo: $validTo);
        $certificate->setUsedBy(usedBy: $usedBy);
        $certificate->setCreatedBy(createdBy: $createdBy);
        $certificate->setPublicKey(publicKey: $rawPublicKey);
        $certificate->setRevoked(revoked: false);
        $certificate->setExpired(expired: false);
        $certificate->setAuthority($certificateAuthority);
        $this->certificateRepository->save($certificate, true);
        return $certificate;
    }
}
