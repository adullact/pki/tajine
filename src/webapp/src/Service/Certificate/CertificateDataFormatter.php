<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service\Certificate;

use Symfony\Component\String\Slugger\AsciiSlugger;

use function Symfony\Component\String\u;

class CertificateDataFormatter
{
    /**
     * Normalises data for certificate
     * - by default, use slugger transforms that only includes safe ASCII characters
     * - removes spaces (\n, \t, ' '): in duplicate, at the beginning and end
     * - capitalize
     */
    public function normalizesData(string $data, bool $useSlugger = true): string
    {
        if ($useSlugger === true) {
            $slugger = new AsciiSlugger();
            $sluggedData = $slugger->slug($data)->toString();
            return u($sluggedData)->trim()->collapseWhitespace()->upper()->toString();
        }
        $forbiddenSpecialCharacters = ['"', "'", '[', ']', '{', '}', ',', ':'];
        $data = str_replace($forbiddenSpecialCharacters, " ", "$data");
        return u($data)->trim()->collapseWhitespace()->upper()->toString();
    }
}
