<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Service\Certificate;

use App\Entity\CertificateAuthority;
use App\Repository\CertificateAuthorityRepository;

class CertificateAuthorityService
{
    public function __construct(private CertificateAuthorityRepository $certificateAuthorityRepository)
    {
    }

    public function getCertificateAuthorityByAuthorityKeyIdentifier(
        string $authorityKeyIdentifier,
        string $certCrl
    ): CertificateAuthority {
        $certificateAuthority = $this->certificateAuthorityRepository->findOneBy([
            'subjectKeyIdentifier' => (string)$authorityKeyIdentifier,
            'crl' => (string)$certCrl,
        ]);
        if (is_null($certificateAuthority)) {
            $certificateAuthority = new CertificateAuthority();
            $certificateAuthority->setRevoked(false);
            $certificateAuthority->setExpired(false);
            $certificateAuthority->setSubjectKeyIdentifier("$authorityKeyIdentifier");
            $certificateAuthority->setCrl("$certCrl");
            // @@@TODO refactor to use https://symfony.com/doc/current/reference/constraints/Url.html
        } else {
            // @@@TODO update CRL is NULL in database
        }
        $this->certificateAuthorityRepository->save($certificateAuthority, true);
        return $certificateAuthority;
    }
}
