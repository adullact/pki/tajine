<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
#[UniqueEntity(fields: ['email'], message: 'There is already an account with this email')]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 180, unique: true)]
    private ?string $email = null;

    #[ORM\Column]
    private array $roles = [];

    /**
     * @var string The hashed password
     */
    #[ORM\Column]
    private ?string $password = null;

    #[ORM\Column]
    private ?bool $isActive = null;

    #[ORM\OneToMany(mappedBy: 'createdBy', targetEntity: Certificate::class)]
    private Collection $createdCertificates;

    #[ORM\OneToMany(mappedBy: 'usedBy', targetEntity: Certificate::class)]
    private Collection $usedCertificates;

    #[ORM\OneToOne(mappedBy: 'usedBy', cascade: ['persist', 'remove'])]
    private ?UserToken $userToken = null;

    #[ORM\OneToMany(mappedBy: 'notifiedUser', targetEntity: UserNotice::class, orphanRemoval: true)]
    private Collection $userNotices;

    public function __construct()
    {
        $this->createdCertificates = new ArrayCollection();
        $this->usedCertificates = new ArrayCollection();
        $this->userNotices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string|null
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function isIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return Collection<int, Certificate>
     */
    public function getCreatedCertificate(): Collection
    {
        return $this->createdCertificates;
    }

    public function addCreatedCertificate(Certificate $createdCertificate): self
    {
        if (!$this->createdCertificates->contains($createdCertificate)) {
            $this->createdCertificates->add($createdCertificate);
            $createdCertificate->setCreatedBy($this);
        }

        return $this;
    }

    public function removeCreatedCertificate(Certificate $createdCertificate): self
    {
        if ($this->createdCertificates->removeElement($createdCertificate)) {
            // set the owning side to null (unless already changed)
            if ($createdCertificate->getCreatedBy() === $this) {
                $createdCertificate->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Certificate>
     */
    public function getUsedCertificates(): Collection
    {
        return $this->usedCertificates;
    }

    public function addUsedCertificate(Certificate $usedCertificate): self
    {
        if (!$this->usedCertificates->contains($usedCertificate)) {
            $this->usedCertificates->add($usedCertificate);
            $usedCertificate->setUsedBy($this);
        }

        return $this;
    }

    public function removeUsedCertificate(Certificate $usedCertificate): self
    {
        if ($this->usedCertificates->removeElement($usedCertificate)) {
            // set the owning side to null (unless already changed)
            if ($usedCertificate->getUsedBy() === $this) {
                $usedCertificate->setUsedBy(null);
            }
        }

        return $this;
    }

    public function getUserToken(): ?UserToken
    {
        return $this->userToken;
    }

    public function setUserToken(UserToken $userToken): static
    {
        // set the owning side of the relation if necessary
        if ($userToken->getUsedBy() !== $this) {
            $userToken->setUsedBy($this);
        }

        $this->userToken = $userToken;

        return $this;
    }

    /**
     * @return Collection<int, UserNotice>
     */
    public function getUserNotices(): Collection
    {
        return $this->userNotices;
    }

    public function addUserNotice(UserNotice $userNotice): static
    {
        if (!$this->userNotices->contains($userNotice)) {
            $this->userNotices->add($userNotice);
            $userNotice->setNotifiedUser($this);
        }

        return $this;
    }

    public function removeUserNotice(UserNotice $userNotice): static
    {
        if ($this->userNotices->removeElement($userNotice)) {
            // set the owning side to null (unless already changed)
            if ($userNotice->getNotifiedUser() === $this) {
                $userNotice->setNotifiedUser(null);
            }
        }

        return $this;
    }
}
