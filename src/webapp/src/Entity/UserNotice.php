<?php

namespace App\Entity;

use App\Repository\UserNoticeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserNoticeRepository::class)]
class UserNotice
{
    public const  NOTICE_TYPE_CERT_REDMIND_TO_MANAGER = 2;
    public const NOTICE_TYPE_CERT_REDMIND_TO_END_USER = 3;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $sentAt = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'userNotices')]
    #[ORM\JoinColumn(nullable: false)]
    private ?user $notifiedUser = null;

    #[ORM\Column]
    private ?int $noticeType = null;

    #[ORM\ManyToOne(targetEntity: Certificate::class, inversedBy: 'userNotices')]
    private ?certificate $certificate = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSentAt(): ?\DateTimeImmutable
    {
        return $this->sentAt;
    }

    public function setSentAt(\DateTimeImmutable $sentAt): static
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    public function getNotifiedUser(): ?user
    {
        return $this->notifiedUser;
    }

    public function setNotifiedUser(?user $notifiedUser): static
    {
        $this->notifiedUser = $notifiedUser;

        return $this;
    }

    public function getNoticeType(): ?int
    {
        return $this->noticeType;
    }

    public function setNoticeType(int $noticeType): static
    {
        $this->noticeType = $noticeType;

        return $this;
    }

    public function getCertificate(): ?certificate
    {
        return $this->certificate;
    }

    public function setCertificate(?certificate $certificate): static
    {
        $this->certificate = $certificate;

        return $this;
    }
}
