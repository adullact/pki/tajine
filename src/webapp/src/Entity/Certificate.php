<?php

namespace App\Entity;

use App\Repository\CertificateRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CertificateRepository::class)]
class Certificate
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $serialNumber = null;

    #[ORM\Column(type: Types::DATETIMETZ_IMMUTABLE)]
    private ?\DateTimeImmutable $validFrom = null;

    #[ORM\Column(type: Types::DATETIMETZ_IMMUTABLE)]
    private ?\DateTimeImmutable $validTo = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $subject = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'createdCertificates')]
    private ?user $createdBy = null;

    #[ORM\ManyToOne(inversedBy: 'usedCertificates')]
    private ?user $usedBy = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $publicKey = null;

    #[ORM\Column]
    private ?bool $revoked = null;

    #[ORM\Column]
    private ?bool $expired = null;

//    #[ORM\ManyToOne(inversedBy: 'certificates')]
    #[ORM\ManyToOne()]
    private ?CertificateAuthority $authority = null;

    #[ORM\OneToMany(mappedBy: 'certificate', targetEntity: UserNotice::class)]
    private Collection $userNotices;

    public function __construct()
    {
        $this->userNotices = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getValidFrom(): ?\DateTimeImmutable
    {
        return $this->validFrom;
    }

    public function setValidFrom(\DateTimeImmutable $validFrom): self
    {
        $this->validFrom = $validFrom;

        return $this;
    }

    public function getValidTo(): ?\DateTimeImmutable
    {
        return $this->validTo;
    }

    public function setValidTo(\DateTimeImmutable $validTo): self
    {
        $this->validTo = $validTo;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getCreatedBy(): ?user
    {
        return $this->createdBy;
    }

    public function setCreatedBy(?user $createdBy): self
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    public function getUsedBy(): ?user
    {
        return $this->usedBy;
    }

    public function setUsedBy(?user $usedBy): self
    {
        $this->usedBy = $usedBy;

        return $this;
    }

    public function getPublicKey(): ?string
    {
        return $this->publicKey;
    }

    public function setPublicKey(?string $publicKey): self
    {
        $this->publicKey = $publicKey;

        return $this;
    }

    public function isRevoked(): ?bool
    {
        return $this->revoked;
    }

    public function setRevoked(bool $revoked): self
    {
        $this->revoked = $revoked;

        return $this;
    }

    public function isExpired(): ?bool
    {
        return $this->expired;
    }

    public function setExpired(bool $expired): self
    {
        $this->expired = $expired;

        return $this;
    }

    public function getAuthority(): ?CertificateAuthority
    {
        return $this->authority;
    }

    public function setAuthority(?CertificateAuthority $authority): self
    {
        $this->authority = $authority;

        return $this;
    }

    /**
     * @return Collection<int, UserNotice>
     */
    public function getUserNotices(): Collection
    {
        return $this->userNotices;
    }

    public function addUserNotice(UserNotice $userNotice): static
    {
        if (!$this->userNotices->contains($userNotice)) {
            $this->userNotices->add($userNotice);
            $userNotice->setCertificate($this);
        }

        return $this;
    }

    public function removeUserNotice(UserNotice $userNotice): static
    {
        if ($this->userNotices->removeElement($userNotice)) {
            // set the owning side to null (unless already changed)
            if ($userNotice->getCertificate() === $this) {
                $userNotice->setCertificate(null);
            }
        }

        return $this;
    }
}
