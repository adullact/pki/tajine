<?php

namespace App\Entity;

use App\Repository\CertificateAuthorityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CertificateAuthorityRepository::class)]
class CertificateAuthority
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $serialNumber = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $subjectKeyIdentifier = null;

    #[ORM\Column]
    private ?bool $revoked = null;

    #[ORM\Column]
    private ?bool $expired = null;

    #[ORM\Column(type: Types::DATETIMETZ_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $validFrom = null;

    #[ORM\Column(type: Types::DATETIMETZ_IMMUTABLE, nullable: true)]
    private ?\DateTimeImmutable $validTo = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $subject = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $publicKey = null;

    #[ORM\Column(length: 255)]
    private ?string $crl = null;

//    #[ORM\OneToMany(mappedBy: 'authority', targetEntity: Certificate::class)]
//    private Collection $certificates;

    public function __construct()
    {
//        $this->certificates = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(?string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getSubjectKeyIdentifier(): ?string
    {
        return $this->subjectKeyIdentifier;
    }

    public function setSubjectKeyIdentifier(?string $subjectKeyIdentifier): self
    {
        $this->subjectKeyIdentifier = $subjectKeyIdentifier;

        return $this;
    }

    public function isRevoked(): ?bool
    {
        return $this->revoked;
    }

    public function setRevoked(bool $revoked): self
    {
        $this->revoked = $revoked;

        return $this;
    }

    public function isExpired(): ?bool
    {
        return $this->expired;
    }

    public function setExpired(bool $expired): self
    {
        $this->expired = $expired;

        return $this;
    }

    public function getValidFrom(): ?\DateTimeImmutable
    {
        return $this->validFrom;
    }

    public function setValidFrom(?\DateTimeImmutable $validFrom): self
    {
        $this->validFrom = $validFrom;

        return $this;
    }

    public function getValidTo(): ?\DateTimeImmutable
    {
        return $this->validTo;
    }

    public function setValidTo(?\DateTimeImmutable $validTo): self
    {
        $this->validTo = $validTo;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getPublicKey(): ?string
    {
        return $this->publicKey;
    }

    public function setPublicKey(?string $publicKey): self
    {
        $this->publicKey = $publicKey;

        return $this;
    }

    public function getCrl(): ?string
    {
        return $this->crl;
    }

    public function setCrl(string $crl): self
    {
        $this->crl = $crl;

        return $this;
    }
//
//    /**
//     * @return Collection<int, Certificate>
//     */
//    public function getCertificates(): Collection
//    {
//        return $this->certificates;
//    }
//
//    public function addCertificate(Certificate $certificate): self
//    {
//        if (!$this->certificates->contains($certificate)) {
//            $this->certificates->add($certificate);
//            $certificate->setAuthority($this);
//        }
//
//        return $this;
//    }
//
//    public function removeCertificate(Certificate $certificate): self
//    {
//        if ($this->certificates->removeElement($certificate)) {
//            // set the owning side to null (unless already changed)
//            if ($certificate->getAuthority() === $this) {
//                $certificate->setAuthority(null);
//            }
//        }
//
//        return $this;
//    }
}
