<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
//          ->add('roles')
            ->add('email', EmailType::class, [
                'label' => 'admin.users.user_add.form.label.email',
//               'help' => 'admin.users.user_add.form.label.email.help',
                'constraints' => [
                    new NotBlank([
                        'message' => 'common.validator.help.field.error.required',
                    ]),
                    new Email([
                        'mode' => 'html5',
                        'message' => 'common.validator.help.email.error.not-valid',
                    ]),
                ],
            ])
            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'label' => 'admin.users.user_add.form.label.password',
                'help_html' => true,
                'help' => new TranslatableMessage(
                    message: 'admin.users.user_add.form.label.password.help',
                    parameters: ['%min_password_length%' => $options['min_password_length']],
                    domain: 'messages'
                ),
                'constraints' => [
                    new NotBlank([
                        'message' => 'common.validator.help.field.error.required',
                    ]),
                    new Length([
                        'min' => $options['min_password_length'],
//                      'minMessage' => 'Password should be at least {{ limit }} characters',
                        'minMessage' => new TranslatableMessage(
                            message: 'common.validator.help.password.error.too-short',
                        ),
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'min_password_length' => 16,
            // enable/disable CSRF protection for this form
            'csrf_protection' => true,
            // the name of the hidden HTML field that stores the token
            'csrf_field_name' => 'csrf_token_User',
            // an arbitrary string used to generate the value of the token
            // using a different string for each form improves its security
            'csrf_token_id'   => 'task_UserType',
        ]);
        $resolver->setAllowedTypes('min_password_length', 'int');
    }
}
