<?php

namespace App\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChangePasswordFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('currentPlainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'label' => 'user.change_password.form.label.current-password',
                'help' => 'user.change_password.form.label.current-password.help',
                'constraints' => [
                    new NotBlank([
                        'message' => 'common.validator.help.field.error.required',
                    ]),
                    new Length([
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('newPlainPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'options' => [
                    'attr' => [
                        'autocomplete' => 'new-password',
                        'minlength' => $options['min_password_length'],
                        'maxlength' => 4096, // max length allowed by Symfony for security reasons
                    ],
                ],
                'first_options' => [
                    'constraints' => [
                        new NotBlank([
                            'message' => 'common.validator.help.field.error.required',
                        ]),
                        new Length([
                            'min' => $options['min_password_length'],
//                          'minMessage' => 'Password should be at least {{ limit }} characters',
                            'minMessage' => new TranslatableMessage(
                                message: 'common.validator.help.password.error.too-short',
                            ),
                            'max' => 4096, // max length allowed by Symfony for security reasons
                        ]),
                    ],
                    'label' => 'user.change_password.form.label.new-password',
                    'help_html' => true,
                    'help' => new TranslatableMessage(
                        message: 'user.change_password.form.label.new-password.help',
                        parameters: ['%min_password_length%' => $options['min_password_length']],
                        domain: 'messages'
                    ),
                ],
                'second_options' => [
                    'label' => 'user.change_password.form.label.new-password-repeat',
                    'help' => 'user.change_password.form.label.new-password-repeat.help',
                ],
                'invalid_message' => 'common.validator.help.password.error.new-password-must-match',
                // Instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'min_password_length' => 16,
            // enable/disable CSRF protection for this form
            'csrf_protection' => true,
            // the name of the hidden HTML field that stores the token
            'csrf_field_name' => 'csrf_token_ChangePassword',
            // an arbitrary string used to generate the value of the token
            // using a different string for each form improves its security
            'csrf_token_id'   => 'task_ChangePasswordFormType',
        ]);
        $resolver->setAllowedTypes('min_password_length', 'int');
    }
}
