<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\TranslatableMessage;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AddCertificateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('email', EmailType::class, [
                'label' => 'cert_add.form.label.end_user.email',
                'help' => 'cert_add.form.label.end_user.email.help',
                'constraints' => [
                    new NotBlank(['message' => 'common.validator.help.field.error.required']),
                    new Email([
                        'mode' => 'html5',
                        'message' => 'common.validator.help.email.error.not-valid',
                    ]),
                ],
            ])
            ->add('certificatePassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'attr' => [
                    'autocomplete' => 'new-password',
                    'minlength' => $options['min_password_length'],
                    'maxlength' => 4096, // max length allowed by Symfony for security reasons
                ],
                'label' => 'cert_add.form.label.cert.password',
                'help_html' => true,
                'help' => new TranslatableMessage(
                    message: 'cert_add.form.label.cert.password.help',
                    parameters: ['%min_password_length%' => $options['min_password_length']],
                    domain: 'messages'
                ),
                'constraints' => [
                    new NotBlank(['message' => 'common.validator.help.field.error.required']),
                    new Length([
                        'min' => $options['min_password_length'],
//                      'minMessage' => 'Password should be at least {{ limit }} characters',
                        'minMessage' => new TranslatableMessage(
                            message: 'common.validator.help.password.error.too-short',
                        ),
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],

            ])
            ->add(
                child: 'countryName',
                type: CountryType::class,
                options: [
                    'label' =>  'cert_add.form.label.cert.country',
                    'preferred_choices' => ['France', 'FR' ],
                    'constraints' => [new NotBlank(['message' => 'common.validator.help.field.error.required'])],
                ],
            )
            ->add(
                child: 'stateOrProvinceName',
                type: TextType::class,
                options: [
                    'label' =>  'cert_add.form.label.cert.province',
                    'constraints' => [new NotBlank(['message' => 'common.validator.help.field.error.required'])],
                ],
            )
            ->add(
                child: 'localityName',
                type: TextType::class,
                options: [
                    'label' =>  'cert_add.form.label.cert.locality',
                    'constraints' => [new NotBlank(['message' => 'common.validator.help.field.error.required'])],
                ],
            )
            ->add(
                child: 'organizationName',
                type: TextType::class,
                options: [
                    'label' =>  'cert_add.form.label.cert.organization',
                    'constraints' => [new NotBlank(['message' => 'common.validator.help.field.error.required'])],
                ],
            )
            ->add(
                child: 'organizationalUnitName',
                type: TextType::class,
                options: [
                    'label' =>  'cert_add.form.label.cert.organization_unit',
                    'constraints' => [new NotBlank(['message' => 'common.validator.help.field.error.required'])],
                ],
            )
            ->add(
                child: 'commonName',
                type: TextType::class,
                options: [
                    'label' =>  'cert_add.form.label.cert.common_name',
                    'constraints' => [new NotBlank(['message' => 'common.validator.help.field.error.required'])],
                ],
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'min_password_length' => 16,
            // enable/disable CSRF protection for this form
            'csrf_protection' => true,
            // the name of the hidden HTML field that stores the token
            'csrf_field_name' => 'csrf_token_AddCertificate',
            // an arbitrary string used to generate the value of the token
            // using a different string for each form improves its security
            'csrf_token_id'   => 'task_AddCertificateType',
        ]);
        $resolver->setAllowedTypes('min_password_length', 'int');

//        $resolver->setNormalizer('locality_name', static function (Options $options, $value)
//        {
//            return mb_strtoupper($value);
//        });
    }
}
