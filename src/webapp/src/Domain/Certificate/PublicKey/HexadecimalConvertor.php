<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Domain\Certificate\PublicKey;

class HexadecimalConvertor
{
    /**
     * Converts hexadecimal to decimal for larger numbers
     * than PHP's native hexdec() function allows.
     *
     * Need PHP extension [ bcmath ]
     *
     * @param string $hex hexadecimal value with or without "0x" prefix
     * @return string decimal value
     */
    public static function convertHexadecimalToDecimal(string $hex): string
    {
        // remove hexadecimal prefix [ 0x ]
        if ('0x' === \strtolower(\substr("$hex", 0, 2))) {
            $hex = substr("$hex", 2);
        }
        $len = \strlen($hex);
        $dec = '';
        for ($i = 1; $i <= $len; $i++) {
            $dec = \bcadd($dec, \bcmul((string)\hexdec($hex[$i - 1]), \bcpow('16', (string)($len - $i))));
        }
        return $dec;
    }
}
