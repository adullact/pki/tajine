<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Domain\Certificate\PublicKey;

use App\Domain\Certificate\PublicKey\HexadecimalConvertor;
use App\Domain\Certificate\PublicKey\Dto\PublicKeyRawDto;
use App\Domain\Certificate\PublicKey\Exception\FailedDecodeRawPublicKeyException;

use function Symfony\Component\String\u;

class PublicKeyRawConvertor
{
    public static function convertPemCertificateToPublicKeyRawDto(
        string $rawPemPublicKey
    ): PublicKeyRawDto {
        $certInfo = openssl_x509_parse("$rawPemPublicKey", false);
        if (false === $certInfo) {
            throw new FailedDecodeRawPublicKeyException("Public key convertor: public key can't loaded");
        }
        $serialNumber = $certInfo['serialNumber'];
        $serialNumberDecimal = HexadecimalConvertor::convertHexadecimalToDecimal($serialNumber);
        $name = $certInfo['name'];
        $subject = $certInfo['subject'];
        $validFromTimestamp = $certInfo['validFrom_time_t'];
        $validToTimestamp = $certInfo['validTo_time_t'];
        $validFrom = new \DateTimeImmutable(date('Y-m-d H:i:s', $validFromTimestamp));
        $validTo =   new \DateTimeImmutable(date('Y-m-d H:i:s', $validToTimestamp));

        // Extract CRL
        $crlUri = '';
        if (isset($certInfo['extensions']['crlDistributionPoints'])) {
            $crlDistributionPoints = trim($certInfo['extensions']['crlDistributionPoints']);
            $crlUri = trim(str_replace('Full Name:', '', "$crlDistributionPoints"));
            $crlUri = trim(str_replace('URI:', '', "$crlUri"));
            // @@@TODO refactor to use https://symfony.com/doc/current/reference/constraints/Url.html
        }

        // Extract authorityKeyIdentifier
        $authorityKeyIdentifier = '';
        if (isset($certInfo['extensions']['authorityKeyIdentifier'])) {
            $authorityKeyIdentifier = $certInfo['extensions']['authorityKeyIdentifier'];
            $authorityKeyIdentifier = str_replace('keyid:', '', "$authorityKeyIdentifier");
            $authorityKeyIdentifier = u($authorityKeyIdentifier)->trim()->upper()->toString();
        }
// $certInfo['extensions'] = ["extendedKeyUsage" => "TLS Web Client Authentication"
//                     "basicConstraints" => "CA:FALSE"
//                     "subjectKeyIdentifier" => "FA:AE:39:1A:0C:7F:D4:20:F5:A1:2B:AB:BE:66:AE:33:BB:A7:AD:B1"
//                     "authorityKeyIdentifier" => "keyid:BF:94:17:23:98:A8:AA:B1:50:97:46:CA:52:A4:1D:BD:70:DE:9A:45\n"
//                     "subjectAltName" => "DNS:"
//                     "crlDistributionPoints" => """ \nFull Name:\nURI:http://pki.example.org/crl.pem\n"""

        return new PublicKeyRawDto(
            serialNumber: $serialNumber,
            serialNumberDecimal: $serialNumberDecimal,
            name: $name,
            subject: $subject,
            validFromTimestamp: $validFromTimestamp,
            validToTimestamp: $validToTimestamp,
            validFrom: $validFrom,
            validTo: $validTo,
            crlUri: $crlUri,
            authorityKeyIdentifier: $authorityKeyIdentifier,
        );
    }
}
