<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Domain\Certificate\Pkcs12;

use App\Domain\Certificate\Pkcs12\Exception\Pkcs12ConvertorFailedException;

class Pkcs12Convertor
{
    use Pkcs12HelperTrait;

    /**
     * Convert PEM certificate to PKCS12 certificate
     *
     * @param string $rawPemPublicKey   raw public key (.pem)
     * @param string $rawPemPrivateKey  raw private key (.pem)
     * @param string $pkcs12Password    password of pkcs12 certificate
     * @return string   raw PKCS12 certificate
     * @throws \Exception
     */
    public static function convertPemCertificateToPkcs12(
        string $rawPemPublicKey,
        string $rawPemPrivateKey,
        string $pkcs12Password = ''
    ): string {

        // Load private key, or throw a exception
        $privateKey = self::loadPrivateKeyObject($rawPemPrivateKey); // return OpenSSL object of private key

        // Load public key to check that public key can be used by openssl_pkcs12_export(), or throw a exception
        self::loadPublicKeyObject($rawPemPublicKey); // return OpenSSL object of public key

        // Check private key does correspond to public key, or throw a exception
        self::checkThatPublicKeyMatchesPrivateKey(
            rawPemPublicKey: $rawPemPublicKey,
            rawPemPrivateKey: $rawPemPrivateKey
        );

        // Convert PEM certificate to PKCS12 certificate
        $convertorStatus = \openssl_pkcs12_export(
            certificate: "$rawPemPublicKey",
            output: $outputPkcs12Raw,
            private_key: $privateKey,
            passphrase: "$pkcs12Password"
        );
        if ($convertorStatus === false) {
            throw new Pkcs12ConvertorFailedException("Convert (PEM certificate to PKCS12) didn't work");
        }

        return $outputPkcs12Raw;
    }
}
