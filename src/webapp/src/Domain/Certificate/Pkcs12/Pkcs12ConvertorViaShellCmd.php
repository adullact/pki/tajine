<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Domain\Certificate\Pkcs12;

use App\Domain\Certificate\Pkcs12\Exception\Pkcs12ConvertorFailedException;
use Symfony\Component\Filesystem\Filesystem;

class Pkcs12ConvertorViaShellCmd
{
    use Pkcs12HelperTrait;

    /**
     * Convert PEM certificate to PKCS12 certificate
     *
     * @param string $rawPemPublicKey   raw public key (.pem)
     * @param string $rawPemPrivateKey  raw private key (.pem)
     * @param string $pkcs12Password    password of pkcs12 certificate
     * @return string   raw PKCS12 certificate
     * @throws \Exception
     */
    public static function convertPemCertificateToPkcs12(
        string $rawPemPublicKey,
        string $rawPemPrivateKey,
        string $pkcs12Password = '',
        string $temporaryConverterDirectory = "/tmp"
    ): string {

        // Load private key to check that private key can be used by OpenSSL CLI, or throw a exception
        self::loadPrivateKeyObject($rawPemPrivateKey); // return OpenSSL object of private key

        // Load public key to check that public key can be used by OpenSSL CLI, or throw a exception
        self::loadPublicKeyObject($rawPemPublicKey); // return OpenSSL object of public key

        // Check private key does correspond to public key, or throw a exception
        self::checkThatPublicKeyMatchesPrivateKey(
            rawPemPublicKey: $rawPemPublicKey,
            rawPemPrivateKey: $rawPemPrivateKey
        );

        $filesystem = new Filesystem();
        if (!is_dir($temporaryConverterDirectory)) {
            $filesystem->mkdir($temporaryConverterDirectory);
        } elseif (!is_writable($temporaryConverterDirectory)) {
            throw new \Exception(
                "Convert via OpenSSL CLI (PEM cert to PKCS12) can not write to [ $temporaryConverterDirectory ] dir"
            );
        }
        $filePrefix = "pkcs12convertor_tmp_";
        $publicKeyTmpFile = $filesystem->tempnam("$temporaryConverterDirectory", "$filePrefix", '.public.pem');
        $privateKeyTmpFile = $filesystem->tempnam("$temporaryConverterDirectory", "$filePrefix", '.private.pem');
        $pkcs12TmpFile = $filesystem->tempnam("$temporaryConverterDirectory", "$filePrefix", '.p12');
        $filesystem->dumpFile("$publicKeyTmpFile", "$rawPemPublicKey");
        $filesystem->dumpFile("$privateKeyTmpFile", "$rawPemPrivateKey");

        // OPENSSL_VERSION_TEXT     OpenSSL 3.0.2 15 Mar 2022 ---> Ubuntu 22.04
        //                          OpenSSL 1.1.1 11 Sep 2018 ---> Ubuntu 18.04
        $legacyOption = "-legacy";
        if (str_starts_with(OPENSSL_VERSION_TEXT, 'OpenSSL 1.')) {  // @@@TODO refactor
            $legacyOption = "";
        }

        // Convert PEM certificate to PKCS12 certificate
        $cmd = "";
        $cmd .= "openssl pkcs12 -export $legacyOption";
        $cmd .= " -passout pass:" . escapeshellarg("$pkcs12Password");
        $cmd .= " -out " . escapeshellarg("$pkcs12TmpFile");
        $cmd .= " -in " . escapeshellarg("$publicKeyTmpFile");
        $cmd .= " -inkey " . escapeshellarg("$privateKeyTmpFile");
        exec("$cmd", $cmdOutput, $cmdResultcode);
        if ($cmdResultcode !== 0) {
            $filesystem->remove([
                "$publicKeyTmpFile",
                "$privateKeyTmpFile",
                "$pkcs12TmpFile",
            ]);
            throw new Pkcs12ConvertorFailedException(
                "\nConvert via OpenSSL CLI (PEM certificate to PKCS12) didn't work\nExec error: $cmdResultcode"
            );
        }
        $outputPkcs12Raw = file_get_contents($pkcs12TmpFile);
        $filesystem->remove([
            "$publicKeyTmpFile",
            "$privateKeyTmpFile",
            "$pkcs12TmpFile",
        ]);
        return $outputPkcs12Raw;
    }
}
