<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Domain\Certificate\Pkcs12;

use App\Domain\Certificate\Pkcs12\Exception\InvalidPrivateKeyException;
use App\Domain\Certificate\Pkcs12\Exception\InvalidPublicKeyException;
use App\Domain\Certificate\Pkcs12\Exception\NotMatchingPrivateAndPublicKeysException;

trait Pkcs12HelperTrait
{
    /**
     * Load private key, or throw a exception
     *
     * @param string $rawPemPrivateKey
     * @return \OpenSSLAsymmetricKey
     * @throws \Exception
     */
    public static function loadPrivateKeyObject(
        string $rawPemPrivateKey
    ): \OpenSSLAsymmetricKey {
        $privateKey = \openssl_pkey_get_private($rawPemPrivateKey); // return false or object of private key (OpenSSL)
        if ($privateKey === false) {
            throw new InvalidPrivateKeyException("Private key can't loaded");
        }
        return $privateKey;
    }

    /**
     * Load public key, or throw a exception
     *
     * @param string $rawPemPublicKey
     * @return \OpenSSLAsymmetricKey
     * @throws \Exception
     */
    public static function loadPublicKeyObject(
        string $rawPemPublicKey
    ): \OpenSSLAsymmetricKey {
        $publicKey = \openssl_pkey_get_public($rawPemPublicKey); // return false or object of public key (OpenSSL key)
        if ($publicKey === false) {
            throw new InvalidPublicKeyException("Public key can't loaded");
        }
        return $publicKey;
    }

    /**
     * Check private key does correspond to public key, or throw a exception
     *
     * @param string $rawPemPublicKey
     * @param string $rawPemPrivateKey
     * @return bool
     * @throws \Exception
     */
    public static function checkThatPublicKeyMatchesPrivateKey(
        string $rawPemPublicKey,
        string $rawPemPrivateKey
    ): bool {
        openssl_sign("data to be signed", $signature, $rawPemPrivateKey, OPENSSL_ALGO_SHA256);
        $isValid = openssl_verify("data to be signed", $signature, $rawPemPublicKey, OPENSSL_ALGO_SHA256);
        if ($isValid !== 1) {
            throw new NotMatchingPrivateAndPublicKeysException("Private key does not correspond to public key");
        }
        return true;
    }
}
