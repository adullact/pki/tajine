<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Entity\Certificate;
use App\Entity\CertificateAuthority;
use App\Form\CertificateType;
use App\Repository\CertificateAuthorityRepository;
use App\Repository\CertificateRepository;
use App\Repository\UserRepository;
use App\Service\CommonData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

use function Symfony\Component\String\u;

class CertificationAuthorityController extends AbstractController
{
    public function __construct(private CommonData $commonData)
    {
    }


    #[Route('/certification-authority', name: 'app_public_certificat-authority', methods: ['GET'])]
    public function index(CertificateAuthorityRepository $certificateAuthorityRepository): Response
    {

        try {
            $cfsslServer = $this->getParameter('app.cfssl.api_server');
            $cfsslUrl = "$cfsslServer/api/v1/cfssl/info";
            $ch = curl_init($cfsslUrl);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, "{}");
//          curl_setopt($ch, CURLOPT_POSTFIELDS, '{"label": "root"}');
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); # Return response instead of printing.
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array('Content-Type:application/json')
            );
            $result = curl_exec($ch);
            curl_close($ch);
            if ($result === false) {
                throw new \Exception("CFSSL - HTTP request failed [ $cfsslUrl ]");
            }
            $json = json_decode($result);
            if (!isset($json->success) | $json->success !== true) {
                throw new \Exception("CFSSL - Bad HTTP Response");
            }

            /* {   "success":false,
                    "result":null,
                    "errors":[{"code":405,"message":"Method is not allowed:\"GET\""}],
                    "messages":[]    }       */
            /* {  "success":true,
                    "result":{"certificate":"-----BEGIN CERTIFICATE-----\n ... \n-----END CERTIFICATE-----",
                    "usages":["client auth"],
                    "expiry":"8760h"},
                    "errors":[],
                    "messages":[]    }       */
        } catch (\Exception $e) {
            $httpCode = Response::HTTP_SERVICE_UNAVAILABLE;
            $CfsslStatusCode = "FAILED";
        }

        $rawPemPublicKey = $json->result->certificate;
        $certInfo = \openssl_x509_parse("$rawPemPublicKey", false);
        $certFingerprint = \openssl_x509_fingerprint("$rawPemPublicKey", 'sha256');


        $certName = $certInfo['name'];
        $certSubject = $certInfo['subject'];
        $certSubjectJson = \json_encode($certInfo['subject'], JSON_PRETTY_PRINT | JSON_THROW_ON_ERROR);
        $certIssuer = $certInfo['issuer'];
        $serialNumber = $certInfo['serialNumber'];
        $validFrom = date('Y-m-d', $certInfo['validFrom_time_t']);
        $validFromRaw = $certInfo['validFrom_time_t'];
        $validTo =   date('Y-m-d', $certInfo['validTo_time_t']);

        try {
            $cfsslServer = $this->getParameter('app.cfssl.api_server');
            $cfsslUrl = "$cfsslServer/api/v1/cfssl/certinfo";
            $ch = curl_init($cfsslUrl);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt(
                $ch,
                CURLOPT_POSTFIELDS,
                '{ "certificate": "' . str_replace("\\\\", "\\", "$rawPemPublicKey") . '" }'
            );
//          curl_setopt($ch, CURLOPT_POSTFIELDS, '{"label": "root"}');
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); # Return response instead of printing.
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array('Content-Type:application/json')
            );
            $result = curl_exec($ch);
            curl_close($ch);
            if ($result === false) {
                throw new \Exception("CFSSL - HTTP request failed [ $cfsslUrl ]");
            }
            $json = json_decode($result);
            if (!isset($json->success) | $json->success !== true) {
                throw new \Exception("CFSSL - Bad HTTP Response");
            }

            /* {   "success":false,
                    "result":null,
                    "errors":[{"code":405,"message":"Method is not allowed:\"GET\""}],
                    "messages":[]    }       */
            /* {  "success":true,
                    "result":{"certificate":"-----BEGIN CERTIFICATE-----\n ... \n-----END CERTIFICATE-----",
                    "usages":["client auth"],
                    "expiry":"8760h"},
                    "errors":[],
                    "messages":[]    }       */
        } catch (\Exception $e) {
            $httpCode = Response::HTTP_SERVICE_UNAVAILABLE;
            $CfsslStatusCode = "FAILED";
        }

//
//        dump($result);
//        dump($json);

        // Extract authorityKeyIdentifier
        $authorityKeyIdentifier = '';
        if (isset($certInfo['extensions']['authorityKeyIdentifier'])) {
            $authorityKeyIdentifier = $certInfo['extensions']['authorityKeyIdentifier'];
            $authorityKeyIdentifier = str_replace('keyid:', '', "$authorityKeyIdentifier");
            $authorityKeyIdentifier = u($authorityKeyIdentifier)->trim()->upper()->toString();
        }

        // Extract subjectKeyIdentifier
        $subjectKeyIdentifier = '';
        if (isset($certInfo['extensions']['subjectKeyIdentifier'])) {
            $subjectKeyIdentifier = $certInfo['extensions']['subjectKeyIdentifier'];
            $subjectKeyIdentifier = u($subjectKeyIdentifier)->trim()->upper()->toString();
        }

        // Extract CRL URL
        $certCrl = '';
        $certificateAuthority = $certificateAuthorityRepository->findOneBy([
            'subjectKeyIdentifier' => (string)$subjectKeyIdentifier
        ]);
        if (is_null($certificateAuthority)) {
//            $certificateAuthority = new CertificateAuthority();
//            $certificateAuthority->setSubjectKeyIdentifier("$subjectKeyIdentifier");
//            $certificateAuthorityRepository->save($certificateAuthority, true);
        } else {
            $certCrl = $certificateAuthority->getCrl();
            if (is_null($certificateAuthority->getSerialNumber())) {
                $certificateAuthority->setSerialNumber($certInfo['serialNumber']);
                $certificateAuthority->setValidTo(new \DateTimeImmutable($validTo));
                $certificateAuthority->setValidFrom(new \DateTimeImmutable($validFrom));
                $certificateAuthority->setSubject($certInfo['name']);
                $certificateAuthority->setPublicKey(trim($rawPemPublicKey));
            }
            $certificateAuthorityRepository->save($certificateAuthority, true);
            // @@@TODO refactor to use https://symfony.com/doc/current/reference/constraints/Url.html
        }

        // Extract CRL URL
        $RootCrl = '';
        if (isset($certInfo['extensions']['crlDistributionPoints'])) {
            $crlDistributionPoints = trim($certInfo['extensions']['crlDistributionPoints']);
            $RootCrl = trim(str_replace('Full Name:', '', "$crlDistributionPoints"));
            $RootCrl = trim(str_replace('URI:', '', "$RootCrl"));
            // @@@TODO refactor to use https://symfony.com/doc/current/reference/constraints/Url.html
        }


        return $this->render(
            view: 'certification-authority/display_ca.html.twig',
            parameters: [
                ...$this->commonData->getCommonDataTemplate(),
                'rootCrl' => $RootCrl,
                'certCrl' => $certCrl,
                'certName' => $certName,
                'certSubject' => $certSubject,
                'certSubjectJson' => $certSubjectJson,
                'certIssuer' => $certIssuer,
                'serialNumber' => $serialNumber,
                'validFrom' => $validFrom,
                'validFromRaw' => $validFromRaw,
                'validTo' => $validTo,
                'certFingerprint' => $certFingerprint,
                'certInfo' => print_r($certInfo, true),
                'rawPemPublicKey' => "$rawPemPublicKey",
            ],
        );
    }

    #[Route(
        path:'/certification-authority/{serialNumber}/download',
        name: 'app_public_certificat-authority_download',
        methods: ['GET']
    )]
    public function downloadCertificate(
        string $serialNumber,
        CertificateAuthorityRepository $certificateAuthorityRepository,
    ): Response {
        $certificateAuthority = $certificateAuthorityRepository->findOneBy(['serialNumber' => "$serialNumber"]);
        if (is_null($certificateAuthority) !== false) {
            return new Response(null, Response::HTTP_NOT_FOUND); // HTTP code = 404
        } elseif (empty($certificateAuthority->getPublicKey())) {
            $content = "Error 422 - Unprocessable Entity";
            return new Response("$content", Response::HTTP_UNPROCESSABLE_ENTITY); // HTTP code = 422
        } else {
            $response = new Response();
            $response->headers->set('Content-Type', 'mime/type');
            $response->headers->set(
                'Content-Disposition',
                'attachment;filename="cert_' . $serialNumber . ".pem"
            );
            $response->setContent($certificateAuthority->getPublicKey());
            return $response;
        }
    }
}
