<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller;

use App\Service\CommonData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Doctrine\DBAL\Connection;

class HealthCheckController extends AbstractController
{
    public function __construct(private CommonData $commonData)
    {
    }

    #[Route(
        path:'/health-check',
        name: 'app_health_check',
        methods: ['GET', 'HEAD']
    )]
    public function index(Request $request, \Doctrine\DBAL\Connection $dbConnection): Response
    {
        $softwareName = $this->getParameter('app.software.name');

        $httpCode = Response::HTTP_OK;
        $dbStatusCode = "SUCCESSFUL";
        try {
            $dbConnection->fetchAllAssociative('SELECT * FROM doctrine_migration_versions');
        } catch (\Exception $e) {
            $httpCode = Response::HTTP_SERVICE_UNAVAILABLE;
            $dbStatusCode = "FAILED";
        }

        $CfsslStatusCode = "SUCCESSFUL";
        try {
            $cfsslServer = $this->getParameter('app.cfssl.api_server');
            $cfsslUrl = "$cfsslServer/api/v1/cfssl/info";
            $ch = curl_init($cfsslUrl);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, "{}");
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); # Return response instead of printing.
            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
            curl_setopt(
                $ch,
                CURLOPT_HTTPHEADER,
                array('Content-Type:application/json')
            );
            $result = curl_exec($ch);
            curl_close($ch);

            if ($result === false) {
                throw new \Exception("CFSSL - HTTP request failed [ $cfsslUrl ]");
            }

            $json = json_decode($result);
            if (!isset($json->success) | $json->success !== true) {
                throw new \Exception("CFSSL - Bad HTTP Response");
            }
            /* {   "success":false,
                    "result":null,
                    "errors":[{"code":405,"message":"Method is not allowed:\"GET\""}],
                    "messages":[]    }       */
            /* {  "success":true,
                    "result":{"certificate":"-----BEGIN CERTIFICATE-----\n ... \n-----END CERTIFICATE-----",
                    "usages":["client auth"],
                    "expiry":"8760h"},
                    "errors":[],
                    "messages":[]    }       */
        } catch (\Exception $e) {
            $httpCode = Response::HTTP_SERVICE_UNAVAILABLE;
            $CfsslStatusCode = "FAILED";
        }

        $slugSoftwareName = strtolower((new AsciiSlugger())->slug("$softwareName")->toString()) ;
        $headers = [
            "x-$slugSoftwareName-database-status" => "DB_CONNECTION_$dbStatusCode",
            "x-$slugSoftwareName-cfssl-status" => "CFSSL_CONNECTION_$CfsslStatusCode"
        ];
        if ($request->getMethod() === "HEAD") {
            return new Response(status: $httpCode, headers: $headers);
        } else {
            return $this->render(
                view: 'healthcheck.html.twig',
                parameters: [
                    ...$this->commonData->getCommonDataTemplate(),
                    'dbStatusCode' => $dbStatusCode,
                    'cfsslStatusCode' => $CfsslStatusCode,
                ],
                response: new Response(status: $httpCode, headers: $headers)
            );
        }


//        return $this->redirectToRoute('app_poc_emails', [],Response::HTTP_TEMPORARY_REDIRECT);
                                                    // Response::HTTP_TEMPORARY_REDIRECT ---> HTTP code: 307
                                                    // see: https://developer.mozilla.org/fr/docs/Web/HTTP/Status/307
    }
}
