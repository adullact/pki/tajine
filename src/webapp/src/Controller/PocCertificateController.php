<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller;

use App\Service\Certificate\CertificateRemindLifetime;
use App\Service\CommonData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PocCertificateController extends AbstractController
{
    public function __construct(private CommonData $commonData)
    {
    }

    /**
     * @@@TODO refactor: add real new env parameter  /!\
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    #[Route(
        path: '/poc/ping-reminder/{reminderTokenViaHttpRequest}',
        name: 'app_legacy_cron_certificateRemindLifetime',
        methods: ['GET']
    )]
    public function legacyCronCertReminder(
        string $reminderTokenViaHttpRequest,
        CertificateRemindLifetime $certificateRemindLifetime
    ): Response {
        // @@@TODO refactor: add real new env parameter  /!\
        //         In DEV environnement, you can use following URL:
        //         http://127.0.0.1:8001/poc/ping-reminder/sysadmin@dev.example.org
        $reminderTokenViaEnv = $this->getParameter('app.email.alerting_to');
        if ($reminderTokenViaHttpRequest !== $reminderTokenViaEnv) {
            return new Response("Error 404 - Not Found", Response::HTTP_NOT_FOUND);
        }
        $certificateRemindLifetime->remindCertificateEndOfLifeToManager();
//      return $this->render('home.html.twig', [...$this->commonData->getCommonDataTemplate(),]);
        return new Response('', Response::HTTP_ACCEPTED);
    }
}
