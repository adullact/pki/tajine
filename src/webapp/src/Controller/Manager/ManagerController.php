<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller\Manager;

use App\Repository\CertificateRepository;
use App\Service\CommonData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ManagerController extends AbstractController
{
    public function __construct(private CommonData $commonData)
    {
    }

    #[Route(
        path: '/cert-manager',
        name: 'app_cert-manager',
        methods: ['GET']
    )]
    public function indexManager(): Response
    {
        return $this->redirectToRoute('app_cert-manager_certificates', [], Response::HTTP_TEMPORARY_REDIRECT);
        // Response::HTTP_TEMPORARY_REDIRECT ---> HTTP code: 307
        // see: https://developer.mozilla.org/fr/docs/Web/HTTP/Status/307
    }


    #[Route(
        path: '/cert-manager/certificates',
        name: 'app_cert-manager_certificates',
        methods: ['GET']
    )]
    public function certificates(CertificateRepository $certificateRepository, Request $request): Response
    {
        $maxItemPerPage = $this->getParameter('app.manager_config.default_max_cert_per_page');
        $currentPage =  $request->query->getInt('page', 1);
        $paginatedResult = $certificateRepository->getCertificateCreatedByPaginator(
            page: $currentPage,
            maxItemPerPage: $maxItemPerPage,
            createdById:    $this->getUser()->getId()
        );
        $total = count($paginatedResult);
        $totalPages = (int) ceil($total / $maxItemPerPage);

        return $this->render(
            view: 'user/manager/manager_cert_list.html.twig',
            parameters: [
                ...$this->commonData->getCommonDataTemplate(),
                'certificates' => $paginatedResult,
                'total' => $total,
                'max_per_page' => $maxItemPerPage,
                'totalPages' => $totalPages,
                'currentPage' => $currentPage,
            ],
        );
    }
}
