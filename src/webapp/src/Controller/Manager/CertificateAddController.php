<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller\Manager;

use App\Domain\Certificate\Pkcs12\Pkcs12ConvertorViaShellCmd;
use App\Domain\Certificate\Pkcs12\Pkcs12Convertor;
use App\Domain\Certificate\PublicKey\PublicKeyRawConvertor;
use App\Form\AddCertificateType;
use App\Repository\CertificateRepository;
use App\Service\CommonData;
use App\Service\Mailer\CertificateMailer;
use App\Service\Certificate\CertificateAuthorityService;
use App\Service\Certificate\CertificateService;
use App\Service\Cfssl\CfsslService;
use App\Service\User\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

use function Symfony\Component\String\u;

class CertificateAddController extends AbstractController
{
    public function __construct(private CommonData $commonData)
    {
    }


    #[Route(
        path:'/cert-manager/certificate/{serialNumber}/download',
        name: 'app_cert-manager_certificate_download',
        methods: ['GET']
    )]
    public function downloadCertificate(
        string $serialNumber,
        CertificateRepository $certificateRepository,
    ): Response {
         $certificate = $certificateRepository->findOneBy(['serialNumber' => "$serialNumber"]);
        if (is_null($certificate) !== false) {
            $content = "Error 404 - Not Found Certificate";
            return new Response("$content", Response::HTTP_NOT_FOUND); // HTTP code = 404
        } elseif (is_null($certificate->getCreatedBy())) {
            $content = "Error 422 - Unprocessable Entity";
            return new Response("$content", Response::HTTP_UNPROCESSABLE_ENTITY); // HTTP code = 422
        } elseif ($this->getUser()->getEmail() !== $certificate->getCreatedBy()->getEmail()) {
            $content = "Error 403 - Not allowed to download this certificate";
            return new Response("$content", Response::HTTP_FORBIDDEN); // HTTP code = 403
        } elseif (empty($certificate->getPublicKey())) {
            $content = "Error 422 - Unprocessable Entity";
            return new Response("$content", Response::HTTP_UNPROCESSABLE_ENTITY); // HTTP code = 422
        } else {
            $response = new Response();
            $response->headers->set('Content-Type', 'mime/type');
            $response->headers->set(
                'Content-Disposition',
                'attachment;filename="cert_' . $serialNumber . ".pem"
            );
            $response->setContent($certificate->getPublicKey());
            return $response;
        }
    }

    #[Route(
        path:'/cert-manager/certificate/add',
        name: 'app_cert-manager_certificate_add',
        methods: ['GET', 'POST']
    )]
    public function addCertificate(
        Request $request,
        CertificateAuthorityService $certificateAuthorityService,
        CertificateMailer $certificateMailer,
        UserService $userService,
        CertificateService $certificateService,
        CfsslService $cfsslService,
    ): Response {
        $minPasswordLength = $this->getParameter('app.cert_config.min_password_length');
        $form = $this->createForm(
            type: AddCertificateType::class,
            options: ['min_password_length' => $minPasswordLength]
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->createCertificate(
                form: $form,
                certificateAuthorityService: $certificateAuthorityService,
                certificateMailer: $certificateMailer,
                userService: $userService,
                certificateService: $certificateService,
                cfsslService: $cfsslService,
            );
        }
        return $this->render(
            view: 'user/manager/certificate_add.html.twig',
            parameters: [
                ...$this->commonData->getCommonDataTemplate(),
                'addCertificateForm' => $form->createView(),
            ],
        );
    }

    private function createCertificate(
        FormInterface $form,
        CertificateAuthorityService $certificateAuthorityService,
        CertificateMailer $certificateMailer,
        UserService $userService,
        CertificateService $certificateService,
        CfsslService $cfsslService,
    ): Response {
        $view = "user/manager/certificate_add_result.html.twig";
        /////////////////////////////////////////////////////////////////////
        $userMail = u($form->get('email')->getData())->trim()->collapseWhitespace()->lower()->toString();
        $pkcs12CertPassword = $form->get('certificatePassword')->getData();
        try {
            $jsonPayload = $cfsslService->getCfsslNewCertificateJsonPayload(
                countryName: $form->get('countryName')->getData(),
                stateOrProvinceName: $form->get('stateOrProvinceName')->getData(),
                localityName: $form->get('localityName')->getData(),
                organizationName: $form->get('organizationName')->getData(),
                organizationalUnit: $form->get('organizationalUnitName')->getData(),
                commonName: $form->get('commonName')->getData(),
            );
            $apiReturn = $cfsslService->loadCfsslNewCertificateRequest(jsonPayload: $jsonPayload);
        } catch (\Exception $e) {
            // @TODO improve user error msg + log error
            return $this->render(
                view: "$view",
                parameters: [
                    ...$this->commonData->getCommonDataTemplate(),
                    'errorMsg' => "CFSSL_CONNECTION_FAILED",
                ],
            );
        }
         $apiSuccess = $apiReturn['success']; // boolean

        // Load raw data (certificate + private key)
        $certRaw = $apiReturn['result']['certificate'];
        $keyRaw = $apiReturn['result']['private_key'];
        unset($apiReturn);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////
        $publicKeyRawDto = PublicKeyRawConvertor::convertPemCertificateToPublicKeyRawDto($certRaw);
        $certificateAuthority = $certificateAuthorityService->getCertificateAuthorityByAuthorityKeyIdentifier(
            authorityKeyIdentifier: $publicKeyRawDto->authorityKeyIdentifier,
            certCrl: $publicKeyRawDto->crlUri,
        );
        $certificateUser = $userService->getUserByEmailAndCreateItIfNotExist(userMail: $userMail);
        $certificate = $certificateService->registerNewCertificate(
            serialNumber: $publicKeyRawDto->serialNumber,
            subject: $publicKeyRawDto->name,
            rawPublicKey: (string)$certRaw,
            usedBy: $certificateUser,
            createdBy: $this->getUser(),
            validFrom: $publicKeyRawDto->validFrom,
            validTo: $publicKeyRawDto->validTo,
            certificateAuthority: $certificateAuthority,
        );
        ///////////////////////////////////////////////////////////////////////////////////
        // Convert to .p12 file (PKCS12 certificate + private key), and add password
//        $pkcs12Raw = Pkcs12Convertor::convertPemCertificateToPkcs12(
//            rawPemPublicKey: "$certRaw",
//            rawPemPrivateKey: "$keyRaw",
//            pkcs12Password: "$pkcs12CertPassword"
//        );
        // Convert to .p12 file (PKCS12 certificate + private key), and add password
        $kernelEnvironment = $this->getParameter('kernel.environment');
        $pkcs12FileContent = Pkcs12ConvertorViaShellCmd::convertPemCertificateToPkcs12(
            rawPemPublicKey: "$certRaw",
            rawPemPrivateKey: "$keyRaw",
            pkcs12Password: "$pkcs12CertPassword",
            temporaryConverterDirectory: "../var/tmp/$kernelEnvironment/cert-convertor-tmp"
        );
        unset($keyRaw);
        unset($pkcs12CertPassword);

        // Send PKCC12 file to end user
        try {
//          $appShortName = $this->getParameter('app.shortname');
            $certificateMailer->sendPkc12FileByMail(
                msgFrom: new Address($this->getParameter('app.email.from')),
                msgTo: new Address("$userMail"),
                certificateSerialNumber: $publicKeyRawDto->serialNumber,
                certificateValidToTimestamp: $publicKeyRawDto->validTo->getTimestamp(),
                pkcs12FileContent: "$pkcs12FileContent",
            );
        } catch (TransportExceptionInterface | LoaderError | RuntimeError | SyntaxError $e) {
            $errorMsg = $e->getMessage();
            // TODO refactor: some error prevented email sending;
            //      display an error message or try to resend the message
        }
        unset($pkcs12FileContent);
        ///////////////////////////////////////////////////////////////////////////////////
        return $this->render(
            view: "$view",
            parameters: [
                ...$this->commonData->getCommonDataTemplate(),
                'errorMsg' => '',
                'userMail' => $userMail,
                'certName' => $publicKeyRawDto->name,
                'certSubject' => $publicKeyRawDto->subject,
                'serialNumber' => $publicKeyRawDto->serialNumber,
                'serialNumberDecimal' => $publicKeyRawDto->serialNumberDecimal,
                'validFrom' => $publicKeyRawDto->validFrom->format('Y-m-d H:i:s'),
                'validTo' => $publicKeyRawDto->validTo->format('Y-m-d H:i:s'),
                'certCrl' => $publicKeyRawDto->crlUri,
            ],
        );
    }
}
