<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller;

//namespace Adullact\Tajine\Controller;

use App\Service\CommonData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    public function __construct(private CommonData $commonData)
    {
    }

    #[Route(
        path:'/',
        name: 'app_home',
        methods: ['GET', 'HEAD']
    )]
    public function index(CommonData $commonData, Request $request): Response
    {
//        $request->getPreferredLanguage()
//        $request->getLanguages()
//        $request->getDefaultLocale()
//        $request->getLocale()
//        $session = $request->getSession();
//        $session->start();
//        $session->set("clientIp", $request->getClientIp());
//        // $clientIp = $session->get("clientIp");
//        $session->save();
//      dump($request->getDefaultLocale());     // Webapp
//      dump($request->getLocale());            // Webapp
//      dump($request->getLanguages());         // User-Agent
//      dump($request->getPreferredLanguage()); // User-Agent

//        // returns your User object, or null if the user is not authenticated
//        // use inline documentation to tell your editor your exact User class
//        /** @var \App\Entity\User $user */
//        $user = $this->getUser();
//        // .$user->getFirstName()

        return $this->render('home.html.twig', [...$this->commonData->getCommonDataTemplate(),]);
    }
}
