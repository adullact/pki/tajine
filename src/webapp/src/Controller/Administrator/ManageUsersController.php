<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller\Administrator;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Service\CommonData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class ManageUsersController extends AbstractController
{
    public function __construct(private CommonData $commonData)
    {
    }

    #[Route(
        path: '/admin/users',
        name: 'app_admin_users_list',
        methods: ['GET', 'POST'],
    )]
    public function index(UserRepository $userRepository, Request $request): Response
    {
        $maxItemPerPage = $this->getParameter('app.admin_config.default_max_user_per_page');
        $currentPage =  $request->query->getInt('page', 1);
        $paginatedResult = $userRepository->getUserPaginator(
            page: $currentPage,
            maxItemPerPage: $maxItemPerPage
        );
        $total = count($paginatedResult);
        $totalPages = (int) ceil($total / $maxItemPerPage);

        return $this->render(
            view: 'user/administrator/users/admin_user_list.html.twig',
            parameters: [
                ...$this->commonData->getCommonDataTemplate(),
                'users' => $paginatedResult,
                'total' => $total,
                'max_per_page' => $maxItemPerPage,
                'totalPages' => $totalPages,
                'currentPage' => $currentPage,
            ],
        );


//        dump($this->getParameter('security.role_hierarchy.roles'));
        // @todo use "roles" property instead of "isActive" property
//        return $this->render(
//            view: 'user/administrator/users/admin_user_list.html.twig',
//            parameters: [
//                ...$this->commonData->getCommonDataTemplate(),
//               'users' => $userRepository->findBy([], ['id' => 'ASC']),
//            //              'users' => $userRepository->findBy(['isActive' => true]),
//            ],
//        );
    }


    #[Route(
        path: '/admin/users/add-manager',
        name: 'app_admin_users_add_manager',
        methods: ['GET', 'POST'],
    )]
    public function addManager(
        Request $request,
        UserPasswordHasherInterface $userPasswordHasher,
        UserRepository $userRepository
    ): Response {
        $role = 'ROLE_CERTIFICAT_MANAGER';
        return $this->communAddUser($request, $userPasswordHasher, $userRepository, $role);
    }

    #[Route(
        path: '/admin/users/add-administrator',
        name: 'app_admin_users_add_admin',
        methods: ['GET', 'POST'],
    )]
    public function addAdministrator(
        Request $request,
        UserPasswordHasherInterface $userPasswordHasher,
        UserRepository $userRepository
    ): Response {
        $role = 'ROLE_ADMIN';
        return $this->communAddUser($request, $userPasswordHasher, $userRepository, $role);
    }

    private function communAddUser(
        Request $request,
        UserPasswordHasherInterface $userPasswordHasher,
        UserRepository $userRepository,
        string $role
    ): Response {
        $user = new User();
        $minPasswordLength = $this->getParameter('app.user_config.min_password_length');
        $form = $this->createForm(UserType::class, $user, ['min_password_length' => $minPasswordLength]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(["$role"]);
            $user->setIsActive(true);
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $userRepository->save($user, true);
            return $this->redirectToRoute('app_admin_users_list', [], Response::HTTP_SEE_OTHER);
        }
        return $this->render('user/administrator/users/admin_user_add.html.twig', [
            ...$this->commonData->getCommonDataTemplate(),
            'user' => $user,
            'form' => $form,
            'role' => $role,
        ]);
    }


    #[Route(
        path: '/admin/users/{id}/edit',
        name: 'app_admin_users_edit',
        methods: ['GET', 'POST'],
    )]
    public function editManager(Request $request, User $user, UserRepository $userRepository): Response
    {
        $minPasswordLength = $this->getParameter('app.user_config.min_password_length');
        $form = $this->createForm(UserType::class, $user, ['min_password_length' => $minPasswordLength]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userRepository->save($user, true);
            return $this->redirectToRoute('app_admin_users_list', [], Response::HTTP_SEE_OTHER);
        }
        return $this->render('user/administrator/users/admin_user_edit.html.twig', [
            ...$this->commonData->getCommonDataTemplate(),
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route(
        path: '/admin/users/{id}/delete',
        name: 'app_admin_users_delete',
        methods: ['POST'],
    )]
    public function delete(Request $request, User $user, UserRepository $userRepository): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {
            $userRepository->remove($user, true);
        }

        return $this->redirectToRoute('app_admin_users_list', [], Response::HTTP_SEE_OTHER);
    }



    #[Route(
        path: '/admin/users/{id}',
        name: 'app_admin_users_display',
        methods: ['GET'],
    )]
    public function show(User $user): Response
    {
        return $this->render('user/administrator/users/admin_user_display.html.twig', [
            ...$this->commonData->getCommonDataTemplate(),
            'user' => $user,
        ]);
    }
}
