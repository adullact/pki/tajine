<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller\Administrator\Configuration;

use App\Service\CommonData;
use App\Service\Mailer\MailerAdminSendTestingEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

class AdministratorTestingEmailConfigController extends AbstractController
{
    public function __construct(private CommonData $commonData)
    {
    }

    #[Route(
        path:'/admin/configuration/test-mail',
        name: 'app_admin_config_send_testing_email',
        methods: ['GET']
    )]
    public function sendTestingEmail(MailerAdminSendTestingEmail $mailer): Response
    {
        $appEmailAlertingTo = $this->getParameter('app.email.alerting_to');
        $appEmailFrom = $this->getParameter('app.email.from');
        $errorMsg = '';
        try {
            $mailer->sendTestingEmail(
                msgFrom: new Address("$appEmailFrom"),
                msgTo: new Address("$appEmailAlertingTo")
            );
        } catch (\Throwable $e) {
            // catch         Symfony\Component\Mailer\Exception\TransportException
            // but not catch Symfony\Component\Mailer\Exception\InvalidArgumentException
            //               Symfony\Component\Mailer\Exception\UnsupportedSchemeException
            $errorMsg = $e->getMessage();
        }
        return $this->render(
            view: 'user/administrator/configuration/configuration_sendTestingEmail.html.twig',
            parameters: [
                ...$this->commonData->getCommonDataTemplate(),
                'errorMsg' => $errorMsg,
                'appEmailAlertingTo' => $appEmailAlertingTo,
            ],
        );
    }
}
