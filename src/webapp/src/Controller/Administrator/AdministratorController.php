<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller\Administrator;

use App\Entity\Certificate;
use App\Entity\UserNotice;
use App\Repository\CertificateRepository;
use App\Repository\UserNoticeRepository;
use App\Service\CommonData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdministratorController extends AbstractController
{
    public function __construct(private CommonData $commonData)
    {
    }
//

    #[Route(
        path: '/admin',
        name: 'app_admin',
        methods: ['GET']
    )]
    public function indexAdministrator(): Response
    {
        return $this->redirectToRoute('app_home', [], Response::HTTP_TEMPORARY_REDIRECT);
        // Response::HTTP_TEMPORARY_REDIRECT ---> HTTP code: 307
        // see: https://developer.mozilla.org/fr/docs/Web/HTTP/Status/307
    }


    #[Route(
        path: '/admin/configuration/phpinfo',
        name: 'app_admin_config_phpinfo',
        methods: ['GET']
    )]
    public function displayPhpInfo(): Response
    {
        ob_start();
        phpinfo();
        $strPhpInfo = ob_get_contents();
        ob_clean();
        $response = new Response();
        $response->setContent($strPhpInfo);
        return $response;
    }

    #[Route(
        path: '/admin/configuration',
        name: 'app_admin_config',
        methods: ['GET']
    )]
    public function configuration(): Response
    {
        $now = new \DateTime();
        $appEmailAlertingTo = $this->getParameter('app.email.alerting_to');
        $appEmailFrom = $this->getParameter('app.email.from');
        $cfsslUrl = $this->getParameter('app.cfssl.api_server');
//        kernel.default_locale                                         en
//        kernel.enabled_locales                                        []
        return $this->render(
            view: 'user/administrator/configuration/configuration_display.html.twig',
            parameters: [
                ...$this->commonData->getCommonDataTemplate(),
            //              'app_env' => $this->getParameter('kernel.environment'),
                'appEmailAlertingTo' => $appEmailAlertingTo,
                'app_cert_remind_copy_to_alerting' => $this->getParameter('app.cert_remind.copy_to_alerting'),
                'appEmailFrom' => $appEmailFrom,
                'app_trusted_host' => $this->getParameter('app.trusted_host'),
                'app_default_uri' => $this->getParameter('app.default_uri'),
                'cfsslUrl' => $cfsslUrl,
                'openssl_version' => OPENSSL_VERSION_TEXT,
                'openssl_version_id' => OPENSSL_VERSION_NUMBER,
                'php_version_id' => PHP_VERSION_ID,
                'php_version' => phpversion(),
                'php_loaded_extensions' => \get_loaded_extensions(),
                'date_time_zone' => $now->getTimezone()->getName(),
                'date_now' => $now->format('c'),
                'symfony_version' => \Symfony\Component\HttpKernel\Kernel::VERSION,
            ],
        );
    }


    #[Route(
        path: '/admin/certificates',
        name: 'app_admin_certificates',
        methods: ['GET']
    )]
    public function certificates(CertificateRepository $certificateRepository, Request $request): Response
    {
        $maxItemPerPage = $this->getParameter('app.admin_config.default_max_cert_per_page');
        $currentPage =  $request->query->getInt('page', 1);
        $paginatedResult = $certificateRepository->getCertificatePaginator(
            page: $currentPage,
            maxItemPerPage: $maxItemPerPage
        );
        $total = count($paginatedResult);
        $totalPages = (int) ceil($total / $maxItemPerPage);

        return $this->render(
            view: 'user/administrator/certificates/admin_cert_list.html.twig',
            parameters: [
                ...$this->commonData->getCommonDataTemplate(),
                'certificates' => $paginatedResult,
                'total' => $total,
                'max_per_page' => $maxItemPerPage,
                'totalPages' => $totalPages,
                'currentPage' => $currentPage,
            ],
        );


//        $limit = 150;
//        $certificates = $certificateRepository->findBy(
//            criteria: [],
//            orderBy: ['validFrom' => 'DESC'],
//            limit: $limit,
//        );
//        return $this->render(
//            view: 'user/administrator/certificates/admin_cert_list.html.twig',
//            parameters: [
//                ...$this->commonData->getCommonDataTemplate(),
//                'certificates' => $certificates,
//                'limit' => $limit,
//            ],
//        );
    }


    #[Route(
        path:'/admin/certificates/{serialNumber}',
        name: 'app_admin_certificates_display',
        methods: ['GET']
    )]
    public function certificateDisplay(Certificate $certificate, UserNoticeRepository $userNoticeRepository): Response
    {
        $managerNotices = [];
        $endUserNotices = [];
        $notices = $userNoticeRepository->findBy(['certificate'  => $certificate]);
        foreach ($notices as $notice) {
            if ($notice->getNoticeType() === UserNotice::NOTICE_TYPE_CERT_REDMIND_TO_END_USER) {
                $endUserNotices[] = $notice;
            } elseif ($notice->getNoticeType() === UserNotice::NOTICE_TYPE_CERT_REDMIND_TO_MANAGER) {
                $managerNotices[] = $notice;
            }
        }
        return $this->render(
            view: 'user/administrator/certificates/admin_cert_display.html.twig',
            parameters: [
                ...$this->commonData->getCommonDataTemplate(),
                'certificate' => $certificate,
                'managerNotices' => $managerNotices,
                'endUserNotices' => $endUserNotices,
            ],
        );
    }

    #[Route(
        path:'/admin/certificates/{serialNumber}/download',
        name: 'app_admin_certificates_download',
        methods: ['GET']
    )]
    public function certificateDownload(
        string $serialNumber,
        CertificateRepository $certificateRepository,
    ): Response {
        $certificate = $certificateRepository->findOneBy(['serialNumber' => "$serialNumber"]);
        if (is_null($certificate) !== false) {
            return new Response("Error 404 - Not Found Certificate", Response::HTTP_NOT_FOUND); // HTTP code = 404
        } elseif (empty($certificate->getPublicKey())) {
            $content = "Error 422 - Unprocessable Entity";
            return new Response("$content", Response::HTTP_UNPROCESSABLE_ENTITY); // HTTP code = 422
        } else {
            $response = new Response();
            $response->headers->set('Content-Type', 'mime/type');
            $response->headers->set(
                'Content-Disposition',
                'attachment;filename="cert_' . $serialNumber . ".pem"
            );
            $response->setContent($certificate->getPublicKey());
            return $response;
        }
    }
}
