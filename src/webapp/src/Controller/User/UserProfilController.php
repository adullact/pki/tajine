<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller\User;

use App\Form\User\ChangePasswordFormType;
use App\Repository\UserRepository;
use App\Service\CommonData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserProfilController extends AbstractController
{
    public function __construct(private CommonData $commonData)
    {
    }

    #[Route('/user/profile', name: 'app_user_profile', methods: ['GET'])]
    public function userDisplayMyProfile(AuthenticationUtils $authenticationUtils): Response
    {
        return $this->render('user/profile.html.twig', [
            ...$this->commonData->getCommonDataTemplate(),
        ]);
    }

    #[Route('/user/profile/password', name: 'app_user_profile_change_password', methods: ['GET', 'POST'])]
    public function userChangeMyPassowrd(
        Request $request,
        TranslatorInterface $translator,
        UserPasswordHasherInterface $userPasswordHasher,
        UserRepository $userRepository
    ): Response {
        $minPasswordLength = $this->getParameter('app.user_config.min_password_length');
        $form = $this->createForm(
            type: ChangePasswordFormType::class,
            options: ['min_password_length' => $minPasswordLength]
        );
        $form->handleRequest($request);

        // Check form errors
        $saveNewPasword = false;
        $isInvalidCurrentPassword = false;
        if ($form->isSubmitted()) {
            $user = $this->getUser();
            $currentPlainPassword = $form->get('currentPlainPassword')->getData();
            if ($form->isValid() === false | !$userPasswordHasher->isPasswordValid($user, $currentPlainPassword)) {
                $message = $translator->trans('common.validator.help.password.error');
                $this->addFlash('danger', "$message");
            }

            if (!$userPasswordHasher->isPasswordValid($user, $currentPlainPassword)) {
                $isInvalidCurrentPassword = true;
            } elseif ($form->isValid()) {
                $saveNewPasword = true;
            }
        }

        // Update user password
        if ($saveNewPasword === true) {
            // encode the plain password
            $user->setPassword($userPasswordHasher->hashPassword($user, $form->get('newPlainPassword')->getData()));
            $userRepository->save($user, true);
            $successMsg =  $translator->trans('user.change_password.success');
            $this->addFlash('success', "$successMsg");
            return $this->redirectToRoute('app_user_profile', [], Response::HTTP_SEE_OTHER);
        }

        // Display empty form or form with error messages
        return $this->render('user/profile_change-password.html.twig', [
            ...$this->commonData->getCommonDataTemplate(),
            'changePasswordForm' => $form->createView(),
            'isInvalidCurrentPassword' => $isInvalidCurrentPassword,
        ]);
    }
}
