<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\Controller\User;

use App\Entity\UserToken;
use App\Form\User\ResetPasswordRequestFormType;
use App\Form\User\ResetPasswordFormType;
use App\Repository\UserRepository;
use App\Repository\UserTokenRepository;
use App\Service\CommonData;
use App\Service\Mailer\MailerPasswordResetLink;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\Translation\TranslatorInterface;

class LoginController extends AbstractController
{
    public function __construct(private CommonData $commonData)
    {
    }

    #[Route('/account/login', name: 'app_account_login', methods: ['GET', 'POST'])]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('user/login.html.twig', [
            ...$this->commonData->getCommonDataTemplate(),
            'last_username' => $lastUsername,
            'error'         => $error,
        ]);
    }

    #[Route('/account/logout', name: 'app_account_logout', methods: ['GET'])]
    public function logout(): never
    {
        // controller can be blank: it will never be called!
        throw new \Exception('Don\'t forget to activate logout in security.yaml');
    }

    #[Route('/account/forgot-password', name: 'app_account_forgot_password_request', methods: ['GET', 'POST'])]
    public function forgotPassword(
        Request $request,
        UserRepository $userRepository,
        UserTokenRepository $userTokenRepository,
        TokenGeneratorInterface $tokenGenerator,
        MailerPasswordResetLink $mailer,
    ): Response {
        $tokenLifetime = $this->getParameter('app.user_config.reset_password_token_lifetime');
        $form = $this->createForm(ResetPasswordRequestFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $userRepository->findOneByEmail($form->get('email')->getData());
            if ($user) {
                // Update or create reset password token
                $now = new \DateTimeImmutable();
                $validTo = $now->add(\DateInterval::createFromDateString("${tokenLifetime} seconds"));
                $token = $user->getUserToken();
                if (is_null($token)) {
                    $token = new UserToken();
                }
                $token->setToken($tokenGenerator->generateToken());
                $token->setCreatedAt($now);
                $token->setValidTo($validTo);
                $token->setUsedBy($user);
                $userTokenRepository->save($token, true);
                $resetPassowrdUrl = $this->generateUrl(
                    route:'app_account_forgot_password_new-password',
                    parameters:['tokenKey' => $token->getToken()],
                    referenceType: UrlGenerator::ABSOLUTE_URL
                );

                $emailSent = false; // TODO use this in template
                try {
                    $mailer->sendPasswordResetLink(
                        msgFrom: new Address($this->getParameter('app.email.from')),
                        msgTo: new Address($user->getEmail()),
                        resetPasswordUrl: $resetPassowrdUrl
                    );
                    $emailSent = true;
                } catch (\Throwable $e) {
                    // catch         Symfony\Component\Mailer\Exception\TransportException
                    // but not catch Symfony\Component\Mailer\Exception\InvalidArgumentException
                    //               Symfony\Component\Mailer\Exception\UnsupportedSchemeException
                    $errorMsg = $e->getMessage();  // TODO log error
                }
                // complexify a time attack: add a time pause varying between 100 and 300 milliseconds
                usleep(microseconds: rand(100_000, 300_000));
            } else {
                // complexify a time attack: add a time pause varying between 150 and 350 milliseconds
                usleep(microseconds: rand(150_000, 350_000));
            }

            return $this->render('user/forgot_passord_request_result.html.twig', [
                ...$this->commonData->getCommonDataTemplate(),
                'forgotPasswordForm' => $form->createView(),
            ]);
        }

        // Display empty form or form with error messages
        return $this->render('user/forgot_passord_request_form.html.twig', [
            ...$this->commonData->getCommonDataTemplate(),
            'forgotPasswordForm' => $form->createView(),
        ]);
    }

    #[Route(
        path:'/account/forgot-password/t/{tokenKey}',
        name: 'app_account_forgot_password_new-password',
        methods: ['GET', 'POST']
    )]
    public function forgotPasswordCheckToken(
        string $tokenKey,
        Request $request,
        UserRepository $userRepository,
        UserTokenRepository $tokenRepository,
        UserPasswordHasherInterface $userPasswordHasher,
        TranslatorInterface $translator,
    ): Response {
        $minPasswordLength = $this->getParameter('app.user_config.min_password_length');
        $form = $this->createForm(
            type: ResetPasswordFormType::class,
            options: ['min_password_length' => $minPasswordLength]
        );
        $form->handleRequest($request);

        $validToken = true;
        $token = $tokenRepository->findOneByToken($tokenKey);
        $now = new \DateTimeImmutable();
        if (is_null($token)) {
            $validToken = false;
        } elseif (($now->getTimestamp() - $token->getValidTo()->getTimestamp() ) > 0) {
            $validToken = false;
            $tokenRepository->remove($token, true);
        }

        if ($validToken === false) {
            return $this->render('user/forgot_passord_not_valid_token.html.twig', [
                ...$this->commonData->getCommonDataTemplate(),
            ]);
        }

        // Update user password
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $token->getUsedBy();
            // encode the plain password
            $user->setPassword($userPasswordHasher->hashPassword($user, $form->get('newPlainPassword')->getData()));
            $userRepository->save($user, true);
            $tokenRepository->remove($token, true);
            $successMsg =  $translator->trans('user.change_password.success');
            $this->addFlash('success', "$successMsg");
            return $this->redirectToRoute('app_account_login', [], Response::HTTP_SEE_OTHER);
        }

        // Display empty form or form with error messages
        return $this->render('user/forgot_passord_form.html.twig', [
            ...$this->commonData->getCommonDataTemplate(),
            'forgotPasswordForm' => $form->createView(),
        ]);
        // encode the plain password
//            $user->setPassword($userPasswordHasher->hashPassword($user, $form->get('newPlainPassword')->getData()));
//            $userRepository->save($user, true);
//            $successMsg =  $translator->trans('user.change_password.success');
//            $this->addFlash('success', "$successMsg");
//            return $this->redirectToRoute('app_user_profile', [], Response::HTTP_SEE_OTHER);
    }
}
