<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\CertificateAuthority;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppCertificateAuthorityFixtures extends Fixture
{
    public const CERT_AUTHORITY_1 = 'cert-authority_1';

    public function load(ObjectManager $manager): void
    {

        $certAuthority = new CertificateAuthority();
        $certAuthority->setSubjectKeyIdentifier('71:2B:AD:BD:C8:36:17:5D:DF:CE:E5:B7:EA:07:14:15:64:52:3B:F8');
        $certAuthority->setRevoked(false);
        $certAuthority->setExpired(false);
        $certAuthority->setCrl('http://crl.example.org/crl-VAGRANTEXAMPLEINTERMEDIATECAGEN1.crl');
        $manager->persist($certAuthority);
        $manager->flush();

        // Other fixtures can get this object
        // using the [ AppCertificateAuthorityFixtures::CERT_AUTHORITY ] constant
        $this->addReference(self::CERT_AUTHORITY_1, $certAuthority);
    }
}
