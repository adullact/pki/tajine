<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasher;

class AppUserFixtures extends Fixture
{
    public const USER_REFERENCE_1 = 'test_user_1_tajine@example.org';
    public const USER_REFERENCE_2 = 'test_user_2_tajine@example.org';
    public const USER_REFERENCE_3 = 'test_user_3_tajine@example.org';
    public const USER_REFERENCE_4 = 'test_user_4_tajine@example.org';
    public const USER_REFERENCE_5 = 'test_user_5_tajine@example.org';
    public const USER_REFERENCE_6 = 'test_user_6_tajine@example.org';
    public const USER_REFERENCE_7 = 'test_user_7_tajine@example.org';
    public const USER_REFERENCE_8 = 'test_user_8_tajine@example.org';
    public const USER_REFERENCE_9 = 'test_user_9_tajine@example.org';


    public const USER_REFERENCE_ADMIN_1 = 'test_admin_1_tajine@example.org';
    public const USER_REFERENCE_ADMIN_2 = 'test_admin_2_tajine@example.org';
    public const USER_REFERENCE_ADMIN_3 = 'test_admin_3_tajine@example.org';

    /**
     * Manager account, used for the following tests:
     * - security
     * - certificates
     * - password change of connected user
     */
    public const USER_REFERENCE_MANAGER_1 = 'test_manager_1_tajine@example.org';

    /**
     * Manager account, used for the following tests:
     * - certificates
     * - password change of connected user
     */
    public const USER_REFERENCE_MANAGER_2 = 'test_manager_2_tajine@example.org';

    /**
     * Manager account, used for the following tests:
     * - forget password form
     */
    public const USER_REFERENCE_MANAGER_3 = 'test_manager_3_tajine@example.org';

    /**
     * Manager account, used for the following tests:
     * - forget password form
     */
    public const USER_REFERENCE_MANAGER_4 = 'test_manager_4_tajine@example.org';

    /**
     * Manager account, used for the following tests:
     * - forget password form
     */
    public const USER_REFERENCE_MANAGER_5 = 'test_manager_5_tajine@example.org';

    /**
     * Manager account, used for the following tests:
     * - forget password form
     */
    public const USER_REFERENCE_MANAGER_6 = 'test_manager_6_tajine@example.org';

    /**
     * Manager account, used for the following tests:
     * - forget password form
     */
    public const USER_REFERENCE_MANAGER_7 = 'test_manager_7_tajine@example.org';

    /**
     * Manager account, used for the following tests:
     * - forget password form
     */
    public const USER_REFERENCE_MANAGER_8 = 'test_manager_8_tajine@example.org';

    /**
     * Manager account, used for the following tests:
     * - forget password form
     */
    public const USER_REFERENCE_MANAGER_9 = 'test_manager_9_tajine@example.org';


    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail("admin-tajine@example.org");
        $user->setPassword(
            '$2y$13$AtLx12OyrkdkNz3vAh06deqVR8m/xI3PpzqPmo/e51DzckpM24nKK'
        ); // password: admin-tajine_PaSsWord_IsNotSoSecretChangeIt
        $user->setIsActive(true);
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);

        // create 3 user with ROLE_USER
        $users = [];
        for ($i = 1; $i < 10; $i++) {
            $user = new User();
            $user->setEmail("test_user_${i}_tajine@example.org");
//          $user->setPassword("user_${i}_password");
            $user->setPassword(
                '$2y$13$AtLx12OyrkdkNz3vAh06deqVR8m/xI3PpzqPmo/e51DzckpM24nKK'
            ); // identical to admin-tajine@example.org password
            $user->setIsActive(false);
            $manager->persist($user);
            $manager->flush();
            $users[$i] = $user;
        }
        $this->addReference(self::USER_REFERENCE_1, $users[1]);
        $this->addReference(self::USER_REFERENCE_2, $users[2]);
        $this->addReference(self::USER_REFERENCE_3, $users[3]);
        $this->addReference(self::USER_REFERENCE_4, $users[4]);
        $this->addReference(self::USER_REFERENCE_5, $users[5]);
        $this->addReference(self::USER_REFERENCE_6, $users[6]);
        $this->addReference(self::USER_REFERENCE_7, $users[7]);
        $this->addReference(self::USER_REFERENCE_8, $users[8]);
        $this->addReference(self::USER_REFERENCE_9, $users[9]);

        // create 3 user with ROLE_CERTIFICAT_MANAGER
        $users = [];
        for ($i = 1; $i < 10; $i++) {
            $user = new User();
            $user->setEmail("test_manager_${i}_tajine@example.org");
//          $user->setPassword("manager_${i}_password");
            $user->setPassword(
                '$2y$13$AtLx12OyrkdkNz3vAh06deqVR8m/xI3PpzqPmo/e51DzckpM24nKK'
            ); // identical to admin-tajine@example.org password
            $user->setIsActive(true);
            $user->setRoles(['ROLE_CERTIFICAT_MANAGER']);
            $manager->persist($user);
            $manager->flush();
            $users[$i] = $user;
        }
        $this->addReference(self::USER_REFERENCE_MANAGER_1, $users[1]);
        $this->addReference(self::USER_REFERENCE_MANAGER_2, $users[2]);
        $this->addReference(self::USER_REFERENCE_MANAGER_3, $users[3]);
        $this->addReference(self::USER_REFERENCE_MANAGER_4, $users[4]);
        $this->addReference(self::USER_REFERENCE_MANAGER_5, $users[5]);
        $this->addReference(self::USER_REFERENCE_MANAGER_6, $users[6]);
        $this->addReference(self::USER_REFERENCE_MANAGER_7, $users[7]);
        $this->addReference(self::USER_REFERENCE_MANAGER_8, $users[8]);
        $this->addReference(self::USER_REFERENCE_MANAGER_9, $users[9]);

        // create 3 user with ROLE_ADMIN
        $users = [];
        for ($i = 1; $i < 4; $i++) {
            $user = new User();
            $user->setEmail("test_admin_${i}_tajine@example.org");
//          $user->setPassword("admin_${i}_password");
            $user->setPassword(
                '$2y$13$AtLx12OyrkdkNz3vAh06deqVR8m/xI3PpzqPmo/e51DzckpM24nKK'
            ); // identical to admin-tajine@example.org password
            $user->setIsActive(true);
            $user->setRoles(['ROLE_ADMIN']);
            $manager->persist($user);
            $manager->flush();
            $users[$i] = $user;
        }
        $this->addReference(self::USER_REFERENCE_ADMIN_1, $users[1]);
        $this->addReference(self::USER_REFERENCE_ADMIN_2, $users[2]);
        $this->addReference(self::USER_REFERENCE_ADMIN_3, $users[3]);
    }
}
