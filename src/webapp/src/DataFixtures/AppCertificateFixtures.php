<?php

/*
 * This file is part of the Tajine software.
 * <https://gitlab.adullact.net/adullact/pki/tajine>
 *
 * Copyright (c) ADULLACT   <https://adullact.org>
 *               Association des Développeurs et Utilisateurs de Logiciels Libres
 *               pour les Administrations et les Collectivités Territoriales
 *
 * Tajine is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this software. If not, see <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

declare(strict_types=1);

namespace App\DataFixtures;

use App\DataFixtures\AppCertificateAuthorityFixtures;
use App\DataFixtures\AppUserFixtures;
use App\Entity\Certificate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class AppCertificateFixtures extends Fixture implements DependentFixtureInterface
{
    public function getDependencies(): array
    {
        return [
            AppCertificateAuthorityFixtures::class,
            AppUserFixtures::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        /////////////////////////////////////////////////////////////////////////////////
        $cert1 = new Certificate();
        $cert1->setSerialNumber('0x468B91C7D3F967FBD62F969A251CF740E1F066DA');
        $cert1->setValidFrom(new \DateTimeImmutable('2023-06-19 16:11:00 +00:00'));
        $cert1->setValidTo(new \DateTimeImmutable('2024-06-18 16:11:00 +00:00'));
        $cert1->setSubject(
            '/C=FR' .
            '/ST=PYRENEES-ATLANTIQUES' .
            '/L=CAUDIES-DE-CONFLENT' .
            '/O=MAIRIE-DE-CAUDIES-DE-CONFLENT' .
            '/OU=SERVICE-DES-SPORS-DE-LA-VILLE-DE-CAUDIES-DE-CONFLENT' .
            '/CN=TEST_USER_1_TAJINE@EXAMPLE.ORG'
        );
        $cert1->setCreatedBy($this->getReference(AppUserFixtures::USER_REFERENCE_MANAGER_2, 'App\Entity\User'));
        $cert1->setUsedBy($this->getReference(AppUserFixtures::USER_REFERENCE_1, 'App\Entity\User'));
        $cert1->setRevoked(false);
        $cert1->setExpired(false);
        $cert1->setAuthority($this->getReference(
            AppCertificateAuthorityFixtures::CERT_AUTHORITY_1,
            'App\Entity\CertificateAuthority'
        ));
        $cert1->setPublicKey('-----BEGIN CERTIFICATE-----
MIIEqDCCA5CgAwIBAgIURouRx9P5Z/vWL5aaJRz3QOHwZtowDQYJKoZIhvcNAQEL
BQAweTELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMSUwIwYDVQQK
ExxWQUdSQU5UIEVYQU1QTEUgT1JHQU5JWkFUSU9OMS0wKwYDVQQDEyRWQUdSQU5U
IEVYQU1QTEUgSU5URVJNRURJQVRFIENBIEdFTjEwHhcNMjMwNjE5MTYxMTAwWhcN
MjQwNjE4MTYxMTAwWjCB2jELMAkGA1UEBhMCRlIxHTAbBgNVBAgTFFBZUkVORUVT
LUFUTEFOVElRVUVTMRwwGgYDVQQHExNDQVVESUVTLURFLUNPTkZMRU5UMSYwJAYD
VQQKEx1NQUlSSUUtREUtQ0FVRElFUy1ERS1DT05GTEVOVDE9MDsGA1UECxM0U0VS
VklDRS1ERVMtU1BPUlMtREUtTEEtVklMTEUtREUtQ0FVRElFUy1ERS1DT05GTEVO
VDEnMCUGA1UEAwweVEVTVF9VU0VSXzFfVEFKSU5FQEVYQU1QTEUuT1JHMIIBIjAN
BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqNp6FK6KZbSPNjhvis9+bA3z73aH
MiISjB+aC3m3DEQo4C429Tg8s5YdKbdmY2TYYhS6S/H8tlrzQN5Tnwr98FhG+ZFh
wLkhYu9fBH668NhQGJCxucwDDCr5O84lNVonHAO6tG2govS7seBoCqU/F+T8yggN
Bk0QmGthMwabDYLUHBEflBq6hbLFZF2z4Yr3EWa/GEriZPKfH5PDlOOGJN6/a/UQ
gKNyl0ZUPyAxDDdsw0XobZNrHSjhSu53ItDrlbHwAAz4ORs8kkJ13Ya1X0ukDzcw
n+YVTkzEITQMunOtoQTvY/fnSky+WAosuri+/2AcaVNc2NejFF+P+bZLaQIDAQAB
o4HFMIHCMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0O
BBYEFN5bWACRzSNTtWtXLgq4jWNUrkyUMB8GA1UdIwQYMBaAFHErrb3INhdd387l
t+oHFBVkUjv4MAsGA1UdEQQEMAKCADBQBgNVHR8ESTBHMEWgQ6BBhj9odHRwOi8v
Y3JsLmV4YW1wbGUub3JnL2NybC1WQUdSQU5URVhBTVBMRUlOVEVSTUVESUFURUNB
R0VOMS5jcmwwDQYJKoZIhvcNAQELBQADggEBAJurjjB99x81yVq1jMhLuK1KV9qH
8KslQFBSGSYo6ACeiSqeNhK3hgQKt5jD4GHVHgctZ6TlShVkvQvnLdL6msFHqgb2
MDmJOh0LKTb9Sv4Cyvuibpey0EIVqA/LNl4PR7/dwlrpnEawuG4ywbZV4TCIKcuD
6WlVdPfDSr4+j9+4Zk99UtaIFfhCEnFCZyv8cbQ4dKeL4wbzWE/kNV9QRm8o1Z2D
udHDhXunee/i3hxzrnvCzsTe+J8xAHgKNCwMnwDNcl0HE+KWmg522tsGBxI+OYSc
F4JfLMqxlpcp7p/8AZNHZeevRAO23l8p/N7QMj1bTp+AOBFsSaKLoR24Q1s=
-----END CERTIFICATE-----
');
        $manager->persist($cert1);
        /////////////////////////////////////////////////////////////////////////////////
        $cert2 = new Certificate();
        $cert2->setSerialNumber('0x58F3C5938D6B1863BC56525264FBDD2209C60BCF');
        $cert2->setValidFrom(new \DateTimeImmutable('2023-06-19 17:19:00 +00:00'));
        $cert2->setValidTo(new \DateTimeImmutable('2024-06-18 17:19:00 +00:00'));
        $cert2->setSubject(
            '/C=FR' .
            '/ST=HERAULT' .
            '/L=SAINT-ANDRE-DE-BUEGES' .
            '/O=MAIRIE-DE-SAINT-ANDRE-DE-BUEGES' .
            '/OU=SERVICE-TECHNIQUE-DE-LA-VILLE-DE-SAINT-ANDRE-DE-BUEGES' .
            '/CN=TEST_USER_2_TAJINE@EXAMPLE.ORG'
        );
        $cert2->setCreatedBy($this->getReference(AppUserFixtures::USER_REFERENCE_MANAGER_2, 'App\Entity\User'));
        $cert2->setUsedBy($this->getReference(AppUserFixtures::USER_REFERENCE_2, 'App\Entity\User'));
        $cert2->setRevoked(false);
        $cert2->setExpired(false);
        $cert2->setAuthority($this->getReference(
            AppCertificateAuthorityFixtures::CERT_AUTHORITY_1,
            'App\Entity\CertificateAuthority'
        ));
        $cert2->setPublicKey('-----BEGIN CERTIFICATE-----
MIIEoTCCA4mgAwIBAgIUWPPFk41rGGO8VlJSZPvdIgnGC88wDQYJKoZIhvcNAQEL
BQAweTELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMSUwIwYDVQQK
ExxWQUdSQU5UIEVYQU1QTEUgT1JHQU5JWkFUSU9OMS0wKwYDVQQDEyRWQUdSQU5U
IEVYQU1QTEUgSU5URVJNRURJQVRFIENBIEdFTjEwHhcNMjMwNjE5MTcxOTAwWhcN
MjQwNjE4MTcxOTAwWjCB0zELMAkGA1UEBhMCRlIxEDAOBgNVBAgTB0hFUkFVTFQx
HjAcBgNVBAcTFVNBSU5ULUFORFJFLURFLUJVRUdFUzEoMCYGA1UEChMfTUFJUklF
LURFLVNBSU5ULUFORFJFLURFLUJVRUdFUzE/MD0GA1UECxM2U0VSVklDRS1URUNI
TklRVUUtREUtTEEtVklMTEUtREUtU0FJTlQtQU5EUkUtREUtQlVFR0VTMScwJQYD
VQQDDB5URVNUX1VTRVJfMl9UQUpJTkVARVhBTVBMRS5PUkcwggEiMA0GCSqGSIb3
DQEBAQUAA4IBDwAwggEKAoIBAQDXpc6fgIm2fXhJHWlx8PIoUoOpVAhqCXVlyAzd
ZpUTjP+95T05qLH/8ZkEP6NF+qO0C9uvT2mOw3Lbych7DYwHMFU18kXUiQeOrL3y
qLlHm2EYKxwA7YbTZbKrPe40D0GPEjgl74qIZFK/vgnMA47j2d98XkU593Sssrf6
cOtIf/lfDMF9xDYPs+FCP77VV3j2e+2MpAYKL26oqn3HRsr3P4pBg2VnMl2IAeaM
aBkS8lreHrBspfx3Svzc3GC/gg1moD+352uMEO/AjIn+LZqdjYQhNQWODDpfyb31
EOG4CTnfXrZdtPaaV5B7shFwUNUQR9qQNxxss0RhtIlMXwdnAgMBAAGjgcUwgcIw
EwYDVR0lBAwwCgYIKwYBBQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUrepq
HsK5Ao2OH1sXTad6vWeCfVkwHwYDVR0jBBgwFoAUcSutvcg2F13fzuW36gcUFWRS
O/gwCwYDVR0RBAQwAoIAMFAGA1UdHwRJMEcwRaBDoEGGP2h0dHA6Ly9jcmwuZXhh
bXBsZS5vcmcvY3JsLVZBR1JBTlRFWEFNUExFSU5URVJNRURJQVRFQ0FHRU4xLmNy
bDANBgkqhkiG9w0BAQsFAAOCAQEAQOxAriNWsgixJHxNS1YuL60xEJjpnGwILYJb
qtGbb9iD4qD481Pqm1LkiIJbpTF4xaZ07ejMpg8fAc43WifET0KPHCPlx5BM4PXF
O4kKgKbUslFOLMYQkPzTmqdhpCidRgFqzABqTPqOQ4FYLbUuKsL4IH7RvWWiBfg5
N/3xTL7vx5asRjLOlUt/tkWYMK+y2qleiy604/hi5v6/DNdPmbpnqjltmo/ib6cL
Z+KTAnftjHxPtZUSDiiDYIBFSD2EmQNsc341RAcQOw8OdHgAnQxV772AkGN///B8
zNH/Cf3Y7H7VAq+SBYIa6UwXNRi306w2SY9Khi6j5N6GYFxXBg==
-----END CERTIFICATE-----
');
        $manager->persist($cert2);
        /////////////////////////////////////////////////////////////////////////////////
        $cert3 = new Certificate();
        $cert3->setSerialNumber('0x33BDE2E6F0138FA1E24B629B0E3C5D44F4E4386C');
        $cert3->setValidFrom(new \DateTimeImmutable('2023-06-19 17:31:00 +00:00'));
        $cert3->setValidTo(new \DateTimeImmutable('2024-06-18 17:31:00 +00:00'));
        $cert3->setSubject(
            '/C=FR' .
            '/ST=PUY-DE-DOME' .
            '/L=CLERMONT-FERRAND' .
            '/O=CLERMONT-AUVERGNE-METROPOLE' .
            '/OU=SERVICE-INFORMATIQUE-DE-CLERMONT-AUVERGNE-METROPOLE' .
            '/CN=TEST_USER_3_TAJINE@EXAMPLE.ORG'
        );
        $cert3->setCreatedBy($this->getReference(AppUserFixtures::USER_REFERENCE_MANAGER_1, 'App\Entity\User'));
        $cert3->setUsedBy($this->getReference(AppUserFixtures::USER_REFERENCE_3, 'App\Entity\User'));
        $cert3->setRevoked(false);
        $cert3->setExpired(false);
        $cert3->setAuthority($this->getReference(
            AppCertificateAuthorityFixtures::CERT_AUTHORITY_1,
            'App\Entity\CertificateAuthority'
        ));
        $cert3->setPublicKey('-----BEGIN CERTIFICATE-----
MIIEmTCCA4GgAwIBAgIUM73i5vATj6HiS2KbDjxdRPTkOGwwDQYJKoZIhvcNAQEL
BQAweTELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMSUwIwYDVQQK
ExxWQUdSQU5UIEVYQU1QTEUgT1JHQU5JWkFUSU9OMS0wKwYDVQQDEyRWQUdSQU5U
IEVYQU1QTEUgSU5URVJNRURJQVRFIENBIEdFTjEwHhcNMjMwNjE5MTczMTAwWhcN
MjQwNjE4MTczMTAwWjCByzELMAkGA1UEBhMCRlIxFDASBgNVBAgTC1BVWS1ERS1E
T01FMRkwFwYDVQQHExBDTEVSTU9OVC1GRVJSQU5EMSQwIgYDVQQKExtDTEVSTU9O
VC1BVVZFUkdORS1NRVRST1BPTEUxPDA6BgNVBAsTM1NFUlZJQ0UtSU5GT1JNQVRJ
UVVFLURFLUNMRVJNT05ULUFVVkVSR05FLU1FVFJPUE9MRTEnMCUGA1UEAwweVEVT
VF9VU0VSXzNfVEFKSU5FQEVYQU1QTEUuT1JHMIIBIjANBgkqhkiG9w0BAQEFAAOC
AQ8AMIIBCgKCAQEAnAdaL6H4h/HD0AjUQykZN1D6QsuiHo5cUnjQvJdPzqo8UgSC
CPgJtLQF23kANoEDpmhZrH57034thffPA0VKnvchkYZ1Zi6af1KH3ApbYprIUVKd
wbeKXni26ntku6gZP07VAd0tOHMdMRXaJCWR7W2Tclm5V0JNmSRj0HxmjlDh0Gtu
Bf1yFLuNt0iVibBOaMk3YNkhX9qle/tWVn8x/QKQ7xEV3BHzAl4jzyjyCaBK0MKF
MCEYpCJIZz5Voic+LghvgNfT0Me7bf/Aub1SBB9bV1e5F79RgRe/o3fRzYd1ShJ9
+U/jhIPeUw3SzehNps0g2U/bcpQw90vzh0asAwIDAQABo4HFMIHCMBMGA1UdJQQM
MAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0OBBYEFBnXSR3m++XPdJ4M
JCldfwbeoMVjMB8GA1UdIwQYMBaAFHErrb3INhdd387lt+oHFBVkUjv4MAsGA1Ud
EQQEMAKCADBQBgNVHR8ESTBHMEWgQ6BBhj9odHRwOi8vY3JsLmV4YW1wbGUub3Jn
L2NybC1WQUdSQU5URVhBTVBMRUlOVEVSTUVESUFURUNBR0VOMS5jcmwwDQYJKoZI
hvcNAQELBQADggEBADup++TeJBn1fy5slaAYvdvhn5xKRNxR587duk7+0asb1KSt
uPbdhbzHShlfzFXp8r+MIZt/HUEnQ2NUe/GMAji+Gaiam5VYuBx5sOwE0jwLzwvO
iIujhq5cEsDVR4ssLhtXOsAVG2CRFzQX2Wd0IHabWibjvWS5NNiFlMRQUUC2RRs3
Waws86exSMWldBXO0FGiB52bBPprqjD1kUWZwyNVnXElg4IW2hhj4yIVjtgnj/Ir
h1aDclyRXIhTlqoFZsqkGUuL68TtB3iFUEfYm0ovrnHj8RYa0s+p7/v1RONJvch2
iq92oxH5GspcL9UqT7YH4Bu++Cn1MN7U4srbGas=
-----END CERTIFICATE-----
');
        $manager->persist($cert3);
        /////////////////////////////////////////////////////////////////////////////////
        $cert4 = new Certificate();
        $cert4->setSerialNumber('0x7311A33198185484E7516F9655FF7E5BC475CCB9');
        $cert4->setValidFrom(new \DateTimeImmutable('2024-04-23 08:43:00 +00:00'));
        $cert4->setValidTo(new \DateTimeImmutable('2025-04-23 08:43:00 +00:00'));
        $cert4->setSubject(
            '/C=FR' .
            '/ST=EURE-ET-LOIR' .
            '/L=CHARTRES' .
            '/O=CONSEIL-DEPARTEMENTAL-D-EURE-ET-LOIR' .
            '/OU=DSI' .
            '/CN=TEST_USER_4_TAJINE@EXAMPLE.ORG'
        );
        $cert4->setCreatedBy($this->getReference(AppUserFixtures::USER_REFERENCE_MANAGER_1, 'App\Entity\User'));
        $cert4->setUsedBy($this->getReference(AppUserFixtures::USER_REFERENCE_4, 'App\Entity\User'));
        $cert4->setRevoked(false);
        $cert4->setExpired(false);
        $cert4->setAuthority($this->getReference(
            AppCertificateAuthorityFixtures::CERT_AUTHORITY_1,
            'App\Entity\CertificateAuthority'
        ));
        $cert4->setPublicKey('-----BEGIN CERTIFICATE-----
MIIEZzCCA0+gAwIBAgIUcxGjMZgYVITnUW+WVf9+W8R1zLkwDQYJKoZIhvcNAQEL
BQAwaTELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMREwDwYDVQQK
EwhBRFVMTEFDVDExMC8GA1UEAxMoUFJPRFVDVElPTiBBRFVMTEFDVCBJTlRFUk1F
RElBVEUgQ0EgR0VOMTAeFw0yNDA0MjMwNjQzMDBaFw0yNTA0MjMwNjQzMDBaMIGU
MQswCQYDVQQGEwJGUjEVMBMGA1UECBMMRVVSRS1FVC1MT0lSMREwDwYDVQQHEwhD
SEFSVFJFUzEtMCsGA1UEChMkQ09OU0VJTC1ERVBBUlRFTUVOVEFMLUQtRVVSRS1F
VC1MT0lSMQwwCgYDVQQLEwNEU0kxHjAcBgNVBAMTFUNOPUFSUk9VQVJUIEpFQU4t
TUFSQzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOuR4mnII4KZwJCE
mvO09FoAS+jPZasBRLS4LBODH9eiYv+Da21tRBqbUSt3dhpJEanN8z+dh3qZxjvo
xtXGU89Ff9CC6tYomjkUdb4lKjaW2VGYT7fumWhdYdZRcXx33Q7WWQIcqD7XWuAF
PqDsktMqWzWNyx2kFvDkn8j3jvisLUTXIPgPcQlFWX1f1v1b8rKy6v4abFJksf6V
ntXOsqTJCleEXuUUjRJJuF8KkBtKTckRlnouHT11uV8ScjYyXThonFAqtvN2MYN7
o1KmOkY1FYWOt79WlYqj2HWsCYPDp2WsDNGAe3pKcWFxxn0cCbUEmhZkr+GxWXwJ
RaBhSsUCAwEAAaOB2jCB1zAOBgNVHQ8BAf8EBAMCB4AwEwYDVR0lBAwwCgYIKwYB
BQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUQmoN+Ht3ImWg9sXUwjKYtn1M
FgUwHwYDVR0jBBgwFoAUibzY0msxrPyx+TdvwpU8w/H6Kb0wCwYDVR0RBAQwAoIA
MFUGA1UdHwROMEwwSqBIoEaGRGh0dHA6Ly9jcmwuYWR1bGxhY3Qub3JnL2NybC1Q
Uk9EVUNUSU9OQURVTExBQ1RJTlRFUk1FRElBVEVDQUdFTjEuY3JsMA0GCSqGSIb3
DQEBCwUAA4IBAQA0DNfuduE1/vDp/qfxhnYwWN6EKfkH6KyW/YkPZLjI+WXKoSbZ
j/nRx7f8MMePCXhKq92hMzgOIN4V3qpprHj3+kMHpiyTCTtAb7C8ondwT9osAyhI
A+XVfTV5LEik58Hki3D1EDjnReWWCYg0jgDU03xvDBd6ZcjMxySDyQJlL4tro0SM
5zb/iF4q1BS4Xq7rzliAeE4OSVsGJTcpXYeanS+Yipcuh4rnaDlsf94+w8ipphkP
d8OcQJEzex+PUdttIHCPItJzTk+G07u1Y/KKsVv+4uMH7Pj1ssRZDx4SUlLfPjul
sZd4MjH5ugsZ7Dp0EumWZs/dfthW4eoZOHB9
-----END CERTIFICATE-----
');
        $manager->persist($cert4);
        /////////////////////////////////////////////////////////////////////////////////
        $cert5 = new Certificate();
        $cert5->setSerialNumber('0x69322327A0610C364DC3FE5A85E0EEF3F2D3BDDE');
        $cert5->setValidFrom(new \DateTimeImmutable('2023-11-28 21:29:00 +00:00'));
        $cert5->setValidTo(new \DateTimeImmutable('2024-11-27 21:29:00 +00:00'));
        $cert5->setSubject(
            '/C=FR' .
            '/ST=RHONE' .
            '/L=LYON' .
            '/O=RHONE-FR' .
            '/OU=DSI' .
            '/CN=TEST_USER_5_TAJINE@EXAMPLE.ORG'
        );
        $cert5->setCreatedBy($this->getReference(AppUserFixtures::USER_REFERENCE_MANAGER_1, 'App\Entity\User'));
        $cert5->setUsedBy($this->getReference(AppUserFixtures::USER_REFERENCE_5, 'App\Entity\User'));
        $cert5->setRevoked(false);
        $cert5->setExpired(false);
        $cert5->setAuthority($this->getReference(
            AppCertificateAuthorityFixtures::CERT_AUTHORITY_1,
            'App\Entity\CertificateAuthority'
        ));
        $cert5->setPublicKey('-----BEGIN CERTIFICATE-----
MIIEHTCCAwWgAwIBAgIUaTIjJ6BhDDZNw/5aheDu8/LTvd4wDQYJKoZIhvcNAQEL
BQAwaTELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMREwDwYDVQQK
EwhBRFVMTEFDVDExMC8GA1UEAxMoUFJPRFVDVElPTiBBRFVMTEFDVCBJTlRFUk1F
RElBVEUgQ0EgR0VOMTAeFw0yMzExMjgyMDI5MDBaFw0yNDExMjcyMDI5MDBaMFsx
CzAJBgNVBAYTAkZSMQ4wDAYDVQQIEwVSSE9ORTENMAsGA1UEBxMETFlPTjERMA8G
A1UEChMIUkhPTkUtRlIxDDAKBgNVBAsTA0RTSTEMMAoGA1UEAxMDRFVOMIIBIjAN
BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyLhEbA0U8f74uJ8CiZ2tsrVG6Z0P
bOVhCWni2RZzu73RCSBmlib/2EJHqcOXYXzdt7G5yMCPWschz0utNoXsa/aq4ARS
QXKyB6ISB+V4fVxrdfTO7cmW779F8XTrvQG1Lxsxc2cC+0AwRSmBYkLOmvB7YPmq
BVkFaXgI9Vhv0Atbb3IHkUTMUetVdDfakpfpQLyjc1YUwvzrPPvA7F+ZS4WFmNKq
GHX6P0dRU7uRQOaLx2wgwIb+azZ+W6rMIbLXqjH9pazVZfjK4XgUT77pGpWALktq
Cx5235crWP4HvU/XOhYjU7656vLbqYbigL6lb3I6Pa1cxYT1FtZm5boDXwIDAQAB
o4HKMIHHMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0O
BBYEFCeEjuVDCR3RXquZ3uRmqUq52h4aMB8GA1UdIwQYMBaAFIm82NJrMaz8sfk3
b8KVPMPx+im9MAsGA1UdEQQEMAKCADBVBgNVHR8ETjBMMEqgSKBGhkRodHRwOi8v
Y3JsLmFkdWxsYWN0Lm9yZy9jcmwtUFJPRFVDVElPTkFEVUxMQUNUSU5URVJNRURJ
QVRFQ0FHRU4xLmNybDANBgkqhkiG9w0BAQsFAAOCAQEAKVjf+2xf9514Hu6RTB92
+JieK00fnxH0GY581jEh3hEjZCglBQ9YHTff0pQ0AiDZmV+LBfwLvvqsx78IAHjp
Ofas6GKKky3iGrxvrcqY4GvK5jrigbL81ecp544ps1PGgi+3vGOSas6q/yyNxR3P
idC9ZmAVuVUFFcSwxuPgJpWPqsfIRFpAtCnKmgXG+4cMgTl1+N9RNp1woM657v8Y
XKPvmfA+EX/ZbZYx22w5KqXOfChDipqrY7owfu8sLVQsnHWbl5T8CZS5n1iSwkq8
4tl2OPWHXMIJdua5y9eQFXrZQxj+VkfMNvrc33ebItxRgAZPNlqjooo2wGaX0D7C
pg==
-----END CERTIFICATE-----
');
        $manager->persist($cert5);
        /////////////////////////////////////////////////////////////////////////////////
        $cert6 = new Certificate();
        $cert6->setSerialNumber('0x34004D09F44E35567ED6ABB5F6332DFCFA5E5CF1');
        $cert6->setValidFrom(new \DateTimeImmutable('2024-03-19 16:55:00 +00:00'));
        $cert6->setValidTo(new \DateTimeImmutable('2025-03-19 16:55:00 +00:00'));
        $cert6->setSubject(
            '/C=FR' .
            '/ST=RHONE' .
            '/L=VENISSIEUX' .
            '/O=VILLE-DE-VENISSIEUX' .
            '/OU=DIRECTION-DE-L-INNOVATION-ET-DU-DEVELOPPEMENT-NUMERIQUE' .
            '/CN=TEST_USER_6_TAJINE@EXAMPLE.ORG'
        );
        $cert6->setCreatedBy($this->getReference(AppUserFixtures::USER_REFERENCE_MANAGER_1, 'App\Entity\User'));
        $cert6->setUsedBy($this->getReference(AppUserFixtures::USER_REFERENCE_7, 'App\Entity\User'));
        $cert6->setRevoked(false);
        $cert6->setExpired(false);
        $cert6->setAuthority($this->getReference(
            AppCertificateAuthorityFixtures::CERT_AUTHORITY_1,
            'App\Entity\CertificateAuthority'
        ));
        $cert6->setPublicKey('-----BEGIN CERTIFICATE-----
MIIEgjCCA2qgAwIBAgIUNABNCfRONVZ+1qu19jMt/PpeXPEwDQYJKoZIhvcNAQEL
BQAwaTELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMREwDwYDVQQK
EwhBRFVMTEFDVDExMC8GA1UEAxMoUFJPRFVDVElPTiBBRFVMTEFDVCBJTlRFUk1F
RElBVEUgQ0EgR0VOMTAeFw0yNDAzMTkxNTU1MDBaFw0yNTAzMTkxNTU1MDBaMIGv
MQswCQYDVQQGEwJGUjEOMAwGA1UECBMFUkhPTkUxEzARBgNVBAcTClZFTklTU0lF
VVgxHDAaBgNVBAoTE1ZJTExFLURFLVZFTklTU0lFVVgxQDA+BgNVBAsTN0RJUkVD
VElPTi1ERS1MLUlOTk9WQVRJT04tRVQtRFUtREVWRUxPUFBFTUVOVC1OVU1FUklR
VUUxGzAZBgNVBAMTElNWRU4uVkVOSVNTSUVVWC5GUjCCASIwDQYJKoZIhvcNAQEB
BQADggEPADCCAQoCggEBAL1+yVNQ9h9ElIbIIZnUlEeDDXPDA2xg6wgh2dSlgjgX
GAq+Is/Nd5JC9u3J9+JFTVx385aZ4B4DrYZQyUGry4h2TrWsEqZWGdLM8k5hhbyP
aiiC9NoUr0H6c6eTXRGvbKWK1GCyb9DJbzG5WnIdlYOMKwi0wgUhD2664+lGQqvZ
WaTc6ftgIJymXslOpRJDxFpKzSZfIU67whDppfujdAoqlXID0GRetdSZhXuEjvA4
D3+HN6hW5t/Jd+eYWIEUdIbqHW7GtGAeo5PUssrk2DLohPscBwV/hS4bjPrJK3mf
tEvbrV8ZzNFeXMHTMHqq/lwT63/sDNCyI39RWnvS8uUCAwEAAaOB2jCB1zAOBgNV
HQ8BAf8EBAMCB4AwEwYDVR0lBAwwCgYIKwYBBQUHAwIwDAYDVR0TAQH/BAIwADAd
BgNVHQ4EFgQUOQ9+FHRQwSa7wEyw1vTRo3LeVE0wHwYDVR0jBBgwFoAUibzY0msx
rPyx+TdvwpU8w/H6Kb0wCwYDVR0RBAQwAoIAMFUGA1UdHwROMEwwSqBIoEaGRGh0
dHA6Ly9jcmwuYWR1bGxhY3Qub3JnL2NybC1QUk9EVUNUSU9OQURVTExBQ1RJTlRF
Uk1FRElBVEVDQUdFTjEuY3JsMA0GCSqGSIb3DQEBCwUAA4IBAQCWHb5zl3WHNn+F
DvDoD1v7mB1Lf4jXIkWOdl3DR0wePuSnbLwJXd+AHf/fW7n3mGMOFUCbMCm1GO8s
jBTTV4/x782kI+SzdKlJgpT7V323YlrI6IooSFvvoCer0gBj3Xgj1GoMl3iMonF4
Z5lNHa5nRz/HAcT0GSWuINVm8BqQ1/9occl/cvZKjCShghZeRaR/lzZ+lpB7pXkQ
cJkcUS3S7rRgwYPztRnV9xSOELuKfXN4qqsppgdVVyBxczFoF42IvhaKf2wFHF/U
tO6vYN3ilyFZzHMbiniHjLoHidONMkW/IWgL83iThcHDEX4igarlIZxMUh1+sd46
J9NtnKcb
-----END CERTIFICATE-----
');
        $manager->persist($cert6);
        /////////////////////////////////////////////////////////////////////////////////
        $cert7 = new Certificate();
        $cert7->setSerialNumber('0x01CA17C635AE29F6E4E9301F5502FD9224BF61AD');
        $cert7->setValidFrom(new \DateTimeImmutable('2023-10-11 10:30:00 +00:00'));
        $cert7->setValidTo(new \DateTimeImmutable('2024-10-10 10:30:00 +00:00'));
        $cert7->setSubject(
            '/C=FR' .
            '/ST=AUVERGNE-RHONE-ALPES' .
            '/L=THONON-LES-BAINS' .
            '/O=COMMUNE-DE-THONON-LES-BAINS' .
            '/OU=DSI' .
            '/CN=TEST_USER_7_TAJINE@EXAMPLE.ORG'
        );
        $cert7->setCreatedBy($this->getReference(AppUserFixtures::USER_REFERENCE_MANAGER_2, 'App\Entity\User'));
        $cert7->setUsedBy($this->getReference(AppUserFixtures::USER_REFERENCE_7, 'App\Entity\User'));
        $cert7->setRevoked(false);
        $cert7->setExpired(false);
        $cert7->setAuthority($this->getReference(
            AppCertificateAuthorityFixtures::CERT_AUTHORITY_1,
            'App\Entity\CertificateAuthority'
        ));
        $cert7->setPublicKey('-----BEGIN CERTIFICATE-----
MIIEZzCCA0+gAwIBAgIUAcoXxjWuKfbk6TAfVQL9kiS/Ya0wDQYJKoZIhvcNAQEL
BQAwaTELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMREwDwYDVQQK
EwhBRFVMTEFDVDExMC8GA1UEAxMoUFJPRFVDVElPTiBBRFVMTEFDVCBJTlRFUk1F
RElBVEUgQ0EgR0VOMTAeFw0yMzEwMTEwODMwMDBaFw0yNDEwMTAwODMwMDBaMIGk
MQswCQYDVQQGEwJGUjEdMBsGA1UECBMUQVVWRVJHTkUtUkhPTkUtQUxQRVMxGTAX
BgNVBAcTEFRIT05PTi1MRVMtQkFJTlMxJDAiBgNVBAoTG0NPTU1VTkUtREUtVEhP
Tk9OLUxFUy1CQUlOUzEUMBIGA1UECxMLQURNSU4tUzJMT1cxHzAdBgNVBAMMFkwt
TUVUWkBWSUxMRS1USE9OT04uRlIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
AoIBAQDRIQRKe9jdW5zLeuBifCDAPvOhGL+h2HOcqNMH4dXFKRaMN/zdtteDSHAD
xNHdSF5t2yLDC7XiQXy99pT1I6Kp8euR0Ecm8hH/keuUAZ15FcYnC0/FyoShBFbj
HXyBj0NAoFFA7Av6W+QRlnfKCp3/BOz0p2gXmkhuw61EIQzh10uASU4g+GOK/Hh/
0tnU7ryU+iNy/s+42720WxrMK3ticUo8WHFqT9507anqiz/FheDJ7xkpESB3Vh4V
3ANkUmQ8WA7Qi+thmyP3M+/MlhNxfqBgG4ZGeLcMj+jAE1+wx3k9hDSHaPuleEFV
Ki2gMJHVih5SUwN2nRMEfhn3bmcrAgMBAAGjgcowgccwEwYDVR0lBAwwCgYIKwYB
BQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUHCylCTwrqOaJZBtJxBbWjZFf
i6kwHwYDVR0jBBgwFoAUibzY0msxrPyx+TdvwpU8w/H6Kb0wCwYDVR0RBAQwAoIA
MFUGA1UdHwROMEwwSqBIoEaGRGh0dHA6Ly9jcmwuYWR1bGxhY3Qub3JnL2NybC1Q
Uk9EVUNUSU9OQURVTExBQ1RJTlRFUk1FRElBVEVDQUdFTjEuY3JsMA0GCSqGSIb3
DQEBCwUAA4IBAQAcJShtK2zStgKU6UryuF5TPoNaAMhTspcF/oikWFhy3bDE+iSx
ddfgpNdg6zDWSKc97upDGnG/nQJSK/tR/T94RfHRgYvZSHVrTGHFOYiXOsGP7XCa
JKeozZLSdAo+aOXdnzFuYTnP0IvWLkVyHYTtlw/DGuHuA6KqOyNSoOmCfYuPwSq3
ye9LxZCYprV3mOOgy7uGY5jlNmf1HB4wS4E8YUVd/Vtxgwj8D62wt4oRIXc4ePD8
oeJkTxW+Rp0vFICwpeO9f1CVy86wzaakXNfxqOEJAdbQR0Bs5pphv9Dhf3fLMiCx
B7BYhXbnPz0h5RO8kEXyYHEA9whqhYe9bfYK
-----END CERTIFICATE-----
');
        $manager->persist($cert7);
        /////////////////////////////////////////////////////////////////////////////////
        $cert8 = new Certificate();
        $cert8->setSerialNumber('0x52A3516A6D8CFE68F810C1B68BAE87720225D11B');
        $cert8->setValidFrom(new \DateTimeImmutable('2023-08-17 11:56:00 +00:00'));
        $cert8->setValidTo(new \DateTimeImmutable('2024-08-16 11:56:00 +00:00'));
        $cert8->setSubject(
            '/C=FR' .
            '/ST=VAUCLUSE' .
            '/L=PERTUIS' .
            '/O=COMMUNE-DE-PERTUIS' .
            '/OU=FINANCES' .
            '/CN=TEST_USER_7_TAJINE@EXAMPLE.ORG'
        );
        $cert8->setCreatedBy($this->getReference(AppUserFixtures::USER_REFERENCE_MANAGER_2, 'App\Entity\User'));
        $cert8->setUsedBy($this->getReference(AppUserFixtures::USER_REFERENCE_8, 'App\Entity\User'));
        $cert8->setRevoked(false);
        $cert8->setExpired(false);
        $cert8->setAuthority($this->getReference(
            AppCertificateAuthorityFixtures::CERT_AUTHORITY_1,
            'App\Entity\CertificateAuthority'
        ));
        $cert8->setPublicKey('-----BEGIN CERTIFICATE-----
MIIEZzCCA0+gAwIBAgIUcxGjMZgYVITnUW+WVf9+W8R1zLkwDQYJKoZIhvcNAQEL
BQAwaTELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMREwDwYDVQQK
EwhBRFVMTEFDVDExMC8GA1UEAxMoUFJPRFVDVElPTiBBRFVMTEFDVCBJTlRFUk1F
RElBVEUgQ0EgR0VOMTAeFw0yNDA0MjMwNjQzMDBaFw0yNTA0MjMwNjQzMDBaMIGU
MQswCQYDVQQGEwJGUjEVMBMGA1UECBMMRVVSRS1FVC1MT0lSMREwDwYDVQQHEwhD
SEFSVFJFUzEtMCsGA1UEChMkQ09OU0VJTC1ERVBBUlRFTUVOVEFMLUQtRVVSRS1F
VC1MT0lSMQwwCgYDVQQLEwNEU0kxHjAcBgNVBAMTFUNOPUFSUk9VQVJUIEpFQU4t
TUFSQzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOuR4mnII4KZwJCE
mvO09FoAS+jPZasBRLS4LBODH9eiYv+Da21tRBqbUSt3dhpJEanN8z+dh3qZxjvo
xtXGU89Ff9CC6tYomjkUdb4lKjaW2VGYT7fumWhdYdZRcXx33Q7WWQIcqD7XWuAF
PqDsktMqWzWNyx2kFvDkn8j3jvisLUTXIPgPcQlFWX1f1v1b8rKy6v4abFJksf6V
ntXOsqTJCleEXuUUjRJJuF8KkBtKTckRlnouHT11uV8ScjYyXThonFAqtvN2MYN7
o1KmOkY1FYWOt79WlYqj2HWsCYPDp2WsDNGAe3pKcWFxxn0cCbUEmhZkr+GxWXwJ
RaBhSsUCAwEAAaOB2jCB1zAOBgNVHQ8BAf8EBAMCB4AwEwYDVR0lBAwwCgYIKwYB
BQUHAwIwDAYDVR0TAQH/BAIwADAdBgNVHQ4EFgQUQmoN+Ht3ImWg9sXUwjKYtn1M
FgUwHwYDVR0jBBgwFoAUibzY0msxrPyx+TdvwpU8w/H6Kb0wCwYDVR0RBAQwAoIA
MFUGA1UdHwROMEwwSqBIoEaGRGh0dHA6Ly9jcmwuYWR1bGxhY3Qub3JnL2NybC1Q
Uk9EVUNUSU9OQURVTExBQ1RJTlRFUk1FRElBVEVDQUdFTjEuY3JsMA0GCSqGSIb3
DQEBCwUAA4IBAQA0DNfuduE1/vDp/qfxhnYwWN6EKfkH6KyW/YkPZLjI+WXKoSbZ
j/nRx7f8MMePCXhKq92hMzgOIN4V3qpprHj3+kMHpiyTCTtAb7C8ondwT9osAyhI
A+XVfTV5LEik58Hki3D1EDjnReWWCYg0jgDU03xvDBd6ZcjMxySDyQJlL4tro0SM
5zb/iF4q1BS4Xq7rzliAeE4OSVsGJTcpXYeanS+Yipcuh4rnaDlsf94+w8ipphkP
d8OcQJEzex+PUdttIHCPItJzTk+G07u1Y/KKsVv+4uMH7Pj1ssRZDx4SUlLfPjul
sZd4MjH5ugsZ7Dp0EumWZs/dfthW4eoZOHB9
-----END CERTIFICATE-----
');
        $manager->persist($cert8);
        /////////////////////////////////////////////////////////////////////////////////
        $cert9 = new Certificate();
        $cert9->setSerialNumber('0x7EFD415BE3004120984CE178A7EBB0C9DB04E89B');
        $cert9->setValidFrom(new \DateTimeImmutable('2023-08-23 16:10:00 +00:00'));
        $cert9->setValidTo(new \DateTimeImmutable('2024-08-22 16:10:00 +00:00'));
        $cert9->setSubject(
            '/C=FR' .
            '/ST=PYRENEES-ATLANTIQUES' .
            '/L=PAU' .
            '/O=COMMUNE-DE-PAU' .
            '/OU=21640445900010' .
            '/CN=TEST_USER_8_TAJINE@EXAMPLE.ORG'
        );
        $cert9->setCreatedBy($this->getReference(AppUserFixtures::USER_REFERENCE_MANAGER_2, 'App\Entity\User'));
        $cert9->setUsedBy($this->getReference(AppUserFixtures::USER_REFERENCE_8, 'App\Entity\User'));
        $cert9->setRevoked(false);
        $cert9->setExpired(false);
        $cert9->setAuthority($this->getReference(
            AppCertificateAuthorityFixtures::CERT_AUTHORITY_1,
            'App\Entity\CertificateAuthority'
        ));
        $cert9->setPublicKey('-----BEGIN CERTIFICATE-----
MIIEUTCCAzmgAwIBAgIUfv1BW+MAQSCYTOF4p+uwydsE6JswDQYJKoZIhvcNAQEL
BQAwaTELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMREwDwYDVQQK
EwhBRFVMTEFDVDExMC8GA1UEAxMoUFJPRFVDVElPTiBBRFVMTEFDVCBJTlRFUk1F
RElBVEUgQ0EgR0VOMTAeFw0yMzA4MjMxNDEwMDBaFw0yNDA4MjIxNDEwMDBaMIGO
MQswCQYDVQQGEwJGUjEdMBsGA1UECBMUUFlSRU5FRVMtQVRMQU5USVFVRVMxDDAK
BgNVBAcTA1BBVTEXMBUGA1UEChMOQ09NTVVORS1ERS1QQVUxFzAVBgNVBAsTDjIx
NjQwNDQ1OTAwMDEwMSAwHgYDVQQDDBdDLkxBU1NBTExFQEFHR0xPLVBBVS5GUjCC
ASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKx/IOaOFpb1cMk951jupjzy
OdkEdxSZ6o7qf9002irBRS48YccuD1D/jcsmjbRJSUsLolKFc0n+fmPFJKWhJY7a
IwDVatP4heDwh57bCJMNvk2IWRS66UpSfaD3kOUF26nohWn0qzsg1l+69HBAqHRY
Nj6kR4A+6DxFTlEIcKjSCUjCR/Kcej+llDSbxvHf6wll5XZQju/JD/CJQcJ2vrej
pqiPfdH7PXR9sfj7uqCkpwBv1p9+KwwviEFKbKXNlrZRzAaouTEzbbXFgqQ1UILI
+4hubt6yr7CFvI/+PlvpfrkNS+A8DbbzKjDK5dZck57HlDD8oV61DfCFUHm/s+cC
AwEAAaOByjCBxzATBgNVHSUEDDAKBggrBgEFBQcDAjAMBgNVHRMBAf8EAjAAMB0G
A1UdDgQWBBTP/z/0ESRfYBC9F9FKhqGgZmmKUTAfBgNVHSMEGDAWgBSJvNjSazGs
/LH5N2/ClTzD8fopvTALBgNVHREEBDACggAwVQYDVR0fBE4wTDBKoEigRoZEaHR0
cDovL2NybC5hZHVsbGFjdC5vcmcvY3JsLVBST0RVQ1RJT05BRFVMTEFDVElOVEVS
TUVESUFURUNBR0VOMS5jcmwwDQYJKoZIhvcNAQELBQADggEBAC6K6B2gQJz6glpG
p8rzwDWbSPkokzXNIJQuFbLhxzitG3BbH9531nn5/GBrmMWl3YhfnmEEF3lk+COk
Ij52WM0mFM+gzQ44/wzCDPhRxmm5R+c7PmsnpSq0ITf9LU7XCgBopl/x/wdCLm6U
tha8q00HVXooE7mqynw1QhjobhReR4n8oorIY3UWSshVfqQpkNiP5ehbo+0pbyLU
XDf6gXJO56zotfLG0qkCSlF9RNAXeO6RgHYZjak9XcjJKnTws6fu8TkDMdccFIki
LLDkANxoY98Jzo6LwegmXzm+ztdD4Uwi6sSOOgncZDWzSoYRvV2DlBwXZbONvM1K
PV+iXR0=
-----END CERTIFICATE-----
');
        $manager->persist($cert9);
        /////////////////////////////////////////////////////////////////////////////////
        $cert10 = new Certificate();
        $cert10->setSerialNumber('0x4CBD32152451A5F09D7B66FAFA28E2E0DC7D4FC4');
        $cert10->setValidFrom(new \DateTimeImmutable('2023-10-31 11:42:00 +00:00'));
        $cert10->setValidTo(new \DateTimeImmutable('2024-10-30 11:42:00 +00:00'));
        $cert10->setSubject(
            '/C=FR' .
            '/ST=PYRENEES-ATLANTIQUES' .
            '/L=PAU' .
            '/O=CCAS-DE-PAU' .
            '/OU=PILOTAGE-ET-RESSOURCES' .
            '/CN=TEST_USER_8_TAJINE@EXAMPLE.ORG'
        );
        $cert10->setCreatedBy($this->getReference(AppUserFixtures::USER_REFERENCE_MANAGER_2, 'App\Entity\User'));
        $cert10->setUsedBy($this->getReference(AppUserFixtures::USER_REFERENCE_8, 'App\Entity\User'));
        $cert10->setRevoked(false);
        $cert10->setExpired(false);
        $cert10->setAuthority($this->getReference(
            AppCertificateAuthorityFixtures::CERT_AUTHORITY_1,
            'App\Entity\CertificateAuthority'
        ));
        $cert10->setPublicKey('-----BEGIN CERTIFICATE-----
MIIEPzCCAyegAwIBAgIUTL0yFSRRpfCde2b6+iji4Nx9T8QwDQYJKoZIhvcNAQEL
BQAwaTELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMREwDwYDVQQK
EwhBRFVMTEFDVDExMC8GA1UEAxMoUFJPRFVDVElPTiBBRFVMTEFDVCBJTlRFUk1F
RElBVEUgQ0EgR0VOMTAeFw0yMzEwMzExMDQyMDBaFw0yNDEwMzAxMDQyMDBaMH0x
CzAJBgNVBAYTAkZSMR0wGwYDVQQIExRQWVJFTkVFUy1BVExBTlRJUVVFUzEMMAoG
A1UEBxMDUEFVMRQwEgYDVQQKEwtDQ0FTLURFLVBBVTEfMB0GA1UECxMWUElMT1RB
R0UtRVQtUkVTU09VUkNFUzEKMAgGA1UEAxMBWDCCASIwDQYJKoZIhvcNAQEBBQAD
ggEPADCCAQoCggEBAN+eEYiuNSI5EHBDBErwPzPFSN2OowzcR6awj3o51d8l/B3J
UMx+pJ/GWf6HUmLVQaGpDJZFNwMjOm3uKDQub0h5AyQJvp9cXZ9oMvo/c8fh4tJo
mkHXan6vvc3uR0WR7H/NBXmVdaebhiSfvUFXGUNN0A9rppp0TNZiw69EMnyaiVyf
fdrToDS0AjF29+rmLRS0w0Y+mUn5VI5qNGV5Ljx7LmnjOZs4OTi/q+bnE7Ry3Rbp
UYxORzhzRxLDxclzzOgYcfbxvfWWGBdQktEpNhiHHBgXU2fDE5FzDWwQ8nNN2305
mlFuqQjmRZjEUSBQ9r+A8FaTokmH6UMrJbN5YKUCAwEAAaOByjCBxzATBgNVHSUE
DDAKBggrBgEFBQcDAjAMBgNVHRMBAf8EAjAAMB0GA1UdDgQWBBSHmZVyFdgxYLuG
viVohWKPfygScDAfBgNVHSMEGDAWgBSJvNjSazGs/LH5N2/ClTzD8fopvTALBgNV
HREEBDACggAwVQYDVR0fBE4wTDBKoEigRoZEaHR0cDovL2NybC5hZHVsbGFjdC5v
cmcvY3JsLVBST0RVQ1RJT05BRFVMTEFDVElOVEVSTUVESUFURUNBR0VOMS5jcmww
DQYJKoZIhvcNAQELBQADggEBADrzjbjLz+JiBR3kbSxAfazt1xid+t6prqlntUH9
XOFWbhUgjzSfDfVxGikQD4T7ZwTrA3ycbAK4uKudp8p9bVW9PTCEMPVKu/brWPvl
4g/Ha8Q556PSX9Qwp97ar2d0eLTrGw+eMnjtEvUxhfb1U0o3Qwgw8ERw6vX0GQrY
iHl5fla74RsiudDIyRS5FZq34MAug8J+x20LuPR7Jg0XVJQJWTGgC+OZRxFU+qmP
EWHoyNwrsl4PqohfPRiuwHwxpNMVgCJiASFUz1DHfr9YUdma4CMxuslrpyzGdUTg
tON61+HT7zqqPFiUXzrovHM3aWxQ8+jErdhnj5inTAC2Ct8=
-----END CERTIFICATE-----
');
        $manager->persist($cert10);
        /////////////////////////////////////////////////////////////////////////////////
        $manager->flush();
    }
}
