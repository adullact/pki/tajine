<?php

namespace App\Repository;

use App\Entity\Certificate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Certificate>
 *
 * @method Certificate|null find($id, $lockMode = null, $lockVersion = null)
 * @method Certificate|null findOneBy(array $criteria, array $orderBy = null)
 * @method Certificate[]    findAll()
 * @method Certificate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CertificateRepository extends ServiceEntityRepository
{
    public const  NUMBER_OF_DAYS_WHEN_EXPIRY_IS_EXPECTED_SOON = 30;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Certificate::class);
    }

    public function save(Certificate $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);
        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Certificate $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function getCertificatePaginator(int $maxItemPerPage, int $page = 1, $criteria = []): Paginator
    {
        $firstResult = ($page - 1) * $maxItemPerPage;
        $queryBuilder = $this->createQueryBuilder('c');
        if (count($criteria) > 0) {
            foreach ($criteria as $key => $value) {
                $queryBuilder->andWhere("c.$key = :$key");
                $queryBuilder->setParameter("$key", $value);
            }
        }
        $queryBuilder->orderBy('c.validFrom', 'DESC');
        $queryBuilder->setMaxResults($maxItemPerPage);
        $queryBuilder->setFirstResult($firstResult);
        return new Paginator($queryBuilder->getQuery());
    }

    public function getCertificateCreatedByPaginator(int $createdById, int $maxItemPerPage, int $page = 1): Paginator
    {
        return $this->getCertificatePaginator(
            maxItemPerPage: $maxItemPerPage,
            page: $page,
            criteria: ['createdBy' => $createdById],
        );
    }

    /**
     * @return Certificate[] Returns an array of Certificate objects
     */
    public function findCertificatesExpiringSoon(
        int $numberOfDays = self::NUMBER_OF_DAYS_WHEN_EXPIRY_IS_EXPECTED_SOON
    ): array {
        $now =  new \DateTimeImmutable();
        $dateStart = new \DateTimeImmutable();
        $dateEnd = $now->add(\DateInterval::createFromDateString("$numberOfDays day"));
        return $this->createQueryBuilder('cert')
            ->andWhere('cert.validTo > :date_before')
            ->setParameter('date_before', $dateStart->format("Y-m-d 0:0:00"))
            ->andWhere('cert.validTo <= :date_after')
            ->setParameter('date_after', $dateEnd->format("Y-m-d 23:59:59"))
            ->orderBy('cert.validTo', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

//    /**
//     * @return Certificate[] Returns an array of Certificate objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Certificate
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
