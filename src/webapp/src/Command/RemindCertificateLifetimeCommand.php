<?php

namespace App\Command;

use App\Repository\CertificateRepository;
use App\Service\Certificate\CertificateRemindLifetime;
use App\Service\Mailer\CertificateRemindLifetimeMailer;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Scheduler\Attribute\AsPeriodicTask;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

//          bin/console messenger:consume scheduler_default
//#[AsPeriodicTask(frequency: '1 days', from: '09:30', jitter: 60, schedule: 'default')]
//#[AsPeriodicTask(frequency: 10, schedule: 'default')]
#[AsCommand(
    name: 'tajine:remind-certificate-lifetime',
    description: 'Notify end-users and certificate creators if soon-to-expire certificates',
)]
class RemindCertificateLifetimeCommand extends Command
{
    public function __construct(
        private CertificateRepository $certificateRepository,
        private readonly ContainerBagInterface $containerBag,
        private CertificateRemindLifetimeMailer $remindLifetimeMailer,
        private CertificateRemindLifetime $certificateRemindLifetime,
    ) {
        parent::__construct();
    }


    /**
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws RuntimeError
     * @throws LoaderError
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $result = $this->certificateRemindLifetime->remindCertificateEndOfLifeToManager();
        if (false !== $result) {
            $msg = '';
            $msg .= "$result certificates due to expire soon have been processed ";
            $msg .= " (notification to end user and certificate creator)";
            $io->success("SUCCESS");
            $io->success("$msg");
            return Command::SUCCESS;
        } else {
            $io->error('FAILURE : ');
            return Command::FAILURE;
        }
//        try {
//
//        } catch (NotFoundExceptionInterface $e) {
//        } catch (ContainerExceptionInterface $e) {
//        } catch (TransportExceptionInterface $e) {
//        } catch (LoaderError $e) {
//        } catch (RuntimeError $e) {
//        } catch (SyntaxError $e) {
//        }
    }
}
