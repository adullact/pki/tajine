# CFSSL API examples

```bash
CFSSL_API_HOST="http://127.0.0.1:8888"
AUTHORITY_KEY_ID="BF:94:17:23:98:A8:AA:B1:50:97:46:CA:52:A4:1D:BD:70:DE:9A:45"
AUTHORITY_KEY_ID_TO_REVOKE="bf94172398a8aab1509746ca52a41dbd70de9a45" # Should be formatted
                                                                      # without colons and all lowercase

CERT_SERIAL_NUMBER_HEX="0x3A031E2A703D82CCD1A625871850E66F899F0716" # FAIL
CERT_SERIAL_NUMBER_HEX="3A031E2A703D82CCD1A625871850E66F899F0716"
CERT_SERIAL_NUMBER_DECIMAL=$(echo "ibase=16; ${CERT_SERIAL_NUMBER_HEX}" | bc)
echo "HEX = ${CERT_SERIAL_NUMBER_HEX}"
echo "DEC = ${CERT_SERIAL_NUMBER_DECIMAL}"
```

## Get information about a certificate

Documentation: https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_certinfo

Example of use:

```bash
# /api/v1/cfssl/certinfo
curl  -s  -H "Content-Type: application/json"                         \
      -d "{ \"serial\":           \"${CERT_SERIAL_NUMBER_DECIMAL}\",
            \"authority_key_id\": \"${AUTHORITY_KEY_ID}\"}"           \
      -X  POST  ${CFSSL_API_HOST}/api/v1/cfssl/certinfo  | jq
```

Example of result:

```json
{
  "success": true,
  "result": {
    "subject": {
      "common_name": "user@example.org",
      "country": "FR",
      "organization": "Mairie de Montpellier",
      "organizational_unit": "465146154365143651",
      "locality": "Montpellier",
      "province": "Herault",
      "names": [
        "FR",
        "Herault",
        "Montpellier",
        "Mairie de Montpellier",
        "465146154365143651",
        "user@example.org"
      ]
    },
    "issuer": {
      "common_name": "VAGRANT ADULLACT INTERMEDIATE CA GEN1",
      "country": "FR",
      "organization": "ADULLACT",
      "locality": "MONTPELLIER",
      "names": [
        "FR",
        "MONTPELLIER",
        "ADULLACT",
        "VAGRANT ADULLACT INTERMEDIATE CA GEN1"
      ]
    },
    "serial_number": "331190994753008566338719872081805755648472712982",
    "sans": [
      ""
    ],
    "not_before": "2023-03-27T20:23:00Z",
    "not_after": "2024-03-26T20:23:00Z",
    "sigalg": "SHA256WithRSA",
    "authority_key_id": "BF:94:17:23:98:A8:AA:B1:50:97:46:CA:52:A4:1D:BD:70:DE:9A:45",
    "subject_key_id": "64:49:95:3B:EB:92:50:2F:12:95:99:C9:8B:55:F9:F0:97:E3:15:BD",
    "pem": "-----BEGIN CERTIFICATE-----\n (...) \n-----END CERTIFICATE-----\n"
  },
  "errors": [],
  "messages": []
}
```

## Revoke a certificate

Documentation: https://github.com/cloudflare/cfssl/blob/master/doc/api/endpoint_revoke.txt

Example of use:

```bash
# /api/v1/cfssl/revoke
curl  -s  -H "Content-Type: application/json"                         \
      -d "{ \"serial\":           \"${CERT_SERIAL_NUMBER_DECIMAL}\",
            \"authority_key_id\": \"${AUTHORITY_KEY_ID_TO_REVOKE}\",
            \"reason\":           \"superseded\"}"                    \
      -X  POST  ${CFSSL_API_HOST}/api/v1/cfssl/revoke  | jq
```

> Note:
> A [bug impacting the `authority_key_id` format](https://github.com/cloudflare/cfssl/issues/604)
> with its [workaround](https://github.com/cloudflare/cfssl/issues/604#issuecomment-311227496)

Example of result:

```json
{
  "success": true,
  "result": {},
  "errors": [],
  "messages": []
}
```
