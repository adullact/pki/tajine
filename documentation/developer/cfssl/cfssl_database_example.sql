create table certificates
(
    serial_number            bytea not null,
    authority_key_identifier bytea not null,
    ca_label                 bytea,
    status                   bytea not null,
    reason                   integer,
    expiry                   timestamp with time zone,
    revoked_at               timestamp with time zone,
    pem                      bytea not null,
    issued_at                timestamp with time zone,
    not_before               timestamp with time zone,
    metadata                 jsonb,
    sans                     jsonb,
    common_name              text,
    primary key (serial_number, authority_key_identifier)
);


INSERT INTO public.certificates (
         serial_number,
         authority_key_identifier,
         ca_label,
         status,
         reason,
         expiry,
         revoked_at,
         pem,
         issued_at,
         not_before,
         metadata,
         sans,
         common_name
)
VALUES ('331190994753008566338719872081805755648472712982',
        'bf94172398a8aab1509746ca52a41dbd70de9a45',
        '',
        'revoked',
        4,
        '2024-03-26 20:23:00.000000 +00:00',
        '2023-03-27 20:30:49.362813 +00:00',
        e'-----BEGIN CERTIFICATE-----
MIIEJzCCAw+gAwIBAgIUOgMeKnA9gszRpiWHGFDmb4mfBxYwDQYJKoZIhvcNAQEL
BQAwZjELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMREwDwYDVQQK
EwhBRFVMTEFDVDEuMCwGA1UEAxMlVkFHUkFOVCBBRFVMTEFDVCBJTlRFUk1FRElB
VEUgQ0EgR0VOMTAeFw0yMzAzMjcyMDIzMDBaFw0yNDAzMjYyMDIzMDBaMIGNMQsw
CQYDVQQGEwJGUjEQMA4GA1UECBMHSGVyYXVsdDEUMBIGA1UEBxMLTW9udHBlbGxp
ZXIxHjAcBgNVBAoTFU1haXJpZSBkZSBNb250cGVsbGllcjEbMBkGA1UECxMSNDY1
MTQ2MTU0MzY1MTQzNjUxMRkwFwYDVQQDDBB1c2VyQGV4YW1wbGUub3JnMIIBIjAN
BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4wwL2BGy9gVQA7b6tblPKh5m/U6g
cY+n8rOpXDakDKxcgduBGzSOyWpCVdEBrTt2O4EC5pBuvEyhXq6x1CWJrNTwO1p0
gYcYoc181bO34nJ+Gx7w9vESDzT3KS+WUZx304V07guLQtqlKqjylxvBmqm1qBBP
h9vV1rHzoYYJ+1/uhAWap+CShHik0Nujrr39c34dnKYNSvgFFjYXKAEx9G7QubqN
njNZxWK0civQ1a85uXuqhOjpTd8L6qUWf0gq6YQy1akBOUrytqylW6+NXYLhFIx5
iZCxiBuHwB4HlS4CI4MXFWO5X2TcusN2WaljCXLq4FAX7ZO1I56g4wvg+QIDAQAB
o4GkMIGhMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0O
BBYEFGRJlTvrklAvEpWZyYtV+fCX4xW9MB8GA1UdIwQYMBaAFL+UFyOYqKqxUJdG
ylKkHb1w3ppFMAsGA1UdEQQEMAKCADAvBgNVHR8EKDAmMCSgIqAghh5odHRwOi8v
cGtpLmV4YW1wbGUub3JnL2NybC5wZW0wDQYJKoZIhvcNAQELBQADggEBAEvKKh4U
fzucrG0P/WYlQmA3Hy7wyDLk0B0p93Kdm3zBJsMjsm/GiOyUP/i/5YDaaym7mjU8
530OSzE+lqMCa4xkxbYXGVUbUJefK/QkYjRLv5WovIA+unOrnN/G5JxYlSHZl4Th
qWGG/mPHglvMpLFLD8y64ItdjdC11qBG/LXBSs9bkqPAfwInWoQBbxlCdaCT5I4g
PAqTaNc7BCoVdEx6l4zHKihPleXQ+WiICKWpGHKzAvSmhIlMpLszbaURvPq6XsoJ
3sko+7zUoER6SNmoT09JdyPR2UQVduym8LUxwBtf2OTGWtt1UNleNbEaQvIFujYh
tEFsGlHFwKUXOVM=
-----END CERTIFICATE-----
',
        '2023-03-27 20:28:11.697686 +00:00',
        '2023-03-27 20:23:00.000000 +00:00',
        'null',
        '[""]',
        'user@example.org'
);

INSERT INTO public.certificates (
    serial_number,
    authority_key_identifier,
    ca_label,
    status,
    reason,
    expiry,
    revoked_at,
    pem,
    issued_at,
    not_before,
    metadata,
    sans,
    common_name
)
VALUES ('343991113225503166764870653778946302273688699880',
        'bf94172398a8aab1509746ca52a41dbd70de9a45',
        '',
        'good',
        0,
        '2024-03-26 20:22:00.000000 +00:00',
        '0001-01-01 00:00:00.000000 +00:00',
        e'-----BEGIN CERTIFICATE-----
MIIEJzCCAw+gAwIBAgIUPEEYUdL5qqMa30Rw+VS0DBNkA+gwDQYJKoZIhvcNAQEL
BQAwZjELMAkGA1UEBhMCRlIxFDASBgNVBAcTC01PTlRQRUxMSUVSMREwDwYDVQQK
EwhBRFVMTEFDVDEuMCwGA1UEAxMlVkFHUkFOVCBBRFVMTEFDVCBJTlRFUk1FRElB
VEUgQ0EgR0VOMTAeFw0yMzAzMjcyMDIyMDBaFw0yNDAzMjYyMDIyMDBaMIGNMQsw
CQYDVQQGEwJGUjEQMA4GA1UECBMHSGVyYXVsdDEUMBIGA1UEBxMLTW9udHBlbGxp
ZXIxHjAcBgNVBAoTFU1haXJpZSBkZSBNb250cGVsbGllcjEbMBkGA1UECxMSNDY1
MTQ2MTU0MzY1MTQzNjUxMRkwFwYDVQQDDBB1c2VyQGV4YW1wbGUub3JnMIIBIjAN
BgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAumAkWvIGMrXboU/z6VY6ORXUjEQO
alGgcPZrIZKWpRVRN6hQzeOGXNVjuwUHn8k0aieJ4gl93yVGNDDnXR73VIuhCEcw
THYhxymXVoTexHck/v0f82TOMqllP4QSYXtvNpo20LfgwO/mcn9hBJbqkKwFKr+L
7vlKiv6SCj/kggW0+VG59rFNy5vx/n/vVFKWkxZqHUHHWE54MkgrcLE7OzcDubbI
hoWXe/mp5p28BqNpNsrKdGwbFR2gL+LWSfUYA/9EOtm7aIe1CyFUhOLkM4hRm8OX
BL0mpuh6Npt4XMCVexPrXE6RMzrqx1MA4ARvm75TTRd61MFmJHFXFhwCbwIDAQAB
o4GkMIGhMBMGA1UdJQQMMAoGCCsGAQUFBwMCMAwGA1UdEwEB/wQCMAAwHQYDVR0O
BBYEFPVebw5F/95KQphEDUELHL4v03+tMB8GA1UdIwQYMBaAFL+UFyOYqKqxUJdG
ylKkHb1w3ppFMAsGA1UdEQQEMAKCADAvBgNVHR8EKDAmMCSgIqAghh5odHRwOi8v
cGtpLmV4YW1wbGUub3JnL2NybC5wZW0wDQYJKoZIhvcNAQELBQADggEBAKftNFnj
CVwKgbvARKYQuDRabOtfQBoLf882lQJJzCqXgk/vh/owYirvvFMERKD+dIQ0wkc5
rKeMHByl0LpDSm1SSWXwD99wO3ZhN10H//HE/Ita/CGqsqdJHLIvZtUDQxbxf/jX
8mUh48zpemCG28ZN9PlNLugJoxRGmub7x7IEMRbIRo8cWJ54bYqnpUHrLjjTsRQV
XLNz1MuCjnNhKDQ4ZO0GYO2KGqcT2zsTBpCaQnfhjjxW3wodf/KJpr3vhmALXI0g
b7ecs0meKDAIilm5JdZdGV26X1krOwkHvZk2QnEHOo6py8q4SdJw1q9zi/32HpDJ
1h2F3DAgkXsiivw=
-----END CERTIFICATE-----
',
        '2023-03-27 20:26:46.112057 +00:00',
        '2023-03-27 20:22:00.000000 +00:00',
        'null',
        '[""]',
        'user@example.org'
);
