#!/usr/bin/env bash

# stop at first error
set -e

# Variables
export DEBIAN_FRONTEND=noninteractive

##### PREREQUISITES
apt-get update

##### Ubuntu packages
echo "===== Ubuntu packages"
apt-get install -yqq    php8.1           # mandatory to use Synfony
apt-get install -yqq    php8.1-curl      # mandatory to use Tajine
apt-get install -yqq    php8.1-intl      # usefull   to use Synfony (improve performance)
apt-get install -yqq    php8.1-mbstring  # usefull   to use Synfony
apt-get install -yqq    php8.1-pgsql     # mandatory to use Tajine
apt-get install -yqq    php8.1-xml       # mandatory to use Synfony
apt-get install -yqq    unzip            # usefull   to use Composer (improve performance)
apt-get install -yqq    ccze             # devops tool (log colorizer)
apt-get install -yqq    tree             # devops tool
apt-get install -yqq    htop             # devops tool

##### extra shell tools
# - bat: a cat clone with syntax highlighting and Git integration
echo "---------> INSTALL extra shell tools (bat)"
wget --no-verbose  https://github.com/sharkdp/bat/releases/download/v0.13.0/bat_0.13.0_amd64.deb
dpkg -i bat_0.13.0_amd64.deb
rm bat_0.13.0_amd64.deb

## Install Composer
echo "===== Install Composer"
php --version
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
mv -v  composer.phar /usr/local/bin/composer
sudo -u vagrant composer --version

## Install Synfony 6.2 template
cd /tmp
sudo -u vagrant composer create-project symfony/skeleton:"^6.2" /tmp/template_synfony_6.2
cd /tmp/template_synfony_6.2
sudo -u vagrant composer check-platform-reqs
    #    composer-plugin-api 2.3.0   success  ---> via composer
    #    ext-ctype           8.1.2   success  ---> via php8.1     on Ubuntu 22.04
    #    ext-iconv           8.1.2   success  ---> via php8.1     on Ubuntu 22.04
    #    ext-xml             8.1.2   success  ---> via php8.1-xml on Ubuntu 22.04
    #    php                 8.1.2   success  ---> via php8.1     on Ubuntu 22.04
sudo -u vagrant composer require webapp
sudo -u vagrant composer check-platform-reqs
    #    composer-plugin-api  2.3.0      success  ---> via composer
    #    composer-runtime-api 2.2.2      success  ---> via composer
    #    ext-ctype            8.1.2      success  ---> via php8.1     on Ubuntu 22.04
    #    ext-dom              20031129   success  ---> via php8.1-xml on Ubuntu 22.04
    #    ext-filter           8.1.2      success  ---> via php8.1     on Ubuntu 22.04
    #    ext-iconv            8.1.2      success  ---> via php8.1     on Ubuntu 22.04
    #    ext-json             8.1.2      success  ---> via php8.1     on Ubuntu 22.04
    #    ext-libxml           8.1.2      success  ---> via php8.1-xml on Ubuntu 22.04
    #    ext-mbstring         *          success provided by symfony/polyfill-mbstring
    #    ext-phar             8.1.2      success   ---> via php8.1     on Ubuntu 22.04
    #    ext-tokenizer        8.1.2      success   ---> via php8.1     on Ubuntu 22.04
    #    ext-xml              8.1.2      success   ---> via php8.1-xml on Ubuntu 22.04
    #    ext-xmlwriter        8.1.2      success   ---> via php8.1-xml on Ubuntu 22.04
    #    php                  8.1.2      success   ---> via php8.1     on Ubuntu 22.04

# Display software version
cat /etc/os-release
php --version
sudo -u vagrant composer --version
