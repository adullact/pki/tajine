# Prerequisites

- [Composer](https://getcomposer.org/)
- PHP **8.1**
- PHP extensions:
  - `php8.1-bcmath`   (mandatory to use Tajine: convert certificate serial number)
  - `php8.1-intl`     (mandatory to use Infection tool and usefull to use Synfony)
  - `php8.1-mbstring` (usefull to use Synfony)
  - `php8.1-pgsql`  (mandatory to use PostgreSQL)
  - `php8.1-xdebug`   (mandatory to use Infection tool)
  - `php8.1-xml`    (mandatory to use Synfony)
  - `php8.1-zip`    (mandatory to use dbrekelmans/bdi used by Panther)
  - ... <-- **TODO** to be completed if necessary
- prerequisites for CI and developers:
  - Git
  - **PHIVE**

## PHIVE and QA tools

Usage on the project:

- [x] IDE
- [x] manual check
- [x] pre-commit check
- [x] CI check
- [ ] Deployment

see: [QA tools](20_QA_tools.md)

### Install PHIVE

[PHIVE documentation](https://phar.io)

```bash
# Install PHIVE
wget -O phive.phar https://phar.io/releases/phive.phar
wget -O phive.phar.asc https://phar.io/releases/phive.phar.asc
gpg --keyserver pool.sks-keyservers.net --recv-keys 0x9D8A98B29B2D5D79
gpg --verify phive.phar.asc phive.phar
chmod +x phive.phar
sudo mv phive.phar /usr/local/bin/phive
```

### Install QA tools

configuration file : [phive.xml](../../phive.xml)

```bash
# Install all phar files declared in phive.xml
git clone <projectGitUrl>
cd <projectDirectory>

# Option 1
phive install --force-accept-unsigned

# Option 2
phive install php-parallel-lint/php-parallel-lint  --force-accept-unsigned --target ./bin/.phive
phive install
```

### GrumPHP - Registers Git hooks

- [GrumPHP](https://github.com/phpro/grumphp) will run some tests on the committed code.
- If the tests fail, you won't be able to commit your changes.
- configuration file : [grumphp.yml](../grumphp.yml)

```bash
# If necessary, backup your Git hooks directory
tar -czvf .git/hooks.tar.gz .git/hooks/

# Registers Git hooks
./bin/grumphp git:init

# Removes Git hooks
./bin/grumphp git:deinit

# To skip commit checks, add -n or --no-verify flag to commit command
git commit --no-verify -m "your commit message"
git commit -n          -m "your commit message"
```


-------------------

## PHP on Ubuntu LTS

Usage on the project:

- [x] IDE
- [x] manual check
- [x] pre-commit check
- [x] CI check
- [x] Deployment

### Using PHP 8.1 or PHP 8.2 on Ubuntu

```bash
# List installed PHP versions
sudo update-alternatives --list php

# Change PHP version
sudo update-alternatives --config php

# Check PHP version currently in use
php -v
```


### Install PHP 8.1

#### Install PHP 8.1 on Ubuntu 22.04

```bash
# Install PHP 8.1 on Ubuntu 22.04
sudo apt-get update
sudo apt install software-properties-common
sudo apt-get install php8.1
sudo apt-get install php8.1-bcmath    # mandatory to use Tajine (convert certificate serial number)
sudo apt-get install php8.1-intl      # mandatory to use Infection tool and usefull to use Synfony (improve performance)
sudo apt-get install php8.1-mbstring  # usefull   to use Synfony
sudo apt-get install php8.1-pgsql     # mandatory to use PostgreSQL
sudo apt-get install php8.1-xdebug    # mandatory to use Infection tool
sudo apt-get install php8.1-xml       # mandatory to use Synfony
sudo apt-get install php8.1-zip       # mandatory to use dbrekelmans/bdi used by Panther
```

#### Install PHP 8.1 on Ubuntu 18.04 or 20.04

```bash
# Install PHP 8.1 on Ubuntu 18.04 or 20.04
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install software-properties-common
sudo apt-get install php8.1
sudo apt-get install php8.1-bcmath    # mandatory to use Tajine (convert certificate serial number)
sudo apt-get install php8.1-intl      # mandatory to use Infection and usefull to use Synfony (improve performance)
sudo apt-get install php8.1-mbstring  # usefull   to use Synfony
sudo apt-get install php8.1-pgsql     # mandatory to use PostgreSQL
sudo apt-get install php8.1-xdebug    # mandatory to use Infection
sudo apt-get install php8.1-xml       # mandatory to use Synfony
sudo apt-get install php8.1-zip       # mandatory to use dbrekelmans/bdi used by Panther
```


### Install PHP 8.2

#### Install PHP 8.2 on Ubuntu 22.04

> **TODO** not tested yet

```bash
# Install PHP 8.2 on Ubuntu 22.04
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update
sudo apt-get install software-properties-common
sudo apt-get install php8.2
sudo apt-get install php8.2-bcmath    # mandatory to use Tajine (convert certificate serial number)
sudo apt-get install php8.2-intl      # usefull   to use Synfony (improve performance)
sudo apt-get install php8.2-mbstring  # usefull   to use Synfony
sudo apt-get install php8.2-pgsql     # mandatory to use PostgreSQL
sudo apt-get install php8.2-xml       # mandatory to use Synfony
```
