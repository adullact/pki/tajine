# Security vulnerability

## Check security advisories of the webapp PHP dependencies

Security Advisories Database:
- GitHub Advisory Database           https://github.com/advisories
- PHP Security Advisories Database   https://github.com/FriendsOfPHP/security-advisories

`trivy`:
- https://github.com/aquasecurity/trivy
- https://aquasecurity.github.io/trivy/
- https://aquasecurity.github.io/trivy/v0.37/tutorials/integrations/gitlab-ci/

`composer audit`:
- https://getcomposer.org/doc/03-cli.md#audit
- https://php.watch/articles/composer-audit
- https://www.michalspacek.com/check-vulnerable-packages-with-composer-audit
- https://fabienpaitry.fr/composer-audit-et-gitlab-ci-21503e03ef8f

`symfony-cli` use `local-php-security-checker`
- https://github.com/symfony-cli/symfony-cli/blob/main/commands/local_check_security.go
- https://github.com/fabpot/local-php-security-checker

see also: https://github.com/Roave/SecurityAdvisories

```bash
# Check WEBAPP security with `composer audit` command line
# - checks for security vulnerability advisories
# - based only on the installed packages [ vendor/ ]
# - use :  GitHub Advisory Database           https://github.com/advisories
#          PHP Security Advisories Database   https://github.com/FriendsOfPHP/security-advisories
composer audit

# Check WEBAPP security with `composer audit --locked` command line
# - checks for security vulnerability advisories
# - based only on the composer.lock file
# - use :  GitHub Advisory Database           https://github.com/advisories
#          PHP Security Advisories Database   https://github.com/FriendsOfPHP/security-advisories
composer audit --locked          # including dev package
composer audit --locked --no-dev # excluding dev packages

# Check WEBAPP security with TRIVY
# - checks webapp for security vulnerability advisories
# - based only on the composer.lock file, (excluding dev packages)
# - use :  GitHub Advisory Database           https://github.com/advisories
#          PHP Security Advisories Database   https://github.com/FriendsOfPHP/security-advisories
trivy fs .  --dependency-tree --security-checks=vuln

# Check WEBAPP security with Symfony-CLI
# - checks for security vulnerability advisories
# - based only on the composer.lock file
# - use :  PHP Security Advisories Database   https://github.com/FriendsOfPHP/security-advisories
symfony check:security          # including dev package
symfony check:security --no-dev # excluding dev packages
```
