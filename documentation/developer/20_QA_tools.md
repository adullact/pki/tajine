# QA tools

ressource: [PHP QA tools](https://phpqa.io/)

## QA tools installed by PHIVE

- [Prerequisites](10_prerequisites.md)
- [PHIVE documentation](https://phar.io)
- PHIVE configuration file : [phive.xml](../../phive.xml)

Usage on the project:

- [x] IDE
- [x] manual check
- [x] pre-commit check
- [x] CI check
- [ ] Deployment

QA tools installed by PHIVE:

- [ComposerRequireChecker](https://github.com/maglnet/ComposerRequireChecker) ---> analysis of PHP dependencies
- [Infection](https://infection.github.io/) ---> Mutation testing framework

QA tools that could be installed by PHIVE:

- [GrumPHP](https://github.com/phpro/grumphp) --->add a pre-commit hook to Git (= a CI just before creating a commit)
- [PHP-CS-Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer) ---> Check (and fix) respect of PHP coding standards
- [phpcbf](https://github.com/PHPCSStandards/PHP_CodeSniffer) (PHP_CodeSniffer) ---> Check respect of PHP coding standards
- [phpcs](https://github.com/PHPCSStandards/PHP_CodeSniffer) (PHP_CodeSniffer) ---> Fix errors related to PHP coding standards
- [PHPCPD](https://github.com/sebastianbergmann/phpcpd) (PHP Copy/Paste Detector)
- [phploc](https://github.com/sebastianbergmann/phploc)  ---> analyzes structure of project and complexity of PHP code
- [PHPMD](https://github.com/phpmd/phpmd) (PHP Mess Detector) ---> static analysis of PHP code
- [PHP-Parallel-Lint](https://github.com/php-parallel-lint/PHP-Parallel-Lint) --->  PHP linter
- [PHPStan](https://github.com/phpstan/phpstan) ---> static analysis of PHP code
- [Psalm](https://github.com/vimeo/psalm) ---> static analysis of PHP code
- [PHPUnit](https://phpunit.de/) --->  Unit tests (and also integration and functional tests)


## Other useful QA tools

Usage on the project:

- [ ] IDE
- [x] manual check
- [ ] pre-commit check
- [ ] CI check
- [ ] Deployment

Other useful QA tools :

- [PhpMetrics](https://phpmetrics.org/)
- [EdgedesignCZ/phpqa](https://github.com/EdgedesignCZ/phpqa)   (evaluation in progress)
- Helps to discover and install tools (not yet evaluated)
  - [jakzal/toolbox](https://github.com/jakzal/toolbox)  ---> PHAR providing static analysis tools for PHP
  - [jakzal/phpqa](https://github.com/jakzal/phpqa)  ---> Docker image providing static analysis tools for PHP

```bash
# Install
composer global require phpmetrics/phpmetrics
composer global require edgedesign/phpqa --update-no-dev

# Usage
phpmetrics --report-html=myreport.html        ./src/
phpqa      --buildDir ./build  --analyzedDirs ./src/

    # If necessary, add composer home directory to $PATH
    composer config --global home # Display composer home directory
    echo "export PATH=\"\$PATH:$(composer config --global home)/vendor/bin\"" >> .bashrc
```
