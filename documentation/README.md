# Documentation

- [**Developer** documentation](developer/)
- [**System Administrator** documentation](sysadmin/)

---------------------------------------

- [Changelog](../CHANGELOG.md)
- [Contributing](../CONTRIBUTING.md)
- [Code of Conduct](../CODE_OF_CONDUCT.md)
- [License **AGPL v3**](../LICENSE)
