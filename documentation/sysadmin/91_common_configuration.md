# Configuration

Common configuration to use this software.

To deploy webapp in production, we recommend that you create `.env.prod.local` file
and set following environment variables in it:

| Environment variables      | Application secret to generate CSRF tokens                                        |
|----------------------------|-----------------------------------------------------------------------------------|
| `APP_SECRET`               | Application secret to generate CSRF tokens                                        |
| `CFSSL_API_SERVER`         | **CFSSL** server                                                                  |
| `DATABASE_URL`             | Database configuration (**PostgreSQL**)                                           |
| `MAILER_DSN`               | **SMTP** configuration                                                            |
| `WEBAPP_EMAIL_FROM`        | Sender's email address to send emails from the web application                    |
| `WEBAPP_EMAIL_ALERTING_TO` | Email address for alert notifications (web application not working properly, ...) |
| `WEBAPP_NAME`              | Name of web application displayed to users                                        |
| `WEBAPP_SHORTNAME`         | Shortname of web application displayed to users                                   |
| `WEBAPP_TRUSTED_HOSTS`     | List of allowed domains and IP to prevent HTTP Host header attacks                |

You can [consult `.env.prod` file](../../src/webapp/.env.prod)  where all these environment variables are documented.

> **Warning**:
> do not edit directly `.env.prod` file, because it will be **overwritten** during the **next update**.

## Create a `.env.prod.local` file

```bash
# Copy [ env.prod ] file into a new file : [ .env.prod.local ]
cp -v /var/www/webapp/.env.prod  /var/www/webapp/.env.prod.local
```

## Edit `.env.prod.local` file

```bash
# Edit .env.prod.local file
vim /var/www/webapp/.env.prod.local
```
