# Install and update without `composer`

## Prerequisites

- See: [common prerequisites](90_common_prerequisites.md) (PHP, PHP extensions)


## How to install?

To install this software on your production server without `composer`, you can use the following documentation:

### Download

- Choose version to install by consulting [CHANGELOG](../../CHANGELOG.md) file.
- [Download archive](https://gitlab.adullact.net/adullact/pki/tajine/-/packages) of version you have chosen
- Extract archive you have downloaded

```bash
# Extract archive you have downloaded
VERSION=0.2.0  # version you have downloaded
mkdir /var/www/webapp
tar -xzvf tajine-v${VERSION}.tgz -C /var/www/webapp
```

### Install

1. Stop HTTP server
2. [Create a `.env.prod.local` file](91_common_configuration.md)
3. [Warm-up Symfony cache](92_common_clear-warm-up-symfony-cache.md)
4. [Running database migrations](93_common_database-migrations.md)
5. Start HTTP server

## How to update?

To update this software on your production server without `composer`, you can use the following documentation:

> **TODO** to be completed

1. Stop HTTP server
2. **TODO** to be completed
3. [Update your `.env.prod.local` file](91_common_configuration.md)
4. [Clear and warm-up Symfony cache](92_common_clear-warm-up-symfony-cache.md)
5. [Running database migrations](93_common_database-migrations.md)
6. Start HTTP server
