# Install and update with `composer`

## Prerequisites

- First see: [common prerequisites](90_common_prerequisites.md) (PHP, PHP extensions)
- [Composer](https://getcomposer.org/)
- [Git](https://git-scm.com/)

## How to install with `composer`?

To install this software on your production server with `composer`, you can use the following command lines:

```bash
# Choose release number to install
# https://gitlab.adullact.net/adullact/pki/tajine/-/blob/main/CHANGELOG.md
VERSION=0.2.0

# Clone repository and switch to chosen version
git clone https://gitlab.adullact.net/adullact/pki/tajine.git
cd tajine/
git checkout "v${VERSION}"

# Install PHP depandencies
cd src/webapp
composer install --no-dev
    ######################################
    # alternative: for non-interactive use
    ######################################
    # composer install --no-dev --no-progress --no-suggest --no-interaction --no-scripts
```

1. Stop HTTP server
2. [Create a `.env.prod.local` file](91_common_configuration.md)
3. [Warm-up Symfony cache](92_common_clear-warm-up-symfony-cache.md)
4. [Running database migrations](93_common_database-migrations.md)
5. Start HTTP server

> **TODO** to be completed


## How to update with `composer`?

To update this software on your production server with `composer`, you can use the following command lines:

> **TODO** to be completed

1. Stop HTTP server
2. **TODO** to be completed
3. [Update your `.env.prod.local` file](91_common_configuration.md)
4. [Clear and warm-up Symfony cache](92_common_clear-warm-up-symfony-cache.md)
5. [Running database migrations](93_common_database-migrations.md)
6. Start HTTP server
