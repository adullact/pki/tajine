# Clear / Warm-up Symfony cache

## Clear _Symfony_ cache

```bash
# Clear Symfony cache
cd /var/www/webapp
APP_ENV=prod APP_DEBUG=0  bin/console cache:clean
```

## Warm-up _Symfony_ cache

```bash
# Warm-up Symfony cache
cd /var/www/webapp
APP_ENV=prod APP_DEBUG=0  bin/console cache:warmup
```
