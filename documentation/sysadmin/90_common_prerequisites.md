# Prerequisites

Common prerequisites to install this software:

- HTTP server
- _PHP_ **8.1**
- PHP extensions:
  - `php8.1-bcmath`   (mandatory to use Tajine: convert certificate serial number)
  - `php8.1-intl`     (usefull to use Synfony: improve performance)
  - `php8.1-mbstring` (usefull to use Synfony)
  - `php8.1-pgsql`  (mandatory to use PostgreSQL)
  - `php8.1-xml`    (mandatory to use Synfony)
  - ... <-- **TODO** to be completed if necessary

## HTTP server

HTTP server like [Apache](https://httpd.apache.org), [Nginx](https://www.nginx.com) or [Caddy](https://caddyserver.com)

See: [Symfony - Configuring a Web Server](https://symfony.com/doc/current/setup/web_server_configuration.html)

## Install PHP `8.1` and PHP extensions on Ubuntu `22.04`

```bash
# Install PHP 8.1 and PHP extensions on Ubuntu 22.04
sudo apt-get update
sudo apt-get install php8.1
sudo apt-get install php8.1-bcmath    # mandatory to use Tajine (convert certificate serial number)
sudo apt-get install php8.1-intl      # usefull   to use Synfony (improve performance)
sudo apt-get install php8.1-mbstring  # usefull   to use Synfony
sudo apt-get install php8.1-pgsql     # mandatory to use PostgreSQL
sudo apt-get install php8.1-xml       # mandatory to use Synfony
php -v
```
