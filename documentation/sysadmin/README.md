# System Administrator documentation

## How to install and update?

2 methods to install and manage this software:

- [Install and update **with** `composer`](95_install-update_with-composer.md)
- [Install and update **without** `composer`](96_install-update_without-composer.md)

See also : [common prerequisites](90_common_prerequisites.md)

## Health Check

Health Check URL: `https://example.org/health-check`

```bash
# Database status
####################################################################################
#     HTTP Code   HTML page contains          HTTP header [ X-Tajine-Database-Status ]
#     200|503     DB_CONNECTION_SUCCESSFUL    DB_CONNECTION_SUCCESSFUL
#     503         DB_CONNECTION_FAILED        DB_CONNECTION_FAILED

CFSSL status
####################################################################################
#     HTTP Code   HTML page contains           HTTP header [ X-Tajine-Cfssl-Status ]
#     200|503     CFSSL_CONNECTION_SUCCESSFUL  CFSSL_CONNECTION_SUCCESSFUL
#     503         CFSSL_CONNECTION_FAILED      CFSSL_CONNECTION_FAILED

curl --head https://example.org/health-check
    # HTTP/1.1 200 OK
    # Content-Type: text/html; charset=UTF-8
    # X-Tajine-Database-Status: DB_CONNECTION_SUCCESSFUL
    # X-Tajine-Cfssl-Status:    CFSSL_CONNECTION_SUCCESSFUL

curl --head https://example.org/health-check
    # HTTP/1.1 503 Service Unavailable
    # Content-Type: text/html; charset=UTF-8
    # X-Tajine-Database-Status: DB_CONNECTION_FAILED
    # X-Tajine-Cfssl-Status:    CFSSL_CONNECTION_SUCCESSFUL

curl --head https://example.org/health-check
    # HTTP/1.1 503 Service Unavailable
    # Content-Type: text/html; charset=UTF-8
    # X-Tajine-Database-Status: DB_CONNECTION_SUCCESSFUL
    # X-Tajine-Cfssl-Status:    CFSSL_CONNECTION_FAILED

curl --head https://example.org/health-check
    # HTTP/1.1 503 Service Unavailable
    # Content-Type: text/html; charset=UTF-8
    # X-Tajine-Database-Status: DB_CONNECTION_FAILED
    # X-Tajine-Cfssl-Status:    CFSSL_CONNECTION_FAILED
```
