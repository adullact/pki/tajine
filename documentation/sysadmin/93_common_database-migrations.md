# Running database migrations

## Dry run database migrations

```bash
# Dry run database migrations
cd /var/www/webapp
bin/console  doctrine:migrations:migrate  --env=prod --no-interaction --no-ansi --dry-run
```

## Running database migrations

```bash
# Running database migrations
bin/console  doctrine:migrations:migrate  --env=prod --no-interaction --no-ansi
```

## Other tools to manage database migrations

```bash
bin/console  doctrine:migrations:current               # Outputs the current version.
bin/console  doctrine:migrations:diff                  # Generate a migration by comparing your current database to your mapping information.
bin/console  doctrine:migrations:dump-schema           # Dump the schema for your database to a migration.
bin/console  doctrine:migrations:execute               # Execute a single migration version up or down manually.
bin/console  doctrine:migrations:generate              # Generate a blank migration class.
bin/console  doctrine:migrations:latest                # Outputs the latest version number
bin/console  doctrine:migrations:migrate               # Execute a migration to a specified version or the latest available version.
bin/console  doctrine:migrations:rollup                # Roll migrations up by deleting all tracked versions and inserting the one version that exists.
bin/console  doctrine:migrations:status                # View the status of a set of migrations.
bin/console  doctrine:migrations:up-to-date            # Tells you if your schema is up-to-date.
bin/console  doctrine:migrations:version               # Manually add and delete migration versions from the version table.
bin/console  doctrine:migrations:sync-metadata-storage # Ensures that the metadata storage is at the latest version.
bin/console  doctrine:migrations:list                  # Display a list of all available migrations and their status.
```

