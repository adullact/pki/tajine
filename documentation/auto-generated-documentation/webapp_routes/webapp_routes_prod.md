app_admin
---------

- Path: /admin
- Path Regex: {^/admin$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Administrator\AdministratorController::indexAdministrator
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_config_phpinfo
------------------------

- Path: /admin/configuration/phpinfo
- Path Regex: {^/admin/configuration/phpinfo$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Administrator\AdministratorController::displayPhpInfo
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_config
----------------

- Path: /admin/configuration
- Path Regex: {^/admin/configuration$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Administrator\AdministratorController::configuration
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_certificates
----------------------

- Path: /admin/certificates
- Path Regex: {^/admin/certificates$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Administrator\AdministratorController::certificates
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_certificates_display
------------------------------

- Path: /admin/certificates/{serialNumber}
- Path Regex: {^/admin/certificates/(?P<serialNumber>[^/]++)$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Administrator\AdministratorController::certificateDisplay
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_certificates_download
-------------------------------

- Path: /admin/certificates/{serialNumber}/download
- Path Regex: {^/admin/certificates/(?P<serialNumber>[^/]++)/download$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Administrator\AdministratorController::certificateDownload
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_config_send_testing_email
-----------------------------------

- Path: /admin/configuration/test-mail
- Path Regex: {^/admin/configuration/test\-mail$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Administrator\Configuration\AdministratorTestingEmailConfigController::sendTestingEmail
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_users_list
--------------------

- Path: /admin/users
- Path Regex: {^/admin/users$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Administrator\ManageUsersController::index
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_users_add_manager
---------------------------

- Path: /admin/users/add-manager
- Path Regex: {^/admin/users/add\-manager$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Administrator\ManageUsersController::addManager
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_users_add_admin
-------------------------

- Path: /admin/users/add-administrator
- Path Regex: {^/admin/users/add\-administrator$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Administrator\ManageUsersController::addAdministrator
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_users_edit
--------------------

- Path: /admin/users/{id}/edit
- Path Regex: {^/admin/users/(?P<id>[^/]++)/edit$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Administrator\ManageUsersController::editManager
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_users_delete
----------------------

- Path: /admin/users/{id}/delete
- Path Regex: {^/admin/users/(?P<id>[^/]++)/delete$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Administrator\ManageUsersController::delete
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_admin_users_display
-----------------------

- Path: /admin/users/{id}
- Path Regex: {^/admin/users/(?P<id>[^/]++)$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Administrator\ManageUsersController::show
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_public_certificat-authority
-------------------------------

- Path: /certification-authority
- Path Regex: {^/certification\-authority$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\CertificationAuthorityController::index
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_public_certificat-authority_download
----------------------------------------

- Path: /certification-authority/{serialNumber}/download
- Path Regex: {^/certification\-authority/(?P<serialNumber>[^/]++)/download$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\CertificationAuthorityController::downloadCertificate
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_health_check
----------------

- Path: /health-check
- Path Regex: {^/health\-check$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\HealthCheckController::index
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_home
--------

- Path: /
- Path Regex: {^/$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|HEAD
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\HomeController::index
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_cert-manager_certificate_download
-------------------------------------

- Path: /cert-manager/certificate/{serialNumber}/download
- Path Regex: {^/cert\-manager/certificate/(?P<serialNumber>[^/]++)/download$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Manager\CertificateAddController::downloadCertificate
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_cert-manager_certificate_add
--------------------------------

- Path: /cert-manager/certificate/add
- Path Regex: {^/cert\-manager/certificate/add$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Manager\CertificateAddController::addCertificate
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_cert-manager
----------------

- Path: /cert-manager
- Path Regex: {^/cert\-manager$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Manager\ManagerController::indexManager
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_cert-manager_certificates
-----------------------------

- Path: /cert-manager/certificates
- Path Regex: {^/cert\-manager/certificates$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\Manager\ManagerController::certificates
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_legacy_cron_certificateRemindLifetime
-----------------------------------------

- Path: /poc/ping-reminder/{reminderTokenViaHttpRequest}
- Path Regex: {^/poc/ping\-reminder/(?P<reminderTokenViaHttpRequest>[^/]++)$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\PocCertificateController::legacyCronCertReminder
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_account_login
-----------------

- Path: /account/login
- Path Regex: {^/account/login$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\LoginController::login
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_account_logout
------------------

- Path: /account/logout
- Path Regex: {^/account/logout$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\LoginController::logout
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_account_forgot_password_request
-----------------------------------

- Path: /account/forgot-password
- Path Regex: {^/account/forgot\-password$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\LoginController::forgotPassword
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_account_forgot_password_new-password
----------------------------------------

- Path: /account/forgot-password/t/{tokenKey}
- Path Regex: {^/account/forgot\-password/t/(?P<tokenKey>[^/]++)$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\LoginController::forgotPasswordCheckToken
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_user_profile
----------------

- Path: /user/profile
- Path Regex: {^/user/profile$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\UserProfilController::userDisplayMyProfile
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


app_user_profile_change_password
--------------------------------

- Path: /user/profile/password
- Path Regex: {^/user/profile/password$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: GET|POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: App\Controller\User\UserProfilController::userChangeMyPassowrd
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true


nelmio_security
---------------

- Path: /csp/report
- Path Regex: {^/csp/report$}sDu
- Host: ANY
- Host Regex: 
- Scheme: ANY
- Method: POST
- Class: Symfony\Component\Routing\Route
- Defaults: 
    - `_controller`: nelmio_security.csp_reporter_controller::indexAction
- Requirements: NO CUSTOM
- Options: 
    - `compiler_class`: Symfony\Component\Routing\RouteCompiler
    - `utf8`: true

