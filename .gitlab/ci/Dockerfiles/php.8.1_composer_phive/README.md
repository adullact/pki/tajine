# CI ~ Prerequisites for PHP 8.1, Composer, Phive, ...

Docker image of prerequisites for PHP **8.1**, Composer, Phive, Symfony-CLI, Xdebug and Make.

## To update Docker image

### Edit Dockerfile

Edit [Dockerfile](Dockerfile) according to your needs
and in addition you must to change in this file `LABEL version="1.3.4"`
following [semantic versioning](http://semver.org/) recommendations:

 ```shell script
 MAJOR.MINOR.PATCH
    # MAJOR ---> a breaking change (incompatible API changes)
    # MINOR ---> add a new feature
    # PATCH ---> fix a bug
```

```dockerfile
# Set Docker LABEL and display software versions
LABEL version="1.3.4"  \
      description="Prerequisites for PHP 8.1, Composer, Phive, Symfony-CLI, Xdebug and Make"
```

### Build Docker image and push to container registry

Build new Docker image and push to [Gitlab container registry](https://gitlab.adullact.net/adullact/pki/tajine/container_registry)

```bash
# Configure variables
# in particular DOCKER_IMAGE_VERSION
# which must be identical to LABEL.version in Dockerfile
GITLAB_URI="gitlab.adullact.net:4567"
GITLAB_REPOSITORY="adullact/pki/tajine"
DOCKER_IMAGE_NAME="php-8.1_composer_phive"
DOCKER_IMAGE_VERSION="v1.3.4" # must be identical to LABEL.version in Dockerfile

# Login to Gitlab
docker login "${GITLAB_URI}"

# Build new Docker image
docker build --progress plain -t "${GITLAB_URI}/${GITLAB_REPOSITORY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}" .
docker images | grep "${DOCKER_IMAGE_NAME}"

# Push to Gitlab container registry
docker push "${GITLAB_URI}/${GITLAB_REPOSITORY}/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}"

# Logout to remove Gitlab credentials from $HOME/.docker/config.json file
docker logout "${GITLAB_URI}"
```


