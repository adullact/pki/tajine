# Tajine

## About Tajine software

**Tajine** is a registration authority (RA) frontend,
plugged to backend [CFSSL](https://github.com/cloudflare/cfssl).

It's a free software created by [Adullact](https://adullact.org)
and available under [GNU **AGPL** v3 license](LICENSE).

## Documentation

- [**Developer** documentation](documentation/developer/)
- [**System Administrator** documentation](documentation/sysadmin/)

---------------------------------------

- [Changelog](CHANGELOG.md)
- [Contributing](CONTRIBUTING.md)
- [Code of Conduct](CODE_OF_CONDUCT.md)

## License

[**AGPL v3** - GNU Affero General Public License](LICENSE)
